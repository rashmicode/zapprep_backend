var qbchat = require('./qbchat');
var config = require('./config');
//qbchat.init();
//qbchat.createSession(function(err, session){
//	console.log("Default instance create session - ", err);
//	qbchat.login(function(err, result){
//		console.log("Login instance result - ", err);
//		qbchat.addUserToRoom("59085715a28f9ad142000009", [28198366], function(err, result){
//			console.log("Default instance user add to room result - ", err);
//		})
//	})
//});
//
//var QB = qbchat.extraQB();
//qbchat.extraInitWithParams(QB, "", "", "");
//qbchat.extraCreateSession(QB, function(err, session){
//	console.log('new instance create session completed. err ', err);
//	qbchat.extraLogin(QB, "zapprep", function(err, result){
//		console.log("New instance login - ", err);
//		qbchat.extraAddUserToRoom(QB, "59085715a28f9ad142000009", [28198366], function(err, result){
//			console.log("extra add user to room result - ", err, result);
//		});
//	})
//});
//
//var QB = qbchat.extraQB();
//qbchat.extraInit(QB);
//qbchat.extraCreateSession(QB, function(err, session){
//        console.log('new instance create session completed. err ', err);
//        qbchat.extraLogin(QB, "zapprep", function(err, result){
//                console.log("New instance login - ", err);
//                qbchat.extraUpdateUser(QB, 28198366, {login: "harshit_d8cc", "password": "zapprep123$"}, function(err, result){
//                        console.log("extra update user result - ", err, result);
//                });
//        })
//});
//
//var QB = qbchat.extraQB();
//qbchat.extraInit(QB);
//qbchat.extraCreateSession(QB, function(err, session){
//        console.log('new instance create session completed. err ', err);
//        qbchat.extraAddUserToRoom(QB, "59085715a28f9ad142000009", [28198366], function(err, result){
//        	console.log("extra add user to room result - ", err, result);
//        });
//});

//
//qbchat.extraCreateSessionWithParams(QB, config["qb-test"]["params"], function(err, session){
//    console.log('Got extra create session err, result = ', err, session);
//    qbchat.extraCreateUser(QB, config["qb-test"]["test_user_params"], config['qb-test']['params'], function(err, user){
//        console.log('Got extra user create err, result = ', err, user);
//        if(user && user.id && user.login) {
//            qbchat.extraUpdateUser(QB, user.id, {full_name: "Harshit Test"},
//                config['qb-test']['test_user_creds'], function(err, user){
//                    console.log('extra update user finished - err, result = ', err, user);
//                });
//        }
//    })
//});

// TEST 2:

//qbchat.extraCreateSessionWithParams(QB, config["qb-test"]["test_user_creds"], function(err, session){
//    console.log('Got extra create session err, result = ', err, session);
//    qbchat.extraUpdateUser(QB, config["qb-test"]['test_user_id'], {full_name: "Harshit Test"},
//        config['qb-test']['test_user_creds'], function(err, user){
//            console.log('extra update user finished - err, result = ', err, user);
//        });
//});

// TEST 3:

//qbchat.extraCreateUser(QB, config["qb-test"]['test_user_params'], config["qb-test"]['params'], function(err, user){
//    if(user && user.id) {
//        qbchat.extraCreateRoom(QB, 2, "General", config['qb-test']['params'], function(err, newRoom){
//            console.log('extra create room finished - err, result = ', err, newRoom);
//            if(newRoom && newRoom._id) {
//                var eqb = qbchat.extraQB();
//                qbchat.extraInitWithParams(eqb, config["qb-test"].appId, config["qb-test"].authKey, config['qb-test'].authSecret);
//                qbchat.extraAddUserToRoom(eqb, newRoom._id, [user.id], config['qb-test']['params'], function(err, result){
//                    console.log('add user to room err, result3 = ', err, result);
//                });
//                if(err) {
//                    console.log("Error adding user to room. Trying to delete user and room now - ");
//                    qbchat.extraDeleteUser(eqb, user.id, config['qb-test']['test_user_creds'], function(err, session){
//                        console.log('delete user result - ', err, session);
//                        qbchat.extraDeleteRoom(eqb, newRoom._id, config['qb-test']['params'], function(err, result){
//                            console.log('delete room result - ', err, result);
//                        });
//                    })
//                }
//            }
//        });
//    }
//});
var mongoose = require('mongoose');

// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', function (err) {
        console.error('MongoDB connection error: ' + err);
        process.exit(-1);
    }
);

var QB = qbchat.extraQB();
qbchat.extraInit(QB);

var Path = require('./path/path.model');
var User = require('./user/user.model');
var Enroll = require('./enrollment/enroll.model');
var Concept = require('./concept/concept.model');
var async = require('async');

function escapeRegExpChars(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

//User.find().exec(function(err, users){
//    var dup_users = [];
//    async.eachSeries(users, function(user, user_cb){
//        console.log('trying to create chat user for - ', user.username);
//        User.find({username: new RegExp('^' + escapeRegExpChars(user.username) + '$', 'i')}).exec(function(err, dbusers){
//            if(dbusers.length > 1) {
//                console.log('duplicate users for -', user.username);
//                dbusers.forEach(function(dbuser){
//                    console.log('dup dbuser - ', dbuser.username, user.username);
//                });
//                dup_users.push(user.username);
//                var index = 0;
//                async.eachSeries(dbusers, function(dbuser, db_cb){
//                    dbuser['username'] = dbuser.username + index.toString();
//                    index++;
//                    qbchat.extraCreateUser(QB, {
//                        login: dbuser.username,
//                        password: config.qb.defaultPassword,
//                        full_name: dbuser.name,
//                        custom_data: dbuser.avatar
//                    }, config.qb.params, function(err, newUser){
//                        if(err) {
//                            console.log('err creating chat user for - ', dbuser.username, err);
//                            db_cb();
//                        } else if(newUser.id){
//                            console.log('finished creating chat user for - ', dbuser.username);
//                            dbuser['qbId'] = newUser.id;
//                            dbuser.save(function(err){
//                                if(err) {
//                                    console.log('error saving to db. ref user id : ', newUser.id, dbuser.username);
//                                    db_cb(err);
//                                } else db_cb();
//                            });
//                        } else db_cb();
//                    });
//                }, function(done){
//                    console.log('finished all duplicate users for username - ', user.username, index);
//                    user_cb();
//                });
//            } else user_cb();
//        })
//    }, function(done){
//        console.log('finished finding duplicate list - ');
//        async.eachSeries(dup_users, function(dbuser, db_cb){
//            qbchat.extraDeleteUser(QB, dbuser.qbId, {login: dbuser.username, password: config.qb.defaultPassword}, function(err, res){
//                if(err) console.log('error deleting duplicate user - ', err)
//            })
//        }, function(err){
//            console.log('finished updating duplicate list');
//        })
//    });
//});

//Path.find().deepPopulate('producer').exec(function(err, paths){
//    async.eachSeries(paths, function(path, path_cb){
//        qbchat.extraCreateRoom(QB, 2, 'General', config.qb.params, function(err, newRoom){
//            if(err) {
//                console.log('error creating general room', err, path._id);
//                path_cb(err);
//            }  else if(newRoom && newRoom._id && path.producer && path.producer.qbId) {
//                path['qbId'] = newRoom._id;
//                qbchat.extraCreateRoom(QB, 2, "Mentor", config.qb.params, function(err, mentorRoom) {
//                    if(err) {
//                        console.log('error creating mentor room', err, path._id);
//                    }
//                    if(mentorRoom && mentorRoom._id) {
//                        console.log('mentor room created for - ', path._id);
//                        path['mentorRoomId'] = mentorRoom._id;
//                    }
//                    path.save(function(err){
//                        if(err) {
//                            console.log('error saving room id to db - ', newRoom._id, path._id);
//                            path_cb(err);
//                        } else path_cb();
//                    });
//                });
////                qbchat.extraAddUserToRoom(QB, newRoom._id, [path.producer.qbId], config.qb.params, function(err, result){
////                    if(err) {
////                        console.log('error adding producer to general room', err);
////                    } else if(result) console.log('finished adding mentor to general room', path.producer.username);
////                    path['qbId'] = newRoom._id;
////                    path.save(function(err){
////                        if(err) {
////                            console.log('error saving room id to path - ', newRoom._id, path._id);
////                            path_cb(err);
////                        } else path_cb();
////                    });
////                })
//            } else path_cb();
//        });
//    }, function(done){
//        console.log('finished processing path channels');
//    })
//});
//
//Concept.find().deepPopulate('producer').exec(function(err, concepts){
//    async.eachSeries(concepts, function(concept, concept_cb){
//        qbchat.extraCreateRoom(QB, 2, concept.title, config.qb.params, function(err, newRoom){
//            if(err) return concept_cb(err);
//            if(newRoom && newRoom._id && concept.producer && concept.producer.qbId) {
//                concept['qbId'] = newRoom._id;
//                concept.save(function(err){
//                    if(err) {
//                        console.log('error saving room id to concept - ', newRoom._id, concept._id);
//                        concept_cb(err);
//                    } else {
//                        console.log('finished creating concept channel - ', concept.title, concept.path);
//                        concept_cb();
//                    }
//                });
////                qbchat.extraAddUserToRoom(QB, newRoom._id, [concept.producer.qbId], config.qb.params, function(err, result){
////                    if(err) {
////                        console.log('error adding producer to general room', err);
////                    } else if(result) console.log('finished adding mentor to general room', concept.producer.username);
////                    concept['qbId'] = newRoom._id;
////                    concept.save(function(err){
////                        if(err) {
////                            console.log('error saving room id to concept - ', newRoom._id, concept._id);
////                            concept_cb(err);
////                        } else concept_cb();
////                    });
////                })
//            } else concept_cb();
//        });
//    }, function(done){
//        console.log('finished processing concept channels');
//    })
//});

//Path.find().deepPopulate('producer').exec(function(err, paths){
//    async.eachSeries(paths, function(path, path_cb){
//        qbchat.extraCreateRoom(QB, 2, 'Mentor', config.qb.params, function(err, newRoom){
//            if(err) return path_cb(err);
//            if(newRoom && newRoom._id && path.producer && path.producer.qbId) {
//                qbchat.extraAddUserToRoom(QB, newRoom._id, [path.producer.qbId], config.qb.params, function(err, result){
//                    if(err) {
//                        console.log('error adding producer to general room', err);
//                    } else if(result) console.log('finished adding mentor to general room', path.producer.username);
//                    path['mentorRoomId'] = newRoom._id;
//                    path.save(function(err){
//                        if(err) {
//                            console.log('error saving room id to path - ', newRoom._id, path._id);
//                            path_cb(err);
//                        } else path_cb();
//                    });
//                })
//            } else path_cb();
//        });
//    }, function(done){
//        console.log('finished processing mentor path channels');
//    })
//});
//


//Path.find().deepPopulate('producer concepts').exec(function(err, paths){
//    async.eachSeries(paths, function(path, path_cb){
//        if(path.producer.qbId) {
//            var rooms = [];
//            rooms.push(path.qbId);
//            var concepts = path.concepts;
//            if(concepts && concepts.length > 0) {
//                concepts.forEach(function(concept){
//                    if(concept.qbId) rooms.push(concept.qbId);
//                });
//            }
//            async.eachSeries(rooms, function(roomId, room_cb){
//                qbchat.extraAddUserToRoom(QB, roomId, [parseInt(path.producer.qbId)], config.qb.params, function(err, result){
//                    if(err) {
//                        console.log('error adding producer to general room', err, path.producer.username);
//                    } else if(result) console.log('finished adding mentor to general room', path.producer.username);
//                    room_cb();
//                });
//            }, function(done){
//                console.log('finished processing user - ', path.producer.username);
//                path_cb();
//            });
//        } else path_cb();
//    })
//});

var create_qb_user = function(user, user_cb){
    console.log('login error for user -', user.username, user.qbId, user._id);
    if(user.username && user.email) {
        var params = {login: user.username, email: user.email, password: config.qb.defaultPassword};
        if(user.name) params['full_name'] = user.name;
        if(user.avatar) params['custom_data']  = user.avatar;

        qbchat.extraCreateUser(QB, params, config.qb.params, function(err, newUser){
            if(err) {
                console.log('error creating new user for - ', user.username, user._id, err);
                user_cb();
            } else if(newUser && newUser.id) {
                user['qbId'] = newUser.id;
                console.log('created qbId for user - ', user.username, user.qbId);
                user.save(function(err){
                    if(err) {
                        console.log('error saving new user for - ', user.username, user._id, err);
                        user_cb();
                    } else user_cb();
                })
            }
        })
    } else {
        console.log('username / email not found for user - ', user._id);
        user_cb();
    }
};

// Quickblox update ids script
//async.series([
//    function(cb){
//        User.find().exec(function(err, users){
//            if(err) console.log(err);
//            if(users && users.length > 0) {
//                async.eachSeries(users, function(user, user_cb){
//                     if(user.qbId) {
//                         qbchat.extraCreateSession(QB, function(err, session){
//                             if(session) {
//                                 qbchat.extraLogin(QB, {login: user.username, password: config.qb.defaultPassword}, function(err, result){
//                                     if(err) {
//                                         create_qb_user(user, user_cb);
//                                     } else if(result) {
//                                         console.log('already created user for -', user.username);
//                                         user_cb();
//                                     } else {
//                                         console.log('Unknown login status returned for user - ', user.username, user.qbId, user._id);
//                                         create_qb_user(user, user_cb);
//                                     }
//                                 })
//                             } else {
//                                 console.log('Unknown session error for user - ', err, user.username, user.qbId, user._id);
//                                 user_cb();
//                             }
//                         })
//                     } else {
//                         console.log('qbId not found / already saved for user - ', user.username, user._id);
//                         user_cb();
//                     }
//                 }, function(done){
//                     console.log('finished updating all users');
//                     cb();
//                 })
//            }
//        })
//    }
//], function(done){
//    console.log('finished updating all results');
//});

Enroll.find().deepPopulate('producer path path.concepts').exec(function(err, enrolls){
    async.eachSeries(enrolls, function(enroll, enroll_cb){
        console.log('started processing user - ', enroll.producer.username);
        if(enroll.producer && enroll.producer.qbId && enroll.path && enroll.path.qbId) {
            var rooms = [];
            console.log('got id - ', enroll.producer.qbId);
            rooms.push(enroll.path.qbId);
            if(enroll.path.mentorRoomId) rooms.push(enroll.path.mentorRoomId);
            if(enroll.path.concepts && enroll.path.concepts.length > 0) {
                var concepts = enroll.path.concepts;
                concepts.forEach(function(concept){
                    if(concept.qbId) rooms.push(concept.qbId);
                });
            }
            console.log('got rooms - ', rooms);
            async.eachSeries(rooms, function(roomId, room_cb){
                qbchat.extraAddUserToRoom(QB, roomId, [parseInt(enroll.producer.qbId)], config.qb.params, function(err, result){
                    if(err) {
                        console.log('error adding producer to general room', err, enroll.producer.username);
                    } else if(result) console.log('finished adding mentor to general room', enroll.producer.username);
                    room_cb();
                });
            }, function(done){
                console.log('finished processing user - ', enroll.producer.username);
                enroll_cb();
            });
        } else enroll_cb();
    }, function(done){
        console.log('finished processing mentor path channels');
    })
});