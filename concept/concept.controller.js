var async = require('async');
var validator = require('validator');
validator.extend('isImage', function (mimetype) {
    if (mimetype == 'image/jpeg' || mimetype == 'image/png') return true;
    else return false;
});

var Notifications = require('../notifications');
var notify = new Notifications();

var config = require('../config.js');
var customUtils = require('../utils.js');
var qbchat = require('../qbchat.js');

var multer = require('multer'),
    fs = require('fs');
var Path = require('./../path/path.model');
var Concept = require('./concept.model');
var User = require('./../user/user.model');
var Enrollment = require('./../enrollment/enroll.model');
var Object = require('./../object/object.model');

exports.getConcepts = function (req, res) {
    var user_search_obj = {};
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    if (user_search_obj && req.query.pathId) {
        User.findOne(user_search_obj).exec(function (err, user) {
            Concept.find({path: req.query.pathId}).deepPopulate('objects path producer').exec(function (err, concepts) {
                if (err || !concepts) return handleError('Error getting concepts for given path.', res);
                if (concepts) {
                    var resp = {data: []};
                    concepts.sort(function (a, b) {
                        return parseFloat(a.pos) - parseFloat(b.pos);
                    });
                    concepts.forEach(function (concept) {
                        var item = concept.toJSON();
                        item['id'] = concept._id;
                        item['objects'] = [];
                        if(concept.path && concept.path._id) item.path['id'] = concept.path._id;
                        concept.objects.sort(function (a, b) {
                            return parseFloat(a.pos) - parseFloat(b.pos);
                        });
                        concept.objects.forEach(function (object) {
                            var obj = object.toJSON();
                            delete obj.path;
                            delete obj.concept;
                            delete obj.producer;
                            obj['id'] = obj['_id'];
                            item.objects.push(obj);
                        });
                        resp.data.push(item);
                    });
                    var path = concepts[0].path;
                    if (path.mentorPaid && path.producer.toString() !== user._id) {
                        Enrollment.findOne({path: path._id, isEnrolled: true, producer: user._id}).exec(function (err, enroll) {
                            if (err) return handleError('error finding subscription details', res);
                            if (!enroll) {
                                resp['mentorAccess'] = false;
                                resp['mentorRenewPopup'] = false;
                                return res.status(200).json(resp);
                            }
                            else {
                                var currentTime = new Date();
                                var expiryTime = new Date(enroll.expiresOn);
                                if (currentTime > expiryTime) {
                                    resp['mentorAccess'] = false;
                                    resp['mentorRenewPopup'] = true;
                                    return res.status(200).json(resp);
                                } else {
                                    resp['mentorAccess'] = true;
                                    resp['mentorRenewPopup'] = false;
                                    return res.status(200).json(resp);
                                }
                            }
                        })
                    } else {
                        resp['mentorAccess'] = true;
                        resp['mentorRenewPopup'] = false;
                        return res.status(200).json(resp);
                    }
                }
            })
        });
    } else return res.status(422).json({errors: ['No path id or user id found']});
};

var addConsumersToRoom = function (room, id) {
    Path.findOne({_id: id}).exec(function (err, path) {
        if (err) console.log(err);
        Enrollment.find({path: path._id, isEnrolled: true}).deepPopulate('producer').exec(function (err, enrolls) {
            if (err) console.log(err);
            if (enrolls) {
                var consumers = [];
                enrolls.forEach(function (enroll) {
                    if (enroll.producer.qbId) consumers.push(enroll.producer.qbId);
                });
                qbchat.addUserToRoom(room, consumers, function (err, result) {
                    if (err) console.log(err);
                })
            }
        })
    });
};

exports.createConcept = function (req, res) {
    var responseObj = {};
    var reqBody = req.body;

    var errors = [];

    if (!validator.isLength(reqBody.title, 4, 65)) errors.push("Title must be between 8-65 characters");
    if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");
    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        if(user_search_obj) {
            User.findOne(user_search_obj).exec(function (err, user) {
                if (err || !user) return handleError('please login again or try later.', res);
                else {
                    Path.findOne({_id: reqBody.pathId}).exec(function(err, path){
                       if(err || !path) return handleError('error finding path details', res);
                        Concept.find({path: reqBody.pathId}).exec(function (err, concepts) {
                            if (err) return handleError('error getting path details', res);
                            concepts.sort(function (a, b) {
                                return parseFloat(a.pos) - parseFloat(b.pos);
                            });
                            var newPosition = concepts[concepts.length - 1].pos;
                            qbchat.createRoom(2, reqBody.title, function (err, newRoom) {
                                debugger;
//                                if (false) {
                                if (err) {
                                    responseObj["errors"] = [err.toString()];
                                    res.status(503);
                                    res.json(responseObj);
                                } else {
                                    if(newRoom._id && user.qbId) {
                                        qbchat.addUserToRoom(newRoom._id, [user.qbId], function (err, result) {
                                            if (err) console.log(err);
                                        });
                                        addConsumersToRoom(newRoom._id, path._id);
                                    }
                                    Concept.create({
                                        title: reqBody.title,
                                        pos: newPosition + 1,
                                        "objects": [],
                                        path: path._id,
                                        producer: user._id,
                                        qbId: newRoom._id
                                    }, function (err, concept) {
                                        path.concepts.push(concept._id);
                                        path.save(function(err){
                                            if(err) return handleError(err);
                                            var resp = {data: concept.toJSON()};
                                            resp.data['id'] = concept._id;
                                            res.status(201).json(resp);
                                            var notifObj = {
                                                "link": path._id.toString(),
                                                "text": reqBody.title,
                                                "photo": path.coverPhotoThumb,
                                                title: "New Concept",
                                                "is_read": 'false',
                                                "is_clicked": "false"
                                            };
                                            notify.emit('newConcept', notifObj, path.producer);
                                            var date = new Date();
                                            var chatObj = {
                                                "created": date.getTime(),
                                                "id": date.getTime(),
                                                "type": "newChannel",
                                                "pathId": path._id,
                                                "pathTitle": reqBody.title,
                                                "channelName": reqBody.title,
                                                "channelId": newRoom._id
                                            };
//                                        notify.emit('wordForChat', chatObj);
                                        });
                                    });
                                }
                            })
                        })
                    });
                }
            });
        } else return handleError('error finding user details. please retry logging in.', res);
    }
};

exports.deleteConcept = function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = [];
    var channel;

    if (validator.isNull(reqBody.id)) errors.push("Concept id has to be specified");
    /**
     * Proudly removing the dependency of parent pathId
     * and bringing more integrity to humanity.
     */
    //if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        if(user_search_obj) {
            User.findOne(user_search_obj).exec(function(err, user){
               if(err || !user) return handleError('error getting user details. please retry logging in.', res);
               else {
                    Concept.findOne({_id: reqBody.id, producer: user._id}).exec(function (err, concept) {
                        if (err) return handleError('Error deleting concept', res);
                        if(!concept) return handleError('Concept not found or you do not have appropriate permissions to delete this concept', res);
                        if (concept && parseFloat(concept.pos) > 0) {
                            if (concept.qbId) qbchat.deleteRoom(concept.qbId, function (err, res) {
                                if (err) console.log(err)
                            });
                            Path.findOne({_id: concept.path}).exec(function (err, path) {
                                if (err) return handleError('error deleting concept', res);
                                var idx = path.concepts.indexOf(concept._id);
                                if (idx > -1) {
                                    path.concepts.splice(idx, 1);
                                    path.save(function (err) {
                                        if (err) return handleError('error deleting concept', res);
                                        async.each(concept.objects, function (obj, cb) {
                                            Object.findOne({_id: obj}).exec(function (err, object) {
                                                if (err) {
                                                    cb(err);
                                                }
                                                if (object) object.remove(function (err) {
                                                    cb();
                                                });
                                                else cb('Not found');
                                            })
                                        }, function (errs) {
                                            if (errs) return handleError('error deleting concept objects', res);
                                            else {
                                                concept.remove(function (err) {
                                                    if (err) return handleError('error deleting concept', res);
                                                    return res.status(200).json({data: {success: "Concept deleted successfully"}});
                                                });
                                            }
                                        })
                                    })
                                }
                            });
                        }
                    })
                }
            });
        } else return handleError('error getting user details. please retry logging in.', res);
    }
};

exports.updateConcept = function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = [];

    if (!validator.isNull(reqBody.title))
        if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");
    //if (validator.isNull(reqBody.index)) errors.push("Index has to be specified");
    if (validator.isNull(reqBody.id)) errors.push("Concept id has to be specified");
    /**
     * proudly removed dependency on parent pathId
     * like it should always have been
     */
    //if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

//    if (!validator.isNull(reqBody.pos))
//        if (validator.isDecimal(reqBody.pos)) {
//            reqBody.pos = parseFloat(reqBody.pos);
//            /**
//             * INTEGRITY CHECK:
//             * Make sure introduction concept is always position 0
//             */
//            if (reqBody.pos <= 0) {
//                errors.push("relative position(pos) of the concept cannot be <= 0");
//            }
//        } else {
//            errors.push("relative position(pos) of the concept must be a valid decimal number");
//        }

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        User.findOne(user_search_obj).exec(function(err, user){
            if(err || !user) return handleError('Error finding user details', res);
            Concept.findOne({_id: reqBody.id, producer: user._id}).exec(function(err, concept){
                if(err || !concept) return handleError('error finding concept details or you do not ' +
                    'have appropriate permissions', res);
                else {
                    if(reqBody.title) concept['title'] = reqBody.title;
                    if(reqBody.pos) concept['pos'] = reqBody.pos;
                    Concept.findOne({path: concept.path, pos: reqBody.pos}).exec(function(err, conflict_concept){
                        if(err) return handleError('error finding concept details', res);
                        if(conflict_concept) {
                            conflict_concept['pos'] = (parseFloat(conflict_concept.pos) + 1000).toString();
                            conflict_concept.save(function(err){
                                if(err) return handleError('error saving concept details', res);
                                concept.save(function(err){
                                    if(err) return handleError('error saving concept details', res);
                                    qbchat.changeRoomName(concept.qbId, reqBody.title, function (err, res) {
                                        console.log("changed Room name");
                                    });

                                    var date = new Date();
                                    var chatObj = {
                                        "type": "newChannelName",
                                        "pathId": concept.path,
                                        "created": date.getTime(),
                                        "id": date.getTime(),
                                        "channelTitle": reqBody.title
                                    };
//                                notify.emit("wordForChat", chatObj);
                                    responseObj['data'] = concept.toJSON();
                                    responseObj['data']['id'] = concept._id;
                                    return res.status(200).json(responseObj);
                                });
                            });
                        } else {
                            concept.save(function(err){
                                if(err) return handleError('error saving concept details', res);
                                qbchat.changeRoomName(concept.qbId, reqBody.title, function (err, res) {
                                    console.log("changed Room name");
                                });

                                var date = new Date();
                                var chatObj = {
                                    "type": "newChannelName",
                                    "pathId": concept.path,
                                    "created": date.getTime(),
                                    "id": date.getTime(),
                                    "channelTitle": reqBody.title
                                };
//                                notify.emit("wordForChat", chatObj);
                                responseObj['data'] = concept.toJSON();
                                responseObj['data']['id'] = concept._id;
                                return res.status(200).json(responseObj);
                            });
                        }
                    });
                }
            })
        });
    }
};

function handleError(message, res) {
    res.status(422);
    res.json({"errors": [message]});
}

exports.getVidyoToken = function(req,res){
    var user_search_obj = {};
    if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    if (user_search_obj) {
        User.findOne(user_search_obj).exec(function (err, user) {
            if (err || !user) return handleError('Error getting users for generating vidyo Token', res);
            var resp = {};
            resp['vidyo_token'] = customUtils.generateVidyoToken(config.vidyo.key,config.vidyo.appID,user.username,config.vidyo.expiresInSeconds, "");
            return res.status(200).json(resp);
        });
    } else return res.status(422).json({errors: ['No users found']});
};
