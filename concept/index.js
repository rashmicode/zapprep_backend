'use strict';

var express = require('express');
var controller = require('./concept.controller');
var config = require('../config');
var passport = require('passport');
var qbchat = require('../qbchat');
var multer = require('multer');
var router = express.Router();


var qbchat_session = function(req, res, next) {
  debugger;
  qbchat.getSession(function (err, session) {
    if (err) {
      debugger;
      console.log("Recreating session");
      qbchat.createSession(function (err, result) {
        debugger;
        if (err) {
          res.status(503);
          res.json({"errors": ["Can't connect to the chat server, try again later"]});
        } else next();
      })
    } else next();
  })
};

router.get('/getVidyoToken', passport.authenticate('bearer', {session: false}), controller.getVidyoToken);
router.get('/', passport.authenticate('bearer', {session: false}), controller.getConcepts);
router.post('/', passport.authenticate('bearer', {session: false}), controller.createConcept);
router.patch('/', passport.authenticate('bearer', {session: false}), controller.updateConcept);
router.delete('/', passport.authenticate('bearer', {session: false}), controller.deleteConcept);

module.exports = router;
