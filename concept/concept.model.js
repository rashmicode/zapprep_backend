'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ConceptSchema = new Schema({
  title: String,
  pos: String,
  room: String,
  qbId: String,
  path: {
    type: Schema.ObjectId,
    ref: 'Path'
  },
  producer: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  objects: [{
      type: Schema.ObjectId,
      ref: 'Object'
  }]
});

ConceptSchema.virtual('id').get(function() {
    return this._id;
});

module.exports = mongoose.model('Concept', ConceptSchema);
var deepPopulate = require('mongoose-deep-populate')(mongoose);

ConceptSchema.plugin(deepPopulate, {
  whitelist: ['objects', 'path', 'producer'],
  populate: {
    objects: {
      select: 'pos title desc type url concept path producer mimetype image urlThumb'
    },
      path: {
          select: 'mentorPaid coursePaid producer'
      },
      producer: {
          select: 'name avatar userDesc username tagline qbId email'
      }
  }
});

