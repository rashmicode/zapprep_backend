/**
 * I'll tell you a secret:
 * The order of sub categories of courses also determines the
 * order it will show on the app :)
 */

module.exports = {
    paths: {
        categories_v2: {
            'CAT': ['Quants', 'Verbal', 'LR/DI', 'Admission Interview', 'Notice Board','Video Lessons','Test Series'],
            'UPSC': ['Current Affairs', 'History', 'Geography', 'Political Science', 'Economics', 'Reasoning',
                'Environment and Ecology', 'Science', 'Art and Culture', 'Interview', 'Notice Board','Video Lessons','Test Series'],
            'SSC': ['General Intelligence and Reasoning', 'General Awareness', 'Quantitative Aptitude', 'English Language',
                'Interview', 'Notice Board','Video Lessons','Test Series'],
            'IBPS': ['Reasoning', 'Quants', 'English', 'General Awareness', 'Computer', 'Interview','Video Lessons','Test Series'],
            'Placements': ['Quants', 'Verbal', 'LR/DI', 'HR Interview', 'Other Courses', 'Notice Board', 'Company Wise Preparation','Video Lessons','Test Series'],
            'GATE': ['Quants', 'ME', 'EEE', 'ECE', 'CE', 'CS', 'Maths', 'Notice Board','Video Lessons','Test Series'],
            'NDA': ['Mathematics', 'English', 'General Science', 'Social Studies', 'Current Affairs', 'SSB', 'Notice Board','Video Lessons','Test Series'],
            '12th Board': ['Maths', 'Physics', 'Biology', 'Chemistry', 'English', 'Social Science','Video Lessons','Test Series'],
            'NEET': ['Physics', 'Chemistry', 'Biology', 'Admission Counselling', 'Notice Board','Video Lessons','Test Series'],
            'JEE': ['Physics', 'Chemistry', 'Maths', 'Admission Counselling', 'Notice Board','Video Lessons','Test Series'],
            'SAT': ['Critical Reading / Writing', 'Maths', 'Science', 'Interview','Video Lessons','Test Series'],
            'GRE': ['Verbal', 'Quants', 'Interview','Video Lessons','Test Series'],
            'GMAT': ['Integrated Reasoning', 'Quantitative', 'Verbal', 'Interview','Video Lessons','Test Series'],
            '10th Board': ['Maths', 'Physics', 'Chemistry', 'Biology', 'English', 'Social Science','Video Lessons','Test Series']
        },
        categories: {
            "Placement Preparation": {
                thumbnail: "https://s3.amazonaws.com/pyoopil-prod-server/ic_expensive_2_96.png",
                sub: [
                    "Quantitative Aptitude",
                    "Verbal",
                    "Logical Reasoning",
                    "HR interview",
                    "Colleges & Training Institute",
                    "Company Wise Preparation"
                ]
            },
            "CAT": {
                thumbnail: "https://s3.amazonaws.com/pyoopil-prod-server/ic_monitor_96.png",
                sub: [
                    "Quantitative Aptitude",
                    "Verbal",
                    "Logical Reasoning",
                    "Admission interview preperations",
                    "Admission Counselling"
                ]
            },
            "GMAT": {
                thumbnail: "https://s3.amazonaws.com/pyoopil-prod-server/ic_bullish_96.png",
                sub: [
                    "Verbal",
                    "Math",
                    "Integrated Reasoning",
                    "Analytical Writing",
                    "Admission Interview Preparation",
                    "Admission Counselling"
                ]
            },
            "GRE": {
                thumbnail: "https://s3.amazonaws.com/pyoopil-prod-server/ic_robot_96.png",
                sub: [
                    "Quantitative Aptitude",
                    "Verbal Reasoning",
                    "Analytical Writing",
                    "M.S Admission interview Preparation",
                    "Admission Counselling for M.S"
                ]
            },
            "Bank PO and SSC": {
                thumbnail: "https://s3.amazonaws.com/pyoopil-prod-server/ic_controller_96.png",
                sub: [
                    "Quantitative Aptitude",
                    "Verbal",
                    "Logical Reasoning",
                    "General Knowledge",
                    "General Studies",
                    "Interview Preparation"
                ]
            }
        },
        //the id here is the pathId
        discountCodes: {
            'ZAPPTCS': {
                id: "598c7fe1b6fa6f560a03383a",
                percentage: 37.5
            },
            'DCE': {
                id: "59c95937f0c6ea4182daa129",
                percentage: 40
            },
            'QSKDOTNET': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QYAHOOCYBER': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },            
            'QBOOKHUB': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },            
            'QNAMASKAR': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QXPRESSCC': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QHARIOM': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QPRIYANKACC': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QPOOJACC': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QSUCCESSPT': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QMAASHAKTI': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QSHIVANICOM': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QCAREERMAT': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QMANYACOMP': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QSATYAM': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QHARIOMCOM': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QCYBERTREN': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'QBHARATCOM': {
                id: "5967a02c768f486fd14f7b1c",
                percentage: 38
            },
            'DSKDOTNET': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DYAHOOCYBER': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 50
            },            
            'DBOOKHUB': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },            
            'DNAMASKAR': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },            
            'DXPRESSCC': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DHARIOM': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DPRIYANKACC': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DPOOJACC': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DSUCCESSPT': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DMAASHAKTI': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DSHIVANICOM': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DCAREERMAT': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DMANYACOMP': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DSATYAM': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DHARIOMCOM': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DCYBERTREN': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            },
            'DBHARATCOM': {
                id: "5a32a87488e8cf71f04444a4",
                percentage: 37
            }
        }
    },
    "marketplace": {
        max_horizontal_tiles: 8
    },
    "user": {
        defaultProfilePhoto: "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg"
    },
    "vimeo": {
        regex: /https:\/\/player\.vimeo\.com\/video\/([0-9]+)/i,
        route: "http://api2.pyoopil.com:3000/user/vimeo?id="
    },
    "graphRelations": {
        users: {
            producesPath: "produces",
            relatedPaths: "related"
        },
        paths: {
            producedBy: "isProduced"
        }
    }
}
