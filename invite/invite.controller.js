var Path = require('./../path/path.model');
var Enrollment = require('./../enrollment/enroll.model');
var User = require('./../user/user.model');
var Invite = require('./invite.model');

var _ = require('lodash');

var Notifications = require('../notifications');
var notify = new Notifications();

exports.getInvites = function (req, res) {
    var responseObj = {more: false};
    Path.findOne({_id: req.query.pathId}).deepPopulate('producer').exec(function (err, path) {
        if (err) return handleError('Error finding path with given id', res);
            Enrollment.find({path: path._id, isEnrolled: true}).select('producer').deepPopulate('producer').exec(function (err, enrolls) {
                if (err) return handleError('error finding related paths.', res);
                responseObj['data'] = [];
                var u_ids = [];
                enrolls.forEach(function (enroll) {
                    if (enroll.producer && enroll.producer._id) {
                        var item = enroll.toJSON();
                        item.producer['id'] = item.producer._id;
                        item.producer['invite'] = false;
                        item.producer['isAccepted'] = false;
                        u_ids.push(item.producer._id);
                        responseObj.data.push(item.producer);
                    }
                });
                Invite.find({path: path._id,user:u_ids,invite: true}).exec(function (err, invites) {
                    if (err) return handleError('Error finding invites for given path.', res);
                    if (invites) {
                        _.forEach(invites, function (invite) {
                            var idx = findIndexOfKeyInObjectArr(responseObj.data, '_id', invite.user);
                            if (idx > -1){
                                responseObj.data[idx]['invite'] = true;
                                responseObj.data[idx]['isAccepted'] = invite.isAccepted;  
                            }
                        })
                    }
                    return res.status(200).json(responseObj)
                });
            });
    });
};

exports.invite = function (req, res) {
    var responseObj = {};
    var errors = [];

    var user_search_obj = {};
    if(req.body.uid) user_search_obj['_id'] = req.body.uid;

    if(user_search_obj) {
        User.findOne(user_search_obj).select('name username email avatar qbId').exec(function (err, user) {
            if (err || !user) return handleError('Error finding user details for user.', res);
            Path.findOne({_id: req.body.pathId}).exec(function (err, path) {
                if (err || !path) return handleError('Error finding path details for user.', res);
                Invite.findOne({path: path._id, user: user._id}).exec(function (err, invite) {
                    if (err) return handleError('Error searching for video invites.', res);
                    if (!invite) {
                        var payload = {
                            'invite': true,
                            'isAccepted': false,
                            'path': path._id,
                            'user': user._id,
                            'timestamp': (new Date()).getTime().toString()
                        };
                        Invite.create(payload, function (err, invite) {
                            if (err) return handleError('Error creating video invites.', res);
                            var notifyObj = {
                                "is_read": "false",
                                "is_clicked": "false",
                                "title": "Video Tutoring Invite",
                                "text": path.title,
                                "photo": "https://s3-ap-southeast-1.amazonaws.com/zapprepasset/ic_chat_video.png",
                                "link": path._id,
                                "id": (new Date()).getTime()
                            };
                            notify.emit('newVideoTutorInvite', notifyObj, req.body.uid);
                            responseObj['invite'] = invite.invite;
                            return res.status(200).json(responseObj);
                        });
                    } else {
                        if(invite.invite==true){
                            return res.status(422).json({errors: ['You have already sent the invitation.']});
                        }else{
                            invite['invite'] = true;
                            invite['isAccepted'] = false;
                            invite.save(function (err) {
                                if (err) return handleError('Error creating video invites.', res);
                                var notifyyObj = {
                                    "is_read": "false",
                                    "is_clicked": "false",
                                    "title": "Video Tutoring Invite",
                                    "text": path.title,
                                    "photo": "https://s3-ap-southeast-1.amazonaws.com/zapprepasset/ic_chat_video.png",
                                    "link": path._id,
                                    "id": (new Date()).getTime()
                                };
                                notify.emit('newVideoTutorInvite', notifyyObj, req.body.uid);
                                responseObj['invite'] = invite.invite;
                                return res.status(200).json(responseObj);
                            });                            
                        }
                    }
                })
            })
        })
    }else return handleError('error finding user details. please try logging in again.', res);
};

exports.cancelInvite = function (req, res) {
    var responseObj = {};
    var errors = [];

    var user_search_obj = {};
    if(req.body.uid) user_search_obj['_id'] = req.body.uid;

    if(user_search_obj) {
        User.findOne(user_search_obj).select('name username email avatar qbId').exec(function (err, user) {
            if (err || !user) return handleError('Error finding user details for user.', res);
            Path.findOne({_id: req.body.pathId}).exec(function (err, path) {
                if (err || !path) return handleError('Error finding path details for user.', res);
                Invite.findOne({path: path._id, user: user._id}).exec(function (err, invite) {
                    if (err) return handleError('Error searching for video invites.', res);
                    if (invite) {
                        invite['invite'] = false;
                        invite['isAccepted'] = false;
                        invite.save(function (err) {
                            if (err) return handleError('Error creating video invites.', res);
                            responseObj['invite'] = invite.invite;
                            return res.status(200).json(responseObj);
                        });
                    } else {
                        return res.status(422).json({errors: ['You have not yet sent the invitation.']});
                    }
                })
            })
        })
    }else return handleError('error finding user details. please try logging in again.', res);
};

exports.acceptInvite = function (req, res) {
    var responseObj = {};
    var errors = [];

    var user_search_obj = {};
    if(req.body.uid) user_search_obj['_id'] = req.body.uid;

    if(user_search_obj) {
        User.findOne(user_search_obj).select('name username email avatar qbId').exec(function (err, user) {
            if (err || !user) return handleError('Error finding user details for user.', res);
            Path.findOne({_id: req.body.pathId}).exec(function (err, path) {
                if (err || !path) return handleError('Error finding path details for user.', res);
                Invite.findOne({path: path._id, user: user._id,invite:true}).exec(function (err, invite) {
                    if (err) return handleError('Error searching for video invites.', res);
                    if (invite) {
                        invite['invite'] = true;
                        invite['isAccepted'] = true;
                        invite.save(function (err) {
                            if (err) return handleError('Error creating video invites.', res);
                            // var notifyyObj = {
                            //     "is_read": "false",
                            //     "is_clicked": "false",
                            //     "title": "Video Tutoring Invite",
                            //     "text": "Invite request for Video Tutoring",
                            //     "photo": "https://s3-ap-southeast-1.amazonaws.com/zapprepasset/ic_chat_video.png",
                            //     "link": path._id,
                            //     "id": (new Date()).getTime()
                            // };
                            // notify.emit('newVideoTutorInvite', notifyyObj, req.body.uid);
                            responseObj['isAccepted'] = invite.isAccepted;
                            return res.status(200).json(responseObj);
                        });                            
                    } else {
                        return res.status(422).json({errors: ['Invitiation expired.']});
                    }
                })
            })
        })
    }else return handleError('error finding user details. please try logging in again.', res);
};

function findIndexOfKeyInObjectArr(obj_arr, key, val) {
    for (var item = 0; item < obj_arr.length; item++) {
        if (obj_arr[item][key] && obj_arr[item][key].toString() == val.toString()) return item;
    }
    return -1;
}

function handleError(message, res) {
    res.status(422);
    res.json({"errors": [message]});
}
