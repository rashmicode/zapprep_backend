'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var InviteSchema = new Schema({
  invite: {
    type: Boolean,
    default: false
  },
  isAccepted: {
    type: Boolean,
    default: false
  },  
  path: {
    type: Schema.ObjectId,
    ref: 'Path'
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  timestamp: String
});

InviteSchema.virtual('id').get(function() {
    return this._id;
});

module.exports = mongoose.model('Invite', InviteSchema);