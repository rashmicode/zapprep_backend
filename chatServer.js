console.log("Starting Chat Server")
/**
 * TODO : Firebase discourages use of keys such as timestamp,
 * use their post.set for keys that handle it uniquely and well
 * @type {Logger|exports}
 */
var bunyan = require('bunyan');
var log = bunyan.createLogger({
    name: 'pyoopil2chatServer',
    streams: [
        {
            path: __dirname + '/chatServer_errors.log'  // log ERROR and above to a file
        }
    ]
});

// Mentors is a special user type in my project. Modify/delete as per need

var config = require('./config.js');
var ChatUser = require("./chat");

var Firebase = require("firebase");
var myFirebaseRef = new Firebase(config.firebase.url, config.firebase.secret);

var QB = require('quickblox');
QB.init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var now = new Date().getTime() / 1000
var dbUtils = require('./dbUtils')

var rooms = [];
var channels = {}, // all channels -> {"Room XMPP ID": "CourseName:::ChannelName:::CourseDbId"} 
    onlineUsers = {}, // online users in each channel -> {"Room XMPP ID" : [XMPP ID of online users]}
    mentors = {}, // mentors -> {"CourseDbId" : "User XMPP ID:::UserDbId"}
    allUsers = {}; // all users -> {"username" : "User XMPP ID:::UserDbId:::UserGcmId"}
var receiver;

// DB Call for creating cache for all channels and mentors
// so that DB call is not made for lookups
//db.list('paths', {limit: 100})
//    .then(function (paths) {
//        paths.body.results.forEach(function (path) {
//            pathId = path.path.key;
//            path = path.value;
//            pathName = path.title;
//            channels[path.mentorRoomId] = pathName + ":::Mentor:::" + pathId;
//            channels[path.qbId] = pathName + ":::General:::" + pathId;
//            mentors[pathId] = path.producer.qbId;
//            path.concepts.forEach(function (concept) {
//                channels[concept.qbId] = pathName + ":::" + concept.title + ":::" + pathId;
//            })
//        })
//    })
//    .fail(function (err) {
//        log.error("Couldn't get the paths for caching, you are doomed");
//    });

dbUtils.getAllItems("paths", dbUtils.createGetAllItemsQuery())
    .then(function (paths) {
        paths.forEach(function (path) {
            var pathId = path.id
            var pathName = path.title;
            channels[path.mentorRoomId] = pathName + ":::Mentor:::" + pathId;
            channels[path.qbId] = pathName + ":::General:::" + pathId;
            mentors[pathId] = path.producer.qbId;
            path.concepts.forEach(function (concept) {
                channels[concept.qbId] = pathName + ":::" + concept.title + ":::" + pathId;
            })
        })
    })
    .fail(function (err) {
        log.error("Couldn't get the paths for caching, you are doomed");
    });

dbUtils.getAllItems("users", dbUtils.createGetAllItemsQuery())
    .then(function (users) {
        users.forEach(function (user) {
            allUsers[user.username] = user.qbId + ":::" + user.id + ":::" + user.gcmId;
        })
    })
    .fail(function (err) {
        console.log(err)
        console.log("Couldn't get the users for caching, you are doomed");
    });
// DB call for creating cache for all Users
//db.list('users', {limit: 100})
//    .then(function (users) {
//        users.body.results.forEach(function (user) {
//            user = user.value;
//            allUsers[user.username] = user.qbId + ":::" + user.id + ":::" + user.gcmId;
//        })
//    })
//    .fail(function (err) {
//        console.log("Couldn't get the users for caching, you are doomed");
//    });

//var cacheUsers = function (offset) {
//    db.newSearchBuilder()
//        .collection('users')
//        .limit(100)
//        .offset(offset)
//        .query('@path.kind:item')
//        .then(function (users) {
//            users.body.results.forEach(function (user) {
//                user = user.value;
//                allUsers[user.username] = user.qbId + ":::" + user.id + ":::" + user.gcmId;
//            });
//
//            offset += 100;
//            var remainingUsers = users.body.total_count - offset;
//            if (remainingUsers > 0) {
//                cacheUsers(offset);
//            }
//        })
//        .fail(function (err) {
//            console.log("Couldn't get the users for caching, you are doomed");
//        });
//};
//
//cacheUsers(0);

// Create QB session and set things into motion
QB.createSession(config.qb.params, function (err, session) {
    //var rooms = []
    getDialogs(400, 0)
    var firstTime = true

    function getDialogs(limit, offset) {
        // Get a list of all the channels
        QB.chat.dialog.list({limit: limit, skip: offset}, function (err, res) {
            var total = res.total_entries
            var done = limit + offset
            //res.items = []
            //console.log(res)
            res.items.forEach(function (room) {
                rooms.push(room.xmpp_room_jid);
            })

            if (done < total)
                getDialogs(limit, offset + 400)
            else
                dialogsDone()

            // Declare receiver with credentials as:
            // 'server': {
            //     'jid': "JID of the receiver",
            //     'password': "Password",
            //     'host': "chat.quickblox.com"
            // }
        });
    }

    /**
     * hack to get it called just once (firstTime)
     */
    function dialogsDone() {
        if (firstTime) {
            firstTime = false
            receiver = new ChatUser({
                credentials: config.qb.server,
                defaults: false,
                on: {online: XJoinRooms, stanza: onStanza} // assign functions for 'online' and 'stanza' events
            })
            console.log("listening")
        }
    }
});

var XJoinRooms = function () {
    var count = 0
    rooms.forEach(function (room) {
        count++
        // Join Rooms
        receiver.join(room + '/' + config.qb.adminId);
    })
    console.log("Number of Rooms joined: " + count)
    log.info("Number of Rooms joined: " + count)
}

// Custom implementation for sending notifications
onStanza = function (stanza) {
    if (stanza.name === "message") {
        var senderType, senderList;
        var timestamp = stanza.children[1].children[0].children[0]
        if (timestamp >= now) {
            var msg = stanza.children[0].children[0]
            var senderId = stanza.attrs.from.split('/')[1]
            var dialogId = stanza.children[1].children[3].children[0]

            //TODO: OMG LOOK AT THAT USERNAME SIZE LIMITATION BELOW :O
            //6,20 hardcoded here :O
            //usernames have already been upgraded to max 12 characters :O
            var atClass = msg.match(/@[Cc]lass/g)
            var mentions = msg.match(/\(@[a-zA-Z0-9_]{6,20}\)/g)
            console.log(onlineUsers[dialogId])
            log.info(onlineUsers[dialogId])

            if (atClass) {
                console.log("chatServer : detects atClass")
                log.info("chatServer : detects atClass")
                var senderUserId = getUserId(allUsers, senderId);
                var channelDetails = channels[dialogId].split(':::')
                var nofObj = {
                    "timestamp": timestamp,
                    "msg": msg,
                    "senderUserId": senderUserId,
                    "channelDetails": channelDetails,
                    "dialogId": dialogId,
                    "onlineUsers": onlineUsers[dialogId]
                }
                console.log("chatServer : writes to firebase : nofObj")
                log.info("chatServer : writes to firebase: nofObj")
                console.log(nofObj)
                log.info({'nofObj': nofObj})
                myFirebaseRef.child("chatServer/atClass/" + timestamp).set(nofObj);

                //Note that here we are directly firing the notification
                //Not checking if the users the notif is being fired to are online or not
                //(like in the case below)
                //hmmmm.....
                //this is a UX decision.
            } else if (mentions) {
                mentions = mentions.map(function (m) {
                    var temp = m.replace('(@', '')
                    return temp.replace(')', '')
                }).filter(function (m) {
                    if (typeof allUsers[m] !== 'undefined') {
                        if (!is_online(dialogId, allUsers[m].split(':::')[0])) {
                            return 1;
                        } else {
                            return 0;
                        }
                    } else {
                        return 0;
                    }
                })

                if (typeof mentions[0] !== 'undefined') {
                    if (is_mentor(dialogId, senderId)) {
                        senderType = "Mentor";
                    } else {
                        senderType = "Peer";
                    }
                    var senderUserId = getUserId(allUsers, senderId);
                    var channelDetails = channels[dialogId].split(':::')
                    var receiverIds = [], receiverGcmIds = [];
                    mentions.forEach(function (mention) {
                        mentionDetails = allUsers[mention].split(':::');
                        receiverIds.push(mentionDetails[1])
                        receiverGcmIds.push(mentionDetails[2])
                    })
                    var nofObj = {
                        "timestamp": timestamp,
                        "msg": msg,
                        "senderUserId": senderUserId,
                        "channelDetails": channelDetails,
                        "dialogId": dialogId,
                        "senderType": senderType,
                        "receiverIds": receiverIds,
                        "receiverGcmIds": receiverGcmIds
                    }
                    console.log("Notification sent to " + mentions);
                    console.log(nofObj)
                    myFirebaseRef.child("chatServer/mentions/" + timestamp).set(nofObj);
                }
            }
            if (is_mentor(dialogId, senderId) && !(is_mentorChannel(dialogId))) {
                var senderUserId = getUserId(allUsers, senderId);
                var channelDetails = channels[dialogId].split(':::')
                var nofObj = {
                    "timestamp": timestamp,
                    "msg": msg,
                    "senderUserId": senderUserId,
                    "channelDetails": channelDetails,
                    "dialogId": dialogId,
                    "onlineUsers": onlineUsers[dialogId]
                }
                console.log("Notification sent to everybody because mentor is too chatty");
                log.info("Notification sent to everybody because mentor is too chatty");
                console.log(nofObj)
                log.info({'nofObj': nofObj})
                myFirebaseRef.child("chatServer/mentorMessages/" + timestamp).set(nofObj);
            }
        }
    } else if (stanza.attrs.id || stanza.attrs.type) {
        var dialogId = stanza.attrs.from.split('@')[0].split('_')[1];
        var senderId = stanza.attrs.from.split('/')[1];
        if (typeof dialogId !== 'undefined') {
            if (stanza.attrs.type !== "unavailable") {
                if (dialogId in onlineUsers) {
                    if (onlineUsers[dialogId].indexOf(senderId) == -1) {
                        onlineUsers[dialogId].push(senderId)
                    }
                } else {
                    onlineUsers[dialogId] = [senderId]
                }
                console.log("added " + senderId)
                console.log(onlineUsers[dialogId])
                log.info("added " + senderId)
                log.info({'onlineUsers[dialogId]': onlineUsers[dialogId]})
            } else {
                if (dialogId in onlineUsers) {
                    var index = onlineUsers[dialogId].indexOf(senderId);
                    if (index > -1) {
                        onlineUsers[dialogId].splice(index, 1)
                        console.log("removed " + senderId)
                        console.log(onlineUsers[dialogId])
                        log.info("removed " + senderId)
                        log.info(onlineUsers[dialogId])
                    }
                }
            }
        }
    }
};


// 
// Helper methods
// 
var is_mentor = function (channel, user) {
    var pathId = channels[channel].split(":::")[2]
    var mentorId = mentors[pathId]
    if (mentorId == user)
        return 1;
    else
        return 0;
}

var is_mentorChannel = function (channel) {
    var channelName = channels[channel].split(':::')[1]
    if (channelName == 'Mentor')
        return 1;
    else
        return 0;
}

var is_online = function (channel, user) {
    if (typeof onlineUsers[channel] !== 'undefined') {
        if (onlineUsers[channel].indexOf(user) > -1) {
            return 1;
        }
        else {
            return 0;
        }
    } else {
        return 0;
    }
}

var getKey = function (obj, value) {
    for (var key in obj) {
        if (obj[key] == value) {
            return key;
        }
    }
    return null;
};

var getUserId = function (obj, value) {
    for (var key in obj) {
        var thing = obj[key].split(':::')
        if (thing[0] == value) {
            return thing[1];
        }
    }
    return null;
};


// 
// Listening for events to add info to cache
// 
myFirebaseRef.child('chatServer/addToCache').on("child_added", function (snapshot, prevChildKey) {
    var nof = snapshot.val();
    timestamp = nof.id.split("@")[0]
    if (timestamp / 1000 > (now - 60)) {
        switch (nof.type) {
            case 'newUser':
                addUserToCache(nof)
                break;

            case 'newChannel':
                addChannelToCache(nof)
                break;

            case 'newMentor':
                addMentorToCache(nof)
                break;

            case 'newGcmId':
                updateGCMInCache(nof)
                break;

            case 'newCourseName':
                updateCourseName(nof)
                break;

            case 'newChannelName':
                updateChannelName(nof)
                break;

            default:
                console.log("Unrecognized notification type")
                log.error("Unrecognized notification type")
        }
    }
}, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
});


// 
// Helper functions to add infot to cache
// 
var addUserToCache = function (user) {
    allUsers[user.username] = user.qbId + ":::" + user.dbId + ":::" + user.gcmId;
    console.log("New User Added:" + user.username);
    log.info("New User Added:" + user.username);
}

var addChannelToCache = function (channel) {
    channels[channel.channelId] = channel.pathTitle + ":::" + channel.channelName + ":::" + channel.pathId;
    receiver.join(channel.channelId + '/' + config.qb.adminId);
    console.log("Joined " + channel.channelId);
    log.info("Joined " + channel.channelId);
}

var addMentorToCache = function (mentor) {
    mentors[mentor.pathId] = mentor.qbId;
    console.log("New Mentor Added: " + mentor.qbId);
    log.info("New Mentor Added: " + mentor.qbId);
}

var updateGCMInCache = function (user) {
    if (typeof allUsers[user.username] !== 'undefined') {
        var oldGcm = allUsers[user.username].split(':::')[2]
        allUsers[user.username] = allUsers[user.username].replace(oldGcm, user.gcmId)
        console.log("Updated GCM ID for " + user.username);
        log.info("Updated GCM ID for " + user.username);
    }
}

var updateCourseName = function (path) {
    var roomId, oldName;
    for (var key in channels) {
        thing = channels[key].split(':::');
        if (thing[2] == path.pathId) {
            roomId = key;
            oldName = thing[0];
            break;
        }
    }
    channels[key] = channels[key].replace(oldName, path.pathTitle)
    console.log("Updated Course name for " + path.pathTitle);
    log.info("Updated Course name for " + path.pathTitle);
}

var updateChannelName = function (path) {
    var roomId, oldName;
    for (var key in channels) {
        thing = channels[key].split(':::');
        if (thing[2] == path.pathId) {
            roomId = key;
            oldName = thing[1];
            break;
        }
    }
    channels[key] = channels[key].replace(oldName, path.channelTitle)
    console.log("Updated Channel name for " + path.channelTitle);
    log.info("Updated Channel name for " + path.channelTitle);
}