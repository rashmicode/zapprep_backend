var validator = require('validator');
var _ = require('lodash');

var constants = require('../constants');

var Notifications = require('../notifications');
var notify = new Notifications();

var config = require('../config.js'),
    customUtils = require('../utils.js');

var multer = require('multer'),
    fs = require('fs');

var Object = require('./object.model');
var Concept = require('./../concept/concept.model');
var Path = require('./../path/path.model');
var User = require('./../user/user.model');

validator.extend('isYoutubeURL', function (url) {
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? true : false;
});

validator.extend('isVimeoURL', function (url) {
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? true : false;
});

exports.createObject = function (req, res) {
    var responseObj = {};
    var filetype;
    var reqBody = req.body;
    var errors = [];
    var isVimeo = false;

    var emitNotif = function (pathId, type, mentorId) {
        var notifObj = {
            "link": pathId,
            'title': 'New Content',
            "is_read": 'false',
            "text": "New Content was posted in course.",
            "is_clicked": 'false'
        };
        notify.emit('newObject', notifObj, mentorId);
    };
    if (validator.isNull(reqBody.conceptId)) errors.push("Concept id has to be specified");

    /**
     * Proudly remove such parent dependencies
     */
    //if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");
    if (!validator.isIn(reqBody.type, ['file', 'link', 'text', 'image'])) errors.push("type should be file/link/text/image. For video use type link");

    if (!validator.isNull(reqBody.title))
        if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");

    /**
     * pos (position) is not required and needs to be auto - generated
     * */
    //if (validator.isDecimal(reqBody.pos)) {
    //    reqBody.pos = parseFloat(reqBody.pos)
    //    if (reqBody.pos < 0) {
    //        errors.push("relative position(pos) of the concept cannot be less than 0");
    //    }
    //} else {
    //    errors.push("relative position of the concept must be a valid decimal number");
    //}

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;

        if(user_search_obj) {
            User.findOne(user_search_obj).exec(function (err, user) {
                if (err || !user) return handleError('error finding logged in user. try logging in again.', res);
                debugger;
                if (reqBody.conceptId) {
                    Concept.findOne({_id: reqBody.conceptId, producer: user._id}).deepPopulate('objects').exec(function (err, concept) {
                        debugger;
                        if (err || !concept) return handleError('error finding concept with given id or you do not ' +
                            'have sufficient privileges to add new object', res);
                        var objects = _.clone(concept.objects);
                        if (objects.length > 0) {
                            objects.sort(function (a, b) {
                                return parseFloat(a.pos) - parseFloat(b.pos);
                            });
                        }
                        var newPosition = objects.length > 0 ? objects[objects.length - 1].pos + 1 : 0;
                        var obj_payload = {
                            type: reqBody.type,
                            pos: newPosition,
                            title: reqBody.title,
                            desc: reqBody.desc,
                            concept: concept._id,
                            path: concept.path,
                            producer: user._id
                        };
                        debugger;
                        switch (reqBody.type) {
                            case 'text':
                                if (!validator.isLength(reqBody.desc, 8, 10000)) errors.push("Description cannot be empty (8-10000 characters)");
                                if (errors.length > 0) {
                                    responseObj["errors"] = errors;
                                    res.status(422);
                                    res.json(responseObj);
                                } else {
                                    debugger;
                                    Object.create(obj_payload, function (err, object) {
                                        debugger;
                                        if (err || !object) return handleError('error creating object', res);
                                        concept.objects.push(object._id);
                                        concept.save(function (err) {
                                            debugger;
                                            if (err) return handleError('Error updating concept with object.', res);
                                            emitNotif(concept.path.toString(), 'text', user._id.toString());
                                            var resp = {data: object.toJSON()};
                                            resp.data['id'] = object._id;
                                            return res.status(201).json(resp)
                                        });
                                    });
                                }
                                break;
                            case 'link':
                                if (!validator.isNull(reqBody.desc))
                                    if (!validator.isLength(reqBody.desc, 8, 3000)) errors.push("Description must be between 8-3000 characters");
                                if (validator.isURL(req.body.url)) {
                                    if (req.body.url.indexOf("vimeo.com") > -1) {
                                        //all good
                                        var patt = constants.vimeo.regex
                                        var found = req.body.url.match(patt)

                                        if (found) {
                                            isVimeo = true
                                            reqBody.url = constants.vimeo.route + found[1]
                                        } else {
                                            errors.push("Please enter the Vimeo URL in the format : https://player.vimeo.com/video/<videoId>")
                                        }
                                    }
                                } else {
                                    errors.push("Url must be a valid URL");
                                }

                                if (errors.length > 0) {
                                    responseObj["errors"] = errors;
                                    res.status(422);
                                    res.json(responseObj);
                                } else {
                                    customUtils.getLinkInfo(reqBody.url, function (info) {
                                        if (validator.isYoutubeURL(req.body.url)) {
                                            filetype = 'video';
                                        } else if (isVimeo) {
                                            filetype = 'vimeo'
                                        } else {
                                            filetype = 'link';
                                        }
                                        obj_payload['type'] = filetype;
                                        obj_payload['url'] = reqBody.url;
                                        if (info && info.image)
                                            obj_payload["image"] = info.image;
                                        else
                                            obj_payload["image"] = constants.defaultLinkUrl;
                                        Object.create(obj_payload, function (err, object) {
                                            if (err || !object) return handleError('error creating object', res);
                                            concept.objects.push(object._id);
                                            concept.save(function (err) {
                                                if (err) return handleError('Error updating concept with object.', res);
                                                emitNotif(concept.path.toString(), 'link', user._id.toString());
                                                var resp = {data: object.toJSON()};
                                                resp.data['id'] = object._id;
                                                return res.status(201).json(resp)
                                            });
                                        });
                                    });
                                }
                                break;
                            case 'image':
                            case 'file':
                                if (!validator.isNull(reqBody.desc))
                                    if (!validator.isLength(reqBody.desc, 8, 3000)) errors.push("Description must be 8-3000 characters");
                                if (validator.isNull(req.files.file)) errors.push("Please select a file to upload");

                                if (!validator.isNull(reqBody.title))
                                    if (!validator.isLength(reqBody.title, 4, 65)) errors.push("Title must be between 4-65 characters");

                                if (!validator.isNull(reqBody.desc))
                                    if (!validator.isLength(reqBody.desc, 20)) errors.push("Description must be greater than 20 characters");

                                if (errors.length > 0) {
                                    responseObj["errors"] = errors;
                                    res.status(422);
                                    res.json(responseObj);
                                } else {
                                    customUtils.upload(req.files.file, function (info) {
                                        if (req.files.file.mimetype.match(/^image.*/)) {
                                            filetype = 'image';
                                        } else {
                                            filetype = 'file';
                                        }
                                        obj_payload['type'] = filetype;
                                        obj_payload['size'] = req.files.file.size;
                                        obj_payload['mimetype'] = req.files.file.mimetype;
                                        obj_payload['url'] = (info && info.url) ? info.url : "";
                                        obj_payload['urlThumb'] = (info && info.urlThumb) ? info.urlThumb : "";
                                        if (filetype == "file")
                                            obj_payload["urlThumb"] = getFilePreviewUrl(obj_payload["mimetype"]);
                                        Object.create(obj_payload, function (err, object) {
                                            if (err || !object) return handleError('error creating object', res);
                                            concept.objects.push(object._id);
                                            concept.save(function (err) {
                                                if (err) return handleError('Error updating concept with object.', res);
                                                emitNotif(concept.path.toString(), filetype, user._id.toString());
                                                var resp = {data: object.toJSON()};
                                                resp.data['id'] = object._id;
                                                return res.status(201).json(resp)
                                            });
                                        });
                                    });
                                }
                                break;
                            default:
                                return handleError("Type must be file/link/text", res);
                                break;
                        }
                    });
                }
            })
        } else return handleError('error finding user details. please retry logging in.', res);
    }
};

exports.updateObject = function (req, res) {
//    if (Object.keys(req.body).length === 0) {
//        res.status(422);
//        res.json({"errors": ["No key was sent in the body"]});
//        return;
//    }

    var responseObj = {};
    var filetype;
    var reqBody = req.body;
    var response = null;
    var errors = [];

    if (validator.isNull(reqBody.id)) errors.push("Object id has to be specified");
//    if (!validator.isNull(reqBody.pos))
//        if (validator.isDecimal(reqBody.pos)) {
//            reqBody.pos = parseFloat(reqBody.pos);
//            /**
//             * INTEGRITY CHECK:
//             * Make sure object's position is not negative
//             */
//            if (reqBody.pos < 0) {
//                errors.push("relative position(pos) of the object cannot be <= 0");
//            }
//        } else {
//            errors.push("relative position(pos) of the object must be a valid decimal number");
//        }
    if (!validator.isNull(reqBody.title))
        if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        if(user_search_obj) {
            User.findOne(user_search_obj).exec(function (err, user) {
                if (err || !user) return handleError('error finding logged in user. try logging in again.', res);
                Object.findOne({_id: reqBody.id, producer: user._id}).deepPopulate('objects').exec(function (err, object) {
                    if (err || !object) return handleError('error finding object with given id or you do not ' +
                        'have sufficient privileges to add new object', res);
                    switch (object.type) {
                        case 'text':
                            if (!validator.isLength(reqBody.desc, 8, 10000)) errors.push("Description cannot be empty (8-10000 characters)");
                            if (errors.length > 0) {
                                responseObj["errors"] = errors;
                                res.status(422);
                                res.json(responseObj);
                            } else {
                                if (reqBody.title) object['title'] = reqBody.title;
                                if (reqBody.desc) object['desc'] = reqBody.desc;
                                if (reqBody.pos) object['pos'] = reqBody.pos;
                                object.save(function (err) {
                                    if (err || !object) return handleError('error saving object', res);
                                    var resp = {data: object.toJSON()};
                                    resp.data['id'] = object._id;
                                    return res.status(201).json(resp);
                                });
                            }
                            break;
                        case 'video':
                        case 'link':
                            if (!validator.isNull(reqBody.desc))
                                if (!validator.isLength(reqBody.desc, 8, 3000)) errors.push("Description must be between 8-3000 characters");
                            if (validator.isURL(req.body.url)) {
                                if (req.body.url.indexOf("vimeo.com") > -1) {
                                    //all good
                                    var patt = constants.vimeo.regex;
                                    var found = req.body.url.match(patt);

                                    if (found) {
                                        var isVimeo = true
                                        reqBody.url = constants.vimeo.route + found[1]
                                    } else {
                                        errors.push("Please enter the Vimeo URL in the format : https://player.vimeo.com/video/<videoId>")
                                    }
                                }
                            } else {
                                errors.push("Url must be a valid URL");
                            }

                            if (errors.length > 0) {
                                responseObj["errors"] = errors;
                                res.status(422);
                                res.json(responseObj);
                            } else {
                                customUtils.getLinkInfo(reqBody.url, function (info) {
                                    if (validator.isYoutubeURL(req.body.url)) {
                                        filetype = 'video';
                                    } else if (isVimeo) {
                                        filetype = 'vimeo'
                                    } else {
                                        filetype = 'link';
                                    }
                                    if (reqBody.title) object['title'] = reqBody.title;
                                    if (reqBody.desc) object['desc'] = reqBody.desc;
                                    if (reqBody.pos) object['pos'] = reqBody.pos;
                                    if (reqBody.url) object['url'] = reqBody.url;
                                    if (info && info.image)
                                        object["image"] = info.image;
                                    else
                                        object["image"] = constants.defaultLinkUrl;
                                    object.save(function (err) {
                                        if (err || !object) return handleError('error saving object', res);
                                        var resp = {data: object.toJSON()};
                                        resp.data['id'] = object._id;
                                        return res.status(201).json(resp);
                                    });
                                });
                            }
                            break;
                        case 'image':
                        case 'file':
                            if (!validator.isNull(reqBody.desc))
                                if (!validator.isLength(reqBody.desc, 8, 3000)) errors.push("Description must be 8-3000 characters");
                            if (validator.isNull(req.files.file)) errors.push("Please select a file to upload");

                            if (!validator.isNull(reqBody.title))
                                if (!validator.isLength(reqBody.title, 4, 65)) errors.push("Title must be between 4-65 characters");

                            if (!validator.isNull(reqBody.desc))
                                if (!validator.isLength(reqBody.desc, 20)) errors.push("Description must be greater than 20 characters");

                            if (errors.length > 0) {
                                responseObj["errors"] = errors;
                                res.status(422);
                                res.json(responseObj);
                            } else {
                                customUtils.upload(req.files.file, function (info) {
                                    if (req.files.file.mimetype.match(/^image.*/)) {
                                        filetype = 'image';
                                    } else {
                                        filetype = 'file';
                                    }
                                    if (filetype) object['type'] = filetype;
                                    if (req.files.file.size) object['size'] = req.files.file.size;
                                    if (req.files.file.mimetype) object['mimetype'] = req.files.file.mimetype;
                                    if (info && info.url) object['url'] = info.url;
                                    if (info && info.urlThumb) object['urlThumb'] = info.urlThumb;
                                    if (reqBody.title) object['title'] = reqBody.title;
                                    if (reqBody.desc) object['desc'] = reqBody.desc;
                                    if (reqBody.pos) object['pos'] = reqBody.pos;
                                    object.save(function (err) {
                                        if (err) return handleError('Error saving object', res);
                                        var resp = {data: object.toJSON()};
                                        resp.data['id'] = object._id;
                                        return res.status(201).json(resp);
                                    })
                                });
                            }
                            break;
                        default:
                            return handleError("Type must be file/link/text", res);
                            break;
                    }
                });
            })
        } else return handleError('error finding user details. please retry logging in.', res);
    }
};

exports.deleteObject = function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = [];

    if (validator.isNull(reqBody.id)) errors.push("Object id has to be specified");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        if(user_search_obj) {
            User.findOne(user_search_obj).exec(function(err, user){
                if(err || !user) return handleError('Error finding user details. please retry logging in.', res);
                Object.findOne({_id: reqBody.id, producer: user._id}).exec(function (err, obj) {
                    if (err || !obj) return handleError('Error finding object details or you do not have appropriate permissions to delete this object.', res);
                    Concept.findOne({_id: obj.concept, producer: user._id}).exec(function (err, concept) {
                        if (err || !concept) return handleError('Error finding concept details or you do not have appropriate permissions to delete this object.', res);
                        var idx = concept.objects.indexOf(obj._id);
                        if (idx > -1) {
                            concept.objects.splice(idx, 1);
                            concept.save(function (err) {
                                if (err) return handleError('Error saving concept details', res);
                                obj.remove(function (err) {
                                    if (err) return handleError('Error removing object details', res);
                                    return res.status(200).json({data: {success: "Object Deleted"}});
                                });
                            })
                        }
                    })
                })
            })
        } else return handleError('error finding user details. please retry logging in.', res);
    }
};

var getFilePreviewUrl = function (mimeType) {
    var url = "";
    switch (mimeType) {
        //These are the mimetypes which google docs support. Please dont add any more
        case "application/msword":
        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
        case "application/vnd.openxmlformats-officedocument.wordprocessingml.template":
        case "application/vnd.oasis.opendocument.text":
            url = "https://s3.amazonaws.com/pyoopil-prod-server/doc_file_type.png";
            break;
        case "application/vnd.ms-excel":
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.template":
            url = "https://s3.amazonaws.com/pyoopil-prod-server/excel_file_type.png";
            break;
        case "application/vnd.openxmlformats-officedocument.presentationml.template":
        case "application/vnd.openxmlformats-officedocument.presentationml.slideshow":
        case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
        case "application/vnd.openxmlformats-officedocument.presentationml.slide":
        case "application/mspowerpoint":
        case "application/powerpoint":
        case "application/vnd.ms-powerpoint":
        case "application/x-mspowerpoint":
            url = "https://s3.amazonaws.com/pyoopil-prod-server/ppt_file_type.png";
            break;
        case "application/pdf":
            url = "https://s3.amazonaws.com/pyoopil-prod-server/pdf_file_type.png";
            break;
        default:
            url = "https://s3.amazonaws.com/pyoopil-prod-server/file_type.png";
    }
    return url
};

function handleError(message, res) {
    res.status(422);
    res.json({"errors": [message]});
}
