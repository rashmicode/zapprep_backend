'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ObjectSchema = new Schema({
  type: {
    type: String
    // enum: ['video']
  },
  pos: Number,
  url: String,
  title: String,
  desc: String,
  image: String,
  size: Number,
  mimetype: String,
  urlThumb: String,
  concept: {
    type: Schema.ObjectId,
    ref: 'Concept'
  },
  path: {
    type: Schema.ObjectId,
    ref: 'Path'
  },
  producer: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

ObjectSchema.virtual('id').get(function() {
    return this._id;
});

module.exports = mongoose.model('Object', ObjectSchema);
var deepPopulate = require('mongoose-deep-populate')(mongoose);

ObjectSchema.plugin(deepPopulate, {
  whitelist: [],
  populate: {}
});

