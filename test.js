/**
 * Created by ankan on 8/10/15.
 */

var util = require('util');
var customUtils = require('./utils.js');
var config = require('./config.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);


var fireNewConceptNotif = function (offset, pathId) {
    var promiseResult;

    db.newGraphReader()
        .get()
        .limit(100)
        .offset(offset)
        .from('paths', pathId)
        .related('isConsumed')
        .then(function (result) {
            promiseResult = result;
        })
        .then(function () {
            offset += 100;
            console.log(promiseResult.body.total_count);
            var remainingResults = promiseResult.body.total_count - offset;
            if (remainingResults > 0) {
                fireNewConceptNotif(offset, pathId);
            }
        })
};

fireNewConceptNotif(0, "0c3484518b604c4a")