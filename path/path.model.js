'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PathSchema = new Schema({
    studentCount: Number,
    newStudents: Number,
    mentorUSD: Number,
    mentorINR: Number,
    courseINR: Number,
    reviewer_count: Number,
    rating: Number,
    courseUSD: Number,
    selectedCurrency: {
        type: String,
        enum: ['USD', 'INR']
    },
    mentorAvailable: Boolean,
    accessId: String,
    title: String,
    category: String,
    qbId: String,
    mentorRoomId: String,
    subCat: String,
    desc: String,
    coverPhoto: String,
    coverPhotoThumb: String,
    producer: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    created: {
        type: Date,
        default: Date
    },
    isPrivate: {
        type: Boolean,
        default: true
    },
    mentorPaid: {
        type: Boolean
    },
    coursePaid: {
        type: Boolean
    },
    concepts: [
        {
            type: Schema.ObjectId,
            ref: 'Concept'
        }
    ]
});
PathSchema.virtual('id').get(function() {
    return this._id;
});
module.exports = mongoose.model('Path', PathSchema);
var deepPopulate = require('mongoose-deep-populate')(mongoose);

PathSchema.plugin(deepPopulate, {
    whitelist: ['concepts', 'producer', 'concepts.objects'],
    populate: {
        'concepts': {
            select: 'qbId title pos producer objects'
        },
        'producer': {
            select: 'name avatar userDesc username tagline qbId email onlineStatus'
        },
        'concepts.objects': {
            select: 'title pos desc url type image size mimetype urlThumb'
        }
    }
});

