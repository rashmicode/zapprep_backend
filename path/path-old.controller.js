var validator = require('validator');
var constants = require('../constants');
var kew = require('kew');
var parseUrl = require('parseurl');

validator.extend('isImage', function (mimetype) {
    return (mimetype == 'image/jpeg' || mimetype == 'image/png') ? true : false;
});
validator.extend('isYoutubeURL', function (url) {
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? true : false;
});

var passport = require('passport');
var _ = require('lodash');

var Notifications = require('../notifications');
var notify = new Notifications();

var config = require('../config.js'),
    customUtils = require('../utils.js');
var qbchat = require('../qbchat.js');

var multer = require('multer'),
    fs = require('fs');

const util = require('util');

const NodeCache = require("node-cache");
const myCache = new NodeCache({stdTTL: 3600, checkperiod: 3620});


function myXOR(a, b) {
    return ( a || b ) && !( a && b );
}

function stringToBoolean(theString) {
    if (theString == "true") {
        return true;
    } else {
        return false;
    }
}

/**
 * Remove duplicate items in an array
 * http://stackoverflow.com/questions/6940103/how-do-i-make-an-array-with-unique-elements-i-e-remove-duplicates
 * @param a
 * @returns {Array}
 */
function getUniqueElements(a) {
    var temp = {};
    for (var i = 0; i < a.length; i++)
        temp[a[i]] = true;
    var r = [];
    for (var k in temp)
        r.push(k);
    return r;
}

function escapeRegExpChars(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

var Enrollment = require('./../enrollment/enroll.model');
var Payment = require('./../payment/payment.model');
var Path = require('./path.model');
var User = require('./../user/user.model');
var Concept = require('./../concept/concept.model');
var Object = require('./../object/object.model');

exports.search = function (req, res) {
    var query = req.query.query;
    query = new RegExp(escapeRegExpChars(query), 'i');
    //TODO: get subscribed paths for user and search in path title, concept title, author title
    var user_search_obj = {};
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    var path_search_obj = {$or: [
        {'title': query},
        {'producer.name': query},
        {'concepts.title': query}
    ], isPrivate: false};
    if (user_search_obj) {
        User.findOne(user_search_obj).exec(function (err, user) {
            if (err || !user) return handleError('error finding user details', res);
            Path.find(path_search_obj).deepPopulate('producer concepts concepts.objects').exec(function (err, paths) {
                if (err) return handleError('Error getting paths', res);
                var resp = {data: [], more: false, categories: getAllCategories()};
                var path_ids = [];
                paths.forEach(function (path) {
                    var item = path.toJSON();
                    item['id'] = path._id;
                    item['allowEnroll'] = false;
                    path_ids.push(path._id);
                    resp.data.push(item);
                });
                Enrollment.find({_id: path_ids, producer: user._id, isEnrolled: true}).select('path').exec(function (err, enrolls) {
                    if (err) return handleError('Error finding subscription details', res);
                    if (enrolls) {
                        _.forEach(enrolls, function (enroll) {
                            var idx = findIndexOfKeyInObjectArr(resp.data, '_id', enroll.path);
                            if (idx > -1) resp.data[idx]['allowEnroll'] = true;
                        })
                    }
                    return res.status(200).json(resp);
                });
            });
        })
    } else return handleError('error finding user details', res);
};

exports.search_v2 = function (req, res) {
    var category = req.query.cat;
    var subCategory = req.query.subCat;
    var path_search_obj = {isPrivate: false};

    if (category) {
        path_search_obj['category'] = category;
    }
    if (subCategory) {
        path_search_obj['subCat'] = subCategory;
    }
    var user_search_obj = {};
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    if (user_search_obj) {
        User.findOne(user_search_obj).exec(function (err, user) {
            if (err || !user) return handleError('error finding user details', res);
            Path.find(path_search_obj).deepPopulate('producer concepts concepts.objects').exec(function (err, paths) {
                if (err) return handleError('Error getting paths', res);
                var resp = {data: [], more: false, categories: getAllCategories(), order: [], tags: []};
                resp['hasSubCat'] = !subCategory;
                var path_ids = [];
                paths.forEach(function (path) {
                    var item = path.toJSON();
                    item['id'] = path._id;
                    item['allowEnroll'] = false;
                    if (item.producer && item.producer._id) item.producer['id'] = item.producer._id;
                    _.forEach(item.concepts, function (concept) {
                        concept['id'] = concept._id;
                        _.forEach(concept.objects, function (obj) {
                            obj['id'] = obj._id;
                        });
                    });
                    if (path.subCat && resp.order.indexOf(path.subCat) == -1) resp.order.push(path.subCat);
                    path_ids.push(path._id);
                    resp.data.push(item);
                });
                Enrollment.find({_id: path_ids, producer: user._id, isEnrolled: true}).select('path').exec(function (err, enrolls) {
                    if (err) return handleError('Error finding subscription details', res);
                    if (enrolls) {
                        _.forEach(enrolls, function (enroll) {
                            var idx = findIndexOfKeyInObjectArr(resp.data, '_id', enroll.path);
                            if (idx > -1) resp.data[idx]['allowEnroll'] = true;
                        })
                    }
                    return res.status(200).json(resp);
                });
            });
        })
    } else return handleError('error finding user details', res);
};

exports.home = function (req, res) {
    var user_search_obj = {};
    if (req.user && req.user._id) {
        user_search_obj['_id'] = req.user._id;
    } else if (req.query.access_token) {
        user_search_obj['access_token'] = req.query.access_token;
    }
    if (user_search_obj) {
        User.findOne(user_search_obj).exec(function (err, user) {
            if (err || !user) return handleError('error finding user details. Please retry logging in.', res);
            kew.all(
                [
                    // getLatestPathsPromise(constants.marketplace.max_horizontal_tiles, 1),
                    getPathsOfSubCategory("Placement Preparation", "Company Wise Preparation", 50, 2, null),
                    getPathsOfCategoryPromise("Business", constants.marketplace.max_horizontal_tiles, 1),
                    getPathsOfCategoryPromise("Computer Science", constants.marketplace.max_horizontal_tiles, 1),
                    getPopularPathsPromise(constants.marketplace.max_horizontal_tiles, 1)
                ]
            )
                .then(function (resultsArray) {
                    var pathArrays = [];
                    resultsArray.forEach(function (result) {
                        if (result.length > 0) {
                            pathArrays = pathArrays.concat(result);
                        }
                    });
                    var order = ['Popular'];
                    var path_ids = [];
                    pathArrays.forEach(function (path) {
                        if (path._id) {
                            path_ids.push(path._id);
                        }
                        if (path.concepts && path.concepts.length > 0) {
                            path.concepts = [path.concepts[0]];
                        }
                        if (!path.category) path['category'] = 'Popular';
                        if (!path.subCat) path['subCat'] = 'Latest';
                        if (!path.homeCat) path['homeCat'] = 'Popular';
                        if (path.subCat && order.indexOf(path.subCat) == -1) order.push(path.subCat);
                    });
                    Enrollment.find({producer: user._id, _id: {$in: path_ids}, isEnrolled: true}).exec(function (err, enrolls) {
                        console.log('inside enroll paths home - ', err);
                        if (err) return handleError('error finding user subscription details');
                        if (enrolls) {
                            _.forEach(pathArrays, function (path) {
                                var idx = findIndexOfKeyInObjectArr(enrolls, 'path', path._id);
                                path['allowEnroll'] = idx == -1;
                            });
                        }
                        var responseObj = {
                            categories: getAllCategories(),
                            data: pathArrays,
                            order: order,
                            tags: [
                                "Popular",
                                "Latest"
                            ],
                            more: false,
                            hasSubCat: true
                        };
                        return res.status(200).json(responseObj);
                    });
                });
        })
    }
    else return handleError('Error finding user details. Please retry logging in.', res);
};

exports.codes = function (req, res) {
    var user_search_obj = {};
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    if (user_search_obj) {
        User.findOne(user_search_obj).exec(function (err, user) {
            if (err || !user) return handleError('error finding user details. retry logging in.', res);
            var path_search_obj = {};
            if (req.query.accessCode) path_search_obj['accessId'] = req.query.accessCode;
            else if (req.query.promoCode && constants.paths.discountCodes.hasOwnProperty(req.query.promoCode))
                path_search_obj['_id'] = constants.paths.discountCodes[req.query.promoCode].id;
            if (path_search_obj) {
                Path.findOne(path_search_obj).exec(function (err, path) {
                    if (err) return handleError('error finding path details.', res);
                    if (!path) return res.status(404).json({errors: ['No path found.']});
                    if (req.query.accessCode) {
                        return res.status(200).json({data: {id: path._id}});
                    } else if (req.query.promoCode) {
                        var promo = constants.paths.discountCodes[req.query.promoCode];
                        console.log(promo, req.query.promoCode);
                        if (promo) return res.status(200).json({data: promo});
                        else return res.status(404).json({errors: ['given promo code not found']});
                    } else return handleError('Please enter access code or promo code to continue.', res);
                })
            } else return handleError('Please enter access code or promo code to continue.', res);
        })
    } else return handleError('error finding user details. retry logging in.', res);
};

exports.updatePath = function (req, res) {
    var reqBody = req.body;
    var responseObj = {};
    var errors = new Array();

    if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");
    if (!validator.isLength(reqBody.desc, 20)) errors.push("Description must be greater than 20 characters");
    if (validator.isNull(req.files.coverPhoto)) errors.push("A Cover photo must be uploaded");
    if (!validator.isNull(req.files.coverPhoto))
        if (!validator.isImage(req.files.coverPhoto.mimetype)) errors.push("Cover photo must be an image");
    if (validator.isBoolean(reqBody.coursePaid)) {
        if (reqBody.coursePaid == "true") {
            /**
             * XOR logic to make sure either courseUSD or courseINR is set, not both
             */
            if (!validator.isNull(reqBody.courseUSD)) {
                if (validator.isDecimal(reqBody.courseUSD)) {
                    reqBody.courseUSD = parseFloat(reqBody.courseUSD);
                    reqBody.courseUSD = Math.round(reqBody.courseUSD * 100) / 100;
                    reqBody.courseINR = reqBody.courseUSD * parseInt(config.currency.USDtoINR);
                    reqBody.selectedCurrency = "USD"
                } else {
                    errors.push("Please enter a valid USD currency for the course")
                }
            } else if (!validator.isNull(reqBody.courseINR)) {
                if (validator.isDecimal(reqBody.courseINR)) {
                    reqBody.courseINR = parseFloat(reqBody.courseINR);
                    reqBody.courseINR = Math.round(reqBody.courseINR * 100) / 100;
                    reqBody.courseUSD = reqBody.courseINR / parseFloat(config.currency.USDtoINR);
                    reqBody.mentorUSD = Math.round(reqBody.courseUSD * 100) / 100;
                    reqBody.selectedCurrency = "INR"
                } else {
                    errors.push("Please enter a valid INR currency for the course")
                }
            }
        } else {
            reqBody['courseUSD'] = undefined;
            reqBody['courseINR'] = undefined;
        }
    } else {
        errors.push("Please indicate if the course is paid (true/false)");
    }
    if (validator.isBoolean(reqBody.mentorPaid)) {
        if (reqBody.mentorPaid == "true") {
            /**
             * XOR logic to make sure either mentorUSD or mentorINR is set, not both
             */
            if (!validator.isNull(reqBody.mentorUSD)) {
                if (validator.isDecimal(reqBody.mentorUSD)) {
                    reqBody.mentorUSD = parseFloat(reqBody.mentorUSD);
                    reqBody.mentorUSD = Math.round(reqBody.mentorUSD * 100) / 100;
                    reqBody.mentorINR = reqBody.mentorUSD * parseInt(config.currency.USDtoINR);
                    reqBody.selectedCurrency = "USD"
                } else {
                    errors.push("Please enter a valid USD currency for the mentor channel")
                }
            } else if (!validator.isNull(reqBody.mentorINR)) {
                if (validator.isDecimal(reqBody.mentorINR)) {
                    reqBody.mentorINR = parseFloat(reqBody.mentorINR);
                    reqBody.mentorINR = Math.round(reqBody.mentorINR * 100) / 100;
                    reqBody.mentorUSD = reqBody.mentorINR / parseFloat(config.currency.USDtoINR);
                    reqBody.mentorUSD = Math.round(reqBody.mentorUSD * 100) / 100;
                    reqBody.selectedCurrency = "INR"
                } else {
                    errors.push("Please enter a valid INR currency for the mentor channel")
                }
            }
        } else {
            reqBody['mentorUSD'] = undefined;
            reqBody['mentorINR'] = undefined;
        }
    } else {
        errors.push("Please indicate if the mentor channel is paid (true/false)");
    }
    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        if (user_search_obj) {
            User.findOne(user_search_obj).exec(function (err, user) {
                if (err) return handleError('Please retry logging in.', res);
                if (user) {
                    Path.findOne({_id: reqBody.pathId, producer: user._id}).exec(function (err, path) {
                        if (err) return handleError('error finding path details', res);
                        if (!path) return handleError('Path with given id not found', res);
                        else {
                            customUtils.upload(req.files.coverPhoto, function (coverPhotoInfo) {
                                if (reqBody.title) path['title'] = reqBody.title;
                                if (reqBody.desc) path['desc'] = reqBody.desc;
                                if (reqBody.isPrivate != undefined) path['isPrivate'] = stringToBoolean(reqBody.isPrivate);
                                if (reqBody.category) path['category'] = reqBody.category;
                                if (reqBody.subCat) path['subCat'] = reqBody.subCat;
                                if (reqBody.coursePaid) path['coursePaid'] = reqBody.coursePaid;
                                if (reqBody.courseINR) path['courseINR'] = reqBody.courseINR;
                                if (reqBody.courseUSD) path['courseUSD'] = reqBody.courseUSD;
                                if (reqBody.mentorPaid) path['mentorPaid'] = reqBody.mentorPaid;
                                if (reqBody.mentorINR) path['mentorINR'] = reqBody.mentorINR;
                                if (reqBody.mentorUSD) path['mentorUSD'] = reqBody.mentorUSD;

                                if (req.files.coverPhoto) {
                                    path["coverPhoto"] = coverPhotoInfo.url;
                                    path["coverPhotoThumb"] = coverPhotoInfo.urlThumb
                                }
                                path.save(function (err) {
                                    if (err) return handleError('error saving course details', res);
                                    responseObj['data'] = path.toJSON();
                                    responseObj.data['id'] = path._id;
                                    if (req.files.coverPhoto) {
                                        notify.getRecieversAndUpdatePhotos('path', path._id, coverPhotoInfo.urlThumb, user.gcmId)
                                    }
                                    if (reqBody.title) {
                                        var date = new Date();
                                        var chatObj = {
                                            "type": "newCourseName",
                                            "pathId": path._id,
                                            "created": date.getTime(),
                                            "id": date.getTime(),
                                            "pathTitle": reqBody['title']
                                        };
//                                        notify.emit("wordForChat", chatObj);
                                    }
                                    var notifObj = {
                                        "where": path._id
                                    };
//                                    notify.emit('pathEdited', notifObj);
                                    return res.status(201).json(responseObj);
                                })
                            })
                        }
                    });
                } else return handleError('Please retry logging in.', res);
            });
        } else return handleError('Error finding user details. Please try logging in again.', res);
    }
};

exports.createPath = function (req, res) {
    var reqBody = req.body;
    var responseObj = {};
    var errors = [];

    if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");
    if (!validator.isLength(reqBody.desc, 20)) errors.push("Description must be greater than 20 characters");
    if (!validator.isIn(reqBody.category, config.categories)) errors.push("Select a valid category for this Learning Path");
    if (!validator.isYoutubeURL(reqBody.introVid)) errors.push("Introduction video must be a valid Youtube URL");
    if (validator.isNull(req.files.coverPhoto)) errors.push("A Cover photo must be uploaded");
    if (validator.isNull(reqBody.category)) errors.push("A Course Category is required.");
    if (validator.isNull(reqBody.subCat)) errors.push("A Course Sub Category is required.");
    if (!validator.isNull(req.files.coverPhoto))
        if (!validator.isImage(req.files.coverPhoto.mimetype)) errors.push("Cover photo must be an image");
    if (validator.isBoolean(reqBody.coursePaid)) {
        if (reqBody.coursePaid == "true") {
            /**
             * XOR logic to make sure either courseUSD or courseINR is set, not both
             */
            if (!validator.isNull(reqBody.courseUSD)) {
                if (validator.isDecimal(reqBody.courseUSD)) {
                    reqBody.courseUSD = parseFloat(reqBody.courseUSD);
                    reqBody.courseUSD = Math.round(reqBody.courseUSD * 100) / 100;
                    reqBody.courseINR = reqBody.courseUSD * parseInt(config.currency.USDtoINR);
                    reqBody.selectedCurrency = "USD"
                } else {
                    errors.push("Please enter a valid USD currency for the course")
                }
            } else if (!validator.isNull(reqBody.courseINR)) {
                if (validator.isDecimal(reqBody.courseINR)) {
                    reqBody.courseINR = parseFloat(reqBody.courseINR);
                    reqBody.courseINR = Math.round(reqBody.courseINR * 100) / 100;
                    reqBody.courseUSD = reqBody.courseINR / parseFloat(config.currency.USDtoINR);
                    reqBody.mentorUSD = Math.round(reqBody.courseUSD * 100) / 100;
                    reqBody.selectedCurrency = "INR"
                } else {
                    errors.push("Please enter a valid INR currency for the course")
                }
            }
        } else {
            reqBody['courseUSD'] = undefined;
            reqBody['courseINR'] = undefined;
        }
    } else {
        errors.push("Please indicate if the course is paid (true/false)");
    }
    if (validator.isBoolean(reqBody.mentorPaid)) {
        if (reqBody.mentorPaid == "true") {
            /**
             * XOR logic to make sure either mentorUSD or mentorINR is set, not both
             */
            if (!validator.isNull(reqBody.mentorUSD)) {
                if (validator.isDecimal(reqBody.mentorUSD)) {
                    reqBody.mentorUSD = parseFloat(reqBody.mentorUSD);
                    reqBody.mentorUSD = Math.round(reqBody.mentorUSD * 100) / 100;
                    reqBody.mentorINR = reqBody.mentorUSD * parseInt(config.currency.USDtoINR);
                    reqBody.selectedCurrency = "USD"
                } else {
                    errors.push("Please enter a valid USD currency for the mentor channel")
                }
            } else if (!validator.isNull(reqBody.mentorINR)) {
                if (validator.isDecimal(reqBody.mentorINR)) {
                    reqBody.mentorINR = parseFloat(reqBody.mentorINR);
                    reqBody.mentorINR = Math.round(reqBody.mentorINR * 100) / 100;
                    reqBody.mentorUSD = reqBody.mentorINR / parseFloat(config.currency.USDtoINR);
                    reqBody.mentorUSD = Math.round(reqBody.mentorUSD * 100) / 100;
                    reqBody.selectedCurrency = "INR"
                } else {
                    errors.push("Please enter a valid INR currency for the mentor channel")
                }
            }
        } else {
            reqBody['mentorUSD'] = undefined;
            reqBody['mentorINR'] = undefined;
        }
    } else {
        errors.push("Please indicate if the mentor channel is paid (true/false)");
    }
    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        if (user_search_obj) {
            User.findOne(user_search_obj).exec(function (err, user) {
                if (err) return handleError('Please retry logging in.', res);
                if (user) customUtils.upload(req.files.coverPhoto, function (coverPhotoInfo) {
                    customUtils.getLinkInfo(req.body.introVid, function (introVidInfo) {
                        var payload = {
                            "created": new Date(),
                            "accessId": customUtils.generateToken(3),
                            "title": reqBody.title,
                            "desc": reqBody.desc,
                            //"buying": reqBody.buying,
                            //"price": reqBody.price,
                            "coverPhoto": coverPhotoInfo.url,
                            "coverPhotoThumb": coverPhotoInfo.urlThumb,
                            "producer": user._id,
                            "studentCount": 0,
                            "newStudents": 0,
                            "isPrivate": false,
                            "mentorPaid": stringToBoolean(reqBody.mentorPaid),
                            "coursePaid": stringToBoolean(reqBody.coursePaid),
                            "category": reqBody.category,
                            "subCat": reqBody.subCat,
                            "concepts": [],
                            "mentorUSD": reqBody.mentorUSD,
                            "mentorINR": reqBody.mentorINR,
                            "courseINR": reqBody.courseINR,
                            "courseUSD": reqBody.courseUSD,
                            "selectedCurrency": reqBody.selectedCurrency,
                            "mentorAvailable": true
                        };
                        Path.create(payload, function (err, path) {
                            if (err) return handleError('Error creating path', res);
                            if (path) {
                                Concept.create({
                                    title: "Introduction",
                                    pos: 0,
                                    objects: [],
                                    producer: user._id,
                                    path: path._id
                                }, function (err, concept) {
                                    if (err) return handleError('Error creating path', res);
                                    if (concept) {
                                        Object.create({
                                            type: 'video', pos: 0, url: reqBody.introVid, title: introVidInfo.title,
                                            concept: concept._id, path: path._id,
                                            desc: reqBody.introVidDesc, "image": introVidInfo.image, producer: user._id
                                        }, function (err, object) {
                                            if (err) return handleError('Error creating path', res);
                                            if (object) {
                                                concept['objects'] = [object._id];
                                                concept.save(function (err) {
                                                    if (err) return handleError('Error creating path', res);
                                                    path['concepts'] = [concept._id];
                                                    path.save(function (err) {
                                                        if (err) return handleError('Error creating path', res);
                                                        var resp = {data: path.toJSON()};
                                                        resp.data['id'] = path._id;
                                                        res.status(201).json(resp);
                                                        var notifObj = {
                                                            "pathId": path._id,
                                                            "pathTitle": path.title,
                                                            "producerId": user._id,
                                                            "producerName": user.name,
                                                            "producerPhoto": user.avatarThumb
                                                        };
//                                                        notify.emit('newPath', notifObj);

                                                        var date = new Date();
                                                        var chatObjMentor = {
                                                            "created": date.getTime(),
                                                            "id": date.getTime(),
                                                            "pathId": path._id,
                                                            "qbId": user.qbId,
                                                            "type": "newMentor"
                                                        };

//                                                        notify.emit('wordForChat', chatObjMentor);

                                                        var chatObj = {
                                                            "created": date.getTime(),
                                                            "type": "newChannel",
                                                            "pathId": path._id,
                                                            "pathTitle": reqBody.title
                                                        };


                                                        qbchat.createRoom(2, 'General', function (err, newRoom) {
                                                            if (err) console.log(err);
                                                            else {
                                                                qbchat.addUserToRoom(newRoom._id, [user.qbId], function (err, result) {
                                                                    if (err) console.log("Error adding user to room - ", err);
                                                                    path['qbId'] = newRoom._id;
                                                                    chatObj["id"] = date.getTime() + "@1";
                                                                    chatObj["channelName"] = "General Channel";
                                                                    chatObj["channelId"] = newRoom._id;
//                                                                    notify.emit('wordForChat', chatObj);
                                                                    path.save();
                                                                });
                                                            }
                                                        });

                                                        qbchat.createRoom(2, "Mentor", function (err, newRoom) {
                                                            if (err) console.log(err);
                                                            else {
                                                                qbchat.addUserToRoom(newRoom._id, [user.qbId], function (err, result) {
                                                                    if (err) console.log(err);
                                                                    path['mentorRoomId'] = newRoom._id;
                                                                    chatObj["id"] = date.getTime() + "@2";
                                                                    chatObj["channelName"] = "Mentor Channel";
                                                                    chatObj["channelId"] = newRoom._id;
//                                                                    notify.emit('wordForChat', chatObj);
                                                                    path.save();
                                                                });
                                                            }
                                                        });

                                                        qbchat.createRoom(2, 'Introduction', function (err, newRoom) {
                                                            if (err) console.log(err);
                                                            else {
                                                                qbchat.addUserToRoom(newRoom._id, [user.qbId], function (err, result) {
                                                                    if (err) console.log(err);
                                                                    concept['qbId'] = newRoom._id;
                                                                    chatObj["id"] = date.getTime() + "@3";
                                                                    chatObj["channelName"] = "Introduction";
                                                                    chatObj["channelId"] = newRoom._id;
//                                                                    notify.emit('wordForChat', chatObj);
                                                                    concept.save();
                                                                });
                                                            }
                                                        });
                                                    });
                                                })
                                            } else return handleError('Error creating object for path.', res);
                                        })
                                    } else return handleError('Error creating path', res);
                                });
                            } else return handleError('Error creating path', res);
                        });
                    });
                });
                else return handleError('Please retry logging in.', res);
            });
        } else return handleError('Error finding user details. Please try logging in again.', res);
    }
};

exports.deletePath = function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = new Array();
    var channels = [];

    if (validator.isNull(reqBody.id)) errors.push("PathId has to be specified");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        if (user_search_obj) {
            User.findOne(user_search_obj).exec(function (err, user) {
                if (err || !user) return handleError('Error finding path with given id or you do not have sufficient access to delete this path', res);
                Path.findOne({_id: reqBody.id, producer: user._id}).deepPopulate('concepts').exec(function (err, path) {
                    if (err || !path) return handleError('Error finding path with given id or you do not have sufficient access to delete this path', res);
                    var channels = [];
                    if (path.qbId) channels.push(path.qbId);
                    var conceptIds = [];
                    _.forEach(path.concepts, function (concept) {
                        if (concept.qbId) channels.push(concept.qbId);
                        conceptIds.push(concept._id);
                    });
                    Concept.find({_id: {$in: conceptIds}}).exec(function(err, concepts){
                        path.remove(function (err) {
                            if (err) return handleError('Error removing path', res);
                            res.status(200).json({data: {success: 'Path deleted'}});
                            if(concepts && concepts.length > 0) {
                                async.eachSeries(concepts, function(concept, concept_cb){
                                    concept.remove(function(err){
                                        concept_cb();
                                    })
                                })
                            }
                            channels.forEach(function (channel) {
                                qbchat.deleteRoom(channel, function (err, res) {
                                    if (err) console.log(err);
                                });
                            });
                        });
                    });
                })
            })
        } else return handleError("Error finding user details. Please try logging in again.", res);
    }
};

exports.getPathSuggestions = function (req, res) {
    var query = req.query.query;
    var errors = [];
    if (validator.isNull(query)) errors.push("No search query was specified");
    if (errors.length > 0) {
        return handleError(errors, res);
    } else {
        Path.find({
            $or: [
                {title: new RegExp(escapeRegExpChars(query), 'i')},
                {'concepts.title': new RegExp(escapeRegExpChars(query), 'i')},
                {'producer.name': new RegExp(escapeRegExpChars(query), 'i')}
            ], isPrivate: false
        }).select('producer concepts title').deepPopulate('producer concepts').exec(function (err, paths) {
            if (err) return handleError('Error searching for paths.', res);
            var resp = {};
            var autoSuggest = [];
            var suggestArrays = {
                producers: [],
                concepts: [],
                courses: []
            };

            function pushIfUnique(item, type) {
                switch (type) {
                    case "producer":
                        if (suggestArrays.producers.indexOf(item) == -1) suggestArrays.producers.push(item);
                        break;
                    case "concept":
                        if (suggestArrays.concepts.indexOf(item) == -1) suggestArrays.concepts.push(item);
                        break;
                    case "course":
                        if (suggestArrays.courses.indexOf(item) == -1) suggestArrays.courses.push(item);
                        break
                }
            }

            paths.forEach(function (p) {
                var path = p.toJSON();
                pushIfUnique(path.producer.name, "producer");
                pushIfUnique(path.title, "course");
                path.concepts.forEach(function (concept) {
                    pushIfUnique(concept.title, "concept")
                })
            });
            autoSuggest = suggestArrays.courses.concat(suggestArrays.producers, suggestArrays.concepts);
            resp = {"total_count": autoSuggest.length, "data": autoSuggest};
            return res.status(200).json(resp);
        });
    }
};

exports.getSubscribedUsers = function (req, res) {
    /**
     * HERE YE' HERE YE'
     * wrath of the sins of android be here
     * so android app makes infinite api calls in the peoplePage activity
     * if the number of users > 100 (the pagination is buggy)
     * that makes crazy amount of database calls that exhausts the database API limit.
     * cache the shit :)
     */
    var responseObj = {more: false};
    Path.findOne({_id: req.query.pathId}).deepPopulate('producer').exec(function (err, path) {
        if (err) return handleError('Error finding path with given id', res);
        Enrollment.find({path: path._id, isEnrolled: true}).select('producer').deepPopulate('producer')
            .exec(function (err, enrolls) {
                if (err) return handleError('error finding related paths.', res);
                responseObj['data'] = [];
                if (path.producer && path.producer._id) {
                    var item = path.toJSON();
                    item.producer['isMentor'] = true;
                    item.producer['id'] = path.producer._id;
                    responseObj.data.push(item.producer);
                }
                enrolls.forEach(function (enroll) {
                    if (enroll.producer && enroll.producer._id) {
                        var item = enroll.toJSON();
                        item.producer['id'] = item.producer._id;
                        responseObj.data.push(item.producer);
                    }
                });
                return res.status(200).json(responseObj);
            });
    });
};

exports.getExploreCourseData = function (req, res) {
    var user_search_obj = {};
    if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    else if (req.user._id) user_search_obj['_id'] = req.user._id;
    if (user_search_obj) {
        User.findOne(user_search_obj).exec(function (err, user) {
            if (err) return handleError('error finding user details', res);
            if (req.query.pathId) {
                Path.findOne({_id: req.query.pathId}).deepPopulate('producer concepts concepts.objects').exec(function (err, path) {
                    if (err) return handleError('Error finding path details', res);
                    var responseObj = {data: path.toJSON()};
                    console.log("concepts - ", responseObj.data.concepts);
                    if (responseObj.data.concepts && responseObj.data.concepts.length > 0) {
                        var concepts = responseObj.data.concepts;
                        _.forEach(concepts, function(concept){
                            var objects = concept.objects;
                            if (objects.length > 0) {
                                var object = _.clone(objects[0]);
                                object['id'] = object._id;
                                delete concept.objects;
                                concept['objects'] = [object];
                            }
                            concept['id'] = concept._id;
                        });

                        responseObj.data['concepts'] = concepts;
                    }
                    if (path.producer && path.producer._id) responseObj.data.producer['id'] = path.producer._id;
                    responseObj.data['id'] = path._id;
                    responseObj['data']['isOwner'] = path.producer._id.toString() == user._id.toString();
                    Enrollment.findOne({path: path._id, producer: user._id, isEnrolled: true}).exec(function (err, enroll) {
                        if (err) return handleError("Error finding user enrollment details", res);
                        responseObj['data']['allowEnroll'] = !enroll;
                        return res.status(200).json(responseObj);
                    });
                });
            } else {
                Path.find({isPrivate: false}).sort('-studentCount').deepPopulate('concepts producer').exec(function (err, paths) {
                    if (err) return handleError('error finding paths', res);
                    Enrollment.find({path: {$in: paths}, producer: user._id, isEnrolled: true}).exec(function (err, enrolls) {
                        if (err) return handleError('Error finding enrollment details', res);
                        var responseObj = {data: []};
                        responseObj['autoSuggest'] = [];
                        paths.forEach(function (p) {
                            var path = p.toJSON();
                            if (path._id) {
                                path['id'] = path._id;
                                var idx = findIndexOfKeyInObjectArr(enrolls, 'path', path._id);
                                path['allowEnroll'] = idx > -1;
                            }
                            path['isOwner'] = path.producer._id.toString() == user._id.toString();
                            if (path.title) responseObj.autoSuggest.push(path.title);
                            if (path.producer.name) responseObj.autoSuggest.push(path.producer.name);
                            path.concepts.forEach(function (concept) {
                                if (concept.title) responseObj.autoSuggest.push(concept.title);
                                if (concept.title && concept.title.toLowerCase() != 'introduction') {
                                    delete concept.objects;
                                }
                            });
                            responseObj.data.push(path);
                        });
                        responseObj['categories'] = getAllCategories();
                        return res.status(200).json(responseObj);
                    });
                })
            }
        })
    }
    else return handleError('error finding user details, please try logging in again', res);
};

exports.userProfile = function (req, res) {
    if (req.query.pathId) {
        var respObj = {data: {}};
        Path.findOne({_id: req.query.pathId}).deepPopulate('producer concepts concepts.objects').exec(function (err, path) {
            if (err) return handleError('Error finding path details', res);
            var responseObj = {data: path.toJSON()};
            var concepts = path.concepts;
            if (concepts.length > 0) {
                delete responseObj.data.concepts;
                var concept = concepts[0].toJSON();
                var objects = concept.objects;
                if (objects.length > 0) {
                    var object = objects[0];
                    object['id'] = object._id;
                    delete concept.objects;
                    concept['objects'] = [object];
                }
                concept['id'] = concept._id;
                responseObj.data['concepts'] = [concept];
            }
            responseObj.data['id'] = path._id;
            respObj['data']['allowEnroll'] = true;
            respObj['data']['isOwner'] = false;
            return res.status(200).json(respObj);
        })
    } else {
        Path.find({isPrivate: false}).deepPopulate('producer concepts concepts.objects').sort('-studentCount').exec(function (err, paths) {
            if (err) return handleError('error finding paths', res);
            var resp = {data: []};
            paths.forEach(function (p) {
                var path = p.toJSON();
                path['id'] = p._id;
                path['allowEnroll'] = true;
                path['isOwner'] = false;
                var concepts = path.concepts;
                if (concepts.length > 0) {
                    var concept = concepts[0].toJSON();
                    var objects = concept.objects;
                    if (objects.length > 0) {
                        var object = objects[0];
                        object['id'] = object._id;
                        delete concept.objects;
                        concept['objects'] = [object];
                    }
                    delete path.concepts;
                    concept['id'] = concept._id;
                    path['concepts'] = [concept];
                }
                resp.data.push(path);
            });
            return res.status(200).json(resp);
        })
    }
};

var getPathsOfSubCategory = function (cat, subCat, page, limit, requesterUserId) {
    var response = kew.defer();
    Path.find({category: cat, subCat: subCat, isPrivate: false}).deepPopulate('concepts concepts.objects producer').exec(function (err, paths) {
        if (err) response.reject({errors: ['error getting home paths.'], errorObj: err});
        var resp = [];
        if (paths) {
            paths.forEach(function (path) {
                var item = path.toJSON();
                item['id'] = path._id;
                resp.push(item)
            });
        }
        response.resolve(resp);
    });
    return response;
};

var getPopularPathsPromise = function (page, limit) {
    var response = kew.defer();
    Path.find({isPrivate: false}).deepPopulate('concepts concepts.objects producer').sort('-studentCount').exec(function (err, paths) {
        if (err) response.reject({errors: ['error getting home paths.'], errorObj: err});
        var resp = [];
        paths.forEach(function (path) {
            var item = path.toJSON();
            item['id'] = path._id;
            resp.push(item)
        });
        response.resolve(resp);
    });
    return response;
};

var getPathsOfCategoryPromise = function (cat, page, limit) {
    var response = kew.defer();
    Path.find({category: cat, isPrivate: false}).deepPopulate('concepts concepts.objects producer').exec(function (err, paths) {
        if (err) response.reject({errors: ['error getting home paths.'], errorObj: err});
        var resp = [];
        paths.forEach(function (path) {
            var item = path.toJSON();
            item['id'] = path._id;
            resp.push(item)
        });
        response.resolve(resp);
    });
    return response;
};

var getAllCategories = function () {
    var cat = constants.paths.categories_v2;
    var resp = [];
    _.forEach(cat, function (c, v) {
        resp.push({cat: v});
    });
    return resp;
};


function handleError(message, res) {
    res.status(422);
    res.json({"errors": [message]});
}

function findIndexOfKeyInObjectArr(obj_arr, key, val) {
    for (var item = 0; item < obj_arr.length; item++) {
        if (obj_arr[item][key] && obj_arr[item][key].toString() == val.toString()) return item;
    }
    return -1;
}

var Legacy = require('./../auth/legacy.model');
var async = require('async');
var bcrypt = require('bcryptjs');

exports.migrateOldData = function (req, res) {
    async.series([
//        function(user_cb){
//            debugger;
//            Legacy.find({"path.collection": 'users'}).exec(function(err, legacy_users){
//                console.log('got - ', legacy_users.length, ' legacy records');
//                debugger;
//                if(err) {
//                    console.log('error finding legacy users', err.toString());
//                    user_cb();
//                }
//                async.eachSeries(legacy_users, function(path, path_user_cb){
//                    debugger;
//                    console.log('starting to process user - ', path.value.email);
//                    var user_data = {};
//                    var producer_data = path.value;
//                    if(producer_data.name) user_data['name'] = producer_data.name;
//                    if(producer_data.username) user_data['username'] = producer_data.username;
//                    if(producer_data.email) user_data['email'] = producer_data.email;
//                    if(producer_data.avatar) user_data['avatar'] = producer_data.avatar;
//                    if(producer_data.avatarThumb) user_data['avatarThumb'] = producer_data.avatarThumb;
//                    if(producer_data.isVerified && producer_data.isVerified.toString() != 'true') user_data['verification_token'] = producer_data.isVerified;
//                    if(producer_data.qbId) user_data['qbId'] = producer_data.qbId;
//                    if(producer_data.password && producer_data.password == 'dummyPassword') user_data['hashedPassword'] = bcrypt.hashSync(producer_data.password, 8);
//                    else if(producer_data.password && producer_data.password != 'dummyPassword') user_data['hashedPassword'] = producer_data.password;
//                    if(producer_data.google) user_data['googleId'] = producer_data.google;
//                    if(producer_data.facebook) user_data['facebookId'] = producer_data.facebook;
//                    if(producer_data.gcmId) user_data['gcmId'] = producer_data.gcmId;
//
//                    User.findOne({email: user_data.email}).exec(function(err, user){
//                        debugger;
//                        if(err) {
//                            console.log('error finding user in path = ', err.toString());
//                            path_user_cb();
//                        }
//                        if(!user) {
//                            console.log('saving user - ', path.value.email);
//                            User.create(user_data, function(err, user){
//                                debugger;
//                                if(err) {
//                                    console.log('err creating user with - ', user_data);
//                                    path_user_cb();
//                                }
//                                else {
//                                    if(user) console.log('created user with username - ', user.username, ' , email - ', user.email);
//                                    path_user_cb();
//                                }
//                            });
//                        } else path_user_cb();
//                    });
//                }, function(err){
//                    console.log('finished processing users from records');
//                    user_cb();
//                });
//            });
//    },
//        function (path_cb) {
//            debugger;
//            Legacy.find({"path.collection": 'paths'}).exec(function (err, legacy_paths) {
//                debugger;
//                if (err) return handleError(err.toString(), res);
//                async.eachSeries(legacy_paths, function (path, callback) {
//                    debugger;
//                    var path_data = {};
//                    var path_val = path.toJSON();
//                    if (path_val.value.accessId) path_data['accessId'] = path_val.value.accessId;
//                    if (path_val.value.title) path_data['title'] = path_val.value.title;
//                    if (path_val.value.desc) path_data['desc'] = path_val.value.desc;
//                    if (path_val.value.coverPhoto) path_data['coverPhoto'] = path_val.value.coverPhoto;
//                    if (path_val.value.coverPhotoThumb) path_data['coverPhotoThumb'] = path_val.value.coverPhotoThumb;
//                    if (path_val.value.isPrivate) path_data['isPrivate'] = path_val.value.isPrivate;
//                    if (path_val.value.mentorPaid) path_data['mentorPaid'] = path_val.value.mentorPaid;
//                    if (path_val.value.coursePaid) path_data['coursePaid'] = path_val.value.coursePaid;
//                    if (path_val.value.cat) path_data['category'] = path_val.value.cat;
//                    if (path_val.value.subCat) path_data['subCat'] = path_val.value.subCat;
//                    if (path_val.value.mentorUSD) path_data['mentorUSD'] = path_val.value.mentorUSD;
//                    if (path_val.value.mentorINR) path_data['mentorINR'] = path_val.value.mentorINR;
//                    if (path_val.value.courseINR) path_data['courseINR'] = path_val.value.courseINR;
//                    if (path_val.value.courseUSD) path_data['courseUSD'] = path_val.value.courseUSD;
//                    if (path_val.value.selectedCurrency) path_data['selectedCurrency'] = path_val.value.selectedCurrency;
//                    if (path_val.value.mentorAvailable) path_data['mentorAvailable'] = path_val.value.mentorAvailable;
//                    if (path_val.value.mentorRoomId) path_data['mentorRoomId'] = path_val.value.mentorRoomId;
//                    if (path_val.value.qbId) path_data['qbId'] = path_val.value.qbId;
//                    if (path_val.value.studentCount) path_data['studentCount'] = path_val.value.studentCount;
//                    if (path_val.value.newStudents) path_data['newStudents'] = path_val.value.newStudents;
//                    if(path_val.value.producer && path_val.value.producer.email) {
//                        User.findOne({email: path_val.value.producer.email}).exec(function (err, user) {
//                            debugger;
//                            if (err) {
//                                console.log('err with - ', path_data);
//                                callback();
//                            }
//                            if (!user) {
//                                console.log('producer user not found for - ', path_data);
//                            } else {
//                                path_data['producer'] = user._id;
//                            }
//                            Path.create(path_data, function (err, saved_path) {
//                                debugger;
//                                if (err) {
//                                    console.log('err with - ', path_data);
//                                    callback();
//                                }
//                                if (path_val.value.concepts && path_val.value.concepts.length > 0) {
//                                    debugger;
//                                    var concepts = path_val.value.concepts;
//                                    async.map(concepts, function (concept, concept_cb) {
//                                        debugger;
//                                        var concept_data = {};
//                                        if (concept.pos) concept_data['pos'] = concept.pos;
//                                        if (concept.qbId) concept_data['qbId'] = concept.qbId;
//                                        if (concept.title) concept_data['title'] = concept.title;
//                                        concept_data['path'] = saved_path._id;
//                                        concept_data['producer'] = user ? user._id : undefined;
//                                        Concept.create(concept_data, function (err, saved_concept) {
//                                            debugger;
//                                            if (err) {
//                                                console.log('error saving concept details - ', concept_data);
//                                                concept_cb(err, null);
//                                            }
//                                            var objects = concept.objects;
//                                            if(objects && objects.length > 0) {
//                                                async.map(objects, function (obj, obj_cb) {
//                                                    debugger;
//                                                    var obj_data = {};
//                                                    if (obj.image) obj_data['image'] = obj.image;
//                                                    if (obj.pos) obj_data['pos'] = obj.pos;
//                                                    if (obj.title) obj_data['title'] = obj.title;
//                                                    if (obj.type) obj_data['type'] = obj.type;
//                                                    if (obj.url) obj_data['url'] = obj.url;
//                                                    if (obj.desc) obj_data['desc'] = obj.desc;
//                                                    if (obj.size) obj_data['size'] = obj.size;
//                                                    if (obj.mimetype) obj_data['mimetype'] = obj.mimetype;
//                                                    if (obj.urlThumb) obj_data['urlThumb'] = obj.urlThumb;
//                                                    obj_data['concept'] = saved_concept._id;
//                                                    obj_data['path'] = saved_path._id;
//                                                    obj_data['producer'] = user ? user._id : undefined;
//                                                    Object.create(obj_data, function (err, saved_obj) {
//                                                        if (err) {
//                                                            console.log('error creating object for - ', obj_data);
//                                                            obj_cb(err, null);
//                                                        }
//                                                        obj_cb(null, saved_obj._id);
//                                                    });
//                                                }, function (err, results) {
//                                                    debugger;
//                                                    if (results) {
//                                                        saved_concept['objects'] = results;
//                                                        saved_concept.save(function (err) {
//                                                            if (err) {
//                                                                console.log('error saving objects for concept - ', saved_concept._id);
//                                                                concept_cb(err, null);
//                                                            }
//                                                            concept_cb(null, saved_concept._id);
//                                                        })
//                                                    }
//                                                })
//                                            } else concept_cb(null, saved_concept._id);
//                                        });
//                                    }, function (err, results) {
//                                        debugger;
//                                        if (results) {
//                                            saved_path['concepts'] = results;
//                                            saved_path.save(function (err) {
//                                                if (err) {
//                                                    console.log('error saving concepts for path - ', saved_path._id);
//                                                    callback();
//                                                }
//                                                callback();
//                                            });
//                                        } else callback();
//                                    })
//                                } else {
//                                    debugger;
//                                    console.log('no concepts found for path title - ', saved_path.title, ', in category - ', saved_path.subCat);
//                                    callback();
//                                }
//                            });
//                        });
//                    } else callback();
//                }, function (err) {
//                    console.log('finished processing paths.');
//                    path_cb();
//                });
//            })
//        },
        function (rel_cb) {
            console.log('to process relationships now - ');
            Legacy.find({"path.kind": "relationship", "path.relation": "isConsumed"}).exec(function(err, items){
                if(err) rel_cb(err);
                console.log("items.length - ", items.length);
                async.eachSeries(items, function(dbitem, item_cb){
                    var item = dbitem.toJSON();
                    console.log('processing item - ', item, item['path']['source'], item['path']['destination']);
                    if(!validator.isNull(item['path']['source']['key'])) {
                        Legacy.findOne({"path.key": item['path']['source']['key']}).exec(function(err, legacy_path){
                            if(err) item_cb(err);
                            if(!validator.isNull(legacy_path.value.accessId) && !validator.isNull(item['path']['destination']['key'])) {
                                Legacy.findOne({"path.key": item['path']['destination']['key']}).exec(function(err, legacy_user){
                                    if(err) item_cb(err);
                                    if(!validator.isNull(legacy_user.value.email)) {
                                        User.findOne({email: new RegExp(escapeRegExpChars(legacy_user.value.email), 'i')}).exec(function(err, user){
                                            if(err || !user) item_cb(err);
                                            Path.findOne({accessId: new RegExp(escapeRegExpChars(legacy_path.value.accessId), 'i')}).exec(function(err, path){
                                                if(err || !path) item_cb(err);
                                                Enrollment.findOne({path: path._id, producer: user._id, isEnrolled:true}).exec(function(err, enroll){
                                                    if(!enroll) {
                                                        var expire_date = new Date();
                                                        expire_date.setFullYear(expire_date.getFullYear() + 1);
                                                        var enroll_data = {
                                                            path: path._id,
                                                            producer: user._id,
                                                            isMentorPaid: legacy_path.value.mentorPaid,
                                                            isCoursePaid: legacy_path.value.coursePaid,
                                                            expiresOn: expire_date,
                                                            isEnrolled: true
                                                        };
                                                        Enrollment.create(enroll_data, function(err, enroll){
                                                            if(err) item_cb(err);
                                                            else {
                                                                console.log('finished creating subscription for user - ', user.email, path.accessId);
                                                                item_cb();
                                                            }
                                                        });
                                                    } else if(err) {
                                                        item_cb(err);
                                                    } else if(enroll){
                                                        if(!path.studentCount)
                                                            path['studentCount'] = 0;
                                                        if(!path.newStudents)
                                                            path['newStudents'] = 0;
                                                        path['studentCount']++;
                                                        path['newStudents']++;
                                                        path.save(function(err){
                                                            console.log('update student count - ', path.studentCount, path.newStudents, path.title, path._id);
                                                            item_cb();
                                                        });
                                                    } else {
                                                        item_cb();
                                                    }
                                                })
                                            })
                                        })
                                    } else item_cb(legacy_user);
                                })
                            } else item_cb(legacy_path);
                        })
                    } else item_cb(item);
                }, function(err){
                    console.log('finished isProduced - ', err);
                    rel_cb();
                })
            });
        }
    ], function (err, results) {

        console.log('finished processing all records');
    });
};