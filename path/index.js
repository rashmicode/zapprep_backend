'use strict';

var express = require('express');
var controller = require('./path.controller');
var config = require('../config');
var passport = require('passport');
var qbchat = require('../qbchat');
var multer = require('multer');
var router = express.Router();


router.get('/search', passport.authenticate('bearer', {session: false}), controller.search);
router.get('/home', passport.authenticate('bearer', {session: false}), controller.home);
router.get('/v2', passport.authenticate('bearer', {session: false}), controller.search_v2);
router.get('/codes', passport.authenticate('bearer', {session: false}), controller.codes);
router.get('/home', passport.authenticate('bearer', {session: false}), controller.home);
router.get('/suggest', controller.getPathSuggestions);
router.get('/users', controller.getSubscribedUsers);
router.post('/', passport.authenticate('bearer', {session: false}), multer(), qbchat.reCreateSession, controller.createPath);
router.patch('/', passport.authenticate('bearer', {session: false}), multer(), controller.updatePath);
router.delete('/', passport.authenticate('bearer', {session: false}), controller.deletePath);
// router.get('/', function (req, res, next) {
//   if (req.query.access_token) next();
//   else next('route');
// }, passport.authenticate('bearer', {session: false}), controller.search2);
// router.get('/', controller.search3);
router.get('/', function (req, res, next) {
  if (req.query.access_token) next();
  else next('route');
}, passport.authenticate('bearer', {session: false}), controller.getExploreCourseData);
router.get('/', controller.userProfile);
router.get('/migrate/v1', controller.migrateOldData);

module.exports = router;
