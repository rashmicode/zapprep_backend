'use strict';

var express = require('express');
var controller = require('./review.controller');
var config = require('../config');
var passport = require('passport');
var qbchat = require('../qbchat');
var multer = require('multer');
var router = express.Router();

var qbchat_session = function(req, res, next) {
  debugger;
  qbchat.getSession(function (err, session) {
    if (err) {
      debugger;
      console.log("Recreating session");
      qbchat.createSession(function (err, result) {
        debugger;
        if (err) {
          res.status(503);
          res.json({"errors": ["Can't connect to the chat server, try again later"]});
        } else next();
      })
    } else next();
  })
};

router.get('/', function (req, res, next) {
  if (req.query.access_token) next();
  else next('route');
}, passport.authenticate('bearer', {session: false}), controller.getReviews);
router.get('/', controller.getMarketPlaceReviews);
router.post('/', passport.authenticate('bearer', {session: false}), controller.createReview);
router.patch('/', passport.authenticate('bearer', {session: false}), controller.updateReview);

module.exports = router;
