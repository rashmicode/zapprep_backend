var Notifications = require('../notifications');
var notify = new Notifications();

var _ = require('lodash');

var validator = require('validator');

var config = require('../config.js'),
    customUtils = require('../utils.js');

var multer = require('multer'),
    fs = require('fs');

var Object = require('./../object/object.model');
var Concept = require('./../concept/concept.model');
var Path = require('./../path/path.model');
var Review = require('./review.model');
var User = require('./../user/user.model');

exports.getReviews = function (req, res) {
    var responseObj = {};
    var errors = [];
    if (validator.isNull(req.query.pathId)) errors.push("PathId has to be specified");
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    } else {
        var user_search_obj = {};
        if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        if(user_search_obj) {
            User.findOne(user_search_obj).exec(function (err, user) {
                if (err || !user) return handleError('Error finding user details for user.', res);
                Path.findOne({_id: req.query.pathId}).exec(function (err, path) {
                    if (err || !path) return handleError('Error finding path details for user.', res);
                    console.log('found producer - ', path.producer, ', logged user id - ', user._id);
                    Review.find({path: path._id}).deepPopulate('user').sort('-timestamp').exec(function (err, reviews) {
                        if (err || !reviews) return handleError('Error finding reviews for given path.', res);
                        var resp = {data: [], more: false};
                        resp['allowPublish'] = path.producer.toString() != user._id.toString();
                        reviews.forEach(function(review){
                            var item = review.toJSON();
                            item['id'] = review._id;
                            item['pathId'] = path._id;
                            delete item.path;
                            if(item.user) {
                                item['allowEdit'] = item.user && item.user._id ? item.user._id.toString() == user._id.toString() : false;
                                if(item.allowEdit) resp.allowPublish = false;
                            }
                            if(item.user.toString() == user._id.toString()) {
                                resp.data.unshift(item);
                            } else resp.data.push(item);
                        });
                        return res.status(200).json(resp)
                    })
                })
            })
        } else return handleError('error finding user details. please try logging in again.', res);
    }
};

exports.getMarketPlaceReviews = function (req, res) {
    var errors = [];
    if (validator.isNull(req.query.pathId)) errors.push("PathId has to be specified");
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    } else {
        Review.find({path: req.query.pathId}).exec(function (err, reviews) {
            if (err) return handleError('Error finding reviews for given path.', res);
            var resp = {data: []};
            reviews.forEach(function(review){
                var item = review.toJSON();
                item['id'] = review._id;
                item['allowEdit'] = false;
                resp.data.push(item);
            });
            return res.status(200).json(resp);
        });
    }
};

exports.updateReview = function (req, res) {
    var reqBody = req.body;
    var responseObj = {};
    var errors = [];

    if (validator.isNull(reqBody.id)) errors.push("id has to be specified");
    if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

    //optional editable fields
    if (!validator.isNull(reqBody.rating))
        if (!validator.isInt(reqBody.rating, {
            min: 1,
            max: 5
        })) errors.push("Please enter a valid rating");
    if (!validator.isNull(reqBody.title))
        if (!validator.isLength(reqBody.title, 0, 40)) errors.push("Title must be max 40 characters");
    if (!validator.isNull(reqBody.review))
        if (!validator.isLength(reqBody.review, 0, 500)) errors.push("Review must be max 500 characters");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        if(user_search_obj) {
            User.findOne(user_search_obj).select('name username email avatar qbId').exec(function (err, user) {
                if (err || !user) return handleError('Error finding user details for user.', res);
                Path.findOne({_id: req.body.pathId}).exec(function (err, path) {
                    if (err || !path) return handleError('Error finding path details for user.', res);
                    if (path.producer.toString() == user._id.toString()) {
                        return res.status(422).json({errors: ['You cannot post / update a review on your own course.']});
                    } else {
                        Review.findOne({_id: reqBody.id, user: user._id}).exec(function (err, review) {
                            if (err) return handleError('Error searching for user reviews.', res);
                            if (!review) {
                                return res.status(422).json({errors: ['No review found with given id or you do not have sufficient ' +
                                    'permissions to update this review.']});
                            } else {
                                if (path.rating && path.reviewer_count > 0) {
                                    var sum = parseFloat(review.rating) * parseInt(path.reviewer_count) - parseInt(review.rating) + parseInt(reqBody.rating);
                                    path['rating'] = (parseFloat(sum) + parseInt(review.rating)) / path.reviewer_count;
                                    path['rating'] = Math.round(path.rating * 10) / 10;
                                    path.save(function (err) {
                                        if (reqBody.rating) review['rating'] = Math.round(reqBody.rating * 10) / 10;
                                        if (reqBody.title) review['title'] = reqBody.title;
                                        if (reqBody.review) review['review'] = reqBody.review;
                                        review['timestamp'] = (new Date()).getTime().toString();
                                        if (err) return handleError('Error saving reviews.', res);
                                        review.save(function (err) {
                                            if (err) return handleError('Error saving reviews.', res);
                                            var resp = {data: review.toJSON()};
                                            resp.data['id'] = review._id;
                                            resp.data['pathId'] = path._id;
                                            resp.data['user'] = user.toJSON();
                                            resp.data['allowEdit'] = true;
                                            return res.status(201).json(resp);
                                        });
                                    })
                                } else {
                                    path['rating'] = review.rating;
                                    path['reviewer_count'] = 1;
                                    path.save(function (err) {
                                        if (err) return handleError('Error saving reviews.', res);
                                        if (reqBody.rating) review['rating'] = Math.round(reqBody.rating * 10) / 10;
                                        if (reqBody.title) review['title'] = reqBody.title;
                                        if (reqBody.review) review['review'] = reqBody.review;
                                        review['timestamp'] = (new Date()).getTime().toString();
                                        review.save(function (err) {
                                            if (err) return handleError('Error saving reviews.', res);
                                            var resp = {data: review.toJSON()};
                                            resp.data['id'] = review._id;
                                            resp.data['pathId'] = path._id;
                                            resp.data['user'] = user.toJSON();
                                            resp.data['allowEdit'] = true;
                                            return res.status(201).json(resp);
                                        });
                                    });
                                }
                            }
                        })
                    }
                })
            });
        }
        else return handleError('error finding user details. please try logging in again.', res);
    }
};

exports.createReview = function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = [];

    if (!validator.isInt(reqBody.rating, {
        min: 1,
        max: 5
    })) errors.push("Please enter a valid rating");
    if (!validator.isLength(reqBody.title, 0, 40)) errors.push("Title must be max 40 characters");
    if (!validator.isLength(reqBody.review, 0, 500)) errors.push("Review must be max 500 characters");
    if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");


    if (errors.length > 0) {
        console.log(errors);
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var user_search_obj = {};
        if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;

        if(user_search_obj) {
            User.findOne(user_search_obj).select('name username email avatar qbId').exec(function (err, user) {
                if (err || !user) return handleError('Error finding user details for user.', res);
                Path.findOne({_id: req.body.pathId}).exec(function (err, path) {
                    if (err || !path) return handleError('Error finding path details for user.', res);
                    if (path.producer.toString() == user._id.toString()) {
                        return res.status(422).json({errors: ['You cannot post a review on your own course.']});
                    } else {
                        Review.findOne({path: path._id, user: user._id}).exec(function (err, review) {
                            if (err) return handleError('Error searching for user reviews.', res);
                            if (!review) {
                                var payload = {
                                    'rating': reqBody['rating'],
                                    'title': reqBody['title'],
                                    'review': reqBody['review'],
                                    'path': path._id,
                                    'user': user._id,
                                    'timestamp': (new Date()).getTime().toString()
                                };
                                Review.create(payload, function (err, review) {
                                    if (err) return handleError('Error creating user reviews.', res);
                                    if (path.rating && path.reviewer_count > 0) {
                                        var sum = parseFloat(review.rating) * parseInt(path.reviewer_count);
                                        path.reviewer_count++;
                                        path['rating'] = (parseFloat(sum) + parseInt(review.rating)) / path.reviewer_count;
                                        path['rating'] = Math.round(path.rating * 10) / 10;
                                        path.save(function (err) {
                                            if (err) return handleError('Error saving reviews.', res);
                                            var resp = {data: review.toJSON()};
                                            resp.data['id'] = review._id;
                                            resp.data['pathId'] = path._id;
                                            resp.data['user'] = user.toJSON();
                                            resp.data.user['id'] = user._id;
                                            return res.status(201).json(resp);
                                        })
                                    } else {
                                        path['rating'] = review.rating;
                                        path['reviewer_count'] = 1;
                                        path.save(function (err) {
                                            if (err) return handleError('Error saving reviews.', res);
                                            var resp = {data: review.toJSON()};
                                            resp.data['id'] = review._id;
                                            resp.data['pathId'] = path._id;
                                            resp.data['user'] = user.toJSON();
                                            resp.data.user['id'] = user._id;
                                            return res.status(201).json(resp);
                                        });
                                    }
                                });
                            } else {
                                return res.status(422).json({errors: ['You have already published a review for this course.']});
                            }
                        })
                    }
                })
            })
        }
        else return handleError('error finding user details. please try logging in again.', res);
    }
};

function handleError(message, res) {
    res.status(422);
    res.json({"errors": [message]});
}
