'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReviewSchema = new Schema({
  title: String,
  rating: {
    type: Number,
    min: 0,
    max: 5
  },
  review: String,
  path: {
    type: Schema.ObjectId,
    ref: 'Path'
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  timestamp: String
});
ReviewSchema.virtual('id').get(function() {
    return this._id;
});

module.exports = mongoose.model('Review', ReviewSchema);
var deepPopulate = require('mongoose-deep-populate')(mongoose);

ReviewSchema.plugin(deepPopulate, {
  whitelist: ['user'],
  populate: {
      'user': {
      select: 'name avatar userDesc username tagline qbId email'
    }
  }
});

