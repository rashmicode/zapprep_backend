'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LegacySchema = new Schema({
    kind: String,
    path: {
        kind: String,
        collection: String,
        key: String,
        ref: String
    },
    value: Schema.Types.Mixed
}, {collection: 'legacy'});

module.exports = mongoose.model('Legacy', LegacySchema);
var deepPopulate = require('mongoose-deep-populate')(mongoose);