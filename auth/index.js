'use strict';

var express = require('express');
var passport = require('passport');
var config = require('../config');
var User = require('../user/user.model');
var _ = require('lodash');
var bcrypt = require('bcryptjs');
var request = require('request');

var kew = require('kew');
var async = require('async');

var multer = require('multer'),
  fs = require('fs');

var customUtils = require('../utils.js');
var qbchat = require('../qbchat.js');

var date = new Date();
var now = date.getTime();

var Firebase = require("firebase");
var myFirebaseRef = new Firebase(config.firebase.url, config.firebase.secret);

var Notifications = require('../notifications');
var notify = new Notifications();
var CronJob = require('cron').CronJob;

var mailgun = require('mailgun-js')({apiKey: config.mailgun.key, domain: config.mailgun.domain});
var validator = require('validator');
var jwt = require('jsonwebtoken');

validator.extend('isImage', function (mimetype) {
  if (mimetype.match(/^image.*/)) return true;
  else return false;
});


var router = express.Router();

router.use('/login', function (req, res, next) {
  qbchat.getSession(function (err, session) {
    if (err) {
      console.log("Recreating session");
      qbchat.createSession(function (err, result) {
        if (err) {
          res.status(503);
          res.json({"errors": ["Can't connect to the chat server, try again later"]});
        } else next();
      })
    } else next();
  })
}, function (req, res) {

});
router.use('/signup', function (req, res) {

});
router.use('/forgotPassword', function (req, res) {

});
// router.use('/local', require('./local'));
router.use('/facebook2', function (req, res) {

});
router.use('/google2', function (req, res) {

});

module.exports = router;
