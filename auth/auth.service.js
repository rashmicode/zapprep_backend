'use strict';
var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/environment');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var compose = require('composable-middleware');
var User = require('../api/user/user.model');
var Series = require('../api/series/series.model');
var Result = require('../api/accelerate/result/result.model');

var validateJwt = expressJwt({ secret: config.secrets.session });

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated() {
    return compose()
        // Validate jwt
        .use(function (req, res, next) {
            // allow access_token to be passed through query parameter as well
            if (req.query && req.query.hasOwnProperty('access_token')) {
                req.headers.authorization = 'Bearer ' + req.query.access_token;
            }
            validateJwt(req, res, next);
        })
        // Attach user to request
        .use(function (req, res, next) {
            User.findOne({_id: req.user._id},'-salt -hashedPassword', function (err, user) {
                if (err) return next(err);
                if (!user) return res.status(403).send('Unauthorized');
                req.user = user;
                next();
            });
        });
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
function hasRole(roleRequired) {
    if (!roleRequired) throw new Error('Required role needs to be set');

    return compose()
        .use(isAuthenticated())
        .use(function meetsRequirements(req, res, next) {
            if (req.user.role.indexOf(roleRequired)!=-1) {
                next();
            }
            else {
                res.status(403).send('Forbidden');
            }
        });
}

/**
 * Returns a jwt token signed by the app secret
 */
function signToken(id) {
    return jwt.sign({ _id: id }, config.secrets.session, { expiresIn: 24 * 60 * 60 * 5 });
}

function canDownloadSeriesSolution() {
    return compose()
        .use(isAuthenticated())
        .use(function downloadSolution(req, res, next){
            Series.findOne({_id: req.params.id}).exec(function(err, series){
                if(err) return res.status(400).json({message: 'Error finding series name'});
                if(!series) return res.status(404).json({message: 'Series name not found'});
                else if(series.requires_roles && req.user.role.indexOf(series.requires_roles) > -1) {
                    //TODO: check if valid paper id and has solution and can download solution here
                    var paper_idx = req.params.paper ? findIndexOfKeyInObjectArr(series.papers, '_id', req.params.paper) : -1;
                    if(paper_idx > -1) {
                        var paper = series.papers[paper_idx];
                        if(paper.allow_solution_download_direct) {
                            res.locals['file_url'] = paper.solution.url;
                            res.locals['file_name'] = paper.solution.alt;
                            next();
                        } else if(paper.requires_attempt) {
                            Result.find({user: req.user._id, paper: paper.paper}).select('total').exec(function(err, results){
                                if(err) return res.status(500).send(err);
                                if(results.length > 0) {
                                    if(parseInt(paper.order.split('.').pop()) == 1) {
                                        res.locals['file_url'] = paper.solution.url;
                                        res.locals['file_name'] = paper.solution.alt;
                                        next();
                                    } else if(parseInt(paper.order.split('.').pop()) > 1) {
                                        var required_order = paper.order.split('.');
                                        required_order.push((parseInt(required_order.pop()) - 1).toString());
                                        var paper_order_idx = findIndexOfKeyInObjectArr(series.papers, 'order', required_order.join('.'));
                                        if(paper_order_idx > -1) {
                                            Result.findOne({paper: series.papers[paper_order_idx].paper, user: req.user._id})
                                                .select('total').exec(function(err, result) {
                                                if(err) return res.status(500).send(err);
                                                if(result) {
                                                    res.locals['file_url'] = paper.solution.url;
                                                    res.locals['file_name'] = paper.solution.alt;
                                                    next();
                                                } else return res.status(501).json({message: 'Please finish the paper: ' +
                                                series.papers[paper_order_idx].description + ' in order to download this test'})
                                            })
                                        } else return res.status(404).json({message: 'Not Found'});
                                    } else return res.status(500).json({message: 'Internal Server Error. Please contact the site admin'});
                                } else return res.status(501).json({message: 'Please attempt the paper at least once to download solutions'});
                            });
                        } else if(parseInt(paper.order.split('.').pop()) > 1){
                            var required_order = paper.order.split('.');
                            required_order.push((parseInt(required_order.pop()) - 1).toString());
                            var paper_order_idx = findIndexOfKeyInObjectArr(series.papers, 'order', required_order.join('.'));
                            if(paper_order_idx > -1) {
                                Result.findOne({paper: series.papers[paper_order_idx].paper, user: req.user._id})
                                    .select('total').exec(function(err, result) {
                                    if(err) return res.status(500).send(err);
                                    if(result) {
                                        res.locals['file_url'] = paper.solution.url;
                                        res.locals['file_name'] = paper.solution.alt;
                                        next();
                                    } else return res.status(500).json({message: 'Please finish the paper: ' +
                                    series.papers[paper_order_idx].description + ' in order to download this test'});
                                })
                            } return res.status(404).json({message: 'Not Found'});
                        } else if(parseInt(paper.order.split('.').pop()) == 1) {
                            res.locals['file_url'] = paper.solution.url;
                            res.locals['file_name'] = paper.solution.alt;
                            next();
                        }
                    } else return res.status(404).json({message: 'Not found'})
                } else if(series.requires_roles && req.user.role.indexOf(series.requires_roles) === -1)
                    return res.status(500).json({message: 'Please register for Test Series first to download solution'});
                else return res.status(404).json({message: 'Not found'});
            });
        })
}

/**
 * Set token cookie directly for oAuth strategies
 */
function setTokenCookie(req, res) {
    if (!req.user) return res.status(404).json({ message: 'Something went wrong, please try again.'});
    var token = signToken(req.user._id, req.user.role);
    res.cookie('token', JSON.stringify(token));
    res.redirect('/');
}

exports.isAuthenticated = isAuthenticated;
exports.hasRole = hasRole;
exports.signToken = signToken;
exports.setTokenCookie = setTokenCookie;
exports.canDownloadSeriesSolution = canDownloadSeriesSolution;

function findIndexOfKeyInObjectArr(obj_arr, key, val){
    for(var item = 0; item < obj_arr.length; item++) {
        if(obj_arr[item][key] && obj_arr[item][key] == val) return item;
    }
    return -1;
}