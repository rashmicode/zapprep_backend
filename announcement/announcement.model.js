'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AnnouncementSchema = new Schema({
  desc: String,
  path: {
    type: Schema.ObjectId,
    ref: 'Path'
  },
  timestamp: {
      type: Date,
      default: Date
  }
});

AnnouncementSchema.virtual('id').get(function() {
    return this._id;
});

module.exports = mongoose.model('Announcement', AnnouncementSchema);
var deepPopulate = require('mongoose-deep-populate')(mongoose);

AnnouncementSchema.plugin(deepPopulate, {
  whitelist: [],
  populate: {}
});

