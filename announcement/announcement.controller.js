var Notifications = require('../notifications');
var notify = new Notifications();

var validator = require('validator');

var Path = require('./../path/path.model');
var Announcement = require('./announcement.model');

exports.postAnnouncements = function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = new Array();

    if (validator.isNull(reqBody.desc)) errors.push("Description must be present");
    if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        Path.findOne({_id: req.body.pathId}).exec(function (err, path) {
            if (err || !path) return handleError('Error finding path with given id.', res);
            Announcement.create({path: path._id, desc: reqBody.desc, timestamp: new Date()}, function (err, announcement) {
                if (err || !announcement) return handleError('Error finding announcements for given path.', res);
                var notifyObj = {
                    "is_read": "false",
                    "is_clicked": "false",
                    "title": "Announcement",
                    "text": reqBody.desc,
                    "photo": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-tssc-files/announcement.png",
                    "link": path._id,
                    "id": (new Date()).getTime()
                };
                notify.emit('newAnnouncement', notifyObj, path.producer);
                var resp = {data: announcement.toJSON()};
                resp.data['id'] = announcement._id;
                return res.status(201).json(resp);
            });
        });
    }
};

exports.getAnnouncements = function (req, res) {
    var errors = [];
    if (validator.isNull(req.query.pathId)) errors.push("PathId has to be specified");
    if (errors.length > 0) {
        return res.status(422).json({errors: errors});
    } else {
        Path.findOne({_id: req.query.pathId}).exec(function (err, path) {
            if (err || !path) return handleError('Error finding path with given id.', res);
            Announcement.find({path: path._id}).sort('-timestamp').exec(function (err, announcements) {
                if (err) return handleError('Error finding announcements for given path.', res);
                var resp = {data: []};
                announcements.forEach(function (ac) {
                    var item = ac.toJSON();
                    item['id'] = ac._id;
                    resp.data.push(item);
                });
                return res.status(200).json(resp);
            })
        })
    }
};

function handleError(message, res) {
    res.status(422);
    res.json({"errors": [message]});
}
