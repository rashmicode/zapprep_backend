define({ "api": [
  {
    "type": "get",
    "url": "/user/paths",
    "title": "Dashboard",
    "name": "getDashboard",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "access_token",
            "description": "<p>(in URL) Access Token of the current User</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "limit",
            "description": "<p>(in URL) (Optional) Number of paths per page. Default 10</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "page",
            "description": "<p>(in URL) (Optional) Page Number. Default 1</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n     \"more\": true,\n     \"data\": [\n         {\n             \"title\": \"Star Trek\",\n             \"desc\": \"Get to know the basics of movie industry by following this course on start trek\",\n             \"buying\": \"free\",\n             \"coverPhoto\": \"https://s3.amazonaws.com/test-bucket-ketan/f78947c74b788406a23591902c6e36e8.JPG\",\n             \"coverPhotoThumb\": \"https://s3.amazonaws.com/test-bucket-ketanresized/resized-f78947c74b788406a23591902c6e36e8.JPG\",\n             \"concepts\": [\n                 {\n                    \"id\": \"Q43rWW8oyLJbY1mY\",\n                    \"title\": \"Introduction\",\n                    \"qbId\": \"55574a626390d8ebef01ba98\"\n                 }\n             ],\n             \"qbId\": \"55574a626390d8ab91035862\",\n             \"mentorRoomId\": \"55574a626390d8261901ebb1\",\n             \"studentCount\": 1,\n             \"isOwner\": true,\n             \"id\": \"0a11e1090960fead\"\n         },\n         {\n             \"title\": \"New Star Trek\",\n             \"desc\": \"Get to know the basics of movie industry by following this course on start trek\",\n             \"buying\": \"free\",\n             \"coverPhoto\": \"https://s3.amazonaws.com/test-bucket-ketan/f78947c74b788406a23591902c6e36e8.JPG\",\n             \"coverPhotoThumb\": \"https://s3.amazonaws.com/test-bucket-ketanresized/resized-f78947c74b788406a23591902c6e36e8.JPG\",\n             \"producer\": {\n                 \"username\": \"ketanbhatt12345\",\n                 \"email\": \"ktbt1012345@gmail.com\",\n                 \"avatar\": \"https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png\",\n                 \"qbId\": 3066379\n             },\n             \"concepts\": [\n                 {\n                    \"id\": \"Q43rWW8oyLJbY1mY\",\n                    \"title\": \"Introduction\",\n                    \"qbId\": \"55574a626390d8ebef01ba98\"\n                 }\n             ],\n             \"qbId\": \"55574a626390d8ab91035862\",\n             \"mentorRoomId\": \"55574a626390d8261901ebb1\",\n             \"studentCount\": 1,\n             \"isOwner\": false,\n             \"id\": \"0a11e1090960fead\"\n         }\n     ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/user.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user",
    "title": "User Profile",
    "name": "getUserProfile",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "access_token",
            "description": "<p>(in URL) Access Token of the current User</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>(in URL) (Optional) Email of User to Get</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "limit",
            "description": "<p>(in URL) (Optional) Number of paths per page. Default 10</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "page",
            "description": "<p>(in URL) (Optional) Page Number. Default 1</p> "
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"username\": \"ketannewbhatt123\",\n         \"email\": \"newktbt12330@gmail.com\",\n         \"avatar\": \"https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png\",\n         \"qbId\": 3125204,\n         \"paths\": [\n             {\n                 \"title\": \"Star Trek\",\n                 \"desc\": \"Get to know the basics of movie industry by following this course on start trek\",\n                 \"buying\": \"free\",\n                 \"coverPhoto\": \"https://s3.amazonaws.com/test-bucket-ketan/f78947c74b788406a23591902c6e36e8.JPG\",\n                 \"coverPhotoThumb\": \"https://s3.amazonaws.com/test-bucket-ketanresized/resized-f78947c74b788406a23591902c6e36e8.JPG\",\n                 \"concepts\": [\n                     {\n                         \"id\": \"Q43rWW8oyLJbY1mY\",\n                         \"title\": \"Introduction\",\n                         \"qbId\": \"55574a626390d8ebef01ba98\"\n                     }\n                 ],\n                 \"qbId\": \"55574a626390d8ab91035862\",\n                 \"mentorRoomId\": \"55574a626390d8261901ebb1\",\n                 \"studentCount\": 1,\n                 \"isOwner\": true,\n                 \"id\": \"0a11e1090960fead\"\n             }\n         ]\n     },\n     \"more\": true\n }",
          "type": "json"
        }
      ]
    },
    "description": "<p>Returns User Profile for the specified email Otherwise returns Profile of Logged In User</p> ",
    "version": "0.0.0",
    "filename": "routes/user.js",
    "groupTitle": "User"
  },
  {
    "type": "patch",
    "url": "/user",
    "title": "User Profile Settings Page",
    "name": "patchUserProfile",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "access_token",
            "description": "<p>(in URL) Access Token of the current User</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>(Optional) Name of the User</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "userDesc",
            "description": "<p>(Optional) User Description</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "avatar",
            "description": "<p>(Optional) User Profile Picture</p> "
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "routes/user.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/enroll",
    "title": "Enroll User",
    "name": "userEnroll",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "access_token",
            "description": "<p>(in URL) Access Token of the current User</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "pathId",
            "description": "<p>Path ID of the path to enroll in</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "success",
            "description": "<p>Success Response</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"success\": \"You just got (En)rolled!\"\n         }\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/user.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/login",
    "title": "User Login",
    "name": "userLogin",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>Unique Email of the User.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "password",
            "description": "<p>Password for the User.</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "username",
            "description": "<p>Unique handle for the User.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>Unique Email of the User.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "avatar",
            "description": "<p>Avatar link of the User.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "qbId",
            "description": "<p>QuickBlox ID of the User.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "access_token",
            "description": "<p>Access Token for the User.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"username\": \"ketannewbhatt123\",\n         \"email\": \"newktbt12330@gmail.com\",\n         \"avatar\": \"https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png\",\n         \"qbId\": 3125204,\n         \"access_token\": \"53a1b0db6b5ada46ccecc0de41bfdb67\"\n         }\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/user.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/logout",
    "title": "User Logout",
    "name": "userLogout",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "access_token",
            "description": "<p>(in URL) Access Token of the current User</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "success",
            "description": "<p>Success Response</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n     \"data\": {\n         \"success\": \"Successfully and securely logged out, kind off\"\n         }\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/user.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user/signup",
    "title": "User Signup",
    "name": "userSignup",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "username",
            "description": "<p>Unique handle for the User.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>Unique Email of the User.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "password",
            "description": "<p>Password for the User.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the User.</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "userDesc",
            "description": "<p>(Optional) Description of the User.</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "username",
            "description": "<p>Unique handle for the User.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "email",
            "description": "<p>Unique Email of the User.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the User.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "avatar",
            "description": "<p>Avatar link of the User.</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "userDesc",
            "description": "<p>(Optional) Description of the User.</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 Created\n {\n     \"data\": {\n         \"name\": \"Ketan Bhatt\",\n         \"username\": \"ketannewbhatt123\",\n         \"email\": \"newktbt12330@gmail.com\",\n         \"avatar\": \"https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png\",\n         \"userDesc\": \"This is the user description, if he entered.\"\n         }\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/user.js",
    "groupTitle": "User"
  }
] });