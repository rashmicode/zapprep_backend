'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EnrollmentSchema = new Schema({
  path: {
    type: Schema.ObjectId,
    ref: 'Path'
  },
  producer: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  expiresOn: Date,
  isMentorPaid: Boolean,
  isCoursePaid: Boolean,
  payment_ref: {
    type: Schema.ObjectId,
    ref: 'Payment'
  },
  isEnrolled: Boolean
});

EnrollmentSchema.virtual('id').get(function() {
    return this._id;
});

module.exports = mongoose.model('Enrollment', EnrollmentSchema);
var deepPopulate = require('mongoose-deep-populate')(mongoose);

EnrollmentSchema.plugin(deepPopulate, {
  whitelist: ['producer', 'path.producer', 'path', 'path.concepts', 'path.concepts.objects'],
  populate: {
    'producer': {
      select: 'name avatar userDesc qbId avatarThumb username onlineStatus'
    },
    'path.producer': {
      select: 'name avatar userDesc qbId avatarThumb onlineStatus'
    },
    'path': {
      select: 'mentorPaid coursePaid mentorAvailable courseINR courseUSD concepts title category mentorRoomId subCat ' +
          'qbId desc coverPhoto coverPhotoThumb producer username tagline studentCount'
    },
    'path.concepts': {
      select: 'title pos desc objects qbId'
    },
    'path.concepts.objects': {
      select: 'title pos desc url type image size mimetype urlThumb'
    }
  }
});

