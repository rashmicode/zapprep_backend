#!/bin/bash
echo "...Running awesome script..."
while [ 1 ]
do
	node chatServer.js &
	PID=$!
	sleep 1800
	kill -INT $PID
	echo "Chat Server Killed"
done
