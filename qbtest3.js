var qbchat = require('./qbchat');
var config = require('./config');
var mongoose = require('mongoose');

// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', function (err) {
        console.error('MongoDB connection error: ' + err);
        process.exit(-1);
    }
);

var QB = qbchat.extraQB();
qbchat.extraInit(QB);

var Path = require('./path/path.model');
var User = require('./user/user.model');
var Enroll = require('./enrollment/enroll.model');
var Concept = require('./concept/concept.model');
var async = require('async');

// Quickblox update ids script
async.series([
    function(cb){
        Concept.find().exec(function(err, concepts){
            console.log('found concepts - ', concepts.length);
            async.eachSeries(concepts, function(concept, concept_cb){
                console.log('updating - ', concept.qbId, concept.title);
                if(concept.qbId && concept.title) {
                    qbchat.extraChangeRoomName(QB, concept.qbId, concept.title, config.qb.params, function(err, result){
                        if(err) {
                            console.log('error updating room - ', err, ", concept Id - ", concept._id);
                            //TODO: create room and add user to room here
                            concept_cb();
                        } else if(result) {
                            //TODO: debug this result to check occupant ids, if producer qbId not present then add it to room
                            console.log('updated - ', concept.qbId, concept.title);
                            concept_cb();
                        } else {
                            console.log('unknown error while changing room - ', err, result, concept._id);
                            concept_cb();
                        }
                    });
                } else {
                    console.log('qbid not found concept - ', concept._id);
                    concept_cb();
                }
            }, function(done){
                cb();
            })
        })
    }
], function(done){
    console.log('finished updating all results');
});