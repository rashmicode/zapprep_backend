var util = require('util');
var customUtils = require('./utils.js');
var config = require('./config.js');
var _ = require('lodash');
var mongoose = require('mongoose');

var request = require('request');

var gcm = require('node-gcm');
var sender = new gcm.Sender(config.gcm.apiKey);
var message = new gcm.Message();

var myFirebaseRef = require("./firebase");
var EventEmitter = require('events').EventEmitter;
var NF = new NotificationFactory();
var Enroll = require('./enrollment/enroll.model');
var Path = require('./path/path.model')

function Notifications() {
    EventEmitter.call(this);

    // this.on('enrolment', function(data){
    // 	var recieverIds = [];
    // 	var recieverGcmIds = [];
    // 	var type = "app";

    // 	var date = new Date();
    // 	var nofObj = {
    // 		"created": date.getTime(),
    // 		"id": date.getTime(),
    // 		"is_clicked": false,
    // 		"is_read": false,
    // 		"link": data.where,
    // 		"title": "New User enroled in your path"
    // 	};
    // 	recieverIds.push(data.producerId);
    // 	recieverGcmIds.push(data.producerGcmId);

    // 	NF.sendNotification(nofObj, recieverIds, recieverGcmIds, type);
    // });

    this.on('welcome', function (data) {
        console.log('sending welcome notifications');
        var date = new Date();
        var titleString = "Hello " + data.name + "!";

        var nofObj = {
            "created": date.getTime(),
            "id": date.getTime(),
            "is_clicked": false,
            "is_read": false,
            "link": 'Dashboard',
            "title": titleString,
            "text": "Welcome to Zapprep! We look forward to providing you a great learning experience :)",
            "photo": "https://s3.amazonaws.com/pyoopil-prod-server/Circular+Logo.png"
        };
        console.log('sending welcome notification.');
        NF.sendNotification(nofObj, [data.user], [data.gcmId], 'app');
    });

    this.on('newConcept', function (data, mentorId) {
        console.log('sending new concept notifications');
        var date = new Date();
        //TODO: fix notifications here
        Enroll.find({path: mongoose.Types.ObjectId(data.link), isEnrolled: true}).select('producer').exec(function(err, enrolls){
            var receiverIds = [];
            data['id'] = date.getTime();
            _.forEach(enrolls, function(enroll){
                if(enroll.producer) receiverIds.push(enroll.producer);
            });
            receiverIds.push(mentorId);
            NF.sendNotification(data, receiverIds, [], 'app');
        })
    });

    this.on('newObject', function (data, mentorId) {
        var date = new Date();
        console.log('sending new object notifications');
        Enroll.find({isEnrolled: true, path: mongoose.Types.ObjectId(data.link)}).select('producer').exec(function(err, enrolls){
            var receiverIds = [];
            data['id'] = date.getTime();
            _.forEach(enrolls, function(enroll){
                if(enroll.producer) receiverIds.push(enroll.producer);
            });
            receiverIds.push(mentorId);
            NF.sendNotification(data, receiverIds, [], 'app');
        });
    });

    this.on('pathEdited', function(data){
    	var recieverIds = [];
    	var recieverGcmIds = [];
    	var type = "app";

    	var date = new Date();
    	var nofObj = {
    		"created": date.getTime(),
    		"id": customUtils.randomizer(),
    		"is_clicked": false,
    		"is_read": false,
    		"link": data.where,
    		"title": "Some Path Settings were changed"
    	};

    	//TODO: update notification
    });

    this.on('newPath', function (data) {
        var type = "app";

        var date = new Date();
        var nofObj = {
            "created": date.getTime(),
            "id": date.getTime(),
            "is_clicked": false,
            "is_read": false,
            "link": data.pathId,
            "title": "New Course",
            "text": data.producerName + " has published a new course - " + data.pathTitle + ". Check it out!",
            "photo": data.producerPhoto
        };
        //TODO: new path notifications to be added
    });

    this.on('mentorMaybe', function (data) {
        console.log('sending mentor online');
        Path.find({producer: mongoose.Types.ObjectId(data.link)}).select('_id').exec(function(err, paths){
            var pathIds = [];
            _.forEach(paths, function(path){
                if(path._id) pathIds.push(path._id);
            });
            Enroll.find({isEnrolled: true, path: {$in: pathIds}}).select('producer').exec(function(err, enrolls){
                var receiverIds = [];
                _.forEach(enrolls, function(enroll){
                    if(enroll.producer) receiverIds.push(enroll.producer);
                });
                receiverIds.push(data.link);
                NF.sendNotification(data, receiverIds, [], 'app');
            });
        });
    });

    this.on('newAnnouncement', function (data, mentorId) {
        console.log('sending new announcement notifications');
        Enroll.find({path: mongoose.Types.ObjectId(data.link), isEnrolled: true}).select('producer').exec(function(err, enrolls){
            var receiverIds = [];
            _.forEach(enrolls, function(enroll){
                if(enroll.producer) receiverIds.push(enroll.producer);
            });
            receiverIds.push(mentorId);
            NF.sendNotification(data, receiverIds, [], 'app');
        });
    });

    this.on('newVideoTutorInvite', function (data, userId) {
        console.log('sending invitation for video tutoring');
        var receiverIds = [];
        receiverIds.push(userId);
        NF.sendNotification(data, receiverIds, [], 'app');
    });

    //    this.on('feedbackResponse', function (data) {
    //        var type = "both";
    //        var date = new Date();
    //
    //        console.log(data)
    //
    //        var nofObj = {
    //            "created": date.getTime(),
    //            "id": date.getTime(),
    //            "is_clicked": false,
    //            "is_read": false,
    //            "link": "Feedback Channel",
    //            "title": "Response for your Feedback",
    //            "text": "You got a response from Zaprpep Team : " + data.text,
    //            "photo": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-tssc-files/pyoopil-logo.png"
    //        };
    //
    //        if (data.type == "singleUser") {
    //            console.log("singleUser wala feedback notification detected for username " + data.username)
    //
    //        } else if (data.type == "Everyone") {
    //            console.log("Warning! This is a global notification pusher!")
    //            everyoneNotificationDispatcer(0, nofObj, "push")
    //        } else if (data.type == "Classroom") {
    //            console.log("Classroom notif - ")
    //            console.log(data.pathId)
    //            firePathConsumerNotifs(0, data.pathId, nofObj, "both")
    //        }
    //    });

    //    this.on('enrolmentAggregate', function (data) {
    //        var type = "both";
    //        var date = new Date();
    //
    //        if (data.newStudents == 1)
    //            var text = "1 New Person joined your learning track \"" + data.pathName + "\" in the past day. Welcome him!";
    //        else
    //            var text = newStudents + " New People joined your learning track \"" + data.pathName + "\" in the past day. Welcome them!";
    //
    //        var nofObj = {
    //            "created": date.getTime(),
    //            "id": date.getTime(),
    //            "is_clicked": false,
    //            "is_read": false,
    //            "link": data.pathId,
    //            "title": "New Enrolments in \"" + data.pathName + "\"",
    //            "text": text,
    //            "photo": data.pathPhoto
    //        };
    //
    //        NF.sendNotification(nofObj, [data.producerId], [data.producerGcmId], type);
    //
    //    });

    this.on('newMention', function (data) {
        var type = "both";
        var date = new Date();
    });

    this.on('newMentorMessage', function (data) {
        var type = "both";
        var date = new Date();

    });

    this.on('wordForChat', function (data) {
        data["id"] = data["id"] + "@0";
        console.log('sending word for chat notification');
        myFirebaseRef.ref('chatServer').child("addToCache/" + data.id).set(data);
    });

    /**
     * This is a timed notification to inform the App
     * that it needs to calculate the user's unread notification
     */
        //this.on('chatAggregate', function (data) {
        //    var date = new Date();
        //    var nofObj = {
        //        "created": date.getTime(),
        //        "id": date.getTime(),
        //        "is_clicked": false,
        //        "is_read": false,
        //        "link": "Dashboard",
        //        "title": "Chat Aggregate",
        //        "text": "You have unread chats inside your courses",
        //        "photo": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-tssc-files/pyoopil-logo.png"
        //    };
        //    everyoneNotificationDispatcer(0, nofObj, "push")
        //})

    this.on('unreadMessageCount', function (data) {
        //Expected data
        /**
         * data = {
         *      userGcmId : 12312313,
         *      unreadCount : 24
         * }
         */
        var date = new Date();
        var nofObj = {
            "created": date.getTime(),
            "id": date.getTime(),
            "is_clicked": false,
            "is_read": false,
            "link": "Dashboard",
            "title": "Unread Chats",
            "text": "You have " + data.unreadCount + " unread chats inside your courses",
            "photo": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-tssc-files/pyoopil-logo.png"
        };
        NF.sendNotification(nofObj, null, [data.userGcmId], "push");
    })
}

var everyoneNotificationDispatcer = function (offset, nofObj, type) {
    var params = {
        limit: config.pagination.limit,
        offset: offset
    }

}

/**
 * This method fires notification for all consumers of a path
 * -------------------------------------------------------------
 * body Output looks like this:
 * -------------------------------------------------------------
 * { count: 100,
     *   results: [ [Object], [Object], [Object], [Object], [Object] ],
     *   next: '/v0/paths/0c3484518b604c4a/relations/isConsumed?offset=5&limit=5' }
 * -------------------------------------------------------------
 * body output of last page looks like this :
 * -------------------------------------------------------------
 * { count: 6,
     *    results:
     *     [ { path: [Object], value: [Object], reftime: 1443207278236 },
     *       { path: [Object], value: [Object], reftime: 1443570411674 },
     *       { path: [Object], value: [Object], reftime: 1444221122617 },
     *       { path: [Object], value: [Object], reftime: 1441017432260 },
     *       { path: [Object], value: [Object], reftime: 1442519236121 },
     *       { path: [Object], value: [Object], reftime: 1441017791823 } ],
     *    prev: '/v0/paths/0c3484518b604c4a/relations/isConsumed?offset=0&limit=100' }
 *
 *    Also:
 *    http://stackoverflow.com/questions/28250680/how-do-i-access-previous-promise-results-in-a-then-chain
 *    using the simplest - sharing promise values with higher scope
 *
 * @param offset
 * @param pathId
 */

util.inherits(Notifications, EventEmitter);

function NotificationFactory() {
    this.sendNotification = function (nofObj, recieverIds, recieverGcmIds, type) {
        if(nofObj.link) nofObj['link'] = nofObj.link.toString();
        console.log("----------------nofObj : ----------------------");
        //        console.log("----------------" + type);
        console.log(nofObj);
        //        console.log("receivers : ");
        //        console.log(recieverIds);
        //        console.log("receivers GCMs: ");
        //        console.log(recieverGcmIds);
        switch (type) {
            case "both":
            {
                message.addData(nofObj);
                sender.send(message, recieverGcmIds, function (err, result) {
                    if (err) console.error(err);
                    console.log('gcm result = ', result, ', Error: ', err);
                });
                recieverIds.forEach(function (recieverId) {
                    myFirebaseRef.ref('users').child(recieverId + "/nof/" + nofObj.id).set(nofObj);
                    myFirebaseRef.ref('users').child(recieverId + "/count").transaction(function (current_value) {
                        console.log("Notification Count ", current_value);
                        return (current_value || 0) + 1;
                    });
                });
                console.log("Both")
            }
                break;

            case "app":
            {
                recieverIds.forEach(function (recieverId) {
                    myFirebaseRef.ref('users').child(recieverId + "/nof/" + nofObj.id).set(nofObj);
                    myFirebaseRef.ref('users').child(recieverId + "/count").transaction(function (current_value) {
                        return (current_value || 0) + 1;
                    });
                });
                console.log("App")
            }
                break;

            case "push":
            {
                message.addData(nofObj);
                sender.send(message, recieverGcmIds, function (err, result) {
                    if (err) console.error(err);
                });
                console.log("Push")
            }
                break;

            default:
                console.log("Seriously, frontend?")
        }
    }

}

Notifications.prototype.getRecieversAndUpdatePhotos = function (type, dbId, newPhoto, gcmId) {
    if (type == 'path') {

    } else if (type == 'user') {
        var notif_obj = {
            link: newPhoto,
            text: 'You just updated your profile picture.',
            title: 'photo',
            id: (new Date()).getTime()
        };
        NF.sendNotification(notif_obj, [dbId], [gcmId], 'both')
    }
    //TODO: fix profile photo notifications
};

var updatePhotosPath = function (recieverIds, dbId, newPhoto) {
    recieverIds.forEach(function (recieverId) {
        myFirebaseRef.ref('users').child(recieverId + "/nof/").orderByChild("link").equalTo(dbId).on("child_added", function (snapshot) {
            var val = snapshot.val();
            if (val.title != 'New Course' && val.title != 'You have been mentioned!') {
                myFirebaseRef.ref('users').child(recieverId + "/nof/" + snapshot.key()).update({"photo": newPhoto});
            }
        });
    })
};

var getPathFromChannel = function () {

};

/**
 * iterates through the produced courses of a mentor and
 * see if any of them have the flag mentorAvailable = false
 * @param mentorId
 * @param callback
 */
var checkIfMentorAvailable = function (mentorId, callback) {
    var mentorAvailable = true;
    /**
     * mentor will not have more than 100 courses.
     * hmmph. hence this is not recursive.
     */

};

module.exports = Notifications;
