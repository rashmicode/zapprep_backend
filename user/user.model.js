'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require('crypto');
var uuid = require('node-uuid');

var UserSchema = new Schema({
  email: {type: String, lowercase: true, unique: true},
  hashedPassword: String,
  provider: String,
  salt: String,
  created: {
    type: Date,
    default: Date
  },
  facebookId: {
    type: 'String'
  },
  googleId: {
    type: 'String'
  },
  providerData: [{
    data: Schema.Types.Mixed,
    name: String
  }],
  verification_token: String,
  access_token: String,
  name: String,
  avatar: String,
  phone: String,
  avatarThumb: String,
  username: String,
  isVerified: {
    type: Boolean,
    default: false
  },
  last_seen: {
    type: Date,
    default: Date
  },
  qbId: String,
  gcmId: String,
  userDesc: String,
  tagline: String,
  onlineStatus: {
    type: Boolean,
    default: false
  }
});

/**
 * Virtuals
 */
UserSchema.virtual('id').get(function() {
    return this._id;
});
UserSchema
  .virtual('password')
  .set(function (password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function () {
    return this._password;
  });

// Non-sensitive info we'll be putting in the token
UserSchema
  .virtual('token')
  .get(function () {
    return {
      '_id': this._id,
      'role': this.role
    };
  });

/**
 * Validations
 */

// Validate empty email
UserSchema
  .path('email')
  .validate(function (email) {
    return email.length;
  }, 'Email cannot be blank');

// Validate empty password
UserSchema
  .path('hashedPassword')
  .validate(function (hashedPassword) {
    return hashedPassword.length;
  }, 'Password cannot be blank');

// Validate email is not taken
UserSchema
  .path('email')
  .validate(function (value, respond) {
    var self = this;
    this.constructor.findOne({email: value}, function (err, user) {
      if (err) throw err;
      if (user) {
        if (self.id === user.id) return respond(true);
        return respond(false);
      }
      respond(true);
    });
  }, 'The specified email address is already in use.');

var validatePresenceOf = function (value) {
  return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
  .pre('save', function (next) {
    if (!this.isNew) return next();

    if (!validatePresenceOf(this.hashedPassword))
      next(new Error('Invalid password'));
    else
      next();
  });

/**
 * Methods
 */
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */
  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashedPassword;
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */
  makeSalt: function () {
    return crypto.randomBytes(16).toString('base64');
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */
  encryptPassword: function (password) {
    if (!password || !this.salt) return '';
    var salt = new Buffer(this.salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  }
};

module.exports = mongoose.model('User', UserSchema);
var deepPopulate = require('mongoose-deep-populate')(mongoose);

UserSchema.plugin(deepPopulate, {
  whitelist: [],
  populate: {}
});
