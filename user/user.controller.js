var _ = require('lodash');
var bcrypt = require('bcryptjs');
var request = require('request');

var kew = require('kew');
var async = require('async');

var multer = require('multer'),
    fs = require('fs');

var config = require('../config.js');
var customUtils = require('../utils.js');
var qbchat = require('../qbchat.js');
qbchat.init();
var date = new Date();
var now = date.getTime();

var Notifications = require('../notifications');
var notify = new Notifications();

var unreader = require('../unreader');

var bunyan = require('bunyan');

var log = bunyan.createLogger({
    name: 'dashboardErrors',
    streams: [
        {
            path: __dirname + '/dashboardErrors.log'  // log ERROR and above to a file
        }
    ]
});

var CronJob = require('cron').CronJob;

var mailgun = require('mailgun-js')({apiKey: config.mailgun.key, domain: config.mailgun.domain});
var validator = require('validator');
var jwt = require('jsonwebtoken');

/**
 * disable require cache to get a new QB object for consumer
 * so that the sessions dont clash!
 ***/
//Object.keys(require.cache).forEach(function(key) {
//    //delete require.cache[key]
//    if(key.indexOf("node_modules/quickblox") > -1) {
//        console.log(key)
//        delete require.cache[key]
//    }
//})

validator.extend('isImage', function (mimetype) {
    return !!mimetype.match(/^image.*/);
});
var User = require('./user.model');
var Enrollment = require('./../enrollment/enroll.model');
var Path = require('./../path/path.model');
var Payment = require('./../payment/payment.model');

exports.google_login = function (req, res) {
    var responseObj = {};

    var encryptedJwt = req.body.code;

    var decoded = undefined;
    var header = undefined;
    var payload = undefined;

    var avatar = undefined;
    var avatarThumb = undefined;

    var errors = [];

    if (validator.isNull(encryptedJwt)) errors.push("No JWT Token provided");
    else {
        decoded = jwt.decode(encryptedJwt, {complete: true});
        payload = decoded.payload;
        if (validator.isNull(payload.sub)) errors.push("Profile ID is missing");
        if (validator.isNull(payload.email)) errors.push("Email is missing");
        if (validator.isNull(payload.name)) errors.push("Name is missing");
        //if profile is missing set your own profile, because it has not been set in google.
        if (validator.isNull(payload.picture)) {
            avatar = "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg";
            avatarThumb = "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg";
        } else {
            avatar = payload.picture.replace('s96-c/', '');
            avatarThumb = payload.picture;
        }
    }
    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        User.findOne({googleId: payload.sub}).exec(function (err, user) {
            if (err) handleError('Error logging in. Please contact pyoopil immediately.', res);
            if (user) {
                generateTokenAndLogin(user, res);
            } else {
                User.findOne({email: payload.email}).exec(function (err, user) {
                    if (err) handleError('Error logging in. Please contact pyoopil immediately.', res);
                    if (!user) {
                        signUpFreshGoogleUser(payload, avatar, avatarThumb, res)
                    } else {
                        user['googleId'] = payload.sub;
                        user['avatar'] = avatar;
                        user['avatarThumb'] = avatarThumb;
                        user['access_token'] = customUtils.generateToken();
                        user.save(function (err) {
                            var resp = {data: user.toJSON()};
                            resp.data['id'] = user._id;
                            delete resp.data.hashedPassword;
                            delete resp.data.salt;
                            return res.status(201).json(resp);
                        });
                    }
                })
            }
        })
    }
};

exports.facebook_login = function (req, res) {
    var responseObj = {};
    var errors = [];
    var changeEmail = false;

    var accessToken = req.body.code;
    if (validator.isNull(accessToken)) errors.push("Access Token not provided");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        //--------------URLs-----------------------------------------------------------------------
        var accessTokenUrl = "https://graph.facebook.com/v2.3/me?fields=id,name,email&access_token=" + accessToken;
        //--------------URLs End -------------------------------------------------------------------
        request.get({url: accessTokenUrl, json: true}, function (err, response, payload) {
            if (response.statusCode !== 200) {
                return res.status(response.statusCode).send({errors: [payload.error.message]});
            }
            if (validator.isNull(payload.id)) return res.status(409).send("Profile ID could not be retrieved");
            if (validator.isNull(payload.name)) return res.status(409).send("Name could not be retrieved");
            if (validator.isNull(payload.email)) {
                payload.email = "brahmasth" + customUtils.generateToken(4) + "@mailinator.com";
                changeEmail = true;
            }

            var avatar = "https://graph.facebook.com/" + payload.id + "/picture?type=large";
            var avatarThumb = "https://graph.facebook.com/" + payload.id + "/picture";

            User.findOne({facebookId: payload.id}).exec(function (err, user) {
                if (err) return handleError('Error logging in with facebook. Try later', res);
                if (user) {
                    generateTokenAndLogin(user, res);
                    extractFacebookFriends(user._id, accessToken);
                } else {
                    User.findOne({email: payload.email}).exec(function (err, user) {
                        if (err) return handleError('Error finding email from facebook.', res);
                        if (!user) {
                            signUpFreshFacebookUser(payload, avatar, avatarThumb, res, changeEmail, accessToken);
                        } else {
                            user['facebookId'] = payload.id;
                            user['avatar'] = payload.avatar;
                            user['avatarThumb'] = payload.avatarThumb;
                            user['access_token'] = customUtils.generateToken();
                            user.save(function (err) {
                                if (err) return handleError('error updating user data', res);
                                extractFacebookFriends(user._id, user.access_token);
                                var resp = {data: user.toJSON()};
                                resp.data['id'] = user._id;
                                return res.status(201).json(resp);
                            });
                        }
                    })
                }
            })
        });
    }
};

exports.facebook2_login = function (req, res) {
    var responseObj = {};
    var errors = [];
    var changeEmail = false;

    var accessToken = req.body.code;
    if (validator.isNull(accessToken)) errors.push("Access Token not provided");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name'];
        var accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token';
        //var graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');
        var params = {
            code: req.body.code,
            client_id: config.facebookAuth.clientId,
            client_secret: config.facebookAuth.clientSecret,
            redirect_uri: config.facebookAuth.redirect_uri
        };

        // Step 1. Exchange authorization code for access token.
        request.get({url: accessTokenUrl, qs: params, json: true}, function (err, response, accessToken) {
            if (response.statusCode !== 200) {
                return res.status(500).send({errors: [accessToken.error.message]});
            }
            accessToken = accessToken.access_token;
            /**
             * Response format :
             * accessToken = {
                 *         "access_token": "CAAI1acbiMrMBANdKjN3dd8KDDRoy6hc2ZCzAZBuOzorkJYAHNlXUOB75k81VZCJDZArOTNtYhdbrWWvwEeM9qZC2TAhg2pY7lZC4UokGgG2w4Yl0QOuacxxedBIyPLhZBuZCPtugwGySoSOQXeHGpLeZAMa3EygD5uvla0NxtDNeSUk59FMPTnRH3UkxA0vYKWgQZD",
                 *          "token_type": "bearer",
                 *          "expires_in": 5178279
                 * }
             */
            //--------------URLs-----------------------------------------------------------------------
            var graphApiUrl = "https://graph.facebook.com/v2.3/me?fields=id,name,email&access_token=" + accessToken;
            //--------------URLs End -------------------------------------------------------------------
            request.get({url: graphApiUrl, json: true}, function (err, response, payload) {
                if (response.statusCode !== 200) {
                    return res.status(response.statusCode).send({errors: [payload.error.message]});
                }
                if (validator.isNull(payload.id)) return res.status(409).send("Profile ID could not be retrieved");
                if (validator.isNull(payload.name)) return res.status(409).send("Name could not be retrieved");
                if (validator.isNull(payload.email)) {
                    payload.email = "brahmasth" + customUtils.generateToken(4) + "@mailinator.com";
                    changeEmail = true;
                }

                var avatar = "https://graph.facebook.com/" + payload.id + "/picture?type=large";
                var avatarThumb = "https://graph.facebook.com/" + payload.id + "/picture";
                User.findOne({facebookId: payload.id}).exec(function (err, user) {
                    if (err) return handleError('error getting facebook login', res);
                    if (!user) {
                        generateTokenAndLogin(user, res);
                        extractFacebookFriends(user._id, accessToken);
                    } else {
                        User.findOne({email: payload.email}).exec(function (err, user) {
                            if (err) return handleError('error getting facebook login', res);
                            if (!user) {
                                signUpFreshFacebookUser(payload, avatar, avatarThumb, res, changeEmail, accessToken);
                            } else {
                                user['facebookId'] = payload.id;
                                user['avatar'] = avatar;
                                user['avatarThumb'] = avatarThumb;
                                user['access_token'] = customUtils.generateToken();
                                user.save(function (err) {
                                    if (err) return handleError('error saving facebook user data', res);
                                    extractFacebookFriends(user._id, accessToken);
                                    var resp = {data: user.toJSON()};
                                    resp.data['id'] = user._id;
                                    return res.status(201).json(resp);
                                });
                            }
                        })
                    }
                })
            })
        });
    }
};

exports.google2_login = function (req, res) {
    var responseObj = {};
    var accessTokenUrl = 'https://www.googleapis.com/oauth2/v3/token';
    var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';

    //this code is the encrypted jwt
    var params = {
        code: req.body.code,
        client_id: config.googleAuth.clientId,
        client_secret: config.googleAuth.clientSecret,
        redirect_uri: config.googleAuth.redirect_uri,
        grant_type: 'authorization_code'
    };

    // Step 1. Exchange authorization code for access token.
    request.post(accessTokenUrl, {json: true, form: params}, function (err, response, token) {
        var accessToken = token;
        var headers = {Authorization: 'Bearer ' + accessToken};
        var isVerified = undefined;

        if (err) {
            return res.status(500).send({message: 'Invalid authorization code!'});
        } else {
            // Step 2. Retrieve profile information about the current user.
            request.get({url: peopleApiUrl, headers: headers, json: true}, function (err, response, profile) {
                if (profile.error) {
                    return res.status(500).send({message: profile.error.message});
                } else {
                    // Step 3a. Link user accounts
                    if (req.headers.authorization) {
                        User.findOne({googleId: profile.sub}).exec(function (err, user) {
                            if (err) return handleError("error logging in to google.", res);
                            if (!user) {
                                return res.status(409).send({message: 'Looks like a new google account!'});
                            } else {
                                return res.status(409).send({message: 'There is already a Google account that belongs to you'});
                            }
                        });
                    } else {
                        // Step 3b. Create a new user account or return an existing one.
                        User.findOne({googleId: profile.sub}).exec(function (err, user) {
                            if (err) return handleError("error logging in to google.", res);
                            if (user) {
                                user['access_token'] = customUtils.generateToken();
                                user.save(function (err) {
                                    if (err) return handleError("error logging in to google.", res);
                                    var resp = {data: user.toJSON()};
                                    resp.data['id'] = user._id;
                                    return res.status(201).json(resp);
                                })
                            } else {
                                //do a sign up
                                var id = customUtils.generateToken(8);
                                var date = new Date();
                                var generatedUsername = profile.name.split(" ")[0];
                                
                                var user = {
                                    "id": id,
                                    "gcmId": undefined,
                                    "name": profile.name,
                                    "username": generatedUsername,
                                    "email": profile.email,
                                    "password": undefined,
                                    "avatar": profile.picture.replace('sz=50', 'sz=200'),
                                    "avatarThumb": profile.picture,
                                    "userDesc": undefined,
                                    "tagline": undefined,
                                    "isVerified": false,
                                    "verification_token": customUtils.generateToken(),
                                    "last_seen": date.getTime(),
                                    "googleId": profile.sub
                                };

                                //res.status(200).send({message: 'ssup'});

                                qbchat.createUser({
                                    login: user.username,
                                    email: user.email,
                                    password: config.qb.defaultPassword,
                                    full_name: user.name,
                                    custom_data: user.avatar
                                }, function (err, newUser) {
                                    if (err) {
                                    //if (false) {
                                        console.log('qb chat signup error - ', err);
                                        responseObj["errors"] = ["There is a problem with chat server. Please contact Administrator."];
                                        res.status(409);
                                        res.json(responseObj);
                                        return;
                                    } else {
                                        if(newUser && newUser.id) user["qbId"] = newUser.id;
                                        user["access_token"] = customUtils.generateToken();
                                        User.create(user, function (err, user) {
                                            if (err) return handleError(res, err);
                                            if (user) {
                                                var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + user._id + "&token=" + user.verification_token;
                                                var msg = "Hello" + user.name + ",\n\n\nThank you for signing up with Zaprep. Kindly verify your email by clicking on the following link. This shall help us serve you better. \n\n " + urlLink + " \n\n\nLooking forward to providing you a great learning experience on Zaprep.\n\nRegards,\nZaprep Team";
                                                var subject = 'Welcome to Zaprep - Email Verification';
                                                // sendEmail(user.email, subject, msg);
                                                var date = new Date();
                                                var chatObj = {
                                                    "type": "newUser",
                                                    "username": user['username'],
                                                    "qbId": user['qbId'],
                                                    "dbId": user['id'],
                                                    "created": date.getTime(),
                                                    "id": date.getTime()
                                                };

                                                if (typeof user['gcmId'] !== 'undefined')
                                                    chatObj['gcmId'] = user['gcmId'];
                                                else
                                                    chatObj['gcmId'] = 'undefined';

                                                //notify.emit("wordForChat", chatObj);

                                                user['password'] = undefined;

                                                var resp = {data: user.toJSON()};
                                                resp.data['id'] = user._id;
                                                return res.status(201).json(resp);
                                            }
                                        });
                                    }
                                })
                            }
                        });
                    }
                }
            });
        }
    });
};

exports.signup = function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var name = customUtils.toTitleCase(req.body.name);
    var username = req.body.username.toLowerCase();
    var responseObj = {};
    var errors = [];

    if (!validator.isLength(name, 2, 50)) errors.push("Name must be between 2-50 characters");
    if (!name.match(/^[a-zA-Z ]*$/)) errors.push("Name must contain only alphabets");
    if (!validator.isLength(username, 6, 12)) errors.push("Username must be between 6-12 characters");
    if (!username.match(/^[a-zA-Z0-9_]*$/)) errors.push("Username must contain only alphabets, numbers or underscore");
    if (!validator.isLength(password, 8, 20)) errors.push("Password must be between 8-20 characters");
    if (!validator.isEmail(email))
        errors.push("Please enter a valid mail ID");
    else
        email = email.toLowerCase();

    if (!validator.isNull(req.body.userDesc))
        if (!validator.isLength(req.body.userDesc, 20)) errors.push("Description must be greater than 20 characters");

    //if (!validator.isNull(req.body.phone)) errors.push('Please enter your phone number');
    if (!validator.isNull(req.body.tagline))
        if (!validator.isLength(req.body.tagline, 0, 40)) errors.push("Tagline must be less than 40 characters");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        User.findOne({email: email}).exec(function (err, user) {
            if (err) return handleError('Error signing up. ', res);
            if (!user) {
                var hashedPassword = bcrypt.hashSync(req.body.password, 8);
                var id = customUtils.generateToken(8);
                var date = new Date();

                var user = {
                    "id": id,
                    "gcmId": req.body.gcmId,
                    "name": name,
                    "username": req.body.username,
                    "email": req.body.email,
                    "hashedPassword": hashedPassword,
                    "avatar": "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg",
                    "avatarThumb": "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg",
                    "userDesc": req.body.userDesc,
                    "tagline": req.body.tagline,
                    "isVerified": false,
                    "phone": req.body.phone,
                    "last_seen": date.getTime(),
                    "created": date.getTime()
                };
                qbchat.createUser({
                    login: user.username,
                    email: user.email,
                    password: config.qb.defaultPassword,
                    full_name: user.name,
                    custom_data: user.avatar
                }, function (err, newUser) {
                    if (err) {
                    //if (false) {
                        if(err.detail){
                           if(err.detail.email=="has already been taken.") {
                                console.log('qb chatty1 signup error - ', err);
                                responseObj["errors"] = ["Email id is already taken. Please choose a different one."];                               
                           }else if(err.detail.login=="has already been taken.") {
                                console.log('qb chatty2 signup error - ', err);
                                responseObj["errors"] = ["Username is already taken. Please choose a different one."];                               
                           }else{
                                console.log('qb chatty3 signup error - ', err);
                                responseObj["errors"] = ["There is a problem with chat server. Please contact Administrator."];                                
                           }
                        }else if(err.code==422){
                           if(err.message.message=="Failed to save the record") {
                                console.log('qb chatty4 signup error - ', err);
                                responseObj["errors"] = ["Username is already taken. Please choose a different one."];                               
                           }else{
                                console.log('qb chatty5 signup error - ', err);
                                responseObj["errors"] = ["There is a problem with chat server. Please contact Administrator."];                                
                           }
                        }else{
                            console.log('qb chatty6 signup error - ', err);
                            responseObj["errors"] = ["There is a problem with chat server. Please contact Administrator."];                           

                        }
                        res.status(409);
                        res.json(responseObj); 
                        return;
                    } else {
                        if(newUser && newUser.id) user["qbId"] = newUser.id;
                        var verification_token = customUtils.generateToken();
                        user['access_token'] = customUtils.generateToken();
                        user['verification_token'] = verification_token;
                        User.create(user, function (err, user) {
                            if (err) return handleError('error signing up.', res);
                            if (user) {
                                var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + user.id + "&token=" + verification_token;
                                var msg = "Hello " + user.name + ",\n\n" + "Welcome to Zapprep!\nWe have created your mentor profile for Zapprep app.\nPlease download app,login and edit your profile.  We request you to change your profile picture and add your two line bio (e.g M.S From USC USA, MBA From IIM Bangalore )on your profile.\n\n https://play.google.com/store/apps/details?id=in.zapprep.ZapPrep&hl=en \n\n We will get in touch with you soon regarding your service charges and other commercials involved.\nFor any query please write us on mentor@zapprep.in.\n\n Thanks and Regards\nMentor Care Team\n Zap prep Education Services\n Verification URL:" + urlLink;
                                var subject = 'Welcome to Zapprep - Welcome email';
                                //sendEmail(user.email, subject, msg);
                                var date = new Date();
                                var chatObj = {
                                    "type": "newUser",
                                    "username": user['username'],
                                    "qbId": user['qbId'],
                                    "dbId": user['id'],
                                    "created": date.getTime(),
                                    "id": date.getTime()
                                };
                                if (typeof user['gcmId'] !== 'undefined')
                                    chatObj['gcmId'] = user['gcmId'];
                                else
                                    chatObj['gcmId'] = 'undefined';

                                //notify.emit("wordForChat", chatObj);

                                user['hashedPassword'] = undefined;
                                user['verification_token'] = undefined;
                                user['providerData'] = undefined;

                                var notifObj = {
                                   user: user._id.toString(),
                                   name: user.name,
                                   id: date.getTime()
                                };
                                if(user.gcmId) notifObj['gcmId'] = user.gcmId;
                                notify.emit('welcome', notifObj);
                                var resp = {data: user.toJSON()};
                                resp.data['id'] = user._id;
                                return res.status(201).json(resp);
                            }
                        });
                    }
                })
            } else {
                responseObj["errors"] = ["Email id is already taken. Please choose a different one."];
                res.status(409);
                res.json(responseObj);
            }
        })
    }
};

exports.login = function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var responseObj = {};

    var errors = [];

    if (!validator.isLength(password, 8, 20)) errors.push("Password must be between 8-20 characters");
    if (!validator.isEmail(email)) errors.push("Invalid Email");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        User.findOne({email: email}).select('-verification_token -providerData').exec(function (err, user) {
            if (err) return handleError('error logging in.', res);
            if (user) {
                var hash = user.hashedPassword;
                if (bcrypt.compareSync(password, hash)) {
                    // user.body.results[0].value.isVerified != "true"
                    if (false) { // change later, not checking for verification at the moment
                        res.status(409);
                        res.json({"errors": ["Please verify your Email to login"]});
                    } else {
                        user['access_token'] = customUtils.generateToken();
                        user.save(function (err) {
                            if (err) return handleError('error saving user login info: ', user);
                            var resp = {data: user.toJSON()};
                            resp.data['hashedPassword'] = undefined;
                            resp.data['salt'] = undefined;
                            resp.data['id'] = user._id;
                            return res.status(201).json(resp);
                        });
                    }
                } else {
                    responseObj["errors"] = ["Entered password is incorrect"];
                    res.status(401);
                    return res.json(responseObj);
                }
            } else {
                responseObj["errors"] = ["No Account with the entered email exists"];
                res.status(503);
                return res.json(responseObj);
            }
        });
    }
};

exports.logout = function (req, res) {
    var user_search_obj = {};
    if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    if(user_search_obj) {
        User.findOne(user_search_obj).exec(function (err, user) {
            if (err) return handleError('error logging out.', err);
            if (user) {
                user['access_token'] = '';
                user.save(function (err) {
                    return res.status(200).json({data: {"success": "You have been successfully logged out."}});
                });
            } else return res.status(200).json({data: {success: "You have been successfully logged out."}});
        });
    } else return handleError('error finding user details', res);
};

exports.publicProfile = function (req, res) {
    var responseObj = {};
    var allowUpdate;
    var user_search_obj = {};
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    else if (req.query._id) user_search_obj['_id'] = req.query._id;
    else if (req.query.userId) user_search_obj['_id'] = req.query.userId;
    if (validator.isNull(user_search_obj)) {
        responseObj["errors"] = ["Please specify the User ID"];
        return res.status(503).json(responseObj);
    } else {
        User.findOne(user_search_obj).select('-hashedPassword -salt -verification_token -providerData').exec(function (err, user) {
            responseObj['data'] = user.toJSON();
            responseObj.data['id'] = user._id;
            responseObj['data']['allowUpdate'] = false;
            responseObj['data']['paths'] = [];
            Enrollment.find({producer: user._id, isEnrolled: true}).deepPopulate('path path.producer').exec(function (err, enrolls) {
                if (err) return handleError('Error finding user subscriptions', res);
                enrolls.forEach(function (enroll) {
                    var en = enroll.toJSON();
                    if (en.path.producer) {
                        var path = en.path;
                        path['isOwner'] = en.path.producer.toString() == user._id.toString();
                        path['allowEnroll'] = false;
                        path['id'] = en.path._id;
                        delete path.concepts;
                        responseObj.data.paths.push(path);
                    }
                });
                responseObj['more'] = false;
                return res.status(200).json(responseObj);
            });
        });
    }
};

exports.userProfile = function (req, res) {
    var user_search_obj = {};
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    else if (req.query.userId) user_search_obj['_id'] = req.query.userId;
    var allowUpdate;

    var getUserInfo = function (user_search_obj) {
        User.findOne(user_search_obj).select('-hashedPassword -salt -providerData').exec(function (err, user) {
            if (err) return handleError('error finding user with given id.', res);
            if (!user) return res.status(404).json({errors: ['User not found with given id.']});
            else {
                //TODO: refactor and send profile info here
                var resp = {data: user.toJSON(), more: false};
                resp.data['id'] = user._id;
                resp.data['allowUpdate'] = user_search_obj._id ? user._id.toString() == user_search_obj._id.toString() :
                    user.access_token.toString() == user_search_obj.access_token;
                resp.data['paths'] = [];
                Enrollment.find({producer: user._id, isEnrolled: true}).deepPopulate('path path.producer').exec(function (err, enrolls) {
                    if (err) return handleError('error finding subscription details', res);
                    if (enrolls) {
                        _.forEach(enrolls, function (enroll) {
                            if (enroll.path) {
                                var en = enroll.toJSON();
                                var path = _.clone(en.path);
                                delete path.concepts;
                                path['id'] = path._id;
                                path['isOwner'] = path.producer && path.producer._id ? path.producer._id.toString() == user._id.toString() : false;
                                resp.data.paths.push(path);
                            }
                        });
                    }
                    resp['more'] = false;
                    return res.status(200).json(resp);
                });
            }
        });
    };

    if (validator.isNull(user_search_obj)) {
        return res.status(503).json({errors: ["Please specify the User ID"]});
    } else {
        getUserInfo(user_search_obj)
    }
};

exports.updateUserProfile = function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = [];
    var user_search_obj = {};
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;

    if (!validator.isNull(reqBody.name))
        if (!reqBody.name.match(/\w*/g)) errors.push("Name contains illegal characters");

    if (!validator.isNull(reqBody.userDesc))
        if (!validator.isLength(req.body.userDesc, 20)) errors.push("Description must be greater than 20 characters");

    if (!validator.isNull(reqBody.tagline))
        if (!validator.isLength(reqBody.tagline, 0, 40)) errors.push("Tagline must be less than 40 characters");

    if (!validator.isNull(req.files.avatar))
        if (!validator.isImage(req.files.avatar.mimetype)) errors.push("Avatar should be an image type");

    if (!validator.isNull(reqBody.email)) {
        errors.push("Email is invalid");
    }

    if (!validator.isNull(reqBody.username)) {
        if (!validator.isLength(reqBody.username, 6, 20)) {
            errors.push("Username must be between 6-20 characters");
        } else if (!reqBody.username.match(/^[a-zA-Z0-9_]*$/)) {
            errors.push("Username must contain only alphabets, numbers or underscore")
        }
    }

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        if (user_search_obj) User.findOne(user_search_obj).select('-hashedPassword -salt -providerData').exec(function (err, user) {
            if (err) return handleError('error getting user profile.', res);
            if (user) {
                if (req.files.avatar) {
                    customUtils.upload(req.files.avatar, function (avatarInfo) {
                        if (reqBody.userDesc) user['userDesc'] = reqBody.userDesc;
                        if (reqBody.tagline) user['tagline'] = reqBody.tagline;
                        if (reqBody.name) user['name'] = reqBody.name;
                        if (reqBody.gcmId) user['gcmId'] = reqBody.gcmId;
                        if (reqBody.email || reqBody.username) {
                            user['verification_token'] = customUtils.generateToken();
                            user['isVerified'] = false;
                            if (reqBody.username) user['username'] = reqBody.username.toLowerCase();
                            if (reqBody.email) user['email'] = reqBody.email.toLowerCase();
                        }
                        var oldPhoto = null;
                        if (avatarInfo && req.files.avatar) {
                            if (avatarInfo.url) user['avatar'] = avatarInfo.url;
                            if(user.avatarThumb) {
                                oldPhoto = user.avatarThumb;
                            }
                            if (avatarInfo.urlThumb) user['avatarThumb'] = avatarInfo.urlThumb;
                        }
                        user.save(function (err) {
                            if (req.files.avatar) {
                                //TODO: get user relation mapping and fix this
                                console.log('sending profile update notification.');
                                notify.getRecieversAndUpdatePhotos('user', user._id.toString(), user['avatarThumb'], user.gcmId);
                            }
                            console.log('saved user patch request. got gcm id - ', user.gcmId);
                            if (reqBody.gcmId) {
                                var date = new Date();
                                var notifObj = {
                                    "link": user._id.toString(),
                                    "title": "Your Mentor is online",
                                    "text": user.name + " is online, do not miss out on this engagement!",
                                    "photo": user.avatarThumb,
                                    "is_read": 'false',
                                    "is_clicked": 'false',
                                    "id": date.getTime()
                                };
                                notify.emit('mentorMaybe', notifObj);
                                var chatObj = {
                                    "type": "newGcmId",
                                    "username": user.username,
                                    "created": date.getTime(),
                                    "id": date.getTime(),
                                    "gcmId": user['gcmId']
                                };
                                console.log('sending word for chat notification from update user profile - ', chatObj);
                                //notify.emit("wordForChat", chatObj);
                            }
                            if (err) return handleError('Error saving user profile', res);
                            if(user.qbId) {
                                qbchat.login(function(err, newUser){
                                    //if(!err) {
                                    if(false) {
                                        qbchat.updateUser(user.qbId, {
                                            login: user.username,
                                            email: user.email,
                                            full_name: user.name,
                                            custom_data: user.avatarThumb
                                        }, function (err, user) {
                                            if (err) {
                                                console.log('Error in updating chat user2 - ', err);
                                            } else console.log('Updated chat user - ', user);
                                        });
                                    } else console.log("Error logging in QB.");
                                });
                            }
                            if (reqBody.email) {
                                var urlLink = "http://api.pyoopil.com:3000/user/verify?user=" + user._id + "&token=" + user.verification_token;
                                var msg = "Hello! Your Email ID was changed. \nPlease click on the below link to verify your email, excuse our brevity.\n\n" + urlLink + " \n\nLooking forward to see you on Zaprep.\n\nBest Wishes,\nZapprep Team";
                                var subject = 'Email Change - Verify your new E-mail';
                                //sendEmail(user.email, subject, msg);
                            }
                            var resp = {data: user.toJSON()};
                            resp.data['id'] = user._id;
                            return res.status(200).json(resp);
                        })
                    });
                } else {
                    if (reqBody.email) {
                        var newToken = customUtils.generateToken();
                        user['email'] = reqBody.email.toLowerCase();
                        user['isVerified'] = false;
                        if (reqBody.username) user['username'] = reqBody.username.toLowerCase();
                        user['verification_token'] = newToken;
                    }
                    if (reqBody.userDesc) user['userDesc'] = reqBody.userDesc;
                    if (reqBody.tagline) user['tagline'] = reqBody.tagline;
                    if (reqBody.name) user['name'] = reqBody.name;
                    if (reqBody.username) user['username'] = reqBody.username.toLowerCase();
                    if (reqBody.gcmId) user['gcmId'] = reqBody.gcmId;
                    user.save(function (err) {
                        if (err) return handleError('Error saving user profile', res);
                        if (req.files.avatar) {
                            notify.getRecieversAndUpdatePhotos('user', user._id.toString(), user['avatarThumb'], user.gcmId)
                        }
                        console.log('saved user patch request. got gcm id - ', reqBody.gcmId, user.gcmId);
                        if (reqBody.gcmId) {
                            var date = new Date();
                            var chatObj = {
                                "type": "newGcmId",
                                "username": user.username,
                                "created": date.getTime(),
                                "id": date.getTime(),
                                "gcmId": user['gcmId']
                            };
                            console.log('sending word for chat notification from update user profile - ', chatObj);
                            //notify.emit("wordForChat", chatObj);
                        }
                        if(user.qbId) {
                            qbchat.login(function(err, newUser){
                                //if(!err) {
                                if(!false) {
                                    qbchat.updateUser(user.qbId, {
                                        login: user.username,
                                        email: user.email,
                                        full_name: user.name,
                                        custom_data: user.avatarThumb
                                    }, function (err, user) {
                                        if (err) {
                                            console.log('Error in updating chat user2 - ', err);
                                        } else console.log('Updated chat user - ', user);
                                    });
                                } else console.log("Error logging in QB.");
                            });
                        }
                        if (reqBody.email) {
                            var urlLink = "http://api.pyoopil.com:3000/user/verify?user=" + user._id + "&token=" + user.verification_token;
                            var msg = "Hello! Your Email ID was changed. \nPlease click on the below link to verify your email, excuse our brevity.\n\n" + urlLink + " \n\nLooking forward to see you on Zaprep.\n\nBest Wishes,\nZapprep Team";
                            var subject = 'Email Change - Verify your new E-mail';
                        //sendEmail(user.email, subject, msg);
                        }
                        var resp = {data: user.toJSON()};
                        resp.data['id'] = user._id;
                        return res.status(200).json(resp);
                    })
                }
            } else return handleError('User with given id not found', res);
        });
        else return handleError('error finding user details. please retry logging in.');
    }
};

exports.enrollUser = function (req, res) {
    if (!req.body.pathId && !req.body.accessId) {
        res.status(400);
        res.json({"errors": ["Send either one of pathId or accessId"]});
        return;
    }
    var path_search_obj = {};
    //if (validator.isNull(req.body.paymentId)) errors.push("Payment Transaction Id not found.");
    //if (validator.isNull(req.body.amount)) errors.push("Please specify the amount.");
    if (req.body.accessId && !req.body.pathId) {
        // TODO: Find path record with given access id and then implement enrollintocourse here
        path_search_obj['accessId'] = req.body.accessId;
    } else if (req.body.pathId) {
        path_search_obj['_id'] = req.body.pathId;
    }
    var user_search_obj = {};
    console.log("enroll user req body - ", req.body, req.query);
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    User.findOne(user_search_obj).exec(function (err, user) {
        if (err || !user) {
            console.log('err 1 - ', err);
            return handleError('Error finding user details', res);
        }
        Path.findOne(path_search_obj).deepPopulate('concepts').exec(function (err, path) {
            if (err || !path) {
                console.log('err 2 - ', err);
                return handleError('Error finding path with given access id', res);
            }
            Enrollment.findOne({path: path._id, producer: user._id, isEnrolled: true}).exec(function (err, enroll) {
                if (err) {
                    console.log('err 3 - ', err);
                    return handleError('Error finding enrollment data', res);
                }
                if (enroll) {
                    return res.status(201).json({data: {success: 'You have been successfully enrolled in course.'}});
                } else {
                    if (path.coursePaid || req.body.paymentId) {
                        Payment.findOne({paymentId: req.body.paymentId}).exec(function (err, payment) {
                            if (err || !payment) {
                                console.log('err 4 - ', err);
                                return handleError('Error finding payment details', res);
                            }
                            var expiresOn = (new Date()).getTime() + config.mentorSubscription.time;
                            var enrollment_data = {
                                path: path._id,
                                producer: user._id,
                                expiresOn: new Date(expiresOn),
                                isMentorPaid: path.isMentorPaid,
                                isCoursePaid: path.isCoursePaid,
                                payment_ref: payment._id,
                                isEnrolled: true
                            };
                            Enrollment.create(enrollment_data, function (err, enroll) {
                                if (err) {
                                    console.log('err 5 - ', err);
                                    return handleError('Error creating subscription details', res);
                                }
                                var rooms = [];
                                path.concepts.forEach(function (concept) {
                                    if (concept.qbId) rooms.push(concept.qbId);
                                });
                                if (path.mentorRoomId) rooms.push(path.mentorRoomId);
                                if (path.qbId) rooms.push(path.qbId);
                                //commented by yash from 938-940
                                // var QB = qbchat.extraQB();
                                // qbchat.extraInit();
                                async.each(rooms, function (room, cb) {
                                    qbchat.addUserToRoom(room, [user.qbId], function (err, result) {
                                    if (err) console.log('Error adding user to room - ', err);
                                    else console.log('Added user to room - ', result);
                                    ////callback(err);
                                    //    ; //ignore if joining a room fails
                                    //else
                                    cb();
                                });
                                    //commented by yash from 950-958
                                    // qbchat.extraAddUserToRoom(QB, room, [parseInt(user.qbId)], config.qb.params, function (err, result) {
                                    //     if (err) console.log('Error adding user to room - ', err);
                                    //     else console.log('Added user to room - ', result);
                                    //     ////callback(err);
                                    //     //    ; //ignore if joining a room fails
                                    //     //else
                                    //     cb();
                                    // })
                                }, function (err) {
                                    if (err) {
                                        console.log('err 6 - ', err);
                                        return handleError('Error adding users to mentor rooms.', res);
                                    }
                                    if (!path.newStudents) path.newStudents = 0;
                                    if (!path.studentCount) path.studentCount = 0;
                                    path.studentCount++;
                                    path.newStudents++;
                                    var amountInPaise = parseFloat(payment.amount) * 100;
                                    customUtils.captureRazorPayment(payment.paymentId, amountInPaise);
                                    path.save(function (err) {
                                        console.log('err 7 - ', err);
                                        return res.status(201).json({data: {success: 'You have been successfully enrolled in course.'}});
                                    })
                                });
                            })
                        });
                    } else {
                        var expiresOn = (new Date()).getTime() + config.mentorSubscription.time;
                        var enrollment_data = {
                            path: path._id,
                            producer: user._id,
                            expiresOn: new Date(expiresOn),
                            isMentorPaid: path.isMentorPaid,
                            isCoursePaid: path.isCoursePaid,
                            isEnrolled: true
                        };
                        Enrollment.create(enrollment_data, function (err, enroll) {
                            if (err) return handleError('Error creating subscription details', res);
                            var rooms = [];
                            path.concepts.forEach(function (concept) {
                                if (concept.qbId) rooms.push(concept.qbId);
                            });
                            if (path.mentorRoomId) rooms.push(path.mentorRoomId);
                            if (path.qbId) rooms.push(path.qbId);
                            async.each(rooms, function (room, cb) {
                                qbchat.addUserToRoom(room, [user.qbId], function (err, result) {
                                    if (err) console.log('Error adding user to room - ', err);
                                    else console.log('Added user to room - ', result);
                                    ////callback(err);
                                    //    ; //ignore if joining a room fails
                                    //else
                                    cb();
                                });
                            }, function (err) {
                                if (err) return handleError('Error adding users to mentor rooms.', res);
                                if (!path.newStudents) path.newStudents = 0;
                                if (!path.studentCount) path.studentCount = 0;
                                path.studentCount++;
                                path.newStudents++;
                                path.save(function (err) {
                                    return res.status(201).json({data: {success: 'You have been successfully enrolled in course.'}});
                                })
                            });
                        })
                    }
                }
            })
        });
    });
};

exports.getLearningPaths = function (req, res) {
    var responseObj = {};
    var search_obj = {};
    if (req.user && req.user._id) search_obj['_id'] = req.user._id;
    else if (req.query.access_token) search_obj['access_token'] = req.query.access_token;
    if (search_obj) {
        User.findOne(search_obj).exec(function (err, user) {
            if (err) callback(err, null);
            async.parallel([
                function (callback) {
                    Enrollment.find({producer: user._id, isEnrolled: true}).deepPopulate('path path.producer producer path.concepts path.concepts.objects').exec(function (err, enrolls) {
                        if (err) callback(err, null);
                        var paths = [];
                        enrolls.forEach(function (enroll) {
                            var en = enroll.toJSON();
                            if (en.path) {
                                var path = _.clone(en.path);
                                path['allowEnroll'] = false;
                                path['isOwner'] = path.producer._id ? path.producer._id.toString() == user._id.toString() : false;
                                path['id'] = path._id;
                                paths.push(path);
                            }
                        });
                        callback(null, paths);
                    })
                },
                function (callback) {
                    Path.find({producer: user._id}).deepPopulate('concepts concepts.objects producer').exec(function (err, paths) {
                        if (err) callback(err, null);
                        var resp = [];
                        paths.forEach(function (path) {
                            var obj = path.toJSON();
                            obj['allowEnroll'] = false;
                            obj['id'] = path._id;
                            obj['isOwner'] = true;
                            resp.push(obj);
                        });
                        callback(null, resp);
                    })
                },
                function (callback) {
                    var params = {
                        'login': user.username,
                        'password': 'pyoopilDevsRock'
                    };
                    /**
                     * Closure : get dialogs of the specified offset
                     * @param limit
                     * @param offset
                     * @returns {!Promise}
                     */
                    function getDialogs(limit, offset) {
                        var theRooms = kew.defer()
                        QBconsumer.chat.dialog.list({limit: limit, skip: offset}, function (err, res) {
                            if (err)
                                theRooms.reject(err)
                            else {
                                //var rooms = res.items.map(function (room) {
                                //    return room.xmpp_room_jid;
                                //})
                                theRooms.resolve(res.items)
                            }
                        });
                        return theRooms
                    }

                    /**
                     * Closure : get all dialogs promise list
                     * @returns {!Promise}
                     */
                    function getAllDialogPromises() {
                        var promiseListArray = []
                        var promiseList = kew.defer()
                        var offset = 0

                        QBconsumer.chat.dialog.list({limit: 1, skip: 0}, function (err, res) {
                            if (err)
                                promiseList.reject(err)
                            else {
                                var totalCount = res.total_entries
                                var remaining = 0
                                do {
                                    promiseListArray.push(getDialogs(400, offset))
                                    offset += 400
                                    remaining = totalCount - offset;
                                } while (remaining > 0)
                                promiseList.resolve(promiseListArray)
                            }
                        });
                        return promiseList
                    }

                    /**
                     * disable require cache to get a new QB object for consumer
                     * so that the sessions dont clash!
                     * TODO: fine a better way to do this. perhaps create an instance of QB
                     ***/
                    // Object.keys(require.cache).forEach(function (key) {
                    // //delete require.cache[key]
                    // if (key.indexOf("node_modules/quickblox") > -1) {
                    //     //console.log(key)
                    //     delete require.cache[key]
                    // }
                    // });

                    var QBconsumer = require('quickblox');
                    QBconsumer.init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);
                    QBconsumer.createSession(params, function (err, session) {
                        //if (err) {
                        if (false) {
                        //log.error({customMessage: "createSession failed for user", username: username, qbError: err})
                            callback(err, null);
                        } else {
                            getAllDialogPromises()
                                .then(function (promiseArrayList) {
                                    return kew.all(promiseArrayList)
                                })
                                .then(function (promiseResults) {
                                    var allItemsList = [];
                                    promiseResults.forEach(function (item) {
                                        allItemsList = allItemsList.concat(item)
                                    });
                                    callback(null, allItemsList);
                                })
                                .fail(function (err) {
                                    //callback(err, null);
                                    callback(null, []);
                                })
                        }
                    });
                }], function (err, results) {
                if (err) {
                    responseObj["errors"] = ["A server error occured"];
                    console.log(err);
                    res.status(503);
                    res.json(responseObj);
                } else if (results.length >= 3) {
                    responseObj = {data: results[0]};
                    var own_paths = results[1];
                    own_paths.forEach(function (own_path) {
                        var idx = findIndexOfKeyInObjectArr(results[0], '_id', own_path._id);
                        if (idx == -1) responseObj.data.push(own_path);
                    });
                    responseObj['dialogs'] = results[2];
                    responseObj["obsoleteVersion"] = config.appUpdate.obsoleteVersion;
                    responseObj["more"] = false;
                    return res.status(200).json(responseObj);
                }
            });
        });
    } else return handleError('error finding user details. please retry logging in.', res);
    //TODO: find paths registered with logged in user and send
};

exports.verify = function (req, res) {
    var token = req.query.token;
    var userId = req.query.user;
    User.findOne({_id: userId}).exec(function (err, user) {
        if (err) return handleError('error finding user with given id.', res);
        if (!user) return handleError('error finding user with given id.', res);
        else {
            if (user.isVerified) {
                return res.render('verification', {
                    "title": "Verification Complete - Zaprep",
                    "heading": "This email ID is already verified"
                })
            } else if (token == user.verification_token) {
                user['isVerified'] = true;
                user['verification_token'] = undefined;
                user.save(function (err) {
                    if (err) return handleError('error saving user', res);
                    return res.render('verification', {
                        "title": "Verification Complete - Zaprep",
                        "heading": "Thank you for verifying your email"
                    });
                })
            } else {
                return res.render('verification', {
                    "title": "Verification Failed - Zaprep",
                    "heading": "Verification token didn't match. Try a correct token or request for a new verification token from Zaprep."
                });
            }
        }
    })
};

exports.post_verify = function(req, res) {
    var user_search_obj = {};
    if(req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if(req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    else if(req.query._id) user_search_obj['_id'] = req.query._id;
    else if(req.query.id) user_search_obj['id'] = req.query.id;

    if(user_search_obj) {
        User.findOne(user_search_obj).select('verification_token email').exec(function(err, user){
            if(err) return handleError('error finding user details', res);
            var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + user._id + "&token=" + user.verification_token;
            var msg = "Hello! You requested for verification email. \nPlease click on the below link to verify your email, excuse our brevity.\n\n" + urlLink + " \n\nLooking forward to see you on Zaprep.\n\nBest Wishes,\nZaprep Team";
            var subject = 'Verify your E-mail'
            //sendEmail(user.email, subject, msg);
            return res.status(201).json({
                "data": {
                    "success": "Verification Email sent to registered Email ID"
                }
            });
        });
    } else return handleError('error finding user details', res);
};

exports.vimeo = function (req, res) {
    return res.render('verification', {
        name: "https://player.vimeo.com/video/" + req.query.id
    })
};

exports.dummy = function (req, res) {
    return res.render('dummyVimeo');
};

exports.forgotPassword = function (req, res) {
    var email = req.body.email;
    var errors = [];
    if (validator.isNull(email)) errors.push("Please specify your email");
    if (errors.length > 0) {
        return handleError(errors, res);
    } else {
        User.findOne({email: email}).exec(function (err, user) {
            if (err || !user) return handleError('This email ID is not registered with us. Please enter the registered email ID.', res);
            var newPass = customUtils.generateToken();
            user['hashedPassword'] = bcrypt.hashSync(newPass, 8);
            user.save(function (err) {
                if (err) return handleError('error saving password for user.', res);
                var msg = "Hello!\nYour new password for Zaprep is : " + newPass + "\n\nKindly note that this is a system generated password and we strongly recommend that you change your password once you login using this.\n\nLooking forward to providing you a great learning experience on Zaprep.\n\nRegards,\nZapprep Team";
                var subject = 'Request for New Password';
                //sendEmail(user.email, subject, msg);
                return res.status(201).json({data: {success: "Please check your Email for the new password"}});
            })
        })
    }
};

exports.error = function (req, res) {
    var d = new Date();
    var payload = {
        'userId': req.user._id,
        'timestamp': d.getTime(),
        'dump': req.body.dump
    };
    //TODO: make errors table model and update here
    Errors.create(payload, function (err, error) {
        if (err) return handleError('error submitting error details', res);
        return res.status(201).json({data: {success: 'The error report was submitted successfully, thank you.'}})
    })
};

exports.updatePassword = function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = [];
    var user_search_obj = {};
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;

    if (!validator.isLength(reqBody.newPass, 8, 20)) errors.push("Password must be between 8-20 characters");
    if (!validator.isNull(reqBody.currPass)) {
        if (reqBody.newPass != reqBody.rePass) {
            errors.push("Passwords don't match!");
        } else if (user_search_obj) {
            User.findOne(user_search_obj).exec(function (err, user) {
                if (err) return handleError('User not found', res);
                if (!bcrypt.compareSync(reqBody.currPass, user.hashedPassword)) {
                    errors.push("Entered password does not match your current password");
                }
                if (errors.length > 0) {
                    responseObj["errors"] = errors;
                    res.status(422);
                    res.json(responseObj);
                } else {
                    user['hashedPassword'] = bcrypt.hashSync(reqBody.newPass, 8);
                    user['access_token'] = '';
                    user.save(function (err) {
                        if (err) return handleError('Error saving updated password.', res);
                        var resp = {data: user.toJSON()};
                        resp.data['id'] = resp.data['_id'];
                        delete resp.data.hashedPassword;
                        delete resp.data.salt;
                        return res.status(200).json(resp)
                    });
                }
            });
        } else return handleError('error finding user details. please try logging in again.', res);
    }
};

var generateTokenAndLogin = function (user, res) {
    user['access_token'] = customUtils.generateToken();
    user.save(function (err) {
        if (err) return handleError('Error logging in. Try again later.', res);
        var resp = {data: user.toJSON()};
        resp.data['id'] = user._id;
        delete user.hashedPassword;
        delete user.salt;
        return res.status(201).json(resp);
    });
};

var signUpFreshGoogleUser = function (payload, avatar, avatarThumb, res) {
    var responseObj = {};
    var id = customUtils.generateToken(8);
    var date = new Date();

    //ensure max length is 12 chars
    var generatedUsername = payload['name'].split(" ")[0].toLowerCase().substring(0, 7) + "_" + customUtils.generateToken(2);

    //use a dummyPassword to throw "incorrect password"
    //error just in case someone tries to normal login
    //can't keep password field undefined
    var user = {
        "gcmId": undefined,
        "name": payload['name'],
        "username": generatedUsername,
        "email": payload['email'],
        "password": "dummyPassword",
        "avatar": avatar,
        "avatarThumb": avatarThumb,
        "userDesc": undefined,
        "tagline": undefined,
        "isVerified": true,
        "last_seen": date.getTime(),
        "google": payload['sub'],
        "created": date.getTime()
    };
    qbchat.createUser({
        login: user.username,
        email: user.email,
        password: config.qb.defaultPassword,
        full_name: user.name,
        custom_data: user.avatar
    }, function (err, newUser) {
        //if (false) {
        if (err) {
            console.log('qb chat signup error - ', err);
            responseObj["errors"] = ["There is a problem with chat server. Please contact Administrator."];
            res.status(409);
            res.json(responseObj);
        } else {
            if (newUser && newUser.id) user["qbId"] = newUser.id;
            user['access_token'] = customUtils.generateToken();
            User.create(user, function (err, user) {
                if (err) return handleError('error creating user.', res);
                var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + user._id + "&token=" + user.verification_token;
                var msg = "Hello" + user.name + ",\n Thank you for signing up with Pyoopil. Kindly verify your email by clicking on the following link. This shall help us serve you better. \n " + urlLink + " \nLooking forward to providing you a great learning experience on Pyoopil.\n\nRegards,\nPyoopil Team";
                var subject = 'Welcome to Pyoopil - Email Verification';
                //sendEmail(user.email, subject, msg);
                var chatObj = {
                    "type": "newUser",
                    "username": user['username'],
                    "qbId": user['_id'],
                    "dbId": user['_id'],
                    "created": (new Date()).getTime(),
                    id: (new Date()).getTime()
                };
                if (typeof user['gcmId'] !== 'undefined')
                    chatObj['gcmId'] = user['gcmId'];
                else
                    chatObj['gcmId'] = 'undefined';

                //notify.emit("wordForChat", chatObj);

                user['hashedPassword'] = undefined;

                var notifObj = {
                    user: user._id.toString(),
                    name: user.name,
                    id: date.getTime()
                };
                if(user.gcmId) notifObj['gcmId'] = user.gcmId;
                notify.emit('welcome', notifObj);
                user['changeUsername'] = true;
                var resp = {data: user.toJSON()};
                resp.data['id'] = user._id;
                return res.status(201).json(resp);
            });
        }
    })
};

var signUpFreshFacebookUser = function (payload, avatar, avatarThumb, res, changeEmail, accessToken) {
    var responseObj = {};
    var date = new Date();

    //ensure max length is 12 chars
    var generatedUsername = payload['name'].split(" ")[0].toLowerCase().substring(0, 7) + "_" + customUtils.generateToken(2);

    //use a dummyPassword to throw "incorrect password"
    //error just in case someone tries to normal login
    //can't keep password field undefined
    var user = {
        "gcmId": undefined,
        "name": payload['name'],
        "username": generatedUsername,
        "email": payload['email'],
        "password": "dummyPassword",
        "avatar": avatar,
        "avatarThumb": avatarThumb,
        "userDesc": undefined,
        "tagline": undefined,
        "isVerified": false,
        verification_token: customUtils.generateToken(),
        "last_seen": date.getTime(),
        "facebookId": payload['id'],
        "created": date.getTime()
    };

    qbchat.createUser({
        login: user.username,
        email: user.email,
        password: config.qb.defaultPassword,
        full_name: user.name,
        custom_data: user.avatar
    }, function (err, newUser) {
        if (err) {
        //if (false) {
            console.log('qb chat signup error - ', err);
            responseObj["errors"] = ["There is a problem with chat server. Please contact Administrator."];
            res.status(409);
            res.json(responseObj);
        } else {
            if (newUser && newUser.id) user["qbId"] = newUser.id;
            user['access_token'] = customUtils.generateToken();
            User.create(user, function (err, user) {
                if (err) return handleError('error creating user', res);
                extractFacebookFriends(user._id, accessToken);
                //---------------merge facebook friends end --------------------
                var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + user._id + "&token=" + user.verification_token;
                var msg = "Hello" + user.name + ",\n Thank you for signing up with Pyoopil. Kindly verify your email by clicking on the following link. This shall help us serve you better. \n " + urlLink + " \nLooking forward to providing you a great learning experience on Pyoopil.\n\nRegards,\nPyoopil Team";
                var subject = 'Welcome to Pyoopil - Email Verification';
                //sendEmail(user.email, subject, msg);
                var date = new Date();
                var chatObj = {
                    "type": "newUser",
                    "username": user['username'],
                    "qbId": user['qbId'],
                    "dbId": user['_id'],
                    "created": (new Date()).getTime()
                };
                if (typeof user['gcmId'] !== 'undefined')
                    chatObj['gcmId'] = user['gcmId'];
                else
                    chatObj['gcmId'] = 'undefined';

                //notify.emit("wordForChat", chatObj);

                user['hashedPassword'] = undefined;

                var notifObj = {
                    user: user._id.toString(),
                    name: user.name,
                    id: date.getTime()
                };
                if(user.gcmId) notifObj['gcmId'] = user.gcmId;
                notify.emit('welcome', notifObj);
                user['changeUsername'] = true;
                user['changeEmail'] = changeEmail;
                var resp = {data: user.toJSON()};
                resp.data['id'] = user._id;
                return res.status(201).json(resp);
            });
        }
    });
};

var sendEmail = function (email, subject, msg) {
    var data = {
        from: 'Pyoopil Team <no-reply@pyoopil.com>',
        to: email,
        subject: subject,
        text: msg
    };

    mailgun.messages().send(data, function (error, body) {
        if (error) {
            console.log(error)
        }
    });
};

var extractFacebookFriends = function (userId, accessToken) {
    var friendsUrl = "https://graph.facebook.com/v2.3/me/friends?access_token=" + accessToken;
    var friendsData = [];

    var getFriends = function (theUrl) {
        request.get({url: theUrl, json: true}, function (err, response, payload) {
            if (response.statusCode == 200) {
                if (payload.data && payload.data.length > 0) {
                    //we have a few friends to store
                    friendsData.push.apply(friendsData, payload.data)
                }
                if (payload.paging) {
                    //payload.paging.previous means this is the last page
                    if (payload.paging.previous) {
                        //this is the last page, all friendsData is finally here
                        User.findOne({_id: userId}).exec(function (err, user) {
                            if (!err && user) {
                                if (!user.providerData) user['providerData'] = [];
                                user.providerData.push({name: 'facebookFriends', data: friendsData});
                                user.save(function (err) {
                                    if (!err) console.log('friend list saved');
                                });
                            }
                        });
                    } else {
                        getFriends(payload.paging.next)
                    }
                }
            }
        })
    };
    getFriends(friendsUrl)
};

function handleError(message, res) {
    res.status(422);
    res.json({"errors": [message]});
}

function findIndexOfKeyInObjectArr(obj_arr, key, val) {
    for (var item = 0; item < obj_arr.length; item++) {
        if (obj_arr[item][key] && obj_arr[item][key] == val) return item;
    }
    return -1;
}

exports.onlineStatusInActive = function(req, res) {
    var user_search_obj = {};
    if(req.body && req.body.access_token) user_search_obj['access_token'] = req.body.access_token;

    if (user_search_obj) {
        User.findOne(user_search_obj).exec(function (err, user) {
            if (err || !user) return handleError('Error getting users', res);
                user['onlineStatus'] = false;
                user.save(function (err) {
                    if (err) return handleError('Error saving online inactive status.', res);
                    var resp = {};
                    resp['message'] = "success";
                    return res.status(200).json(resp)
                });
        });
    } else return res.status(422).json({errors: ['No users found']});
};

exports.onlineStatusActive = function(req, res) {
    var user_search_obj = {};
    if(req.body && req.body.access_token) user_search_obj['access_token'] = req.body.access_token;

    if (user_search_obj) {
        User.findOne(user_search_obj).exec(function (err, user) {
            if (err || !user) return handleError('Error getting users', res);
                user['onlineStatus'] = true;
                user.save(function (err) {
                    if (err) return handleError('Error saving online inactive status.', res);
                    var resp = {};
                    resp['message'] = "success";
                    return res.status(200).json(resp)
                });
        });
    } else return res.status(422).json({errors: ['No users found']});
};