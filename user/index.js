'use strict';

var express = require('express');
var controller = require('./user.controller');
var config = require('../config');
var passport = require('passport');
var qbchat = require('../qbchat');
var multer = require('multer');
var router = express.Router();

router.post('/auth/google', controller.google_login);
router.post('/auth/facebook', controller.facebook_login);
router.post('/auth/facebook2', controller.facebook2_login);
router.post('/auth/google2', controller.google2_login);
router.post('/signup', controller.signup);
router.post('/login', controller.login);
router.post('/logout', passport.authenticate('bearer', {session: false}), controller.logout);
router.get('/', function (req, res, next) {
  if (req.query.access_token) next();
  else next('route');
}, passport.authenticate('bearer', {session: false}), controller.userProfile);
router.get('/', controller.publicProfile);
router.patch('/', passport.authenticate('bearer', {session: false}), multer(), function (req, res, next) {
    qbchat.getSession(function (err, session) {
        if (err) {
            console.log("Session dropped. Recreating session - ", err);
            qbchat.createSession(function (err, result) {
                if (err) {
                    console.log("Error creating chat session. Trying to Recreate session - ", err);
                    res.status(409);
                    res.json({"errors": ["Can't connect to the chat server, try again later"]});
                } else next();
            });
        } else {
            console.log('Got session info from quickblox - ', session);
            next();
        }
    });
}, controller.updateUserProfile);
router.post('/enroll', passport.authenticate('bearer', {session: false}), controller.enrollUser);
router.get('/paths', passport.authenticate('bearer', {session: false}), controller.getLearningPaths);
router.get('/verify', controller.verify);
router.post('/verify', controller.post_verify);
router.get('/vimeo', controller.vimeo);
router.get('/dummy', controller.dummy);
router.post('/error', passport.authenticate('bearer', {session: false}), controller.error);
router.post('/forgotPassword', controller.forgotPassword);
router.patch('/password', passport.authenticate('bearer', {session: false}), controller.updatePassword);

router.post('/osactive', controller.onlineStatusActive);
router.post('/osinactive', controller.onlineStatusInActive);

module.exports = router;
