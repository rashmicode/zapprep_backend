'use strict';

var express = require('express');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var path = require('path');
var passport = require('passport');
var busboy = require('connect-busboy');
var session = require('express-session');
//TODO: refactor user model file location here
var User = require('../user/user.model');

module.exports = function (app) {
  var env = app.get('env');
  app.use(compression());
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());
  app.use(passport.initialize());
  app.use(passport.session());
  passport.serializeUser(function (user, done) {
    done(null, user._id);
  });
  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });
  var firebaseConfig = {
    apiKey: "some-api-key",
    authDomain: "some-app.firebaseapp.com",
    databaseURL: "https://some-app.firebaseio.com",
    storageBucket: "some-app.appspot.com"
  };

};
