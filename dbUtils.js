var config = require('./config.js');
//var constants = require('./constants')

//var crypto = require('crypto');
//var randomString = require('random-string');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);
//var qbchat = require('./qbchat.js');

//var QB = require('quickblox');
//QB.init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);

//var fs = require('fs'),
//    S3FS = require('s3fs'),
//    s3fsImpl = new S3FS(config.s3.bucket, {
//        accessKeyId: config.s3.access,
//        secretAccessKey: config.s3.secret
//    });

//var Firebase = require("firebase");
//var myFirebaseRef = new Firebase(config.firebase.url, config.firebase.secret);

var kew = require('kew')
//var date = new Date()

/**
 * Inserts the id into every response object
 * extracted from orchestrate's "path" key
 * @param results
 * @returns {Array}
 */
function injectId(results) {
    //lets determine if this is a graph lucene search, then the response becomes this:
    //I'm not sure if this is the way to go
    //{ path:
    //{ kind: 'relationship',
    //    source: [Object],
    //    destination: [Object],
    //    relation: 'isConsumed',
    //    ref: '5ba93c9db0cff93f',
    //    reftime: 1456097864242 },
    //    score: 16.222095489501953,
    //        kind: 'relationship',
    //    reftime: 1456097864242 } ]

    //var response

    //if(results.body.results.length > 0 && results.body.results[0].path.kind == "relationship") {
    //    return []
        //response = results.body.results.map(function(aGraphLuceneResult) {
        //    var value = aGraphLuceneResult.value
        //    value["id"] = aResult.path.key
        //    return value
        //})
    //} else {
        response = results.body.results.map(function (aResult) {
            var value = aResult.value
            value["id"] = aResult.path.key
            return value
        })
    //}
    return response
}

function createFieldQuery(field, value) {
    var theFieldQuery = "value." + field + ":`" + value + "`"
    return theFieldQuery
}

function andQueryJoiner(queries) {
    var returnQuery = ""
    queries.forEach(function (query) {
        returnQuery += "("
        returnQuery += query
        returnQuery += ") AND "
    })
    returnQuery = returnQuery.substring(0, returnQuery.length - 5)
    return returnQuery
}

function orQueryJoiner(queries) {
    var returnQuery = ""
    queries.forEach(function (query) {
        returnQuery += "("
        returnQuery += query
        returnQuery += ") AND "
    })
    returnQuery = returnQuery.substring(0, returnQuery.length - 5)
    return returnQuery
}

/**
 * search a collection by id
 * @param id
 * @returns {string}
 */
function createSearchByIdQuery(id) {
    var searchByIdQuery = "@path.key:" + id + ""
    return searchByIdQuery
}

function createGetAllItemsQuery() {
    return '@path.kind:item'
}

/**
 * check if there is more data that needs to be
 * retrieved through pagination
 * this is applicable for db.newSearchBuilder() type queries
 *
 * Sample data:
 * { count: 100,
 * total_count: 429,
 * results: [],
 * next: '/v0/paths?offset=100&query=*&limit=100' }
 *
 * @param results
 * @returns {boolean}
 */
function checkHasMore(results) {
    if (results.next) {
        return true
    } else {
        return false
    }
}

/**
 * generate a one on one graph relation query
 * @param sourceCollection
 * @param sourceId
 * @param destinationCollection
 * @param destinationId
 */
function createGetOneOnOneGraphRelationQuery(sourceCollection, sourceId, relation, destinationCollection, destinationId) {
    var query =
        "@path.kind:relationship AND @path.source.collection:`" +
        sourceCollection +
        "` AND @path.source.key:`" +
        sourceId + "` AND @path.relation:`" +
        relation + "` AND @path.destination.collection:`" +
        destinationCollection + "` AND @path.destination.key:`" +
        destinationId + "`"
    return query
}

/**
 * generate a one on one graph relation query
 * @param sourceCollection
 * @param sourceId
 * @param destinationCollection
 * @param destinationId
 *
 *
 * ok so the deal here is this:
 * the response is quite different, you cant simply do a injectId and get stuff done here:
 * { path:
     { kind: 'relationship',
       source: [Object],
       destination: [Object],
       relation: 'isConsumed',
       ref: '5ba93c9db0cff93f',
       reftime: 1456097864242 },
    score: 16.222095489501953,
    kind: 'relationship',
    reftime: 1456097864242 } ]

 or maybe injectId can house this logic?
 the graph relationship technique has been abandoned
 because this is response retrieved { kind: 'item', collection: 'users', key: '2d06b22274ae5bad' }
 graph relations cannot be lucenified. you need to use graph reader seperately.
 you cant make a decoupled system that way!

 */
function createGetOneToManyGraphRelationQuery(sourceCollection, sourceId, relation, destinationCollection) {
    var query =
        "@path.kind:relationship AND @path.source.collection:`" +
        sourceCollection +
        "` AND @path.source.key:`" +
        sourceId + "` AND @path.relation:`" +
        relation + "` AND @path.destination.collection:`" +
        destinationCollection + "`"
    return query
}

/**
 *
 * @param collection
 * @param id
 * @param relation
 * @returns {GraphBuilder}
 */
function getGraphResultsPromise(collection, id, relation) {
    return db.newGraphReader()
        .get()
        .from(collection, id)
        .limit(100)
        .related(relation)
}

/**
 * this returns an array of promises that allows to retreive an entire
 * dump of the database collection
 * @param collection
 * @returns {!Promise}
 */
function allItemsPromisesList(collection, query) {
    var promiseList = kew.defer()
    var promiseListArray = []
    var offset = 0
    db.newSearchBuilder()
        .collection(collection)
        .limit(100)
        .offset(offset)
        .query(query)
        .then(function (results) {
            var totalCount = results.body.total_count
            var remaining = 0
            do {
                promiseListArray.push(
                    db.newSearchBuilder()
                        .collection(collection)
                        .limit(100)
                        .offset(offset)
                        .query(query)
                )
                offset += 100
                remaining = totalCount - offset;
            } while (remaining > 0)
            promiseList.resolve(promiseListArray)
        })
        .fail(function (err) {
            promiseList.reject(err)
        })
    return promiseList
}

/**
 * this returns all the items in a database collection
 * asynchronously
 * @param collection
 * @returns {!Promise}
 */
function getAllItems(collection, query) {
    var allItems = kew.defer()
    allItemsPromisesList(collection, query)
        .then(function (promiseList) {
            console.log('promise array resolved')
            return kew.all(promiseList)
        })
        .then(function (promiseResults) {
            console.log("items resolved")
            var allItemsList = []
            promiseResults.forEach(function (item) {
                console.log("yeh kya hua")
                console.log(item.body.results[0].path.destination)
                var injectedItems = injectId(item)
                allItemsList = allItemsList.concat(injectedItems)
            })
            console.log("kaisey hua")
            allItems.resolve(allItemsList)
        })
        .fail(function (err) {
            allItems.reject(err)
        })
    return allItems
}

module.exports = {
    injectId: injectId,
    createFieldQuery: createFieldQuery,
    andQueryJoiner: andQueryJoiner,
    orQueryJoiner: orQueryJoiner,
    checkHasMore: checkHasMore,
    createSearchByIdQuery: createSearchByIdQuery,
    createGetAllItemsQuery: createGetAllItemsQuery,
    //createGetOneToManyGraphRelationQuery: createGetOneToManyGraphRelationQuery,
    createGetOneOnOneGraphRelationQuery: createGetOneOnOneGraphRelationQuery,
    getGraphResultsPromise: getGraphResultsPromise,
    getAllItems: getAllItems
}