var kew = require('kew');
var config = require('./../config.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var customUtils = require('./../utils.js');

var API_KEY = "AIzaSyCZ_hi_ktzOYaXcYwwcCtgMGv1bPcHR2A8"
//var Youtube = require("node-youtubeapi-simplifier")
//var YT = require("youtube-playlist-info")
var fs = require('fs')
var request = require('request')
//Youtube.setup(API_KEY)
var date = new Date()
var qbchat = require('../qbchat.js');
var Notifications = require('../notifications');
var constants = require('../constants');
var notify = new Notifications();
var dbUtils = require('../dbUtils')

function createCourse(title, description, userObj, category, subcategory, conceptsData) {
    //qbchat.getSession(function (err, session) {
    //    if (err) {
    //        console.log("Recreating session");
    //        qbchat.createSession(function (err, result) {
    //            if (err) {
    //                console.log(err)
    //            } else doIt();
    //        })
    //    } else doIt();
    //})

    //function doIt() {
    //customUtils.getLinkInfo(introVidUrl, function (introVidInfo) {
    var payload = {
        "created": date.getTime(),
        "accessId": customUtils.generateToken(3),
        "title": title,
        "desc": description,
        //"buying": reqBody.buying,
        //"price": reqBody.price,
        "coverPhoto": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png",
        "coverPhotoThumb": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png",
        "producer": userObj,
        "studentCount": 0,
        "newStudents": 0,
        "isPrivate": false,
        "mentorPaid": false,
        "coursePaid": false,
        "cat": category,
        "subCat": subcategory,
        //flag to indicate this is a auto generated course
        "autoGen": true,
        concepts: conceptsData,
        //"concepts": [
        //    {
        //        "id": customUtils.randomizer(),
        //        "title": "Introduction",
        //        "pos": 0,
        //        "objects": [
        //            {
        //                "id": customUtils.randomizer(),
        //                "type": "video",
        //                "pos": 0,
        //                "url": introVidUrl,
        //                "title": introVidTitle,
        //                "desc": introVidDescription,
        //                "image": ""
        //            }
        //        ]
        //    }
        //],
        //"mentorUSD": reqBody.mentorUSD,
        //"mentorINR": reqBody.mentorINR,
        //"courseINR": reqBody.courseINR,
        //"courseUSD": reqBody.courseUSD,
        //"selectedCurrency": reqBody.selectedCurrency,
        "mentorAvailable": true
    };


    db.post('paths', payload)
        .then(function (result) {
            var pathId = result.headers.location.match(/[0-9a-z]{16}/)[0];

            db.newGraphBuilder()
                .create()
                .from('users', userObj.id)
                .related('produces')
                .to('paths', pathId);

            db.newGraphBuilder()
                .create()
                .from('paths', pathId)
                .related('isProduced')//Every producer also consumes his course
                .to('users', userObj.id);

            db.newGraphBuilder()
                .create()
                .from('users', userObj.id)
                .related('related')
                .to('paths', pathId);

            payload["id"] = pathId;

            var notifObj = {
                "pathId": pathId,
                "pathTitle": title,
                "producerId": userObj.id,
                "producerName": userObj.name,
                "producerPhoto": userObj.avatarThumb
            };
            notify.emit('newPath', notifObj);

            var date = new Date();
            var chatObjMentor = {
                "created": date.getTime(),
                "id": date.getTime(),
                "pathId": pathId,
                "qbId": userObj.qbId,
                "type": "newMentor"
            }

            notify.emit('wordForChat', chatObjMentor);

            var chatObj = {
                "created": date.getTime(),
                "type": "newChannel",
                "pathId": pathId,
                "pathTitle": title
            }

            qbchat.createRoom(2, 'General', function (err, newRoom) {
                if (err) console.log(err);
                else {
                    qbchat.addUserToRoom(newRoom._id, [userObj.qbId], function (err, result) {
                        if (err) console.log(err);
                    })
                    db.merge('paths', pathId, {"qbId": newRoom._id})
                        .then(function (result) {
                            chatObj["id"] = date.getTime() + "@1";
                            chatObj["channelName"] = "General Channel";
                            chatObj["channelId"] = newRoom._id;
                            notify.emit('wordForChat', chatObj);
                        })
                        .fail(function (err) {
                            console.log(err.body.message);
                        });
                }
            });

            qbchat.createRoom(2, "Mentor", function (err, newRoom) {
                if (err) console.log(err);
                else {
                    qbchat.addUserToRoom(newRoom._id, [userObj.qbId], function (err, result) {
                        if (err) console.log(err);
                    })
                    db.merge('paths', pathId, {"mentorRoomId": newRoom._id})
                        .then(function (result) {
                            chatObj["id"] = date.getTime() + "@2";
                            chatObj["channelName"] = "Mentor Channel";
                            chatObj["channelId"] = newRoom._id;
                            notify.emit('wordForChat', chatObj);
                        })
                        .fail(function (err) {
                            console.log(err.body.message);
                        });
                }
            });

            qbchat.createRoom(2, 'Introduction', function (err, newRoom) {
                if (err) console.log(err);
                else {
                    qbchat.addUserToRoom(newRoom._id, [userObj.qbId], function (err, result) {
                        if (err) console.log(err);
                    })
                    db.newPatchBuilder("paths", pathId)
                        .merge("concepts.0", {"qbId": newRoom._id})
                        .apply()
                        .then(function (result) {
                            chatObj["id"] = date.getTime() + "@3";
                            chatObj["channelName"] = "Introduction";
                            chatObj["channelId"] = newRoom._id;
                            notify.emit('wordForChat', chatObj);
                        })
                        .fail(function (err) {
                            console.log(err.body.message);
                        })
                }
            });
        })
        .fail(function (err) {
            throw err
        })
    //})
    //}
}


var theArray = {
    1: {
        name: "History and Culture",
        sub: {}
    },
    2: {
        name: "Literature and Media",
        sub: {
            a: "Journalism",
            b: "Music and Performing Arts",
            c: "Film",
            d: "Language",
            e: "Literature",
            f: "Media Studies and Communication"
        }
    },
    3: {
        name: "Economics",
        sub: {
            a: "Economics",
            b: "Environment & Natural Resources"
        }
    },
    4: {
        name: "Math and Logic",
        sub: {}
    },
    5: {
        name: "Business",
        sub: {}
    },
    6: {
        name: "Computer Science",
        sub: {
            a: "Software Engineering",
            b: "Mobile and Web Development",
            c: "Algorithms & Data Structures",
            d: "Computer Security and Networks",
            f: "Computer Architecture",
            g: "Advanced Computing",
            h: "Mathematics of Computing",
            i: "Software and Society",
            j: "Graphics",
            k: "Introduction to Computer Science"
        }
    },
    7: {
        name: "Politics and Law",
        sub: {
            a: "Political Science",
            b: "Law"
        }
    },
    8: {
        name: "Social Sciences and Humanities",
        sub: {
            a: "Philosophy",
            b: "Religion",
            c: "Sociology"
        }
    },
    9: {
        name: "Engineering",
        sub: {
            a: "Aerospace",
            b: "Biotechnology",
            c: "Chemical Engineering",
            e: "Electrical Engineering",
            f: "Textile Engineering",
            g: "Automobile Engineering",
            h: "Metallurgy and Material Science",
            i: "Mining",
            j: "Ocean Engineering",
            k: "Nanotechnology",
            l: "Electronics and Communication",
            m: "Engineering Design",
            n: "Civil Engineering",
            o: "Mechanical Engineering",
            p: "Basic Engineering"
        }
    },
    10: {
        name: "Sciences",
        sub: {
            a: "Physics",
            b: "Chemistry",
            c: "Astronomy"
        }
    },
    11: {
        name: "Life Sciences",
        sub: {
            a: "Psychology and Neuroscience",
            b: "Biology",
            c: "Public Health",
            d: "Food"
        }
    }
};

function getCategory(categoryKey) {
    var category = kew.defer()
    try {
        var theCategory = theArray[categoryKey]["name"]
        if (theCategory)
            category.resolve(theCategory)
        else
            category.reject(new Error({message: "Invalid categoryNumber"}))
    } catch (exception) {
        category.reject(exception)
    }
    return category
}

function getSubCategory(categoryKey, subCategoryKey) {
    var subCategory = kew.defer()
    try {
        var theCategory = theArray[categoryKey]["sub"][subCategoryKey]
        //console.log(theCategory)
        if (theCategory)
            subCategory.resolve(theCategory)
        else
            subCategory.reject(new Error({message: "Invalid categoryNumber"}))
    } catch (exception) {
        subCategory.reject(new Error({message: "Invalid category/subcategory"}))
    }
    return subCategory
}

function getUserObjFromToken(access_token) {
    var userObj = kew.defer()
    db.newGraphReader()
        .get()
        .from('tokens', access_token)
        .related('hasUser')
        .then(function (result) {
            var user = result.body;
            if (user.count === 1) {
                delete user.results[0].value.password
                userObj.resolve(user.results[0].value)
            } else {
                //console.log("token has no user");
                userObj.reject(new Error("token has no user"))
            }
        })
        .fail(function (err) {
            userObj.reject(err)
        });
    return userObj
}

function getPlaylistIdFromLink(playlistLink) {
    var playlistId = kew.defer()
    var myRegexp = /(p|list)=([A-Za-z0-9\-_]+)/g;
    var match = myRegexp.exec(playlistLink);
    if (match[2])
        playlistId.resolve(match[2])
    else
        playlistId.reject(new Error({message: "playlist Id could not be matched"}))

    return playlistId
}

var theUniqueness = []
function spitOutDuplicates(item) {
    if (theUniqueness.indexOf(item) > -1) {
        console.log(item)
    } else {
        theUniqueness.push(item)
    }
}

function getPlaylistDescription(playlistId) {
    var description = kew.defer()
    var url = "https://www.googleapis.com/youtube/v3/playlists?part=snippet&id=" + playlistId + "&key=" + API_KEY
    request(url, function (error, response, body) {
        try {
            if (!error && response.statusCode == 200) {
                body = JSON.parse(body)
                //console.log(body) // Show the HTML for the Google homepage.
                //callback(null, body)
                //setTimeout(function() {
                description.resolve(body.items[0].snippet.description)
                //}, 0);
            } else {
                //console.log(error)
                //callback(error, null)
                description.reject(error)
            }
        } catch (exception) {
            description.reject(exception)
        }
    })
    return description
}

function getGeneratedConcepts(playlistId) {
    var concepts = []
    var generatedConcepts = kew.defer()
    var position = 0
    getResults(undefined)

    function getResults(pageToken) {
        var url = "https://www.googleapis.com/youtube/v3/playlistItems?maxResults=50&part=snippet&playlistId=" + playlistId + "&key=" + API_KEY
        if (pageToken) {
            url = url + "&pageToken=" + pageToken
        }
        request(url, function (error, response, body) {
            try {
                if (!error && response.statusCode == 200) {
                    body = JSON.parse(body)

                    body.items.forEach(function (item) {
                        var conceptTitle
                        if (position == 0) {
                            conceptTitle = "Introduction"
                        } else {
                            conceptTitle = item.snippet.title
                        }

                        var conceptData = {
                            "id": customUtils.randomizer(),
                            "title": conceptTitle,
                            "pos": position,
                            "objects": [
                                {
                                    "id": customUtils.randomizer(),
                                    "type": "video",
                                    "pos": 0,
                                    "url": "https://www.youtube.com/watch?v=" + item.snippet.resourceId.videoId,
                                    "title": item.snippet.title,
                                    "desc": item.snippet.description,
                                    "image": ""
                                }
                            ]
                        }
                        position = position + 1000
                        concepts.push(conceptData)
                    })

                    if (body.nextPageToken)
                        getResults(body.nextPageToken)
                    else
                        generatedConcepts.resolve(concepts)
                } else {
                    generatedConcepts.reject(error)
                }
            } catch (exception) {
                generatedConcepts.reject(exception)
            }
        })
    }

    return generatedConcepts
}

var count = 0
var noOfEntries = 0
var errorEntries = []
function appendErrorJsonToFile(error, jsonEntry) {
    errorEntries.push({item: jsonEntry, error: error})
}

function incrementCount(filename) {
    count++
    console.log(count + "-------" + noOfEntries)
    if (count == noOfEntries) {
        console.log("Eureeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeka")
        var str = JSON.stringify(errorEntries)
        fs.writeFile(filename, str, function (err) {
            if (err) {
                console.log(err);
                console.log("file write failed brah")
            } else {
                console.log("The file was saved!");
            }
        })
    }
}

/**
 * {
    "possibleTitle": "Epidemics in Western Society Since 1600",
    "playlistId": "http://www.youtube.com/playlist?list=PL3AE7B3B6917DE8E6&feature=plcp",
    "category": "1",
    "access_token": "603dde10915b3617457cc758a0b5fda9"
}
 */
function parseIncomingJson(inputFile) {
    var ocwErrorsFile = "./courseCreator/" + "ocw_failed.json"
    fs.readFile(inputFile, 'utf8', function (err, data) {
        if (err) throw err;
        var obj = JSON.parse(data);
        //obj = obj.splice(0, 1)
        noOfEntries = obj.length
        obj.forEach(function (item) {
            var hasSubCategory = (item.sub_category) ? true : false
            getPlaylistIdFromLink(item.playlistId)
                .then(function (playlistId) {
                    return playlistId
                })
                .then(function (playlistId) {
                    var aPromise = (hasSubCategory ? getSubCategory(item.category, item.sub_category) : kew.resolve(""))
                    var promises = [
                        getCategory(item.category),
                        aPromise,
                        getUserObjFromToken(item.access_token),
                        getGeneratedConcepts(playlistId),
                        getPlaylistDescription(playlistId)
                    ]
                    return kew.all(promises)
                })
                .then(function (results) {
                    var title = item.possibleTitle
                    var description = results[4]
                    var userObj = results[2]
                    var cat = results[0]
                    var subCat = (hasSubCategory ? results[1] : undefined)
                    var theConcepts = results[3]
                    //var introVidUrl = results[3].url
                    //var introVidTitle = results[3].title
                    //var introVidDescription = results[3].desc

                    incrementCount(ocwErrorsFile)
                    createCourse(title, description, userObj, cat, subCat, theConcepts)
                    //console.log("---------------------" + count)
                    //console.log(title)
                    //console.log(description)
                    //console.log(userObj)
                    //console.log(cat)
                    //console.log(subCat)
                    //console.log(introVidUrl)
                    //console.log(introVidTitle)
                    //console.log(introVidDescription)
                    //console.log("---------------------")
                })
                .fail(function (err) {
                    incrementCount(ocwErrorsFile)
                    appendErrorJsonToFile(err, item, "ocw_failed.json")
                    //console.log("wow")
                    //console.log(item)
                    //console.log(err)
                })
        })
    })
}

/**
 * {
	"playlistId": "PLbMVogVj5nJQ20ZixllzM69agBq-m8ndV",
	"videoCount": 39,
	"title": "Ethics",
	"description": "Ethics by Prof. Vineet Sahu, Department of Humanities and Social Sciences, IIT Kanpur. For more details on NPTEL visit http://nptel.ac.in",
	"publishedAt": "2016-02-08T12:00:01.000Z",
	"category": "8",
		"sub_category": "a",
	"access_token": "fc64773688963c78d5785846e82eb8ba",
	"thumbnails": {
		"default": {
			"url": "https://i.ytimg.com/vi/nlh9V5gd8hg/default.jpg",
			"width": 120,
			"height": 90
		},
		"medium": {
			"url": "https://i.ytimg.com/vi/nlh9V5gd8hg/mqdefault.jpg",
			"width": 320,
			"height": 180
		},
		"high": {
			"url": "https://i.ytimg.com/vi/nlh9V5gd8hg/hqdefault.jpg",
			"width": 480,
			"height": 360
		},
		"standard": {
			"url": "https://i.ytimg.com/vi/nlh9V5gd8hg/sddefault.jpg",
			"width": 640,
			"height": 480
		},
		"maxres": {
			"url": "https://i.ytimg.com/vi/nlh9V5gd8hg/maxresdefault.jpg",
			"width": 1280,
			"height": 720
		}
	}
}
 * @param inputFile
 */
function parseNptelJson(inputFile) {
    var nptelErrorsFile = "./courseCreator/" + "nptel_failed.json"
    fs.readFile(inputFile, 'utf8', function (err, data) {
        if (err) throw err;
        var obj = JSON.parse(data);
        //obj = obj.splice(0, 5)
        noOfEntries = obj.length
        obj.forEach(function (item) {
            var hasSubCategory = (item.sub_category) ? true : false
            var aPromise = (hasSubCategory ? getSubCategory(item.category, item.sub_category) : kew.resolve(""))
            var promises = [
                getCategory(item.category),
                aPromise,
                getUserObjFromToken(item.access_token),
                getGeneratedConcepts(item.playlistId),
            ]
            kew.all(promises)
                .then(function (results) {
                    //console.log(results)
                    var title = item.title
                    var description = item.description
                    var userObj = results[2]
                    var cat = results[0]
                    var subCat = (hasSubCategory ? results[1] : undefined)
                    var theConcepts = results[3]
                    //var introVidUrl = results[3].url
                    //var introVidTitle = results[3].title
                    //var introVidDescription = results[3].desc
                    //
                    incrementCount(nptelErrorsFile)
                    createCourse(title, description, userObj, cat, subCat, theConcepts)
                    //console.log("---------------------" + count)
                    //console.log(title)
                    //console.log(description)
                    //console.log(userObj)
                    //console.log(cat)
                    //console.log(subCat)
                    //console.log(theConcepts)
                    //console.log("---------------------")
                })
                .fail(function (err) {
                    incrementCount(nptelErrorsFile)
                    appendErrorJsonToFile(err, item)
                    //console.log("wow")
                    //console.log(item)
                    //console.log(err)
                })
        })
    })
}

function courseDeletor() {
    qbchat.init();
    qbchat.createSession(function (err, session) {
            if (session) {
                console.log("Session created");
                //delete courses now
                function findAndDeleteCourses(limit, offset) {
                    console.log("findAndDeleteCourses(" + limit + "," + offset + ") called")
                    db.newSearchBuilder()
                        .collection('paths')
                        .limit(limit)
                        .offset(offset)
                        .query('value.cat:*')
                        .then(function (results) {
                            var theCourses = dbUtils.injectId(results)
                            theCourses.forEach(function (result) {
                                console.log(result.title)
                                deleteCourse(result.id)
                            })
                            console.log(results.body.next)
                            if (results.body.next != undefined) {
                                findAndDeleteCourses(100, offset + 100)
                            }
                        })
                        .fail(function (err) {
                            console.log(err)
                            console.log(err)
                        })
                }

                findAndDeleteCourses(100, 0)
            }
            else if (err) console.log(err)
        }
    );
}

function dumpCourses() {
    var courseDump = []
    var firstTime = true
    findAndDumpCourses(100, 0)

    function findAndDumpCourses(limit, offset) {
        console.log("findAndDumpCourses(" + limit + "," + offset + ") called")
        db.newSearchBuilder()
            .collection('paths')
            .limit(limit)
            .offset(offset)
            .query('value.autoGen:true')
            .then(function (results) {
                var theCourses = dbUtils.injectId(results)
                theCourses.forEach(function (result) {
                    if (result.coverPhoto === "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png") {
                        courseDump.push({
                            id: result.id,
                            name: result.title,
                            coverPhoto: ""
                        })
                    }
                })
                console.log(results.body.next)
                if (results.body.next != undefined) {
                    findAndDumpCourses(100, offset + 100)
                } else {
                    thisIsDone()
                }
            })
            .fail(function (err) {
                console.log(err)
                console.log(err)
            })
    }

    function thisIsDone() {
        if (firstTime) {
            firstTime = false
            console.log("bhaisaaaahab")
            var str = JSON.stringify(courseDump)
            fs.writeFile("./courseCreator/autoGenCourses.json", str, function (err) {
                if (err) {
                    console.log(err);
                    console.log("file write failed brah")
                } else {
                    console.log("The file was saved!");
                }
            })
        }
    }
}

function dumpCoursesWierdTitles() {
    var courseDump = []
    var firstTime = true
    findAndDumpCourses(100, 0)

    function findAndDumpCourses(limit, offset) {
        //console.log("findAndDumpCourses(" + limit + "," + offset + ") called")
        db.newSearchBuilder()
            .collection('paths')
            .limit(limit)
            .offset(offset)
            .query('value.autoGen:true')
            .then(function (results) {
                var theCourses = dbUtils.injectId(results)
                theCourses.forEach(function (result) {
                    var theTitle = result.title
                    var specialCharacterRemovedTitle = theTitle.replace(/[\?]/gi, '')
                    if (!(theTitle == specialCharacterRemovedTitle)) {
                        courseDump.push({id: result.id, title: result.title})
                    }
                })
                //console.log(results.body.next)
                if (results.body.next != undefined) {
                    findAndDumpCourses(100, offset + 100)
                } else {
                    thisIsDone()
                }
            })
            .fail(function (err) {
                console.log(err)
                console.log(err)
            })
    }

    function thisIsDone() {
        if (firstTime) {
            firstTime = false
            console.log("bhaisaaaahab")
            var str = JSON.stringify(courseDump)
            fs.writeFile("./courseCreator/autoGenCourses.json", str, function (err) {
                if (err) {
                    console.log(err);
                    console.log("file write failed brah")
                } else {
                    console.log("The file was saved!");
                }
            })
        }
    }
}

function dumpProducers() {
    var producerIds = []
    var firstTime = true
    //findAndDumpCourses(100, 0)
    thisIsDone()

    function findAndDumpCourses(limit, offset) {
        console.log("findAndDumpCourses(" + limit + "," + offset + ") called")
        db.newSearchBuilder()
            .collection('paths')
            .limit(limit)
            .offset(offset)
            .query('*')
            .then(function (results) {
                var theCourses = dbUtils.injectId(results)
                theCourses.forEach(function (result) {
                    if (!(producerIds.indexOf(result.producer.id) > -1)) {
                        producerIds.push(result.producer.id)
                    }
                })
                //console.log(results.body.next)
                if (results.body.next != undefined) {
                    findAndDumpCourses(100, offset + 100)
                } else {
                    thisIsDone()
                }
            })
            .fail(function (err) {
                console.log(err)
                console.log(err)
            })
    }

    function thisIsDone() {
        var producerIds = ['cd1e525776334d59',
            '695b3f25f6deb426',
            'c92deb2853b67de3',
            '700b5f59c6dea520',
            'ff86ae8395416d63',
            '83040cd877349498',
            '8c8af3dd258c1f66',
            'abffb6629f4f8f88',
            'd086e08934af067f',
            'b8e2fb56644c12b4',
            'bfc8cd2a3542d382',
            '5638432511888af6',
            '7d742cb8a464bcc3',
            'bd7f77af0d798a32',
            '91cc525b2debc1fd',
            'e8faaf58ee2d6718',
            'c1774f1c7d8e05b2',
            '3a6414d40c03493d',
            '2299fc11d1b412c5',
            'a3f330abfdd911f5',
            '6ff369961def5447',
            'd174f44a95122c63',
            '66ca2fbc4063e334',
            '3cd6102631098d3d',
            '349db3e26944cd78',
            '8c7036516ff49e55',
            'fb05f0ad68f8f2aa',
            'b2e4ab540eb331c3',
            '2f733ab74d6916d7',
            '86ca4cccb0e6e8eb',
            '8d9cc2b8fec45e4d',
            'ae217ffd3712cd2b',
            '481c12e77e5e984a',
            '62199cc3f4c50a94',
            '88ea937c5563e8e2',
            'e7bc10ad3993e170',
            '32297332cc2bd8da',
            '5eda86443aace533',
            '719f70c148728d22',
            '81d6929124397acd',
            '8e95902661e546ed',
            'dd36388f6f8e18fe',
            '20f1e7e58584f558',
            'd7b275a73e023c20',
            '0348f512cbdd0b64',
            '4e0d219da6ffc295',
            '32aff246f5e5e79c',
            '6ad1f1a9f31c2613',
            '27df025acf8db260',
            '708462dd7924a49d',
            'e8e22b241c4048b1',
            '0bac286339edb3f2',
            'ab0143314a658a00',
            'de0b7da61b07aa46',
            'ac97026960e61d8b',
            'cda1a6227e664b0d',
            '66076dabf38ba0f3',
            '1beae0c08292904c',
            '38b7a4a966036540']

        if (firstTime) {
            console.log(producerIds)
            console.log("here are the producers with more than 100 courses : ")
            producerIds.forEach(function (producer) {
                db.newGraphReader()
                    .get()
                    .limit(100)
                    .from('users', producer)
                    .related(constants.graphRelations.users.producesPath)
                    .then(function (result) {
                        if(result.body.next) {
                            console.log(producer)
                        }
                    })
            })
        }
    }
}

function dumpDuplicateIntroductionVids() {
    var courseDump = []

    function spitOutDuplicates(item, dump) {
        if (theUniqueness.indexOf(item) > -1) {
            console.log(item)
            courseDump.push(dump)
        } else {
            theUniqueness.push(item)
        }
    }

    var courseDump = []
    var firstTime = true
    findAndDumpCourses(100, 0)

    function findAndDumpCourses(limit, offset) {
        console.log("findAndDumpCourses(" + limit + "," + offset + ") called")
        db.newSearchBuilder()
            .collection('paths')
            .limit(limit)
            .offset(offset)
            .query('value.autoGen:true')
            .then(function (results) {
                var theCourses = dbUtils.injectId(results)
                theCourses.forEach(function (result) {
                    try {
                        spitOutDuplicates(result.concepts[0].objects[0].url, result)
                    } catch (Exception) {
                        //console.log(Exception)
                        //console.log("ignore")
                    }
                    //if (result.coverPhoto === "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png") {
                    //    courseDump.push({
                    //        id: result.id,
                    //        name: result.title,
                    //        coverPhoto: "",
                    //    })
                    //}
                })
                console.log(results.body.next)
                if (results.body.next != undefined) {
                    findAndDumpCourses(100, offset + 100)
                } else {
                    thisIsDone()
                }
            })
            .fail(function (err) {
                console.log(err)
                console.log(err)
            })
    }

    function thisIsDone() {
        if (firstTime) {
            console.log("awesome")
            firstTime = false
            console.log("bhaisaaaahab")
            var str = JSON.stringify(courseDump)
            fs.writeFile("./courseCreator/duplicates.json", str, function (err) {
                if (err) {
                    console.log(err);
                    console.log("file write failed brah")
                } else {
                    console.log("The file was saved!");
                }
            })
        }
    }
}


function deleteCourse(pathId) {
    var channels = []
    db.get('paths', pathId)
        .then(function (path) {
            path.body.concepts.forEach(function (concept) {
                channels.push(concept.qbId);
            })
            channels.push(path.body.qbId);
            channels.push(path.body.mentorRoomId);

            channels.forEach(function (channel) {
                qbchat.deleteRoom(channel, function (err, res) {
                    if (err) console.log(err)
                })
            })

            db.remove("paths", pathId, true)
                .then(function (result) {
                    console.log(pathId + " deleted")
                })
                .fail(function (err) {
                    console.log(err)
                });
        })
        .fail(function (err) {
            console.log(err)
        });
}

function uploadCoverPhotos() {
    var inputFile = "./courseCreator/" + "1998-3200.json"
    var imagesFolder = "/home/ankan/Dropbox/OCW/1998-3200/"
    fs.readFile(inputFile, 'utf8', function (err, data) {
        if (err) throw err;
        var obj = JSON.parse(data);
        //obj = obj.splice(0, 6)
        noOfEntries = obj.length
        obj.forEach(function (item) {
            //read and upload image
            //update cover photo in orchestrate
            var filePath = imagesFolder + item.coverPhoto
            fs.readFile(filePath, 'utf8', function (err, data) {
                if (err) {
                    console.log(err)
                    console.log(item)
                }
            })
            customUtils.uploadToS3({path: filePath, originalname: item.coverPhoto})
                .then(function (fileInfo) {
                    item["fileInfo"] = fileInfo
                    appendErrorJsonToFile({}, item, "./courseCreator/uploadCoverSuccess.json")
                    console.log(fileInfo)
                    incrementCount("uploadCoverSuccess.json")
                })
            //    .fail(function (err) {
            //        console.log(err)
            //    })
        })
    })
}

function uploadCoverPhotos2() {
    var inputFile = "./courseCreator/" + "3200plus.json"
    var imagesFolder = "/home/ankan/Dropbox/OCW/Course Images/"
    fs.readFile(inputFile, 'utf8', function (err, data) {
        if (err) throw err;
        var obj = JSON.parse(data);
        //obj = obj.splice(0, 6)
        noOfEntries = obj.length
        obj.forEach(function (item) {
            //read and upload image
            //update cover photo in orchestrate
            var filePath = imagesFolder + item.coverPhoto
            fs.readFile(filePath, 'utf8', function (err, data) {
                if (err) {
                    console.log(err)
                    console.log(item)
                }
            })
            customUtils.uploadToS3({path: filePath, originalname: item.coverPhoto})
                .then(function (fileInfo) {
                    item["fileInfo"] = fileInfo
                    appendErrorJsonToFile({}, item, "./courseCreator/uploadCoverSuccess.json")
                    console.log(fileInfo)
                    incrementCount("uploadCoverSuccess.json")
                })
            //    .fail(function (err) {
            //        console.log(err)
            //    })
        })
    })
}

function uploadCoverPhotosFailed() {
    var inputFile = "./courseCreator/" + "newjson.json"
    var imagesFolder = "/home/ankan/Dropbox/OCW/newJson/"
    fs.readFile(inputFile, 'utf8', function (err, data) {
        if (err) throw err;
        var obj = JSON.parse(data);
        //obj = obj.splice(0, 6)
        noOfEntries = obj.length
        obj.forEach(function (item) {
            //read and upload image
            //update cover photo in orchestrate
            var filePath = imagesFolder + item.coverPhoto
            fs.readFile(filePath, 'utf8', function (err, data) {
                if (err) {
                    console.log(err)
                    console.log(item)
                }
            })
            customUtils.uploadToS3({path: filePath, originalname: item.coverPhoto})
                .then(function (fileInfo) {
                    item["fileInfo"] = fileInfo
                    appendErrorJsonToFile({}, item, "./courseCreator/uploadCoverSuccess.json")
                    console.log(fileInfo)
                    incrementCount("uploadCoverSuccess.json")
                })
            //    .fail(function (err) {
            //        console.log(err)
            //    })
        })
    })
}

function damnNotUploaded() {
    var theDamnMissingEntries = []
    var inputFile = "./courseCreator/" + "3200plus.json"
    var imagesFolder = "/home/ankan/Dropbox/OCW/Course Images/"
    fs.readFile(inputFile, 'utf8', function (err, data) {
        var obj = JSON.parse(data);

        fs.readFile("uploadCoverSuccess.json", 'utf8', function (err, newData) {
            var uploadedObj = JSON.parse(newData)

            obj.forEach(function (item1) {
                var found = false
                uploadedObj.forEach(function (item2) {
                    if (item1.id.indexOf(item2.item.id) > -1) {
                        found = true
                    }
                })
                if (!found) {
                    theDamnMissingEntries.push(item1)
                }
            })
            console.log(JSON.stringify(theDamnMissingEntries))
        })
    })
}

function changeTitles() {
    var theJson = [{
        "id": "0fb84c956f60c2b0",
        "title": "A Law Student's Toolkit"
    }, {
        "id": "0fb84cb91b60c2c6",
        "title": "The Stanford Mini Med School (Winter)"
    }, {
        "id": "0fb84cebee6074d7",
        "title": "Electronics - High Speed Devices & Circuit"
    }, {
        "id": "0fb84d035060c2f9",
        "title": "Algebraic Topology: A Beginner's Course"
    }, {
        "id": "0fb84cc7dd6074b7",
        "title": "Cervantes - Don Quixote"
    }, {
        "id": "0fb84cf6cc6074e1",
        "title": "Mitigating Agriculture's Impact"
    }, {
        "id": "0fb84cd6386074c4",
        "title": "Artificial Intelligence - Introduction to Robotics"
    }, {
        "id": "0fb84d11c76090d0",
        "title": "Modern Theoretical Physics: Einstein's General Theory of Relativity"
    }, {
        "id": "0fb84cb2066074a7",
        "title": "Justice: What's the Right Thing to Do?"
    }, {
        "id": "0fb84c990b607493",
        "title": "Maker of the Modern World - Imperial Britain, 1714 to the Present"
    }, {
        "id": "0fb84d110c6074f1",
        "title": "Mathematics in India - From Vedic Period to Modern Times"
    }, {
        "id": "0fb84cce0960c2d6",
        "title": "Freshman Organic Chemistry"
    }, {
        "id": "0fb84ceb8d6090b8",
        "title": "Electronics & Communication Engineering - Signals and Systems"
    }, {
        "id": "0fb84ca5f760c2ba",
        "title": "David Hume's Central Principles"
    }, {
        "id": "0fb84cc29e6074b1",
        "title": "Darwin's Legacy"
    }, {
        "id": "0fb84e88c86090e5",
        "title": "Artificial Intelligence"
    }, {
        "id": "0fb84d045a6074e4",
        "title": "Global Warming: Understanding the Forecast"
    }, {
        "id": "0fb84ca5d8609083",
        "title": "Plato's Apology of Socrates"
    }, {
        "id": "0fb84cc9da60909c",
        "title": "Strategic Marketing - Contemporary Issues"
    }, {
        "id": "0fb84ceaee6090b7",
        "title": "Electrical Digital Signal Processing"
    }]

    theJson.forEach(function (item) {
        db.merge('paths', item.id, {
            title: item.title
        })
            .then(function (result) {
                console.log("sucksex")
            })
            .fail(function (err) {
                console.log(err)
                console.log(item)
            })
    })
}

function updateCoverPhotos() {
    var inputFile = "./courseCreator/coverPhotos/" + "thumbsFailed.json"
    fs.readFile(inputFile, 'utf8', function (err, data) {
        var obj = JSON.parse(data);
        //obj = obj.splice(0,3)
        count = 0
        obj.forEach(function (ekItem) {

            db.merge('paths', ekItem.item.id, {
                "coverPhoto": ekItem.item.fileInfo.url,
                "coverPhotoThumb": ekItem.item.fileInfo.urlThumb
            })
                .then(function (result) {
                    //console.log("something happend?")
                    count++
                    console.log(count)
                })
                .fail(function (err) {
                    console.log(ekItem)
                    console.log(err)
                })
        })
    })
}

function subCategoryPatcher(cat, subCat, newSubCat) {
    var courseDump = []
    var firstTime = true
    findAndDumpCourses(100, 0)

    function findAndDumpCourses(limit, offset) {
        console.log("findAndDumpCourses(" + limit + "," + offset + ") called")
        db.newSearchBuilder()
            .collection('paths')
            .limit(limit)
            .offset(offset)
            .query('value.autoGen:true')
            .then(function (results) {
                var theCourses = dbUtils.injectId(results)
                theCourses.forEach(function (result) {
                    if (result.cat === cat && result.subCat === subCat) {
                        db.merge('paths', result.id, {
                            subCat: newSubCat
                        })
                            .then(function (result) {
                                console.log("suck sex")
                            })
                            .fail(function (err) {
                                courseDump.push({
                                    id: result.id,
                                    title: result.title,
                                    err: err
                                })
                            })
                    }
                })
                console.log(results.body.next)
                if (results.body.next != undefined) {
                    findAndDumpCourses(100, offset + 100)
                } else {
                    thisIsDone()
                }
            })
            .fail(function (err) {
                console.log(err)
                console.log(err)
            })
    }

    function thisIsDone() {
        if (firstTime) {
            firstTime = false
            console.log("bhaisaaaahab")
            var str = JSON.stringify(courseDump)
            fs.writeFile("./courseCreator/autoGenCourses.json", str, function (err) {
                if (err) {
                    console.log(err);
                    console.log("file write failed brah")
                } else {
                    console.log("The file was saved!");
                }
            })
        }
    }
}

function teddyBearDeletor() {
    qbchat.init();
    var courseDump = []
    var firstTime = true
    findAndDumpCourses(100, 0)

    function findAndDumpCourses(limit, offset) {
        console.log("findAndDumpCourses(" + limit + "," + offset + ") called")
        db.newSearchBuilder()
            .collection('paths')
            .limit(limit)
            .offset(offset)
            .query('*')
            .then(function (results) {
                var theCourses = dbUtils.injectId(results)
                theCourses.forEach(function (result) {
                    if (result.coverPhoto === "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png") {
                        //console.log(result)
                        deletePath(result.id)
                            .then(function (result) {
                                console.log("result")
                                //console.log(result)
                            })
                            .fail(function (err) {
                                console.log("err")
                                console.log(result.id)
                                console.log(err)
                            }
                        )
                    }
                })
                console.log(results.body.next)
                if (results.body.next != undefined) {
                    findAndDumpCourses(100, offset + 100)
                } else {
                    thisIsDone()
                }
            })
            .fail(function (err) {
                console.log(err)
                console.log(err)
            })
    }

    function thisIsDone() {
        if (firstTime) {
            firstTime = false
            console.log("bhaisaaaahab")
            var str = JSON.stringify(courseDump)
            fs.writeFile("./courseCreator/autoGenCourses.json", str, function (err) {
                if (err) {
                    console.log(err);
                    console.log("file write failed brah")
                } else {
                    console.log("The file was saved!");
                }
            })
        }
    }
}

//dumpCourses()

process.on('unhandledRejection', function (reason, p) {
    console.log("Possibly Unhandled Rejection at: Promise ", p, " reason: ", reason);
    //incrementCount("uploadCoverSuccess.json")
    //incrementCount()
    // application specific logging here
});

//qbchat.init();
//qbchat.createSession(function (err, session) {
//        if (session) {
//            console.log("ocw Session created");
//            parseIncomingJson("./courseCreator/ocw.json")
//        }
//        else if (err) console.log(err)
//    }
//);

//qbchat.init();
//qbchat.createSession(function (err, session) {
//        if (session) {
//            console.log("nptel Session created");
//            parseNptelJson("./courseCreator/nptel.json")
//        }
//        else if (err) console.log(err)
//    }
//);

//courseDeletor()
//dumpDuplicateIntroductionVids()
//uploadCoverPhotos()
//uploadCoverPhotos2()
//damnNotUploaded()
//updateCoverPhotos()
//uploadCoverPhotosFailed()
//dumpCoursesWierdTitles()
//changeTitles()
//teddyBearDeletor()
function deletePath(pathId) {
    var channels = []
    var deletedPathStatus = kew.defer()
    var getChatSession = kew.defer()
    qbchat.getSession(function (err, session) {
        if (err) {
            console.log("Recreating session");
            qbchat.createSession(function (err, result) {
                if (err) getChatSession.reject(err)
                else getChatSession.resolve(result)
            })
        } else {
            getChatSession.resolve(session)
        }
    })

    getChatSession
        .then(function (result) {
            return db.get('paths', pathId)
        })
        .then(function (path) {
            path.body.concepts.forEach(function (concept) {
                //this handles the case of courses that don't have concept channels
                if (concept.qbId) channels.push(concept.qbId);
            })
            //no ifs here, because all courses, minimum have these channels always
            //if they dont its an anomaly
            channels.push(path.body.qbId);
            channels.push(path.body.mentorRoomId);

            //hua toh hua
            channels.forEach(function (channel) {
                qbchat.deleteRoom(channel, function (err, res) {
                    if (err) ;//console.log(err)
                })
            })
            return db.remove("paths", pathId, true)
        })
        .then(function (result) {
            deletedPathStatus.resolve({"success": "Path Deleted"});
        })
        .fail(function (err) {
            deletedPathStatus.reject(err)
        })
    return deletedPathStatus
}

function findMissingIntroChatChannel() {
    console.log("wtf")
    qbchat.init();
    var courseDump = []
    var firstTime = true
    findAndDumpCourses(100, 0)

    function findAndDumpCourses(limit, offset) {
        console.log("findAndDumpCourses(" + limit + "," + offset + ") called")
        db.newSearchBuilder()
            .collection('paths')
            .limit(limit)
            .offset(offset)
            .query("value.autoGen:true")
            .then(function (results) {
                console.log("what")
                var theCourses = dbUtils.injectId(results)
                theCourses.forEach(function (result) {
                    if (!result.concepts[0].qbId) {
                        console.log(result.id)
                    }
                })
                console.log(results.body.next)
                if (results.body.next != undefined) {
                    findAndDumpCourses(100, offset + 100)
                } else {
                    thisIsDone()
                }
            })
            .fail(function (err) {
                console.log(err)
                console.log(err)
            })
    }

    function thisIsDone() {
        if (firstTime) {
            firstTime = false
            console.log("bhaisaaaahab")
            var str = JSON.stringify(courseDump)
            fs.writeFile("./courseCreator/autoGenCourses.json", str, function (err) {
                if (err) {
                    console.log(err);
                    console.log("file write failed brah")
                } else {
                    console.log("The file was saved!");
                }
            })
        }
    }
}

findMissingIntroChatChannel()

//dumpProducers()
//db.newSearchBuilder()
//    .collection('paths')
//    .limit(1)
//    .offset(0)
//    .query("value.autoGen:true")
//    .then(function (results) {
//        console.log("what")
//        var theCourses = dbUtils.injectId(results)
//        theCourses.forEach(function (result) {
//            if (!result.concepts[0].qbId) {
//                console.log(result.id)
//            }
//        })
//        console.log(results.body.next)
//        if (results.body.next != undefined) {
//            //findAndDumpCourses(100, offset + 100)
//        } else {
//            //thisIsDone()
//        }
//    })
//    .fail(function (err) {
//        console.log(err)
//        console.log(err)
//    })
