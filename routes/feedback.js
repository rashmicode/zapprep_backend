var express = require('express');
var router = express.Router();

var passport = require('passport');
var request = require('request');

var multer = require('multer');

var Notifications = require('../notifications');
var notify = new Notifications();

var config = require('../config.js');
var customUtils = require('../utils.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var now = new Date().getTime() / 1000

var Firebase = require("firebase");
var myFirebaseRef = new Firebase(config.firebase.url, config.firebase.secret);

myFirebaseRef.child('feedback').on("child_added", function (snapshot, prevChildKey) {
    if (snapshot.key() / 1000 > now - 60) {
        var msg = snapshot.val().text;
        var username = msg.match(/\$(\w+)\:/);

        if (username) {
            if (username[1] != 'pyoopil') {
                console.log(snapshot.key());
                request.post(config.slack.feedbackHook, {
                    body: JSON.stringify({text: "*" + msg + "*"})
                }, function (err, httpResponse, body) {
                    if (err) console.log("Error!")
                });
            }
        }
    }

}, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
});


router.post('/', multer(), function (req, res) {
    var msg = req.body.text;
    console.log(msg);

    var firstchar = msg.match(/^\*/);
    if (!firstchar) {
        console.log("firstchar " + firstchar)
        console.log("Feedback message is " + msg);

        //match the following 3 types of feedback
        //$chintooCandy: asdasdasdasd
        //$Path-0c8060bd2d6045df: sasdasd
        //$Everyone: brahmaaaaaast push baby!
        var dollarMatcher = msg.match(/\$([a-zA-Z0-9_-]+)\:/);
        console.log("username is " + dollarMatcher);

        var everyone = msg.match(/\$Everyone\:/);
        var classroom = msg.match(/\$Path-([A-Za-z0-9]+)\:/);

        if (dollarMatcher) {
            var d = new Date();
            var feedbackId = d.getTime();

            if (everyone) {
                var nofObj = {
                    text: "$pyoopil: " + msg,
                    type: "Everyone"
                };
            } else if (classroom) {
                var nofObj = {
                    text: "$pyoopil: " + msg,
                    type: "Classroom",
                    pathId: classroom[1]
                };
            } else {
                var re = new RegExp("\\$" + dollarMatcher[1] + ": *")
                var text = msg.replace(re, "")
                console.log("red alert -- fedback text parsed is : ")
                console.log(text)
                var nofObj = {
                    text: text,
                    type: "singleUser",
                    username: dollarMatcher[1]
                };
            }

            //if (username) {
            //    var nofObj = {
            //        text: "$pyoopil: " + msg
            //    };
            //} else if (everyone) {
            //    console.log("An <Everyone> has been detected");
            //    var nofObj = {
            //        text: "$Everyone: " + msg
            //    };
            //}
            //else if (classroom) {
            //    var nofObj = {
            //        text: "$Classroom-again: " + msg
            //    };
            //    console.log("Classroom message is " + nofObj.text);
            //}

            myFirebaseRef.child("feedback/" + feedbackId).set(nofObj, function (error) {
                //var re = new RegExp("\\$" + dollarMatcher[1] + ": *")
                //var text = msg.replace(re, "")
                //var notifObj = {
                //    "username": dollarMatcher[1],
                //    "text": text,
                //    "type": nofObj.type,
                //    "pathId": classroom[1]
                //};
                notify.emit('feedbackResponse', nofObj);
            });
            res.json({"status": "ok"});
        } else if (msg != "Come on! Who will do the mention? Stupid.") {
            res.json({"text": "Baklol ho ka. mention karo!"});
        } else {
            res.json({"status": "ok"});
        }
    }
});

module.exports = router;