var express = require('express');
var router = express.Router();

var Notifications = require('../notifications');
var notify = new Notifications();

var passport = require('passport');
var _ = require('lodash');

var validator = require('validator');

var config = require('../config.js'),
    customUtils = require('../utils.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var multer = require('multer'),
    fs = require('fs');


router.route('/')
    //Get Announcements for a Path
    .get([passport.authenticate('bearer', {session: false}), function (req, res) {

        if (!req.query.limit || req.query.limit < 0) req.query.limit = 10;
        if (!req.query.page || req.query.page < 1) req.query.page = 1;

        var limit = req.query.limit;
        var offset = limit * (req.query.page - 1);
        var responseObj = {};

        db.newSearchBuilder()
            .collection('announcements')
            .limit(limit)
            .offset(offset)
            .sort('timestamp', 'desc')
            .query('value.path:`' + req.query.pathId + '`')
            .then(function (result) {
                if (result.body.next) responseObj["more"] = true;
                else responseObj["more"] = false;

                responseObj["data"] = _.cloneDeep(result.body.results);
                for (var i = 0 in responseObj["data"]) {
                    responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
                    responseObj["data"][i] = responseObj["data"][i].value;
                }
                res.json(responseObj);
            })
            .fail(function (err) {
                responseObj["errors"] = [err.body.message];
                res.status(503);
                res.json(responseObj);
            });
    }])

    //Create Announcements for a Path
    .post([passport.authenticate('bearer', {session: false}), multer(), function (req, res) {
        var responseObj = {};
        var reqBody = req.body;
        var errors = new Array();

        if (validator.isNull(reqBody.desc)) errors.push("Description must be present");
        if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

        if (errors.length > 0) {
            responseObj["errors"] = errors;
            res.status(422);
            res.json(responseObj);
        }
        else {
            var d = new Date();
            var timestamp = d.getTime().toString();
            var payload = {
                "path": reqBody.pathId,
                "desc": reqBody.desc,
                "timestamp": timestamp
            }
            db.post('announcements', payload)
                .then(function (result) {
                    var announcementId = result.headers.location.match(/[0-9a-z]{16}/)[0];
                    payload["id"] = announcementId;
                    responseObj["data"] = payload;
                    res.status(201);
                    res.json(responseObj);

                    var notifObj = {
                        "announcementDesc": reqBody.desc,
                        "pathId": reqBody.pathId
                    };
                    notify.emit('newAnnouncement', notifObj);
                })
                .fail(function (err) {
                    responseObj["errors"] = [err.body.message];
                    res.status(503);
                    res.json(responseObj);
                })
        }
    }]);

module.exports = router;