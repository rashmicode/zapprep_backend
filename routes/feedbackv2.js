var express = require('express');
var router = express.Router();

var passport = require('passport');
var request = require('request');

var multer = require('multer');

var Notifications = require('../notifications');
var notify = new Notifications();

var config = require('../config.js');
var customUtils = require('../utils.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var now = new Date().getTime()

var Firebase = require("firebase");
var feedbackRefUrl = config.firebase.url + "/FeedbackUpdated"

var userRef = new Firebase(feedbackRefUrl, config.firebase.secret);

/**
 * Register a listener for a user
 */
userRef.on("child_added", function (snapshot) {
    var username = snapshot.key()
    var messageRef = new Firebase(feedbackRefUrl + "/" + username, config.firebase.secret)
    /**
     * Register a listener for a user's message
     * */
    messageRef.on("child_added", function (snapshot) {
        var messageObj = snapshot.val()
        /**
         * when the server starts, dispatch messages
         * of timestamp 1 minute before the current time
         * */
        if (messageObj.timestamp > (now - 60)) {
            /**
             * The Message from Pyoopil, or the slack channel
             * should not be resent back to the channel
             * */
            if (messageObj.displayName != "Pyoopil") {
                request.post(config.newSlack.feedbackHook, {
                    body: JSON.stringify({text: "*$" + username + " : " + messageObj.text + "*"})
                })
            }
        }
    })
})

router.post('/', multer(), function (req, res) {

    var messageFromSlack = req.body.text
    console.log(messageFromSlack)
    var alreadyPostedInSlack = messageFromSlack.match(/^\*/);
    console.log(alreadyPostedInSlack)

    if (!alreadyPostedInSlack) {
        console.log("accepted")
        var dollarMatcher = messageFromSlack.match(/\$([a-zA-Z0-9_-]+)\:(.*)/);
        //var everyone = messageFromSlack.match(/\$Everyone\:(.*)/);
        //var classroom = messageFromSlack.match(/\$Path-([A-Za-z0-9]+)\:(.*)/);

        if (dollarMatcher) {
            console.log("matched -- ready to fire")
            /**
             * Time to send it to firebase
             * */
            var username = dollarMatcher[1];
            var thisUsersRefUrl = feedbackRefUrl + "/" + username
            var thisUsersRef = new Firebase(thisUsersRefUrl, config.firebase.secret)

            var date = new Date()
            var time = date.getTime()
            var messageObj = {
                displayName: "Pyoopil",
                text: dollarMatcher[2],
                timestamp: time
            }

            thisUsersRef.push().set(messageObj, function (error) {
                var notifObj = {
                    "username": dollarMatcher[1],
                    "text": dollarMatcher[2],
                    "type": "singleUser"
                };
                notify.emit('feedbackResponse', notifObj);
            })
            //var d = new Date();
            //var feedbackId = d.getTime();

            //if (everyone) {
            //var nofObj = {
            //    text: "$pyoopil: " + msg,
            //    type: "Everyone"
            //};
            //} else if (classroom) {
            //var nofObj = {
            //    text: "$pyoopil: " + msg,
            //    type: "Classroom",
            //    pathId: classroom[1]
            //};
            //} else {
            //var re = new RegExp("\\$" + dollarMatcher[1] + ": *")
            //var text = msg.replace(re, "")
            //console.log("red alert -- fedback text parsed is : ")
            //console.log(text)
            //var nofObj = {
            //    text: text,
            //    type: "singleUser",
            //    username: dollarMatcher[1]
            //};
            //}

            //if (username) {
            //    var nofObj = {
            //        text: "$pyoopil: " + msg
            //    };
            //} else if (everyone) {
            //    console.log("An <Everyone> has been detected");
            //    var nofObj = {
            //        text: "$Everyone: " + msg
            //    };
            //}
            //else if (classroom) {
            //    var nofObj = {
            //        text: "$Classroom-again: " + msg
            //    };
            //    console.log("Classroom message is " + nofObj.text);
            //}

            //myFirebaseRef.child("feedback/" + feedbackId).set(nofObj, function (error) {
            //    //var re = new RegExp("\\$" + dollarMatcher[1] + ": *")
            //    //var text = msg.replace(re, "")
            //    //var notifObj = {
            //    //    "username": dollarMatcher[1],
            //    //    "text": text,
            //    //    "type": nofObj.type,
            //    //    "pathId": classroom[1]
            //    //};
            //    notify.emit('feedbackResponse', nofObj);
            //});
            res.json({"status": "ok"});
        } else if (msg != "Come on! Who will do the mention? Stupid.") {
            res.json({"text": "Baklol ho ka. mention karo!"});
        } else {
            res.json({"status": "ok"});
        }
    }
})


//myFirebaseRef.on("child_changed", function (snapshot, prevChildKey) {
//    console.log("Inside firebase callback")
//    var aUsersFeedback = snapshot.val();
//    var userKey = snapshot.key();
//    console.log(aUsersFeedback)
//    for (var key in aUsersFeedback) {
//        if (aUsersFeedback[key].displayName == "Pyoopil") {
//            //ignore
//        } else {
//            //var hopperRef = usersRef.child("gracehop");
//            //hopperRef.update({
//            //    "nickname": "Amazing Grace"
//            //});
//            console.log(aUsersFeedback[key])
//            if (aUsersFeedback[key]["isFired"] == undefined || aUsersFeedback[key]["isFired"] == false) {
//                console.log("firekaro")
//                console.log(aUsersFeedback[key])
//                var userRef = myFirebaseRef.child(userKey)
//                var thisMessageRef = userRef.child(key)
//                thisMessageRef.update({
//                    "isFired": true
//                })
//            }
//            //request.post(config.slack.feedbackHook, {
//            //    body: JSON.stringify({text: "*$" + aUsersFeedback[key].displayName + " : " + aUsersFeedback[key].text + "*"})
//            //})
//
//            //}
//        }
//    }
//}, function (errorObject) {
//    console.log("The read failed: " + errorObject.code);
//});

router.get('/', function (req, res) {
    console.log("what")
    myFirebaseRef = new Firebase(config.firebase.url + "/FeedbackUpdated/7b9e93742e133d97", config.firebase.secret);
    var newPostRef = myFirebaseRef.push();
    var now = new Date().getTime()
    newPostRef.set({
        displayName: "Batman",
        text: "Announcing COBOL, a New Programming Language",
        timestamp: now
    });
})


router.post('/', multer(), function (req, res) {
    var msg = req.body.text;
    console.log(msg);

    var firstchar = msg.match(/^\*/);
    if (!firstchar) {
        console.log("firstchar " + firstchar)
        console.log("Feedback message is " + msg);

        //match the following 3 types of feedback
        //$chintooCandy: asdasdasdasd
        //$Path-0c8060bd2d6045df: sasdasd
        //$Everyone: brahmaaaaaast push baby!
        var dollarMatcher = msg.match(/\$([a-zA-Z0-9_-]+)\:/);
        console.log("username is " + dollarMatcher);

        var everyone = msg.match(/\$Everyone\:/);
        var classroom = msg.match(/\$Path-([A-Za-z0-9]+)\:/);

        if (dollarMatcher) {
            var d = new Date();
            var feedbackId = d.getTime();

            if (everyone) {
                var nofObj = {
                    text: "$pyoopil: " + msg,
                    type: "Everyone"
                };
            } else if (classroom) {
                var nofObj = {
                    text: "$pyoopil: " + msg,
                    type: "Classroom",
                    pathId: classroom[1]
                };
            } else {
                var re = new RegExp("\\$" + dollarMatcher[1] + ": *")
                var text = msg.replace(re, "")
                console.log("red alert -- fedback text parsed is : ")
                console.log(text)
                var nofObj = {
                    text: text,
                    type: "singleUser",
                    username: dollarMatcher[1]
                };
            }

            //if (username) {
            //    var nofObj = {
            //        text: "$pyoopil: " + msg
            //    };
            //} else if (everyone) {
            //    console.log("An <Everyone> has been detected");
            //    var nofObj = {
            //        text: "$Everyone: " + msg
            //    };
            //}
            //else if (classroom) {
            //    var nofObj = {
            //        text: "$Classroom-again: " + msg
            //    };
            //    console.log("Classroom message is " + nofObj.text);
            //}

            //myFirebaseRef.child("feedback/" + feedbackId).set(nofObj, function (error) {
            //    //var re = new RegExp("\\$" + dollarMatcher[1] + ": *")
            //    //var text = msg.replace(re, "")
            //    //var notifObj = {
            //    //    "username": dollarMatcher[1],
            //    //    "text": text,
            //    //    "type": nofObj.type,
            //    //    "pathId": classroom[1]
            //    //};
            //    notify.emit('feedbackResponse', nofObj);
            //});
            res.json({"status": "ok"});
        } else if (msg != "Come on! Who will do the mention? Stupid.") {
            res.json({"text": "Baklol ho ka. mention karo!"});
        } else {
            res.json({"status": "ok"});
        }
    }
});


module.exports = router;
