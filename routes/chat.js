var express = require('express');
var router = express.Router();

var passport = require('passport');
var _ = require('lodash');
var request = require('request');

var multer = require('multer');

var config = require('../config.js');
var qbchat = require('../qbchat.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

router.post('/star', [passport.authenticate('bearer', {session: false}), multer(), function (req, res) {

    var responseObj = {};
    var userId = req.user.results[0].value.id;
    var payload = {
        "userId": userId,
        "roomId": req.body.roomId,
        "roomName": req.body.roomName,
        "messageId": req.body.messageId,
        "message": req.body.message,
        "timestamp": req.body.timestamp,
        "sender": req.body.senderQbId,
        "pathId": req.body.pathId
    };
    
    db.newSearchBuilder()
        .collection('starred')
        .limit(100)
        .query('value.userId:`' + userId + '` AND value.messageId:`' + req.body.messageId + '`')
        .then(function(result) {
            if(result.body.count != 0) {
                res.status(409).json({"errors": ["You have already starred this message"]});
                return;
            }
        })
        .then(function() {
           db.post('starred', payload)
            .then(function(result) {
            responseObj["data"] = payload;
            res.status(201).json(responseObj);
            }) 
        })    
        .fail(function(err) {
            responseObj["errors"] = [err.body.message];
            res.status(503).json(responseObj);
        });

}]);

router.get('/star', [passport.authenticate('bearer', {session: false}), function (req, res) {

    var responseObj = {};
    var userId = req.user.results[0].value.id;

    if(!req.query.limit || req.query.limit<0) req.query.limit = 10;
    if(!req.query.page || req.query.page<1) req.query.page = 1;

    var limit = req.query.limit;
    var offset = limit * (req.query.page - 1);

    db.newSearchBuilder()
        .collection('starred')
        .sort('value.timestamp', 'desc')
        .limit(limit)
        .offset(offset)
        .query('value.userId:`' + userId + '` AND value.pathId:`' + req.query.pathId + '`')
        .then(function(result) {
            if(result.body.next) responseObj["more"] = true;
            else responseObj["more"] = false;

            responseObj["data"] = _.cloneDeep(result.body.results);
            for (var i = 0 in responseObj["data"]) {
                responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
                responseObj["data"][i] = responseObj["data"][i].value;

                responseObj["data"][i].value = undefined;
                responseObj["data"][i].path = undefined;
                responseObj["data"][i].score = undefined;
                responseObj["data"][i].reftime = undefined;
            }

            var senders = responseObj.data.map(function(msg) {
                return msg["sender"]
            })
            qbchat.getUsers(senders, function(err, senders) {
                responseObj["data"].forEach(function(msg) {
                        for(i=0; i<senders.items.length; i++){
                            if(msg.sender == senders.items[i].user.id){
                                msg.sender = senders.items[i].user
                                break;
                            }
                        }
                    });
                res.json(responseObj);
            })
            
        })
        .fail(function(err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        });

}]);

router.delete('/star', [passport.authenticate('bearer', {session: false}), multer(), function (req, res) {

    var responseObj = {};
    var userId = req.user.results[0].value.id;

    db.newSearchBuilder()
        .collection('starred')
        .query('value.userId:`' + userId + '` AND value.messageId:`' + req.body.messageId + '`')
        .then(function(result) {
            var starId = result.body.results[0].path.key;
            db.remove('starred', starId, true)
            .then(function (result) {
                res.status(200).json({"data": {"success": "Message was unstarred"}});
            })
        })
        .fail(function(err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        });

}]);

module.exports = router;