var express = require('express');
var router = express.Router();

var Notifications = require('../notifications');
var notify = new Notifications();

var passport = require('passport');
var _ = require('lodash');

var validator = require('validator');

var config = require('../config.js'),
    customUtils = require('../utils.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var multer = require('multer'),
    fs = require('fs');

//Get a path
router.get('/', function (req, res, next) {
    if (req.query.access_token) next();
    else next('route');
}, passport.authenticate('bearer', {session: false}), function (req, res) {
    if (!req.query.limit || req.query.limit < 0) req.query.limit = 10;
    if (!req.query.page || req.query.page < 1) req.query.page = 1;

    var limit = req.query.limit;
    var offset = limit * (req.query.page - 1);
    var responseObj = {};
    var userObj = req.user.results[0].value;

    var firstResponseObj = {};
    var userReviewExists = false;
    var deleteElement = false;

    //get the producer
    //he cant be allowed to publish a review
    db.get('paths', req.query.pathId)
        .then(function (result) {
            var mentorId = result.body.producer.id;

            if (mentorId == userObj.id) {
                responseObj["allowPublish"] = "false";

                //duplicate/redundant code?
                //maybe?
                //identical to get marketplace ka code
                db.newSearchBuilder()
                    .collection('reviews')
                    .sort('timestamp', 'desc')
                    .limit(limit)
                    .offset(offset)
                    .query('value.pathId:`' + req.query.pathId + '`')
                    .then(function (result) {
                        if (result.body.next) responseObj["more"] = "true";
                        else responseObj["more"] = "false";

                        responseObj["data"] = _.cloneDeep(result.body.results);
                        for (var i = 0 in responseObj["data"]) {
                            responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
                            responseObj["data"][i] = responseObj["data"][i].value;
                        }
                        res.json(responseObj);
                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    });

            } else {
                //if em
                responseObj["allowPublish"] = "true";
                db.newSearchBuilder()
                    .collection('reviews')
                    .query('value.pathId:`' + req.query.pathId + '` AND value.user.id:`' + userObj.id)
                    .then(function (result) {
                        if (result.body.total_count === 0) {
                            responseObj["allowPublish"] = "true";
                        } else {
                            responseObj["allowPublish"] = "false";
                            userReviewExists = "true";
                            firstResponseObj = _.cloneDeep(result.body.results[0].value);
                            firstResponseObj["id"] = result.body.results[0].path.key;
                            firstResponseObj["allowEdit"] = "true";
                        }
                        db.newSearchBuilder()
                            .collection('reviews')
                            .sort('timestamp', 'desc')
                            .limit(limit)
                            .offset(offset)
                            .query('value.pathId:`' + req.query.pathId + '`')
                            .then(function (result) {
                                if (result.body.next) responseObj["more"] = "true";
                                else responseObj["more"] = "false";

                                responseObj["data"] = _.cloneDeep(result.body.results);

                                var deleteThisElement = false;
                                for (var i = 0 in responseObj["data"]) {
                                    console.log(i);
                                    console.log(responseObj["data"][i].value);
                                    if (responseObj["data"][i].value['user']['id'] == userObj.id) {
                                        deleteElement = true;
                                        deleteThisElement = i;
                                    }

                                    responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
                                    responseObj["data"][i] = responseObj["data"][i].value;
                                }

                                if (deleteThisElement) {
                                    responseObj["data"].splice(deleteThisElement, 1);
                                }

                                if (userReviewExists) {
                                    responseObj["data"].unshift(firstResponseObj);
                                }
                                res.status(200);
                                res.json(responseObj);
                            })
                            .fail(function (err) {
                                responseObj["errors"] = [err.body.message];
                                res.status(503);
                                res.json(responseObj);
                            });
                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    });
            }
        })
        .fail(function (err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        });
})
;

//Get Reviews for the marketplace
router.get('/', function (req, res) {
    if (!req.query.limit || req.query.limit < 0) req.query.limit = 10;
    if (!req.query.page || req.query.page < 1) req.query.page = 1;

    var limit = req.query.limit;
    var offset = limit * (req.query.page - 1);
    var responseObj = {};

    db.newSearchBuilder()
        .collection('reviews')
        .sort('timestamp', 'desc')
        .limit(limit)
        .offset(offset)
        .query('value.pathId:`' + req.query.pathId + '`')
        .then(function (result) {
            if (result.body.next) responseObj["more"] = "true";
            else responseObj["more"] = "false";

            responseObj["data"] = _.cloneDeep(result.body.results);
            for (var i = 0 in responseObj["data"]) {
                console.log(responseObj["data"][i].value["id"]);
                console.log(" " + responseObj["data"][i].path.key);

                responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
                responseObj["data"][i] = responseObj["data"][i].value;
            }
            res.json(responseObj);
        })
        .fail(function (err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        });
});

//Create a Review for a Path
router.route('/').post([passport.authenticate('bearer', {session: false}), multer(), function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = new Array();

    var userObj = req.user.results[0].value;

    if (!validator.isInt(reqBody.rating, {
            min: 1,
            max: 5
        })) errors.push("Please enter a valid rating");
    if (!validator.isLength(reqBody.title, 0, 40)) errors.push("Title must be max 40 characters");
    if (!validator.isLength(reqBody.review, 0, 500)) errors.push("Review must be max 500 characters");
    if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");


    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        //check if he is allowed to post a review ---
        //identical code detected
        db.get('paths', reqBody.pathId)
            .then(function (result) {
                var mentorId = result.body.producer.id;

                if (mentorId == userObj.id) {
                    responseObj["errors"] = ["You cannot post a review of your own course"];
                    res.status(422);
                    res.json(responseObj);
                    return;
                }
                db.newSearchBuilder()
                    .collection('reviews')
                    .query('value.pathId:`' + reqBody.pathId + '` AND value.user.id:`' + userObj.id)
                    .then(function (result) {
                        console.log(result.body);
                        if (result.body.total_count === 0) {
                            //-----------------------------------------------------------------------------
                            //Actual meat and potato logic
                            //-----------------------------------------------------------------------------
                            var d = new Date();
                            var timestamp = d.getTime().toString();

                            var payload = {
                                'rating': reqBody['rating'],
                                'title': reqBody['title'],
                                'review': reqBody['review'],
                                'pathId': reqBody['pathId'],
                                'timestamp': timestamp,
                                'user': userObj
                            };

                            db.post('reviews', payload)
                                .then(function (review) {
                                    console.log("entered");
                                    reviewId = review.headers.location.match(/[0-9a-z]{16}/)[0];

                                    responseObj['data'] = payload;
                                    responseObj['data']['id'] = reviewId;
                                    res.status(201);
                                    res.json(responseObj);

                                    //update reviews
                                    var pathMergeObj = {};
                                    db.get('paths', reqBody.pathId)
                                        .then(function (thePath) {
                                            if (!thePath.body['rating']) {
                                                pathMergeObj['rating'] = reqBody['rating'];
                                                pathMergeObj['reviewer_count'] = 1;
                                            } else {

                                                var oldRating = thePath.body['rating'];
                                                var count = thePath.body['reviewer_count'];
                                                var sum = parseFloat(oldRating) * parseInt(count);

                                                sum = parseFloat(sum) + parseInt(reqBody['rating']);
                                                count++;


                                                var newRating = sum / count;
                                                console.log("new Rating" + newRating);
                                                pathMergeObj['rating'] = Math.round(newRating * 10) / 10;
                                                pathMergeObj['reviewer_count'] = count;
                                            }
                                            console.log("the final object merging is ")
                                            console.log(pathMergeObj);

                                            db.merge('paths', reqBody.pathId, pathMergeObj)
                                                .then(function (result) {
                                                })
                                                .fail(function (err) {
                                                    responseObj["errors"] = [err.body.message];
                                                    res.status(503);
                                                    res.json(responseObj);
                                                })
                                        })
                                        .fail(function (err) {
                                            console.log("get failed");
                                            console.log(err.body.message);
                                            responseObj["errors"] = [err.body.message];
                                            res.status(503);
                                            res.json(responseObj);
                                        });
                                })
                                .fail(function (err) {
                                    console.log(err);
                                    responseObj["errors"] = [err.body.message];
                                    res.status(503);
                                    res.json(responseObj);
                                });
                        } else {
                            responseObj["errors"] = ["You have already published a review for this course."];
                            res.status(422);
                            res.json(responseObj);
                            return;
                        }
                    });
            }
        );
    }
}]);

router.route('/').patch([passport.authenticate('bearer', {session: false}), function (req, res) {
    if (Object.keys(req.body).length === 0) {
        res.status(422);
        res.json({"errors": ["No key was sent in the body"]});
        return;
    }

    var reqBody = req.body;
    var responseObj = {};
    var errors = new Array();

    if (validator.isNull(reqBody.id)) errors.push("id has to be specified");
    if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

    //optional editable fields
    if (!validator.isNull(reqBody.rating))
        if (!validator.isInt(reqBody.rating, {
                min: 1,
                max: 5
            })) errors.push("Please enter a valid rating");
    if (!validator.isNull(reqBody.title))
        if (!validator.isLength(reqBody.title, 0, 40)) errors.push("Title must be max 40 characters");
    if (!validator.isNull(reqBody.review))
        if (!validator.isLength(reqBody.review, 0, 500)) errors.push("Review must be max 500 characters");

    //does he have permissions to edit this review?
    //stay tuned for lines of code that will determine this...
    //Post Series A funding
    var userId = req.user.results[0].value.id;

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var d = new Date();
        var theTimestamp = d.getTime().toString();

        var payload = {
            'rating': reqBody['rating'],
            'title': reqBody['title'],
            'review': reqBody['review'],
            'timestamp': theTimestamp
        };

        console.log(payload)

        if (reqBody['rating']) {
            var pathId = reqBody.pathId;
            var reviewId = reqBody.id;
            var newRating = reqBody['rating'];
            //--------------------------------------------------------------------
            //update reviews
            var pathMergeObj = {};
            return db.get('paths', pathId)
                .then(function (thePath) {
                    //get the review for old rating
                    db.get('reviews', reviewId)
                        .then(function (theReview) {
                            var oldRating = theReview.body['rating'];
                            var count = thePath.body['reviewer_count'];
                            var oldAvgRating = thePath.body['rating'];
                            var sum = (parseFloat(oldAvgRating) * parseInt(count)) - parseInt(oldRating) + parseInt(newRating);

                            var newAvgRating = parseFloat(sum) / parseInt(count);


                            pathMergeObj['rating'] = Math.round(newAvgRating * 10) / 10;

                            //update path rating
                            console.log("updatePatchedRating ended");
                            db.merge('paths', pathId, pathMergeObj)
                                .then(function (result) {
                                })
                                .fail(function (err) {
                                    console.log(err.body.message);
                                })

                            //------------------------------------------------------------------------------------
                            //update the entire review
                            console.log("main merge started")
                            db.merge('reviews', reqBody.id, payload)
                                .then(function () {
                                    responseObj["data"] = payload;
                                    responseObj["data"]["id"] = reqBody.id;
                                    res.status(201);
                                    res.json(responseObj);

                                    //if (reqBody['rating'])
                                    //    updatePatchedRating(reqBody.pathId, reqBody.id, reqBody['rating']);
                                })
                                .fail(function (err) {
                                    responseObj["errors"] = [err.body.message];
                                    res.status(503);
                                    res.json(responseObj);
                                });
                            //------------------------------------------------------------------------------------
                        });
                })
                .fail(function (err) {
                    responseObj["errors"] = [err.body.message];
                    res.status(503);
                    res.json(responseObj);
                });
            //--------------------------------------------------------------------
        } else {
            //only do a merge
            console.log("attempting merge reviews !");
            //update the entire review
            db.merge('reviews', reqBody.id, payload)
                .then(function () {
                    console.log("merge reviews success!");
                    responseObj["data"] = payload;
                    responseObj["data"]["id"] = reqBody.id;
                    console.log(responseObj);
                    res.status(201);
                    res.json(responseObj);

                    //if (reqBody['rating'])
                    //    updatePatchedRating(reqBody.pathId, reqBody.id, reqBody['rating']);
                })
                .fail(function (err) {
                    responseObj["errors"] = [err.body.message];
                    res.status(503);
                    res.json(responseObj);
                });
        }
    }
}]);

module.exports = router;