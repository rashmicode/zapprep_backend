/**
 * Developer Notes:
 * The validation system could have been better
 * so now there is a lot of redundant validations
 * any changes in .post() should be manually changed in patch()
 * as well for consistency
 */

var express = require('express');
var router = express.Router();

var validator = require('validator');
validator.extend('isImage', function (mimetype) {
    return (mimetype == 'image/jpeg' || mimetype == 'image/png') ? true : false;
});
var dbUtils = require('../dbUtils')

/**
 * ultimate youtube regex, obviously:
 * http://stackoverflow.com/questions/2964678/jquery-youtube-url-validation-with-regex
 */
validator.extend('isYoutubeURL', function (url) {
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? true : false;
});

var kew = require('kew');
var passport = require('passport');
var _ = require('lodash');

var Notifications = require('../notifications');
var notify = new Notifications();

var config = require('../config.js'),
    customUtils = require('../utils.js');
var qbchat = require('../qbchat.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var multer = require('multer'),
    fs = require('fs');

var Path = require("../models/Path")

const util = require('util')

const NodeCache = require("node-cache");
const myCache = new NodeCache({stdTTL: 3600, checkperiod: 3620});

// console.log(util.inspect(myObject, {showHidden: false, depth: null}))

// alternative shortcut


router.get('/search', [passport.authenticate('bearer', {session: false}), function (req, res) {
    var responseObj = {};
    var userId = req.user.results[0].value.id;

    //lucene escape chars - + - && || ! ( ) { } [ ] ^ " ~ * ? : \
    //    //as taken from https://lucene.apache.org/core/2_9_4/queryparsersyntax.html#Escaping Special Characters
    //    //var escapeCharacters = "\+-\&|!\(\)\{\}\[\]\^\"~\*\? :"

    var query = req.query.query
    //query = validator.trim(query)
    //query = query + "*"

    console.log("query being searched for : ")
    console.log(query)

    //if (query.length < 3) {
    //    res.json({data: []})
    //    return
    //}

    db.newGraphReader()
        .get()
        .limit(100)
        .offset(0)
        .from('users', userId)
        .related('related')
        .then(function (results) {
            var consumedPaths = results.body.results.map(function (path) {
                return path.path.key
            })

            var theQuery = "((value.isPrivate:false) AND (value.title:" + query + " OR value.producer.name:" + query + " OR value.concepts.title:" + query + "))"
            console.log(theQuery)
            db.newSearchBuilder()
                .collection('paths')
                .limit(100)
                .offset(0)
                .query(theQuery)
                .then(function (result) {
                    //if (result.body.next) responseObj["more"] = true;
                    //else responseObj["more"] = false;

                    //responseObj["categories"] = config.categories;

                    responseObj["data"] = _.cloneDeep(result.body.results);
                    for (var i = 0 in responseObj["data"]) {
                        if (consumedPaths.indexOf(responseObj['data'][i].path.key) > -1) {
                            responseObj["data"][i].value["allowEnroll"] = false;
                        } else {
                            responseObj["data"][i].value["allowEnroll"] = true;
                        }
                        if (responseObj["data"][i].value.producer.id == userId) {
                            responseObj["data"][i].value["isOwner"] = true;
                        } else {
                            responseObj["data"][i].value["isOwner"] = false;
                        }

                        responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
                        responseObj["data"][i] = responseObj["data"][i].value;

                        for (var j = 0 in responseObj["data"][i]["concepts"]) {
                            if (responseObj["data"][i]["concepts"][j].title != "Introduction")
                                responseObj["data"][i]["concepts"][j].objects = undefined;
                        }

                        responseObj["data"][i].value = undefined;
                        responseObj["data"][i].path = undefined;
                        responseObj["data"][i].score = undefined;
                        responseObj["data"][i].reftime = undefined;
                    }
                    //responseObj["data"].sort(function (a, b) {
                    //    return b.studentCount - a.studentCount
                    //})
                    res.json(responseObj);
                })
                .fail(function (err) {
                    responseObj["errors"] = [err.body.message];
                    res.status(503);
                    res.json(responseObj);
                });
        })
}])

/**
 * With access token search
 */
router.get('/', function (req, res, next) {
    if (req.query.access_token) next();
    else next('route');
}, passport.authenticate('bearer', {session: false}), function (req, res) {

    if (!req.query.limit || req.query.limit < 0) req.query.limit = 10;
    if (!req.query.page || req.query.page < 1) req.query.page = 1;

    var limit = req.query.limit;
    var offset = limit * (req.query.page - 1);
    var responseObj = {};
    var userId = req.user.results[0].value.id;

    /**
     * auto suggestion array
     */
    var autoSuggest = []

    //TODO : test this with pagination
    db.newGraphReader()
        .get()
        .limit(limit)
        .offset(offset)
        .from('users', userId)
        .related('related')
        .then(function (content) {
            var consumedPaths = content.body.results.map(function (path) {
                return path.path.key
            })

            if (req.query.pathId) {
                db.get('paths', req.query.pathId)
                    .then(function (result) {
                        responseObj["data"] = _.cloneDeep(result.body);
                        if (consumedPaths.indexOf(req.query.pathId) > -1) responseObj['data']['allowEnroll'] = false;
                        else responseObj['data']['allowEnroll'] = true;
                        if (responseObj['data']['producer']['id'] == userId) responseObj['data']['isOwner'] = true;
                        else responseObj['data']['isOwner'] = false;
                        for (var j = 0 in responseObj["data"]["concepts"]) {
                            if (responseObj["data"]["concepts"][j].title != "Introduction")
                                responseObj["data"]["concepts"][j].objects = undefined;
                        }
                        responseObj['data']['id'] = req.query.pathId;
                        res.json(responseObj);
                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    });
            } else {
                //TODO:Use db.list() Dafuq didn't I use it in the first place! (maybe to sort the paths)
                db.newSearchBuilder()
                    .collection('paths')
                    .limit(limit)
                    .offset(offset)
                    .query('value.isPrivate:false AND value.category:*')
                    .then(function (result) {
                        if (result.body.next) responseObj["more"] = true;
                        else responseObj["more"] = false;

                        responseObj["categories"] = config.categories;

                        responseObj["data"] = _.cloneDeep(result.body.results);
                        /**
                         * Dirty stateful code with iteration variables
                         * TODO: move to functional appraoch
                         * TODO: this code is duplicated, have it one place for
                         * both the apis (with and without access token)
                         */
                        for (var i = 0 in responseObj["data"]) {
                            if (consumedPaths.indexOf(responseObj['data'][i].path.key) > -1) {
                                responseObj["data"][i].value["allowEnroll"] = false;
                            } else {
                                responseObj["data"][i].value["allowEnroll"] = true;
                            }
                            if (responseObj["data"][i].value.producer.id == userId) {
                                responseObj["data"][i].value["isOwner"] = true;
                            } else {
                                responseObj["data"][i].value["isOwner"] = false;
                            }

                            responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;

                            //dirtyness
                            responseObj["data"][i] = responseObj["data"][i].value;

                            /**
                             * Let's build the auto complete suggestion list here
                             * @type {path.key|*}
                             */
                            autoSuggest.push(responseObj["data"][i].producer.name)
                            autoSuggest.push(responseObj["data"][i].title)

                            /**
                             * TODO: move to functional approach
                             * .forEach please!
                             * (This is left as an exercise to the reader)
                             */
                            for (var j = 0 in responseObj["data"][i]["concepts"]) {
                                autoSuggest.push(responseObj["data"][i]["concepts"][j].title)
                                //responseObj["data"][i]["concepts"][j].objects.forEach(function (object) {
                                //    autoSuggest.push(object['title'])
                                //})
                                if (responseObj["data"][i]["concepts"][j].title != "Introduction")
                                    responseObj["data"][i]["concepts"][j].objects = undefined;
                            }

                            responseObj["data"][i].value = undefined;
                            responseObj["data"][i].path = undefined;
                            responseObj["data"][i].score = undefined;
                            responseObj["data"][i].reftime = undefined;
                        }
                        autoSuggest = getUniqueElements(autoSuggest)
                        autoSuggest.sort()
                        responseObj["autoSuggest"] = autoSuggest
                        responseObj["data"].sort(function (a, b) {
                            return b.studentCount - a.studentCount
                        })
                        res.json(responseObj);
                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    });
            }
        })
        .fail(function (err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        });
});

/**
 * Without access token search
 */
router.get('/', function (req, res) {
    if (!req.query.limit || req.query.limit < 0) req.query.limit = 10;
    if (!req.query.page || req.query.page < 1) req.query.page = 1;

    var limit = req.query.limit;
    var offset = limit * (req.query.page - 1);
    var responseObj = {};

    if (req.query.pathId) {
        db.get('paths', req.query.pathId)
            .then(function (result) {
                responseObj["data"] = _.cloneDeep(result.body);
                responseObj['data']['allowEnroll'] = true;
                for (var j = 0 in responseObj["data"]["concepts"]) {
                    if (responseObj["data"]["concepts"][j].title != "Introduction")
                        responseObj["data"]["concepts"][j].objects = undefined;
                }
                responseObj['data']['id'] = req.query.pathId;
                res.json(responseObj);
            })
            .fail(function (err) {
                responseObj["errors"] = [err.body.message];
                res.status(503);
                res.json(responseObj);
            });
    } else {
        //TODO:Use db.list() Dafuq didn't I use it in the first place! (maybe to sort the paths)
        db.newSearchBuilder()
            .collection('paths')
            .limit(limit)
            .offset(offset)
            .query('value.isPrivate:false')
            .then(function (result) {
                if (result.body.next) responseObj["more"] = true;
                else responseObj["more"] = false;

                responseObj["categories"] = config.categories;

                responseObj["data"] = _.cloneDeep(result.body.results);
                for (var i = 0 in responseObj["data"]) {

                    responseObj["data"][i].value["allowEnroll"] = true;

                    responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
                    responseObj["data"][i] = responseObj["data"][i].value;

                    for (var j = 0 in responseObj["data"][i]["concepts"]) {
                        if (responseObj["data"][i]["concepts"][j].title != "Introduction")
                            responseObj["data"][i]["concepts"][j].objects = undefined;
                    }

                    responseObj["data"][i].value = undefined;
                    responseObj["data"][i].path = undefined;
                    responseObj["data"][i].score = undefined;
                    responseObj["data"][i].reftime = undefined;
                }
                responseObj["data"].sort(function (a, b) {
                    return b.studentCount - a.studentCount
                })
                res.json(responseObj);
            })
            .fail(function (err) {
                responseObj["errors"] = [err.body.message];
                res.status(503);
                res.json(responseObj);
            });
    }
});

/**
 * version 2 of get paths
 */
router.route('/v2')
    .get([passport.authenticate('bearer', {session: false}), function (req, res, next) {
        var userId = req.user.results[0].value.id;
        var category = req.query.cat
        var subCategory = req.query.subCat
        //var accessCode = req.query.accessCode
        var page = req.query.page || 1
        var limit = req.query.limit || config.pagination.limit
        var responseObj = {
            "more": false,
            "data": []
        }

        if (category && subCategory) {
            //get paginated paths
            Path.getPathsOfSubCategory(category, subCategory, limit, page, userId)
                .then(function (response) {
                    res.status(200)
                    res.json(response)
                })
                .fail(function (err) {
                    responseObj["errors"] = err;
                    res.status(400);
                    res.json(responseObj);
                })
        } else if (category) {
            console.log("cat")
            Path.getPathsOfCategory(category, limit, page, userId)
                .then(function (response) {
                    res.status(200)
                    res.json(response)
                })
                .fail(function (err) {
                    responseObj["errors"] = err;
                    res.status(400);
                    /**
                     * from w3.org
                     * 10.4.1 400 Bad Request
                     * The request could not be understood by the server due to malformed syntax.
                     * The client SHOULD NOT repeat the request without modifications.
                     */
                    res.json(responseObj);
                })
        }
        //else if (accessCode) {
        //    Path.getPathByDiscountCode(accessCode, userId)
        //        .then(function (response) {
        //            res.status(200)
        //            res.json(response)
        //        })
        //        .fail(function (err) {
        //            responseObj["errors"] = [err.body.message];
        //            res.status(400);
        //            res.json(responseObj);
        //        })
        //}
        else {
            res.status(200)
            res.json(responseObj);
        }
    }])

router.route('/home')
    .get([passport.authenticate('bearer', {session: false}), function (req, res, next) {
        var userId = req.user.results[0].value.id

        Path.getHomePaths()
            .then(function (response) {
                res.status(200)
                res.json(response)
            })
            .fail(function (err) {
                res.status(400);
                res.json({errors: [err.body.message], errorObj: err});
            })
    }])

router.route('/codes')
    .get([passport.authenticate('bearer', {session: false}), function (req, res, next) {
        if (req.query.accessCode) {
            Path.getPathIdByAccessCode(req.query.accessCode)
                .then(function (response) {
                    res.status(200)
                    res.json({data: {id: response}})
                })
                .fail(function (err) {
                    res.status(400)
                    res.json({errors: [err.body.message], errorObj: err})
                })
        } else if (req.query.promoCode) {
            Path.getPathIdByDiscountCode(req.query.promoCode)
                .then(function (response) {
                    res.status(200)
                    res.json({data: response})
                })
                .fail(function (err) {
                    res.status(400)
                    res.json({errors: [err.body.message], errorObj: err})
                })
        } else {
            res.status(400)
            res.json({data: {}})
        }
    }])


//Create Path
router.route('/')
    .post([passport.authenticate('bearer', {session: false}), function (req, res, next) {
        qbchat.getSession(function (err, session) {
            if (err) {
                console.log("Recreating session");
                qbchat.createSession(function (err, result) {
                    if (err) {
                        res.status(503);
                        res.json({"errors": ["Can't connect to the chat server, try again later"]});
                    } else next();
                })
            } else next();
        })
    }, multer(), function (req, res) {
        var reqBody = req.body;
        var responseObj = {};
        var errors = new Array();

        if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");
        if (!validator.isLength(reqBody.desc, 20)) errors.push("Description must be greater than 20 characters");
        if (!validator.isIn(reqBody.category, config.categories)) errors.push("Select a valid category for this Learning Path");
        if (!validator.isYoutubeURL(reqBody.introVid)) errors.push("Introduction video must be a valid Youtube URL");
        if (validator.isNull(req.files.coverPhoto)) errors.push("A Cover photo must be uploaded");
        if (!validator.isNull(req.files.coverPhoto))
            if (!validator.isImage(req.files.coverPhoto.mimetype)) errors.push("Cover photo must be an image");
        if (validator.isBoolean(reqBody.coursePaid)) {
            if (reqBody.coursePaid == "true") {
                /**
                 * XOR logic to make sure either courseUSD or courseINR is set, not both
                 */
                if (myXOR(validator.isNull(reqBody.courseUSD), validator.isNull(reqBody.courseINR))) {
                    if (!validator.isNull(reqBody.courseUSD)) {
                        if (validator.isDecimal(reqBody.courseUSD)) {
                            reqBody.courseUSD = parseFloat(reqBody.courseUSD)
                            reqBody.courseUSD = Math.round(reqBody.courseUSD * 100) / 100
                            reqBody.courseINR = reqBody.courseUSD * parseInt(config.currency.USDtoINR);
                            reqBody.selectedCurrency = "USD"
                        } else {
                            errors.push("Please enter a valid USD currency for the course")
                        }
                    } else if (!validator.isNull(reqBody.courseINR)) {
                        if (validator.isDecimal(reqBody.courseINR)) {
                            reqBody.courseINR = parseFloat(reqBody.courseINR)
                            reqBody.courseINR = Math.round(reqBody.courseINR * 100) / 100
                            reqBody.courseUSD = reqBody.courseINR / parseFloat(config.currency.USDtoINR);
                            reqBody.mentorUSD = Math.round(reqBody.courseUSD * 100) / 100
                            reqBody.selectedCurrency = "INR"
                        } else {
                            errors.push("Please enter a valid INR currency for the course")
                        }
                    }
                } else {
                    errors.push("Enter the price of course in either USD or INR, not both");
                }
            } else {
                reqBody['courseUSD'] = undefined;
                reqBody['courseINR'] = undefined;
            }
        } else {
            errors.push("Please indicate if the course is paid (true/false)");
        }
        if (validator.isBoolean(reqBody.mentorPaid)) {
            if (reqBody.mentorPaid == "true") {
                /**
                 * XOR logic to make sure either mentorUSD or mentorINR is set, not both
                 */
                if (myXOR(validator.isNull(reqBody.mentorUSD), validator.isNull(reqBody.mentorINR))) {
                    if (!validator.isNull(reqBody.mentorUSD)) {
                        if (validator.isDecimal(reqBody.mentorUSD)) {
                            reqBody.mentorUSD = parseFloat(reqBody.mentorUSD)
                            reqBody.mentorUSD = Math.round(reqBody.mentorUSD * 100) / 100
                            reqBody.mentorINR = reqBody.mentorUSD * parseInt(config.currency.USDtoINR);
                            reqBody.selectedCurrency = "USD"
                        } else {
                            errors.push("Please enter a valid USD currency for the mentor channel")
                        }
                    } else if (!validator.isNull(reqBody.mentorINR)) {
                        if (validator.isDecimal(reqBody.mentorINR)) {
                            reqBody.mentorINR = parseFloat(reqBody.mentorINR)
                            reqBody.mentorINR = Math.round(reqBody.mentorINR * 100) / 100
                            reqBody.mentorUSD = reqBody.mentorINR / parseFloat(config.currency.USDtoINR);
                            reqBody.mentorUSD = Math.round(reqBody.mentorUSD * 100) / 100
                            reqBody.selectedCurrency = "INR"
                        } else {
                            errors.push("Please enter a valid INR currency for the mentor channel")
                        }
                    }
                } else {
                    errors.push("Enter the price of mentor channel in either USD or INR, not both");
                }
            } else {
                reqBody['mentorUSD'] = undefined;
                reqBody['mentorINR'] = undefined;
            }
        } else {
            errors.push("Please indicate if the mentor channel is paid (true/false)");
        }

        var userObj = req.user.results[0].value;
        var userId = userObj.id;
        userObj.password = undefined;

        if (errors.length > 0) {
            responseObj["errors"] = errors;
            res.status(422);
            res.json(responseObj);
        } else {
            customUtils.upload(req.files.coverPhoto, function (coverPhotoInfo) {
                console.log("customUtils.upload commented happened")
                console.log(coverPhotoInfo)
                customUtils.getLinkInfo(req.body.introVid, function (introVidInfo) {
                    console.log("getLinkInfo happened")
                    console.log(introVidInfo)
                    var introConceptId = customUtils.randomizer();
                    var introObjectId = customUtils.randomizer();
                    var date = new Date();
                    var payload = {
                        "created": date.getTime(),
                        "accessId": customUtils.generateToken(3),
                        "title": reqBody.title,
                        "desc": reqBody.desc,
                        //"buying": reqBody.buying,
                        //"price": reqBody.price,
                        "coverPhoto": coverPhotoInfo.url,
                        "coverPhotoThumb": coverPhotoInfo.urlThumb,
                        "producer": userObj,
                        "studentCount": 0,
                        "newStudents": 0,
                        "isPrivate": true,
                        "mentorPaid": stringToBoolean(reqBody.mentorPaid),
                        "coursePaid": stringToBoolean(reqBody.coursePaid),
                        "category": reqBody.category,
                        "concepts": [
                            {
                                "id": introConceptId,
                                "title": "Introduction",
                                //"pos": reqBody.introConceptPos,
                                "pos": 0,
                                "objects": [
                                    {
                                        "id": introObjectId,
                                        "type": "video",
                                        //"pos": reqBody.introObjectPos,
                                        "pos": 0,
                                        "url": reqBody.introVid,
                                        "title": introVidInfo.title,
                                        "desc": reqBody.introVidDesc,
                                        "image": introVidInfo.image
                                    }
                                ]
                            }
                        ],
                        "mentorUSD": reqBody.mentorUSD,
                        "mentorINR": reqBody.mentorINR,
                        "courseINR": reqBody.courseINR,
                        "courseUSD": reqBody.courseUSD,
                        "selectedCurrency": reqBody.selectedCurrency,
                        "mentorAvailable": true
                    };
                    console.log("about to db.post")
                    console.log("big payload")
                    console.log(util.inspect(payload, false, null))

                    //db.post('testy', payload)
                    //	.then(function(result) {
                    //		console.log("success")
                    //	})
                    //	.fail(function(err) {
                    //		console.log("error")
                    //		console.log(err)
                    //	})

                    db.post('paths', payload)
                        .then(function (result) {
                            console.log("db.post done")
                            var pathId = result.headers.location.match(/[0-9a-z]{16}/)[0];

                            db.newGraphBuilder()
                                .create()
                                .from('users', userId)
                                .related('produces')
                                .to('paths', pathId);

                            db.newGraphBuilder()
                                .create()
                                .from('paths', pathId)
                                .related('isProduced')//Every producer also consumes his course
                                .to('users', userId);

                            db.newGraphBuilder()
                                .create()
                                .from('users', userId)
                                .related('related')
                                .to('paths', pathId);

                            payload["id"] = pathId;

                            console.log("time to respond")
                            console.log(responseObj)
                            responseObj["data"] = payload;
                            res.status(201);
                            res.json(responseObj);

                            var notifObj = {
                                "pathId": pathId,
                                "pathTitle": reqBody.title,
                                "producerId": userId,
                                "producerName": userObj.name,
                                "producerPhoto": userObj.avatarThumb
                            };
                            notify.emit('newPath', notifObj);

                            var date = new Date();
                            var chatObjMentor = {
                                "created": date.getTime(),
                                "id": date.getTime(),
                                "pathId": pathId,
                                "qbId": userObj.qbId,
                                "type": "newMentor"
                            }

                            notify.emit('wordForChat', chatObjMentor);

                            var chatObj = {
                                "created": date.getTime(),
                                "type": "newChannel",
                                "pathId": pathId,
                                "pathTitle": reqBody.title
                            }

                            console.log("about to qbchat.createRoom")

                            qbchat.createRoom(2, 'General', function (err, newRoom) {
                                if (err) console.log(err);
                                else {
                                    qbchat.addUserToRoom(newRoom._id, [userObj.qbId], function (err, result) {
                                        if (err) console.log(err);
                                    })
                                    db.merge('paths', pathId, {"qbId": newRoom._id})
                                        .then(function (result) {
                                            chatObj["id"] = date.getTime() + "@1";
                                            chatObj["channelName"] = "General Channel";
                                            chatObj["channelId"] = newRoom._id;
                                            notify.emit('wordForChat', chatObj);
                                        })
                                        .fail(function (err) {
                                            console.log(err.body.message);
                                        });
                                }
                            });

                            qbchat.createRoom(2, "Mentor", function (err, newRoom) {
                                if (err) console.log(err);
                                else {
                                    qbchat.addUserToRoom(newRoom._id, [userObj.qbId], function (err, result) {
                                        if (err) console.log(err);
                                    })
                                    db.merge('paths', pathId, {"mentorRoomId": newRoom._id})
                                        .then(function (result) {
                                            chatObj["id"] = date.getTime() + "@2";
                                            chatObj["channelName"] = "Mentor Channel";
                                            chatObj["channelId"] = newRoom._id;
                                            notify.emit('wordForChat', chatObj);
                                        })
                                        .fail(function (err) {
                                            console.log(err.body.message);
                                        });
                                }
                            });

                            qbchat.createRoom(2, 'Introduction', function (err, newRoom) {
                                if (err) console.log(err);
                                else {
                                    qbchat.addUserToRoom(newRoom._id, [userObj.qbId], function (err, result) {
                                        if (err) console.log(err);
                                    })
                                    db.newPatchBuilder("paths", pathId)
                                        .merge("concepts.0", {"qbId": newRoom._id})
                                        .apply()
                                        .then(function (result) {
                                            chatObj["id"] = date.getTime() + "@3";
                                            chatObj["channelName"] = "Introduction";
                                            chatObj["channelId"] = newRoom._id;
                                            notify.emit('wordForChat', chatObj);
                                        })
                                        .fail(function (err) {
                                            console.log(err.body.message);
                                        })
                                }
                            });
                            console.log("end of the line")
                        })
                        .fail(function (err) {
                            console.log("db.post error")
                            console.log(err)
                            responseObj["errors"] = [err.body.message];
                            res.status(503);
                            res.json(responseObj);
                        })
                })
            });
        }

    }])


    //Edit Paths
    .patch([passport.authenticate('bearer', {session: false}), multer(), function (req, res) {

        if (Object.keys(req.body).length === 0) {
            res.status(422);
            res.json({"errors": ["No key was sent in the body"]});
            return;
        }
        var reqBody = req.body;
        var responseObj = {};
        var errors = new Array();

        if (validator.isNull(reqBody.pathId)) errors.push("PathId missing");

        if (!validator.isNull(reqBody.title))
            if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");

        if (!validator.isNull(reqBody.desc))
            if (!validator.isLength(reqBody.desc, 20)) errors.push("Description must be greater than 20 characters");

        /**
         * for now category will be manually set
         */
        //if (!validator.isNull(reqBody.category))
        //    if (!validator.isIn(reqBody.category, config.categories)) errors.push("Select a valid category for this Learning Path");

        //if (!validator.isNull(reqBody.buying))
        //    if (!validator.equals(reqBody.buying, "free")) errors.push("Buying should be free");

        /**
         * Deprecated
         */
        //if (!validator.isNull(reqBody.price)) errors.push("Price should be null");
        //if (!validator.isNull(reqBody.category))
        //    if (!validator.isIn(reqBody.category, config.categories)) errors.push("Select a valid category for this Learning Path");

        /**
         * This is stupid.
         * The introduction video is inside the Introduction concept
         * You are not suppose to have it here!
         */
        //if (!validator.isNull(reqBody.introVid)) {
        //    console.log("sup")
        //    if (!validator.isYoutubeURL(reqBody.introVid)) errors.push("Introduction video must be a valid Youtube URL");
        //}

        if (!validator.isNull(req.files.coverPhoto))
            if (!validator.isImage(req.files.coverPhoto.mimetype)) errors.push("Only an image file can be uploaded");

        if (!validator.isNull(reqBody.isPrivate))
            if (validator.isBoolean(reqBody.isPrivate)) {
                reqBody.isPrivate = ((reqBody.isPrivate.toLowerCase()) == "true") ? true : false;
            } else {
                errors.push("isPrivate can be true or false only");
            }

        var userId = req.user.results[0].value.id;

        if (!validator.isNull(reqBody.coursePaid) && !validator.isBoolean(reqBody.coursePaid))
            errors.push("coursePaid must be Boolean (true/false) ")

        //--------------------------------------------------------------------------------------------------------------
        //TODO: this validation is being repeated and is so crappy and messy
        //TODO: the error array has creeped into the front end and android as well
        //TODO: fix this all! post series A?
        //TODO: examine how netflix and wallmart use nodeJs in production

        //note selectedCurrency is reset for consistency
        //detected --- the user is attempting to edit the price of the course
        if (!validator.isNull(reqBody.courseINR) || !validator.isNull(reqBody.courseUSD)) {
            //assume if the user is trying to edit the course, it is already a paid course
            //lets make sure of that
            reqBody.coursePaid = true;
            if (myXOR(validator.isNull(reqBody.courseUSD), validator.isNull(reqBody.courseINR))) {
                if (!validator.isNull(reqBody.courseUSD)) {
                    if (validator.isDecimal(reqBody.courseUSD)) {
                        reqBody.courseUSD = parseFloat(reqBody.courseUSD)
                        reqBody.courseUSD = Math.round(reqBody.courseUSD * 100) / 100
                        reqBody.courseINR = reqBody.courseUSD * parseInt(config.currency.USDtoINR);
                        reqBody.selectedCurrency = "USD"
                    } else {
                        errors.push("Please enter a valid USD currency for the course")
                    }
                } else if (!validator.isNull(reqBody.courseINR)) {
                    if (validator.isDecimal(reqBody.courseINR)) {
                        reqBody.courseINR = parseFloat(reqBody.courseINR)
                        reqBody.courseINR = Math.round(reqBody.courseINR * 100) / 100
                        reqBody.courseUSD = reqBody.courseINR / parseFloat(config.currency.USDtoINR);
                        reqBody.mentorUSD = Math.round(reqBody.courseUSD * 100) / 100
                        reqBody.selectedCurrency = "INR"
                    } else {
                        errors.push("Please enter a valid INR currency for the course")
                    }
                }
            } else {
                errors.push("Enter the price of course in either USD or INR, not both");
            }
        }

        //detected --- the user is attempting to edit the price of the mentor channel
        if (!validator.isNull(reqBody.mentorINR) || !validator.isNull(reqBody.mentorUSD)) {
            //assume if the user is trying to edit the course, it is already a paid mentor channel
            //lets make sure of that. data integrity, you know.
            reqBody.mentorPaid = true;
            /**
             * XOR logic to make sure either mentorUSD or mentorINR is set, not both
             */
            if (myXOR(validator.isNull(reqBody.mentorUSD), validator.isNull(reqBody.mentorINR))) {
                if (!validator.isNull(reqBody.mentorUSD)) {
                    if (validator.isDecimal(reqBody.mentorUSD)) {
                        reqBody.mentorUSD = parseFloat(reqBody.mentorUSD)
                        reqBody.mentorUSD = Math.round(reqBody.mentorUSD * 100) / 100
                        reqBody.mentorINR = reqBody.mentorUSD * parseInt(config.currency.USDtoINR);
                        reqBody.selectedCurrency = "USD"
                    } else {
                        errors.push("Please enter a valid USD currency for the mentor channel")
                    }
                } else if (!validator.isNull(reqBody.mentorINR)) {
                    if (validator.isDecimal(reqBody.mentorINR)) {
                        reqBody.mentorINR = parseFloat(reqBody.mentorINR)
                        reqBody.mentorINR = Math.round(reqBody.mentorINR * 100) / 100
                        reqBody.mentorUSD = reqBody.mentorINR / parseFloat(config.currency.USDtoINR);
                        reqBody.mentorUSD = Math.round(reqBody.mentorUSD * 100) / 100
                        reqBody.selectedCurrency = "INR"
                    } else {
                        errors.push("Please enter a valid INR currency for the mentor channel")
                    }
                }
            } else {
                errors.push("Enter the price of mentor channel in either USD or INR, not both");
            }
        }
        //--------------------------------------------------------------------------------------------------------------

        if (errors.length > 0) {
            responseObj["errors"] = errors;
            res.status(422);
            res.json(responseObj);
        } else {
            customUtils.upload(req.files.coverPhoto, function (coverPhotoInfo) {
                var payload = {
                    "title": reqBody.title,
                    "desc": reqBody.desc,
                    //deprecated fields
                    // ----- "buying": reqBody.buying,
                    // ----- "price": reqBody.price,
                    "isPrivate": reqBody.isPrivate,
                    "category": reqBody.category,
                    //course and mentor paid settings
                    "coursePaid": reqBody.coursePaid,
                    "courseINR": reqBody.courseINR,
                    "courseUSD": reqBody.courseUSD,
                    "mentorPaid": reqBody.mentorPaid,
                    "mentorINR": reqBody.mentorINR,
                    "mentorUSD": reqBody.mentorUSD
                };

                if (req.files.coverPhoto) {
                    payload["coverPhoto"] = coverPhotoInfo.url
                    payload["coverPhotoThumb"] = coverPhotoInfo.urlThumb
                }

                db.merge('paths', reqBody.pathId, payload)
                    .then(function (result) {
                        //Don't want partial data, but full path data for front end
                        //responseObj["data"] = payload;
                        //res.status(201);
                        //res.json(responseObj);
                        db.get('paths', reqBody.pathId)
                            .then(function (result) {
                                result.body.id = reqBody.pathId
                                responseObj["data"] = result.body;
                                res.status(201);
                                res.json(responseObj);
                            })
                            .fail(function (err) {
                                responseObj["data"] = payload;
                                res.status(201);
                                res.json(responseObj);
                            })

                        if (req.files.coverPhoto) {
                            notify.getRecieversAndUpdatePhotos('path', reqBody.pathId, payload['coverPhotoThumb'])
                        }

                        if (typeof payload['title'] !== 'undefined') {
                            var date = new Date();
                            var chatObj = {
                                "type": "newCourseName",
                                "pathId": reqBody.pathId,
                                "created": date.getTime(),
                                "id": date.getTime(),
                                "pathTitle": payload['title']
                            }
                            notify.emit("wordForChat", chatObj)
                        }

                        // var notifObj = {
                        //     "where": reqBody.pathId
                        // };
                        // notify.emit('pathEdited', notifObj);

                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    });
            });

        }
    }])

    .delete([passport.authenticate('bearer', {session: false}), function (req, res, next) {
        qbchat.getSession(function (err, session) {
            if (err) {
                console.log("Recreating session");
                qbchat.createSession(function (err, result) {
                    if (err) {
                        res.status(503);
                        res.json({"errors": ["Can't connect to the chat server, try again later"]});
                    } else next();
                })
            } else next();
        })
    }, multer(), function (req, res) {
        var responseObj = {};
        var reqBody = req.body;
        var errors = new Array();
        var channels = [];

        if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

        if (errors.length > 0) {
            responseObj["errors"] = errors;
            res.status(422);
            res.json(responseObj);
        }
        else {
            db.get('paths', reqBody.pathId)
                .then(function (path) {
                    if (path.body.producer.id == req.user.results[0].value.id) {
                        path.body.concepts.forEach(function (concept) {
                            channels.push(concept.qbId);
                        })
                        channels.push(path.body.qbId);
                        channels.push(path.body.mentorRoomId);
                    } else {
                        responseObj["errors"] = ["Only the mentor can delete the Path"];
                        res.status(403);
                        res.json(responseObj);
                        return;
                    }
                })
                .then(function () {
                    channels.forEach(function (channel) {
                        qbchat.deleteRoom(channel, function (err, res) {
                            if (err) console.log(err)
                        })
                    })
                })
                .then(function () {
                    db.remove("paths", reqBody.pathId, true)
                        .then(function (result) {
                            responseObj["data"] = {"success": "Path Deleted"};
                            res.status(200);
                            res.json(responseObj);
                        })
                        .fail(function (err) {
                            responseObj["errors"] = [err.body.message];
                            res.status(503);
                            res.json(responseObj);
                        });
                })
                .fail(function (err) {
                    responseObj["errors"] = [err.body.message];
                    res.status(503);
                    res.json(responseObj);
                });
        }

    }]);

router.get('/suggest', function (req, res) {
    var query = req.query.query || ""
    Path.getAutoSuggestList(query)
        .then(function (response) {
            res.send(response)
            res.status(200)
        })
})

router.get('/users', function (req, res) {
    /**
     * HERE YE' HERE YE'
     * wrath of the sins of android be here
     * so android app makes infinite api calls in the peoplePage activity
     * if the number of users > 100 (the pagination is buggy)
     * that makes crazy amount of database calls that exhausts the database API limit.
     * cache the shit :)
     */
    var apiUrl = req._parsedUrl

    myCache.get(apiUrl.path, function (err, value) {
        if (!err) {
            if (value == undefined) {
                // key not found
                // normal API response
                if (!req.query.limit || req.query.limit < 0) req.query.limit = 100;
                if (!req.query.page || req.query.page < 1) req.query.page = 1;

                var limit = req.query.limit;
                var offset = (limit * (req.query.page - 1));
                var responseObj = {};
                var getPath = db.get('paths', req.query.pathId)
                var getConsumers = db.newGraphReader()
                    .get()
                    .limit(limit)
                    .offset(offset)
                    .from('paths', req.query.pathId)
                    .related('isConsumed')

                kew.all([getPath, getConsumers])
                    .then(function (content) {

                        var producer = content[0].body.producer;
                        producer["isMentor"] = true

                        if (content[1].body.next) responseObj["more"] = true;
                        else responseObj["more"] = false;

                        responseObj["data"] = _.cloneDeep(content[1].body.results);
                        for (var i = 0 in responseObj["data"]) {

                            responseObj["data"][i] = responseObj["data"][i].value;
                            responseObj["data"][i]["password"] = undefined;


                            //     responseObj["data"][i].value = undefined;
                            //     responseObj["data"][i].path = undefined;
                            //     responseObj["data"][i].score = undefined;
                            //     responseObj["data"][i].reftime = undefined;
                        }

                        responseObj.data.sort(function (a, b) {
                            if (a.name < b.name)
                                return -1
                            if (a.name > b.name)
                                return 1
                            return 0
                        })

                        if (req.query.page == 1) responseObj.data.unshift(producer);
                        myCache.set(apiUrl.path, responseObj, function (err, success) {
                            if (!err && success) {
                                console.log(success);
                                console.log("cache has been set");
                                console.log("key" + apiUrl.path);
                            }
                        });
                        res.json(responseObj);
                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    });
            } else {
                res.json(value);
            }
        }
    });


});

function myXOR(a, b) {
    return ( a || b ) && !( a && b );
}

function stringToBoolean(theString) {
    if (theString == "true") {
        return true;
    } else {
        return false;
    }
}

/**
 * Remove duplicate items in an array
 * http://stackoverflow.com/questions/6940103/how-do-i-make-an-array-with-unique-elements-i-e-remove-duplicates
 * @param a
 * @returns {Array}
 */
function getUniqueElements(a) {
    var temp = {};
    for (var i = 0; i < a.length; i++)
        temp[a[i]] = true;
    var r = [];
    for (var k in temp)
        r.push(k);
    return r;
}

module.exports = router;
