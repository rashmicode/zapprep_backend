var express = require('express');
var router = express.Router();
var validator = require('validator');

var passport = require('passport');

var constants = require('../constants');

var Notifications = require('../notifications');
var notify = new Notifications();

var config = require('../config.js'),
    customUtils = require('../utils.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var multer = require('multer'),
    fs = require('fs');

var concepts = require('./concepts.js');
//TODO: If file key is empty, response timeout - fix that.
//TODO: put all these validations in a seperate js that is then exported
/**
 * ultimate youtube regex, obviously:
 * http://stackoverflow.com/questions/2964678/jquery-youtube-url-validation-with-regex
 */
validator.extend('isYoutubeURL', function (url) {
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? true : false;
});

validator.extend('isVimeoURL', function (url) {
    var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(p)) ? true : false;
});

router.route('/')
    //Create objects in a concept
    .post([passport.authenticate('bearer', {session: false}), multer(), function (req, res) {
        var responseObj = {};
        var filetype;
        var reqBody = req.body;
        var errors = new Array();
        var objectId = customUtils.randomizer();
        var isVimeo = false

        var emitNotif = function (pathId, type, index) {
            var notifObj = {
                "pathId": pathId,
                "objectType": type,
                "conceptIndex": index
            };
            notify.emit('newObject', notifObj);
        }

        //if(validator.isNull(reqBody.index)) errors.push("Index has to be specified");
        if (validator.isNull(reqBody.conceptId)) errors.push("Concept id has to be specified");

        /**
         * Proudly remove such parent dependencies
         */
        //if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");
        if (!validator.isIn(reqBody.type, ['file', 'link', 'text', 'image'])) errors.push("type should be file/link/text/image. For video use type link");

        if (!validator.isNull(reqBody.title))
            if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");

        /**
         * pos (position) is not required and needs to be auto - generated
         * */
        //if (validator.isDecimal(reqBody.pos)) {
        //    reqBody.pos = parseFloat(reqBody.pos)
        //    if (reqBody.pos < 0) {
        //        errors.push("relative position(pos) of the concept cannot be less than 0");
        //    }
        //} else {
        //    errors.push("relative position of the concept must be a valid decimal number");
        //}

        if (errors.length > 0) {
            responseObj["errors"] = errors;
            res.status(422);
            res.json(responseObj);
        } else {
            concepts.convertConceptIdToIndex(reqBody.conceptId, function (err, pathId, index) {
                if (err) {
                    responseObj["errors"] = [err.message];
                    res.status(422);
                    res.json(responseObj);
                } else if (index == 0) {
                    responseObj["errors"] = ["You cannot add objects to the Introduction concept"];
                    res.status(422);
                    res.json(responseObj);
                } else {
                    concepts.getPositionOfLastObject(pathId, index, function (err, position) {
                        if (err) {
                            responseObj["errors"] = [err.message];
                            res.status(503);
                            res.json(responseObj);
                        } else {
                            position = position + 10000.0
                            //like a boss - no need error check since its already done
                            //in convertConceptIdToIndex
                            switch (reqBody.type) {
                                case 'text':
                                    if (!validator.isLength(reqBody.desc, 8, 10000)) errors.push("Description cannot be empty (8-10000 characters)");

                                    if (errors.length > 0) {
                                        responseObj["errors"] = errors;
                                        res.status(422);
                                        res.json(responseObj);
                                    } else {
                                        var payload = {
                                            "id": objectId,
                                            "type": reqBody.type,
                                            "pos": position,
                                            "title": reqBody.title,
                                            "desc": reqBody.desc
                                        };
                                        db.newPatchBuilder("paths", pathId)
                                            .append("concepts." + index + ".objects", payload)
                                            .apply()
                                            .then(function (result) {
                                                var resBody = payload;
                                                responseObj["data"] = resBody;
                                                res.status(201);
                                                res.json(responseObj);
                                                emitNotif(pathId, payload.type, index);
                                            })
                                            .fail(function (err) {
                                                responseObj["errors"] = [err.body.message];
                                                res.status(503);
                                                res.json(responseObj);
                                                return;
                                            });
                                    }
                                    break;
                                case 'link':
                                    if (!validator.isNull(reqBody.desc))
                                        if (!validator.isLength(reqBody.desc, 8, 3000)) errors.push("Description must be between 8-3000 characters");
                                    if (validator.isURL(req.body.url)) {
                                        if (req.body.url.indexOf("vimeo.com") > -1) {
                                            //all good
                                            var patt = constants.vimeo.regex
                                            var found = req.body.url.match(patt)

                                            if (found) {
                                                isVimeo = true
                                                reqBody.url = constants.vimeo.route + found[1]
                                            } else {
                                                errors.push("Please enter the Vimeo URL in the format : https://player.vimeo.com/video/<videoId>")
                                            }
                                        }
                                    } else {
                                        errors.push("Url must be a valid URL");
                                    }

                                    if (errors.length > 0) {
                                        responseObj["errors"] = errors;
                                        res.status(422);
                                        res.json(responseObj);
                                    }
                                    else {
                                        customUtils.getLinkInfo(reqBody.url, function (info) {
                                            //supporting only Youtube for now
                                            if (validator.isYoutubeURL(req.body.url)) {
                                                filetype = 'video';
                                            } else if (isVimeo) {
                                                filetype = 'vimeo'
                                            } else {
                                                filetype = 'link';
                                            }

                                            var payload = {
                                                "type": filetype,
                                                "id": objectId,
                                                "pos": position,
                                                "url": reqBody.url,
                                                "title": reqBody.title,
                                                "desc": reqBody.desc
                                            };

                                            if (info && info.image)
                                                payload["image"] = info.image
                                            else
                                                payload["image"] = constants.defaultLinkUrl
                                            db.newPatchBuilder("paths", pathId)
                                                .append("concepts." + index + ".objects", payload)
                                                .apply()
                                                .then(function (result) {
                                                    var resBody = payload;
                                                    responseObj["data"] = resBody;
                                                    res.status(201);
                                                    res.json(responseObj);
                                                    emitNotif(pathId, payload.type, index);
                                                })
                                                .fail(function (err) {
                                                    responseObj["errors"] = [err.body.message];
                                                    res.status(503);
                                                    res.json(responseObj);
                                                    return;
                                                });
                                        });
                                    }
                                    break;
                                case 'image':
                                case 'file':
                                    if (!validator.isNull(reqBody.desc))
                                        if (!validator.isLength(reqBody.desc, 8, 3000)) errors.push("Description must be 8-3000 characters");
                                    if (validator.isNull(req.files.file)) errors.push("Please select a file to upload");

                                    if (!validator.isNull(reqBody.title))
                                        if (!validator.isLength(reqBody.title, 4, 65)) errors.push("Title must be between 4-65 characters");

                                    if (!validator.isNull(reqBody.desc))
                                        if (!validator.isLength(reqBody.desc, 20)) errors.push("Description must be greater than 20 characters");

                                    if (errors.length > 0) {
                                        responseObj["errors"] = errors;
                                        res.status(422);
                                        res.json(responseObj);
                                    } else {
                                        customUtils.upload(req.files.file, function (info) {

                                            if (req.files.file.mimetype.match(/^image.*/)) {
                                                filetype = 'image';
                                            } else {
                                                filetype = 'file';
                                            }

                                            var payload = {
                                                "type": filetype,
                                                "id": objectId,
                                                "pos": position,
                                                "size": req.files.file.size,
                                                "mimetype": req.files.file.mimetype,
                                                "url": (info && info.url) ? info.url : "",
                                                "urlThumb": (info && info.urlThumb) ? info.urlThumb : "",
                                                "title": reqBody.title,
                                                "desc": reqBody.desc
                                            };
                                            if(filetype == "file")
                                                payload["urlThumb"] = getFilePreviewUrl(payload["mimetype"])

                                            db.newPatchBuilder("paths", pathId)
                                                .append("concepts." + index + ".objects", payload)
                                                .apply()
                                                .then(function (result) {
                                                    var resBody = payload;
                                                    responseObj["data"] = resBody;
                                                    res.status(201);
                                                    res.json(responseObj);
                                                    emitNotif(pathId, payload.type, index);
                                                })
                                                .fail(function (err) {
                                                    responseObj["errors"] = [err.body.message];
                                                    res.status(503);
                                                    res.json(responseObj);
                                                    return;
                                                });
                                        });
                                    }
                                    break;
                                //default:
                                //this is just not possible.
                                //responseObj["errors"] = ["Type must be file/link/text"];
                                //res.status(422);
                                //res.json(responseObj);
                                //return;
                            }
                        }
                    })
                }
            })
        }
    }])

//Edit conceptObjects
router.patch('/', [passport.authenticate('bearer', {session: false}), multer(), function (req, res) {

    if (Object.keys(req.body).length === 0) {
        res.status(422);
        res.json({"errors": ["No key was sent in the body"]});
        return;
    }

    var responseObj = {};
    var filetype;
    var reqBody = req.body;
    var response = null;
    var errors = new Array();

    if (validator.isNull(reqBody.id)) errors.push("Object id has to be specified");
    if (!validator.isNull(reqBody.pos))
        if (validator.isDecimal(reqBody.pos)) {
            reqBody.pos = parseFloat(reqBody.pos)
            /**
             * INTEGRITY CHECK:
             * Make sure object's position is not negative
             */
            if (reqBody.pos < 0) {
                errors.push("relative position(pos) of the object cannot be <= 0");
            }
        } else {
            errors.push("relative position(pos) of the object must be a valid decimal number");
        }
    if (!validator.isNull(reqBody.title))
        if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");

    console.log("all validations done")
    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        concepts.getConceptIndexAndObjectIndex(reqBody.id, function (err, pathId, conceptIndex, objectIndex) {
            if (err) {
                responseObj["errors"] = [err.message];
                res.status(422);
                res.json(responseObj);
            } else {
                console.log("getConceptIndexAndObjectIndex validation done")
                db.get('paths', pathId)
                    .then(function (path) {
                        console.log(path)
                        var conceptObject = path.body.concepts[conceptIndex].objects[objectIndex]
                        console.log(conceptObject)
                        switch (conceptObject.type) {
                            case 'text':
                                console.log("made it to case text")
                                if (!validator.isNull(reqBody.desc))
                                    if (!validator.isLength(reqBody.desc, 8, 10000)) errors.push("Description cannot be empty (8-10000 characters)");
                                if (errors.length > 0) {
                                    responseObj["errors"] = errors;
                                    res.status(422);
                                    res.json(responseObj);
                                } else {
                                    var payload = {
                                        "title": reqBody.title,
                                        "desc": reqBody.desc,
                                        "pos": reqBody.pos
                                    };

                                    db.newPatchBuilder("paths", pathId)
                                        .merge("concepts." + conceptIndex + ".objects." + objectIndex, payload)
                                        .apply()
                                        .then(function (result) {
                                            responseObj["data"] = payload;
                                            res.status(201);
                                            res.json(responseObj);
                                        })
                                        .fail(function (err) {
                                            responseObj["errors"] = [err.body.message];
                                            res.status(503);
                                            res.json(responseObj);
                                        });

                                }
                                break;
                            case 'video':
                            case 'link':
                                console.log("made it to link/video")
                                if (!validator.isNull(reqBody.desc))
                                    if (!validator.isLength(reqBody.desc, 8, 3000)) errors.push("Description must be between 8-3000 characters");
                                if (!validator.isNull(reqBody.url)) {
                                    if (conceptObject.type == "link") {
                                        if (!validator.isURL(reqBody.url)) errors.push("link must be a valid URL");
                                    } else if (conceptObject.type == "video") {
                                        if (!validator.isYoutubeURL(reqBody.url)) errors.push("link must be a valid Youtube URL");
                                    }
                                }
                                if (errors.length > 0) {
                                    responseObj["errors"] = errors;
                                    res.status(422);
                                    res.json(responseObj);
                                }
                                else {
                                    customUtils.getLinkInfo(reqBody.url, function (info) {
                                        var payload = {
                                            "url": reqBody.url,
                                            "title": reqBody.title,
                                            "desc": reqBody.desc,
                                            "pos": reqBody.pos
                                        };
                                        if (info && info.image)
                                            payload["image"] = info.image
                                        else
                                            payload["image"] = constants.defaultLinkUrl
                                        db.newPatchBuilder("paths", pathId)
                                            .merge("concepts." + conceptIndex + ".objects." + objectIndex, payload)
                                            .apply()
                                            .then(function (result) {
                                                responseObj["data"] = payload;
                                                res.status(201);
                                                res.json(responseObj);
                                            })
                                            .fail(function (err) {
                                                responseObj["errors"] = [err.body.message];
                                                res.status(503);
                                                res.json(responseObj);
                                            });
                                    });
                                }
                                break;
                            case 'image':
                            case 'file':
                                if (!validator.isNull(reqBody.desc))
                                    if (!validator.isLength(reqBody.desc, 8, 3000)) errors.push("Description must be between 8-3000 characters");
                                customUtils.upload(req.files.file, function (info) {
                                    if (req.files.file)
                                        if (req.files.file.mimetype.match(/^image.*/)) {
                                            filetype = 'image'
                                        } else {
                                            filetype = 'file'
                                        }
                                    else {
                                        filetype = 'file'
                                    }

                                    var payload = {
                                        "type": filetype,
                                        "size": req.files.file.size,
                                        "mimetype": req.files.file.mimetype,
                                        "url": (info && info.url) ? info.url : "",
                                        "urlThumb": (info && info.urlThumb) ? info.url : "",
                                        "pos": reqBody.pos,
                                        "title": reqBody.title,
                                        "desc": reqBody.desc
                                    };
                                    db.newPatchBuilder("paths", pathId)
                                        .merge("concepts." + conceptIndex + ".objects." + objectIndex, payload)
                                        .apply()
                                        .then(function (result) {
                                            responseObj["data"] = payload;
                                            res.status(201);
                                            res.json(responseObj);
                                        })
                                        .fail(function (err) {
                                            responseObj["errors"] = [err.body.message];
                                            res.status(503);
                                            res.json(responseObj);
                                        });
                                });
                                break;
                        }
                    })
                    .fail(function (err) {
                        //Again not really possible
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    });
            }
        })
    }
}]);

router.delete('/', [passport.authenticate('bearer', {session: false}), multer(), function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = new Array();

    if (validator.isNull(reqBody.id)) errors.push("Object id has to be specified");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    }
    else {
        concepts.getConceptIndexAndObjectIndex(reqBody.id, function (err, pathId, conceptIndex, objectIndex) {
            if (err) {
                responseObj["errors"] = [err.message];
                res.status(422);
                res.json(responseObj);
            } else if (conceptIndex == 0) {
                responseObj["errors"] = ["Objects inside Introduction concept cannot be deleted"];
                res.status(422);
                res.json(responseObj);
            } else {
                db.newPatchBuilder("paths", pathId)
                    .remove("concepts." + conceptIndex + ".objects." + objectIndex)
                    .apply()
                    .then(function (result) {
                        responseObj["data"] = {"success": "Object Deleted"};
                        ;
                        res.status(200);
                        res.json(responseObj);
                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    });
            }
        })
    }
}]);

function getFilePreviewUrl(mimeType) {
    var url = "";
    switch (mimeType) {
        //These are the mimetypes which google docs support. Please dont add any more
        case "application/msword":
        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
        case "application/vnd.openxmlformats-officedocument.wordprocessingml.template":
        case "application/vnd.oasis.opendocument.text":
            url = "https://s3.amazonaws.com/pyoopil-prod-server/doc_file_type.png";
            break;
        case "application/vnd.ms-excel":
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.template":
            url = "https://s3.amazonaws.com/pyoopil-prod-server/excel_file_type.png";
            break;
        case "application/vnd.openxmlformats-officedocument.presentationml.template":
        case "application/vnd.openxmlformats-officedocument.presentationml.slideshow":
        case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
        case "application/vnd.openxmlformats-officedocument.presentationml.slide":
        case "application/mspowerpoint":
        case "application/powerpoint":
        case "application/vnd.ms-powerpoint":
        case "application/x-mspowerpoint":
            url = "https://s3.amazonaws.com/pyoopil-prod-server/ppt_file_type.png";
            break;
        case "application/pdf":
            url = "https://s3.amazonaws.com/pyoopil-prod-server/pdf_file_type.png";
            break;
        default:
            url = "https://s3.amazonaws.com/pyoopil-prod-server/file_type.png";
    }
    return url
}

module.exports = router;