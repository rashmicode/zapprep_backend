var express = require('express');
var router = express.Router();

var passport = require('passport');
var _ = require('lodash');
var bcrypt = require('bcryptjs');
var request = require('request');

var kew = require('kew');
var async = require('async');

var multer = require('multer'),
    fs = require('fs');

var config = require('../config.js');
var customUtils = require('../utils.js');
var qbchat = require('../qbchat.js');
//var qbConsumerChat = require('../qbConsumerChat.js');
var Path = require("../models/Path")
var Chat = require("../models/Chat")

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var date = new Date();
var now = date.getTime();

var Firebase = require("firebase");
var myFirebaseRef = new Firebase(config.firebase.url, config.firebase.secret);

var Notifications = require('../notifications');
var notify = new Notifications();

var unreader = require('../unreader');

var bunyan = require('bunyan');
var log = bunyan.createLogger({
    name: 'dashboardErrors',
    streams: [
        {
            path: __dirname + '/dashboardErrors.log'  // log ERROR and above to a file
        }
    ]
});

var CronJob = require('cron').CronJob;

var mailgun = require('mailgun-js')({apiKey: config.mailgun.key, domain: config.mailgun.domain});
var validator = require('validator');
var jwt = require('jsonwebtoken');

var QB = require('quickblox');
QB.init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);

/**
 * disable require cache to get a new QB object for consumer
 * so that the sessions dont clash!
 ***/
//Object.keys(require.cache).forEach(function(key) {
//    //delete require.cache[key]
//    if(key.indexOf("node_modules/quickblox") > -1) {
//        console.log(key)
//        delete require.cache[key]
//    }
//})

validator.extend('isImage', function (mimetype) {
    if (mimetype.match(/^image.*/)) return true;
    else return false;
});

/**
 * google login exchanging encrypted jwt tokens
 */
router.post('/auth/google', function (req, res, next) {
    qbchat.getSession(function (err, session) {
        if (err) {
            console.log("Recreating session");
            qbchat.createSession(function (err, result) {
                if (err) {
                    res.status(503);
                    res.json({"errors": ["Can't connect to the chat server, try again later"]});
                } else next();
            })
        } else next();
    })
    }, function (req, res) {
    var responseObj = {}

    var encryptedJwt = req.body.code;
    var isVerified = undefined;

    var decoded = undefined;
    var header = undefined;
    var payload = undefined;

    var avatar = undefined;
    var avatarThumb = undefined;

    var errors = new Array();

    if (validator.isNull(encryptedJwt)) errors.push("No JWT Token provided");
    else {
        decoded = jwt.decode(encryptedJwt, {complete: true});
        header = decoded.header;
        payload = decoded.payload;
        if (validator.isNull(payload.sub)) errors.push("Profile ID is missing");
        if (validator.isNull(payload.email)) errors.push("Email is missing");
        if (validator.isNull(payload.name)) errors.push("Name is missing");
        //if profile is missing set your own profile, because it has not been set in google.
        if (validator.isNull(payload.picture)) {
            avatar = "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg";
            avatarThumb = "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg";
        } else {
            avatar = payload.picture.replace('s96-c/', '');
            avatarThumb = payload.picture;
        }
    }

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        db.newSearchBuilder()
            .collection('users')
            .query('value.google:`' + payload.sub + '`')
            .then(function (fetchedUser) {
                if (fetchedUser.body.total_count === 1) {
                    //already signed up by google
                    console.log("already signed up by google");
                    //login time
                    generateTokenAndLogin(fetchedUser, res);
                } else {
                    //not already signed up by google
                    console.log("not already signed up by google");

                    db.newSearchBuilder()
                        .collection('users')
                        .query('email:`' + payload.email + '`')
                        .then(function (user) {
                            if (user.body.total_count === 0) {
                                //email does not exist
                                console.log("email does not exist, sign up time");
                                signUpFreshGoogleUser(payload, avatar, avatarThumb, res);
                            } else {
                                //email does exist, linking accounts...
                                //untested
                                console.log("email does exist, linking accounts...");
                                console.log(user.body.results[0].value.id);
                                var mergeData = {
                                    "google": payload.sub,
                                    "avatar": avatar,
                                    "avatarThumb": avatarThumb
                                };
                                db.merge('users', user.body.results[0].value.id, mergeData)
                                    .then(function (linkedUser) {
                                        console.log("merging happened");
                                        console.log(user.body.results[0].value);
                                        responseObj['data'] = user.body.results[0].value;
                                        responseObj['data']['google'] = mergeData.google;
                                        responseObj['data']['avatar'] = mergeData.avatar;
                                        responseObj['data']['avatarThumb'] = mergeData.avatarThumb;
                                        console.log(responseObj);

                                        var accessToken = customUtils.generateToken();
                                        db.put('tokens', accessToken, {
                                            "user": responseObj['data']['id']
                                        })
                                            .then(function (result) {
                                                db.newGraphBuilder()
                                                    .create()
                                                    .from('tokens', accessToken)
                                                    .related('hasUser')
                                                    .to('users', responseObj['data']['id'])
                                                    .then(function (result) {
                                                        responseObj['data']["access_token"] = accessToken;
                                                        responseObj['data']["password"] = undefined;
                                                        res.status(201);
                                                        res.json(responseObj);
                                                    })
                                            })
                                    })
                            }
                        })
                }
            })
            .fail(function (err) {
                responseObj["errors"] = ['Service failure. Contact Pyoopil immediately'];
                res.status(422);
                res.json(responseObj);
            })
    }
});

router.post('/auth/facebook', function (req, res, next) {
        qbchat.getSession(function (err, session) {
            if (err) {
                console.log("Recreating session");
                qbchat.createSession(function (err, result) {
                    if (err) {
                        res.status(503);
                        res.json({"errors": ["Can't connect to the chat server, try again later"]});
                    } else next();
                })
            } else next();
        })
    }, function (req, res) {
        var responseObj = {};
        var errors = new Array();
        var changeEmail = false;

        var accessToken = req.body.code;
        if (validator.isNull(accessToken)) errors.push("Access Token not provided");

        if (errors.length > 0) {
            responseObj["errors"] = errors;
            res.status(422);
            res.json(responseObj);
        } else {
            //--------------URLs-----------------------------------------------------------------------
            accessTokenUrl = "https://graph.facebook.com/v2.3/me?fields=id,name,email&access_token=" + accessToken;
            //--------------URLs End -------------------------------------------------------------------
            request.get({url: accessTokenUrl, json: true}, function (err, response, payload) {
                if (response.statusCode !== 200) {
                    return res.status(response.statusCode).send({errors: [payload.error.message]});
                }
                if (validator.isNull(payload.id)) return res.status(409).send("Profile ID could not be retrieved");
                if (validator.isNull(payload.name)) return res.status(409).send("Name could not be retrieved");
                if (validator.isNull(payload.email)) {
                    payload.email = "brahmasth" + customUtils.generateToken(4) + "@mailinator.com";
                    changeEmail = true;
                }

                avatar = "https://graph.facebook.com/" + payload.id + "/picture?type=large";
                avatarThumb = "https://graph.facebook.com/" + payload.id + "/picture";

                //----------------------------------------sign up scenarios ----------------------------
                console.log("entering search?");
                db.newSearchBuilder()
                    .collection('users')
                    .query('value.facebook:`' + payload.id + '`')
                    .then(function (fetchedUser) {
                        if (fetchedUser.body.total_count === 1) {
                            //already signed up by facebook
                            console.log("already signed up by facebook");
                            //login time
                            generateTokenAndLogin(fetchedUser, res);
                            extractFacebookFriends(fetchedUser.body.results[0].value.id, accessToken);
                        } else {
                            //not already signed up by facebook
                            console.log("not already signed up by facebook");

                            db.newSearchBuilder()
                                .collection('users')
                                .query('email:`' + payload.email + '`')
                                .then(function (user) {
                                    if (user.body.total_count === 0) {
                                        //email does not exist
                                        console.log("email does not exist, sign up time");
                                        //this also handles extracting friends
                                        signUpFreshFacebookUser(payload, avatar, avatarThumb, res, changeEmail, accessToken);
                                    } else {
                                        //email does exist, linking accounts...
                                        console.log("email does exist, linking accounts...");
                                        console.log(user.body.results[0].value.id);
                                        var mergeData = {
                                            "facebook": payload.id,
                                            "avatar": avatar,
                                            "avatarThumb": avatarThumb
                                        };
                                        db.merge('users', user.body.results[0].value.id, mergeData)
                                            .then(function (linkedUser) {
                                                console.log("merging happened");
                                                console.log(user.body.results[0].value);
                                                responseObj['data'] = user.body.results[0].value;
                                                responseObj['data']['facebook'] = mergeData.facebook;
                                                responseObj['data']['avatar'] = mergeData.avatar;
                                                responseObj['data']['avatarThumb'] = mergeData.avatarThumb;
                                                console.log(responseObj);

                                                var accessToken = customUtils.generateToken();
                                                db.put('tokens', accessToken, {
                                                    "user": responseObj['data']['id']
                                                })
                                                    .then(function (result) {
                                                        db.newGraphBuilder()
                                                            .create()
                                                            .from('tokens', accessToken)
                                                            .related('hasUser')
                                                            .to('users', responseObj['data']['id'])
                                                            .then(function (result) {
                                                                responseObj['data']["access_token"] = accessToken;
                                                                responseObj['data']["password"] = undefined;
                                                                res.status(201);
                                                                res.json(responseObj);
                                                            })
                                                    })
                                            })
                                        extractFacebookFriends(user.body.results[0].value.id, accessToken)
                                    }
                                })
                        }
                    })
                    .fail(function (err) {
                        responseObj["errors"] = ['Service failure. Contact Pyoopil immediately'];
                        res.status(422);
                        res.json(responseObj);
                    })
                //--------------------------------end of signup scenarios----------------------------------------------------


            });
        }
    }
)

router.post('/auth/facebook2', function (req, res, next) {
    qbchat.getSession(function (err, session) {
        if (err) {
            console.log("Recreating session");
            qbchat.createSession(function (err, result) {
                if (err) {
                    res.status(503);
                    res.json({"errors": ["Can't connect to the chat server, try again later"]});
                } else next();
            })
        } else next();
    })
    }, function (req, res) {
    var responseObj = {};
    var errors = new Array();
    var changeEmail = false;

    var accessToken = req.body.code;
    if (validator.isNull(accessToken)) errors.push("Access Token not provided");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {

        var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name'];
        var accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token';
        //var graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');
        var params = {
            code: req.body.code,
            client_id: config.facebookAuth.clientId,
            client_secret: config.facebookAuth.clientSecret,
            redirect_uri: req.body.redirectUri
        };

        // Step 1. Exchange authorization code for access token.
        request.get({url: accessTokenUrl, qs: params, json: true}, function (err, response, accessToken) {
            if (response.statusCode !== 200) {
                return res.status(500).send({errors: [accessToken.error.message]});
            }
            accessToken = accessToken.access_token;
            /**
             * Response format :
             * accessToken = {
                 *         "access_token": "CAAI1acbiMrMBANdKjN3dd8KDDRoy6hc2ZCzAZBuOzorkJYAHNlXUOB75k81VZCJDZArOTNtYhdbrWWvwEeM9qZC2TAhg2pY7lZC4UokGgG2w4Yl0QOuacxxedBIyPLhZBuZCPtugwGySoSOQXeHGpLeZAMa3EygD5uvla0NxtDNeSUk59FMPTnRH3UkxA0vYKWgQZD",
                 *          "token_type": "bearer",
                 *          "expires_in": 5178279
                 * }
             */
            //--------------URLs-----------------------------------------------------------------------
            var graphApiUrl = "https://graph.facebook.com/v2.3/me?fields=id,name,email&access_token=" + accessToken;
            //--------------URLs End -------------------------------------------------------------------
            request.get({url: graphApiUrl, json: true}, function (err, response, payload) {
                if (response.statusCode !== 200) {
                    return res.status(response.statusCode).send({errors: [payload.error.message]});
                }
                if (validator.isNull(payload.id)) return res.status(409).send("Profile ID could not be retrieved");
                if (validator.isNull(payload.name)) return res.status(409).send("Name could not be retrieved");
                if (validator.isNull(payload.email)) {
                    payload.email = "brahmasth" + customUtils.generateToken(4) + "@mailinator.com";
                    changeEmail = true;
                }

                avatar = "https://graph.facebook.com/" + payload.id + "/picture?type=large";
                avatarThumb = "https://graph.facebook.com/" + payload.id + "/picture";

                //----------------------------------------sign up scenarios ----------------------------
                console.log("entering search?");
                db.newSearchBuilder()
                    .collection('users')
                    .query('value.facebook:`' + payload.id + '`')
                    .then(function (fetchedUser) {
                        if (fetchedUser.body.total_count === 1) {
                            //already signed up by facebook
                            console.log("already signed up by facebook");
                            //login time
                            generateTokenAndLogin(fetchedUser, res);
                            extractFacebookFriends(fetchedUser.body.results[0].value.id, accessToken);
                        } else {
                            //not already signed up by facebook
                            console.log("not already signed up by facebook");

                            db.newSearchBuilder()
                                .collection('users')
                                .query('email:`' + payload.email + '`')
                                .then(function (user) {
                                    if (user.body.total_count === 0) {
                                        //email does not exist
                                        console.log("email does not exist, sign up time");
                                        //this also handles extracting friends
                                        signUpFreshFacebookUser(payload, avatar, avatarThumb, res, changeEmail, accessToken);
                                    } else {
                                        //email does exist, linking accounts...
                                        console.log("email does exist, linking accounts...");
                                        console.log(user.body.results[0].value.id);
                                        var mergeData = {
                                            "facebook": payload.id,
                                            "avatar": avatar,
                                            "avatarThumb": avatarThumb
                                        };
                                        db.merge('users', user.body.results[0].value.id, mergeData)
                                            .then(function (linkedUser) {
                                                console.log("merging happened");
                                                console.log(user.body.results[0].value);
                                                responseObj['data'] = user.body.results[0].value;
                                                responseObj['data']['facebook'] = mergeData.facebook;
                                                responseObj['data']['avatar'] = mergeData.avatar;
                                                responseObj['data']['avatarThumb'] = mergeData.avatarThumb;
                                                console.log(responseObj);

                                                var accessToken = customUtils.generateToken();
                                                db.put('tokens', accessToken, {
                                                    "user": responseObj['data']['id']
                                                })
                                                    .then(function (result) {
                                                        db.newGraphBuilder()
                                                            .create()
                                                            .from('tokens', accessToken)
                                                            .related('hasUser')
                                                            .to('users', responseObj['data']['id'])
                                                            .then(function (result) {
                                                                responseObj['data']["access_token"] = accessToken;
                                                                responseObj['data']["password"] = undefined;
                                                                res.status(201);
                                                                res.json(responseObj);
                                                            })
                                                    })
                                            })
                                        extractFacebookFriends(user.body.results[0].value.id, accessToken)
                                    }
                                })
                        }
                    })
                    .fail(function (err) {
                        responseObj["errors"] = ['Service failure. Contact Pyoopil immediately'];
                        res.status(422);
                        res.json(responseObj);
                    })
                //--------------------------------end of signup scenarios----------------------------------------------------


            })
        });
    }
})


/**
 * extracts and merges facebook friend data
 * -----------------------------
 * Data looks like this:
 * -----------------------------
 * {
 *  "data": [
 *     {
 *        "name": "Ankan Adhikari",
 *        "id": "10152996688646213"
 *     },
 *     {
 *        "name": "Sharan Bhargava",
 *        "id": "10153755428974359"
 *     },
 *     {
 *        "name": "Tushar Banka",
 *        "id": "10206602006322787"
 *     }
 *  ],
 *  "paging": {
 *     "next": "https://graph.facebook.com/v2.4/897667640271151/friends?access_token=someAccessToken&limit=25&offset=25&__after_id=enc_AdBcwFC9ggIKy25omRcLqkWi6ak6QWz0BptZAktHBARYXZCZBEO5NLNMZBZCtOvyEaQiyfJcZD"
 *  },
 *  "summary": {
 *     "total_count": 1217
 *  }
 * }
 * ------------------------------
 * Last page looks like this:
 * ------------------------------
 * {
 *  "data": [
 *
 *  ],
 *  "paging": {
 *     "previous": "https://graph.facebook.com/v2.4/897667640271151/friends?limit=25&offset=0&access_token=someAccessToken"
 *  },
 *  "summary": {
 *     "total_count": 1217
 *  }
 * }
 * -------------------------------
 * @param userId
 * @param accessToken
 */
var extractFacebookFriends = function (userId, accessToken) {
    var friendsUrl = "https://graph.facebook.com/v2.3/me/friends?access_token=" + accessToken
    var friendsData = []

    var getFriends = function (theUrl) {
        request.get({url: theUrl, json: true}, function (err, response, payload) {
            if (response.statusCode == 200) {
                if (payload.data && payload.data.length > 0) {
                    //we have a few friends to store
                    friendsData.push.apply(friendsData, payload.data)
                }
                if (payload.paging) {
                    //payload.paging.previous means this is the last page
                    if (payload.paging.previous) {
                        //this is the last page, all friendsData is finally here
                        db.merge('users', userId, {
                            "facebookFriends": friendsData
                        })
                            .then(function (result) {
                            })
                            .fail(function (err) {
                            })
                    } else {
                        getFriends(payload.paging.next)
                    }
                }
            }
        })
    }
    getFriends(friendsUrl)
}

/*
 |--------------------------------------------------------------------------
 | Login with Google
 |--------------------------------------------------------------------------
 */
//router.post('/auth/googleTest', function (req, res) {
//    var responseObj = {};
//    var errors = new Array();
//
//    var code = req.body.code
//    var redirectUri = req.body.redirectUri
//
//    var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
//    var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
//    var params = {
//        code: req.body.code,
//        client_id: config.googleAuth.clientId,
//        client_secret: config.googleAuth.clientSecret,
//        redirect_uri: req.body.redirectUri,
//        grant_type: 'authorization_code'
//    };

// Step 1. Exchange authorization code for access token.
//request.post(accessTokenUrl, {json: true, form: params}, function (err, response, token) {
//var accessToken = token.access_token;
//var headers = {Authorization: 'Bearer ' + accessToken};

//res.status(200).send({accessToken : token})

//// Step 2. Retrieve profile information about the current user.
//request.get({url: peopleApiUrl, headers: headers, json: true}, function (err, response, profile) {
//    if (profile.error) {
//        return res.status(500).send({message: profile.error.message});
//    }
//    // Step 3a. Link user accounts.
//    if (req.headers.authorization) {
//        User.findOne({google: profile.sub}, function (err, existingUser) {
//            if (existingUser) {
//                return res.status(409).send({message: 'There is already a Google account that belongs to you'});
//            }
//            var token = req.headers.authorization.split(' ')[1];
//            var payload = jwt.decode(token, config.TOKEN_SECRET);
//            User.findById(payload.sub, function (err, user) {
//                if (!user) {
//                    return res.status(400).send({message: 'User not found'});
//                }
//                user.google = profile.sub;
//                user.picture = user.picture || profile.picture.replace('sz=50', 'sz=200');
//                user.displayName = user.displayName || profile.name;
//                user.save(function () {
//                    var token = createJWT(user);
//                    res.send({token: token});
//                });
//            });
//        });
//    } else {
//        // Step 3b. Create a new user account or return an existing one.
//        User.findOne({google: profile.sub}, function (err, existingUser) {
//            if (existingUser) {
//                return res.send({token: createJWT(existingUser)});
//            }
//            var user = new User();
//            user.google = profile.sub;
//            user.picture = profile.picture.replace('sz=50', 'sz=200');
//            user.displayName = profile.name;
//            user.save(function (err) {
//                var token = createJWT(user);
//                res.send({token: token});
//            });
//        });
//    }
//});
//});
//});


router.post('/auth/googleTest', function (req, res) {
    var accessTokenUrl = 'https://accounts.google.com/o/oauth2/token?access_type=offline';
    var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect?access_type=offline';
    var params = {
        code: req.body.code,
        client_id: config.googleAuth.clientId,
        client_secret: config.googleAuth.clientSecret,
        redirect_uri: req.body.redirectUri,
        grant_type: 'authorization_code'
    };

    // Step 1. Exchange authorization code for access token.
    request.post(accessTokenUrl, {json: true, form: params}, function (err, response, token) {
        var accessToken = token.access_token;
        var headers = {Authorization: 'Bearer ' + accessToken}

        res.status(200).send({ok: token})
    })
})

/**
 * Traditional access token exchanging google login
 */
router.post('/auth/google2', function (req, res) {
    var responseObj = {}
    var accessTokenUrl = 'https://www.googleapis.com/oauth2/v3/token';
    var peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';

    //this code is the encrypted jwt
    var params = {
        code: req.body.code,
        client_id: '1082471763011-hhq5l2knp2dfris322a20sqvgrgdbjp9.apps.googleusercontent.com',
        client_secret: 'VV1_ucTtDKOTBWcWj095mg67',
        redirect_uri: 'http://192.168.1.169:3000/',
        grant_type: 'authorization_code'
    };

    // Step 1. Exchange authorization code for access token.
    request.post(accessTokenUrl, {json: true, form: params}, function (err, response, token) {
        var accessToken = token;
        var headers = {Authorization: 'Bearer ' + accessToken};
        var isVerified = undefined;

        if (err) {
            return res.status(500).send({message: 'Invalid authorization code!'});
            return res.status(500).send({message: 'Invalid authorization code!'});
        } else {
            // Step 2. Retrieve profile information about the current user.
            request.get({url: peopleApiUrl, headers: headers, json: true}, function (err, response, profile) {
                if (profile.error) {
                    return res.status(500).send({message: profile.error.message});
                } else {
                    // Step 3a. Link user accounts
                    console.log(req.headers.authorization);
                    if (req.headers.authorization) {
                        db.newSearchBuilder()
                            .collection('users')
                            .query('value.google:`' + profile.sub + '`')
                            .then(function (result) {
                                if (result.body.total_count === 1) {
                                    return res.status(409).send({message: 'There is already a Google account that belongs to you'});
                                } else {
                                    return res.status(409).send({message: 'Looks like a new google account!'});
                                }
                            });
                    } else {
                        // Step 3b. Create a new user account or return an existing one.
                        db.newSearchBuilder()
                            .collection('users')
                            .query('value.google:`' + profile.sub + '`')
                            .then(function (user) {
                                if (user.body.total_count === 1) {
                                    console.log('looks like we be loggin in ');
                                    //do a login
                                    //user = _.cloneDeep(user.body.results[0].value);
                                    //var accessToken = customUtils.generateToken();
                                    //var userId = user.id;
                                    //db.put('tokens', accessToken, {
                                    //    "user": userId
                                    //})
                                    //.then(function (result) {
                                    //    db.newGraphBuilder()
                                    //        .create()
                                    //        .from('tokens', accessToken)
                                    //        .related('hasUser')
                                    //        .to('users', userId)
                                    //        .then(function (result) {
                                    //            user["access_token"] = accessToken;
                                    //            responseObj["data"] = user;
                                    //            res.status(201);
                                    //            res.json(responseObj);
                                    //        })
                                    //})
                                    var accessToken = customUtils.generateToken();
                                    var userId = user.body.results[0].value.id;
                                    db.put('tokens', accessToken, {
                                        "user": userId
                                    })
                                        .then(function (result) {
                                            db.newGraphBuilder()
                                                .create()
                                                .from('tokens', accessToken)
                                                .related('hasUser')
                                                .to('users', userId)
                                                .then(function (result) {
                                                    responseObj["data"] = user.body.results[0].value;
                                                    responseObj["data"]["access_token"] = accessToken;
                                                    responseObj["data"]["password"] = undefined;
                                                    res.status(200);
                                                    res.json(responseObj);
                                                })
                                        })
                                } else {
                                    console.log('looks like we be signing up ');
                                    //do a sign up
                                    var id = customUtils.generateToken(8)
                                    var date = new Date();
                                    var generatedUsername = profile.name.split(" ")[0];

                                    var user = {
                                        "id": id,
                                        "gcmId": undefined,
                                        "name": profile.name,
                                        "username": generatedUsername,
                                        "email": profile.email,
                                        "password": undefined,
                                        "avatar": profile.picture.replace('sz=50', 'sz=200'),
                                        "avatarThumb": profile.picture,
                                        "userDesc": undefined,
                                        "tagline": undefined,
                                        "isVerified": isVerified,
                                        "last_seen": date.getTime(),
                                        "google": profile.sub
                                    };

                                    console.log(user);
                                    //res.status(200).send({message: 'ssup'});

                                    qbchat.createUser({
                                        login: user.username,
                                        email: user.email,
                                        password: config.qb.defaultPassword,
                                        full_name: user.name,
                                        custom_data: user.avatar
                                    }, function (err, newUser) {
                                        if (err) {
                                            //responseObj["errors"] = ["There is a problem with your internet connection"];
                                            //res.status(409);
                                            //res.json(responseObj);
                                            //return;
                                            console.log("qb err");
                                        } else {
                                            user["qbId"] = newUser.id;
                                            db.put('users', id, user)
                                                .then(function (result) {
                                                    var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + id + "&token=" + isVerified;
                                                    var msg = "Hello" + user.name + ",\n\n\nThank you for signing up with Zaprep. Kindly verify your email by clicking on the following link. This shall help us serve you better. \n\n " + urlLink + " \n\n\nLooking forward to providing you a great learning experience on Zaprep.\n\nRegards,\nZaprep Team";
                                                    var subject = 'Welcome to Zaprep - Email Verification';
                                                    //sendEmail(user.email, subject, msg);
                                                    var date = new Date();
                                                    var chatObj = {
                                                        "type": "newUser",
                                                        "username": user['username'],
                                                        "qbId": user['qbId'],
                                                        "dbId": user['id'],
                                                        "created": date.getTime(),
                                                        "id": date.getTime()
                                                    }
                                                    if (typeof user['gcmId'] !== 'undefined')
                                                        chatObj['gcmId'] = user['gcmId']
                                                    else
                                                        chatObj['gcmId'] = 'undefined'

                                                    notify.emit("wordForChat", chatObj)

                                                    user['password'] = undefined;

                                                    var notifObj = {
                                                        user: id
                                                    };
                                                    notify.emit('welcome', notifObj)
                                                })
                                                .then(function () {

                                                    var accessToken = customUtils.generateToken();
                                                    var userId = id;
                                                    db.put('tokens', accessToken, {
                                                        "user": userId
                                                    })
                                                        .then(function (result) {
                                                            db.newGraphBuilder()
                                                                .create()
                                                                .from('tokens', accessToken)
                                                                .related('hasUser')
                                                                .to('users', userId)
                                                                .then(function (result) {
                                                                    user["access_token"] = accessToken;
                                                                    responseObj["data"] = user;
                                                                    res.status(201);
                                                                    res.json(responseObj);
                                                                })
                                                        })
                                                })
                                                .fail(function (err) {
                                                    console.log("POST FAIL:" + err);
                                                    responseObj["errors"] = [err.body.message];
                                                    res.status(503);
                                                    res.json(responseObj);
                                                });
                                        }
                                    })
                                }
                            });
                    }
                }
            });
        }
    });
});


//TODO: Trim all user inputs before validating
//TODO: See that if key absent, proper error is sent

/**
 * @api {post} /user/signup User Signup
 * @apiName userSignup
 * @apiGroup User
 *
 * @apiParam {String} username Unique handle for the User.
 * @apiParam {String} email Unique Email of the User.
 * @apiParam {String} password Password for the User.
 * @apiParam {String} name Name of the User.
 * @apiParam {String} userDesc (Optional) Description of the User.
 *
 * @apiSuccess {String} username Unique handle for the User.
 * @apiSuccess {String} email Unique Email of the User.
 * @apiSuccess {String} name Name of the User.
 * @apiSuccess {String} avatar Avatar link of the User.
 * @apiSuccess {String} userDesc (Optional) Description of the User.
 * @apiSuccess {String} id Unique ID of the User.
 * @apiSuccess {String} qbId Quickblox ID for the User.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 Created
 *      {
 *          "data": {
 *              "name": "Ketan Bhatt",
 *              "username": "ketannewbhatt123",
 *              "email": "newktbt12330@gmail.com",
 *              "avatar": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png",
 *              "userDesc": "This is the user description, if he entered.",
 *              "id": "2342345345h",
 *              "qbId": 23234
 *              }
 *      }
 *
 */
router.post('/signup', function (req, res, next) {
    qbchat.getSession(function (err, session) {
        if (err) {
            console.log("Recreating session");
            qbchat.createSession(function (err, result) {
                if (err) {
                    res.status(503);
                    res.json({"errors": ["Can't connect to the chat server, try again later"]});
                } else next();
            })
        } else next();
    })
    }, function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var name = customUtils.toTitleCase(req.body.name);
    var username = req.body.username;
    var responseObj = {};
    var errors = new Array();

    if (!validator.isLength(name, 2, 50)) errors.push("Name must be between 2-50 characters");
    if (!name.match(/^[a-zA-Z ]*$/)) errors.push("Name must contain only alphabets");
    if (!validator.isLength(username, 6, 12)) errors.push("Username must be between 6-12 characters");
    if (!username.match(/^[a-zA-Z0-9_]*$/)) errors.push("Username must contain only alphabets, numbers or underscore");
    if (!validator.isLength(password, 8, 20)) errors.push("Password must be between 8-20 characters");
    if (!validator.isEmail(email))
        errors.push("Please enter a valid mail ID");
    else
        email = email.toLowerCase();

    if (!validator.isNull(req.body.userDesc))
        if (!validator.isLength(req.body.userDesc, 20)) errors.push("Description must be greater than 20 characters");

    if (!validator.isNull(req.body.tagline))
        if (!validator.isLength(req.body.tagline, 0, 40)) errors.push("Tagline must be less than 40 characters");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        db.newSearchBuilder()
            .collection('users')
            .query('value.email:`' + email + '` OR value.username:`' + username + '`')
            .then(function (result) {
                if (result.body.total_count === 0) {
                    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
                    var isVerified = customUtils.generateToken();
                    var id = customUtils.generateToken(8)
                    var date = new Date();

                    var user = {
                        "id": id,
                        "gcmId": req.body.gcmId,
                        "name": name,
                        "username": req.body.username,
                        "email": req.body.email,
                        "password": hashedPassword,
                        "avatar": "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg",
                        "avatarThumb": "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg",
                        "userDesc": req.body.userDesc,
                        "tagline": req.body.tagline,
                        "isVerified": isVerified,
                        "last_seen": date.getTime(),
                        "created": date.getTime()
                    };

                    qbchat.createUser({
                        login: user.username,
                        email: user.email,
                        password: config.qb.defaultPassword,
                        full_name: user.name,
                        custom_data: user.avatar
                    }, function (err, newUser) {
                        if (err) {
                            responseObj["errors"] = ["There is a problem with your internet connection"];
                            res.status(409);
                            res.json(responseObj);
                            return;
                        } else {
                            user["qbId"] = newUser.id
                            db.put('users', id, user)
                                .then(function (result) {
                                    var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + id + "&token=" + isVerified;
                                    var msg = "Hello " + user.name + ",\n\n" + "Welcome to Zapprep!\nWe have created your mentor profile for Zapprep app.\nPlease download app,login and edit your profile.  We request you to change your profile picture and add your two line bio (e.g M.S From USC USA, MBA From IIM Bangalore )on your profile.\n\n https://play.google.com/store/apps/details?id=in.zapprep.ZapPrep&hl=en \n\n We will get in touch with you soon regarding your service charges and other commercials involved.\nFor any query please write us on mentor@zapprep.in.\n\n Thanks and Regards\nMentor Care Team\n Zap prep Education Services\n" ;
                                    var subject = 'Welcome to Zapprep - Welcome email';
                                    sendEmail(user.email, subject, msg);
                                    var date = new Date();
                                    var chatObj = {
                                        "type": "newUser",
                                        "username": user['username'],
                                        "qbId": user['qbId'],
                                        "dbId": user['id'],
                                        "created": date.getTime(),
                                        "id": date.getTime()
                                    }
                                    if (typeof user['gcmId'] !== 'undefined')
                                        chatObj['gcmId'] = user['gcmId']
                                    else
                                        chatObj['gcmId'] = 'undefined'

                                    notify.emit("wordForChat", chatObj)

                                    user['password'] = undefined;

                                    var notifObj = {
                                        user: id,
                                        name: user.name
                                    };
                                    notify.emit('welcome', notifObj)
                                })
                                .then(function () {

                                    var accessToken = customUtils.generateToken();
                                    var userId = id;
                                    db.put('tokens', accessToken, {
                                        "user": userId
                                    })
                                        .then(function (result) {
                                            db.newGraphBuilder()
                                                .create()
                                                .from('tokens', accessToken)
                                                .related('hasUser')
                                                .to('users', userId)
                                                .then(function (result) {
                                                    user["access_token"] = accessToken;
                                                    responseObj["data"] = user;
                                                    res.status(201);
                                                    res.json(responseObj);
                                                })
                                        })
                                })
                                .fail(function (err) {
                                    console.log("POST FAIL:" + err);
                                    responseObj["errors"] = [err.body.message];
                                    res.status(503);
                                    res.json(responseObj);
                                });
                        }
                    })

                } else {
                    responseObj["errors"] = ["Email ID or username already in use"];
                    res.status(409);
                    res.json(responseObj);
                }
            }).fail(function (err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        })
    }
});


/**
 * @api {post} /user/login User Login
 * @apiName userLogin
 * @apiGroup User
 *
 * @apiParam {String} email Unique Email of the User.
 * @apiParam {String} password Password for the User.
 *
 * @apiSuccess {String} username Unique handle for the User.
 * @apiSuccess {String} email Unique Email of the User.
 * @apiSuccess {String} avatar Avatar link of the User.
 * @apiSuccess {String} qbId QuickBlox ID of the User.
 * @apiSuccess {String} access_token Access Token for the User.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "data": {
 *              "username": "ketannewbhatt123",
 *              "email": "newktbt12330@gmail.com",
 *              "avatar": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png",
 *              "qbId": 3125204,
 *              "id": "2342345345h",
 *              "access_token": "53a1b0db6b5ada46ccecc0de41bfdb67"
 *              }
 *      }
 *
 */
router.post('/login', function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var responseObj = {};

    var errors = new Array();

    if (!validator.isLength(password, 8, 20)) errors.push("Password must be between 8-20 characters");
    if (!validator.isEmail(email)) errors.push("Invalid Email");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        db.newSearchBuilder()
            .collection('users')
            .query('value.email:`' + email + '`')
            .then(function (user) {
                if (user.body.total_count === 1) {
                    var hash = user.body.results[0].value.password;
                    if (bcrypt.compareSync(password, hash)) {

                        // user.body.results[0].value.isVerified != "true"
                        if (false) { // change later, not checking for verification at the moment
                            res.status(409);
                            res.json({"errors": ["Please verify your Email to login"]});
                        } else {
                            var accessToken = customUtils.generateToken();
                            var userId = user.body.results[0].value.id;
                            db.put('tokens', accessToken, {
                                "user": userId
                            })
                                .then(function (result) {
                                    db.newGraphBuilder()
                                        .create()
                                        .from('tokens', accessToken)
                                        .related('hasUser')
                                        .to('users', userId)
                                        .then(function (result) {
                                            responseObj["data"] = user.body.results[0].value;
                                            responseObj["data"]["access_token"] = accessToken;
                                            responseObj["data"]["password"] = undefined;
                                            res.status(200);
                                            res.json(responseObj);
                                        })
                                })
                            //TODO: Study promises for this crap to work
                            //.fail(function (err) {
                            //    res.status(503);
                            //    res.json({"password": "Token could not be saved"});
                            //});

                            //customUtils.saveToken(email, accessToken);
                            //.then(function (result) {
                            //    console.log("token saved yeah!");
                            //    res.sendStatus(200);
                            //})
                            //.fail(function (err) {
                            //    console.log(err.body.message);
                            //    console.log("token not saved");
                            //    res.sendStatus(400);
                            //});
                        }
                    } else {
                        responseObj["errors"] = ["Entered password is incorrect"];
                        res.status(401);
                        res.json(responseObj);
                        //deferred.resolve(false);
                    }
                } else {
                    responseObj["errors"] = ["No Account with the entered email exists"];
                    res.status(503);
                    res.json(responseObj);
                    return;
                }
            }).fail(function (err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        });
    }
    //res.send('hello authed world');
});


/**
 * @api {post} /user/logout User Logout
 * @apiName userLogout
 * @apiGroup User
 *
 * @apiParam {String} access_token (in URL) Access Token of the current User
 *
 * @apiSuccess {String} success Success Response
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "data": {
 *              "success": "Successfully and securely logged out, kind off"
 *              }
 *      }
 *
 */
router.post('/logout', [passport.authenticate('bearer', {session: false}), function (req, res) {
    access_token = req.query.access_token;
    var responseObj = {};

    db.remove('tokens', access_token, true)
        .then(function (result) {
            responseObj["data"] = {"success": "You have been successfully logged out"};
            res.status(200);
            res.json(responseObj);
        })
        .fail(function (err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        })

}]);

//TODO: Implement facebook auth and integrate QB user creation
// router.post('/auth/facebook', function (req, res) {

//     //Using this style - https://github.com/sahat/satellizer/blob/master/examples/server/node/server.js

//     var accessTokenUrl = 'https://graph.facebook.com/v2.3/oauth/access_token';
//     var graphApiUrl = 'https://graph.facebook.com/v2.3/me';
//     var params = {
//         code: req.body.code,
//         client_id: config.facebookAuth.clientId,
//         client_secret: config.facebookAuth.clientSecret,
//         redirect_uri: config.facebookAuth.callbackURL
//     };

//     // Step 1. Exchange authorization code for access token.
//     request.get({url: accessTokenUrl, qs: params, json: true}, function (err, response, accessToken) {
//         if (response.statusCode !== 200) {
//             return res.status(500).send({message: accessToken.error.message});
//         }

//         // Step 2. Retrieve profile information about the current user.
//         request.get({url: graphApiUrl, qs: accessToken, json: true}, function (err, response, profile) {
//             if (response.statusCode !== 200) {
//                 return res.status(500).send({message: profile.error.message});
//             }
//             if (req.headers.authorization) {
//                 User.findOne({facebook: profile.id}, function (err, existingUser) {
//                     if (existingUser) {
//                         return res.status(409).send({message: 'There is already a Facebook account that belongs to you'});
//                     }
//                     var token = req.headers.authorization.split(' ')[1];
//                     var payload = jwt.decode(token, config.TOKEN_SECRET);
//                     User.findById(payload.sub, function (err, user) {
//                         if (!user) {
//                             return res.status(400).send({message: 'User not found'});
//                         }
//                         user.facebook = profile.id;
//                         user.picture = user.picture || 'https://graph.facebook.com/v2.3/' + profile.id + '/picture?type=large';
//                         user.displayName = user.displayName || profile.name;
//                         user.save(function () {
//                             var token = createToken(user);
//                             res.send({token: token});
//                         });
//                     });
//                 });
//             } else {
//                 // Step 3b. Create a new user account or return an existing one.
//                 User.findOne({facebook: profile.id}, function (err, existingUser) {
//                     if (existingUser) {
//                         var token = createToken(existingUser);
//                         return res.send({token: token});
//                     }
//                     var user = new User();
//                     user.facebook = profile.id;
//                     user.avatar = 'https://graph.facebook.com/' + profile.id + '/picture?type=large';
//                     user.displayName = profile.name;
//                     user.save(function () {
//                         var token = createToken(user);
//                         res.send({token: token});
//                     });
//                 });
//             }
//         });
//     });
// });


/**
 * @api {get} /user User Profile
 * @apiName getUserProfile
 * @apiGroup User
 *
 * @apiParam {String} access_token (in URL) Access Token of the current User
 * @apiParam {String} email (in URL) (Optional) Email of User to Get
 * @apiParam {String} limit (in URL) (Optional) Number of paths per page. Default 10
 * @apiParam {String} page (in URL) (Optional) Page Number. Default 1
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "data": {
 *              "username": "ketannewbhatt123",
 *              "email": "newktbt12330@gmail.com",
 *              "avatar": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png",
 *              "qbId": 3125204,
 *              "id": "2342345345h",
 *              "paths": [
 *                  {
 *                      "title": "Star Trek",
 *                      "desc": "Get to know the basics of movie industry by following this course on start trek",
 *                      "buying": "free",
 *                      "coverPhoto": "https://s3.amazonaws.com/test-bucket-ketan/f78947c74b788406a23591902c6e36e8.JPG",
 *                      "coverPhotoThumb": "https://s3.amazonaws.com/test-bucket-ketanresized/resized-f78947c74b788406a23591902c6e36e8.JPG",
 *                      "concepts": [
 *                          {
 *                              "id": "Q43rWW8oyLJbY1mY",
 *                              "title": "Introduction",
 *                              "qbId": "55574a626390d8ebef01ba98"
 *                          }
 *                      ],
 *                      "qbId": "55574a626390d8ab91035862",
 *                      "mentorRoomId": "55574a626390d8261901ebb1",
 *                      "studentCount": 1,
 *                      "isOwner": true,
 *                      "id": "0a11e1090960fead"
 *                  }
 *              ]
 *          },
 *          "more": true
 *      }
 *
 * @apiDescription Returns User Profile for the specified email
 * Otherwise returns Profile of Logged In User
 */
router.get('/', function (req, res, next) {
    if (req.query.access_token) next();
    else next('route');
    }, [passport.authenticate('bearer', {session: false}), function (req, res) {

    if (!req.query.limit || req.query.limit < 0) req.query.limit = 10;
    if (!req.query.page || req.query.page < 1) req.query.page = 1;
    var limit = req.query.limit;
    var offset = (limit * (req.query.page - 1));
    var responseObj = {};
    var query_user = req.query.userId;
    var allowUpdate;

    var getUserInfo = function (userId, allowUpdate) {
        db.get('users', userId)
            .then(function (user) {
                db.newGraphReader()
                    .get()
                    .limit(limit)
                    .offset(offset)
                    .from('users', userId)
                    .related('related')
                    .then(function (result) {
                        responseObj["data"] = _.cloneDeep(user.body);
                        responseObj["data"]["password"] = undefined;
                        responseObj["data"]["allowUpdate"] = allowUpdate;
                        if (result.body.count === 0) {
                            responseObj["more"] = false;
                            responseObj["data"]["paths"] = [];
                            res.json(responseObj);
                        } else {
                            if (result.body.next) responseObj["more"] = true;
                            else responseObj["more"] = false;

                            responseObj["data"]["paths"] = _.cloneDeep(result.body.results);
                            for (var i = 0 in responseObj["data"]["paths"]) {


                                if (responseObj["data"]["paths"][i].value.producer.id == req.user.results[0].value.id) {
                                    responseObj["data"]["paths"][i].value.producer = undefined;
                                    responseObj["data"]["paths"][i].value["isOwner"] = true;
                                } else {
                                    responseObj["data"]["paths"][i].value["isOwner"] = false;
                                }

                                responseObj["data"]["paths"][i].value["id"] = responseObj["data"]["paths"][i].path.key;
                                responseObj["data"]["paths"][i] = responseObj["data"]["paths"][i].value;
                                responseObj["data"]["paths"][i]["allowEnroll"] = false;

                                for (var j = 0 in responseObj["data"]["paths"][i]["concepts"]) {
                                    responseObj["data"]["paths"][i]["concepts"][j].objects = undefined;
                                }

                                responseObj["data"]["paths"][i].value = undefined;
                                responseObj["data"]["paths"][i].path = undefined;
                                responseObj["data"]["paths"][i].score = undefined;
                                responseObj["data"]["paths"][i].reftime = undefined;
                            }
                            res.json(responseObj);
                        }
                    })
            })
            .fail(function (err) {
                responseObj["errors"] = [err.body.message];
                res.status(503);
                res.json(responseObj);
            });
    }

    if (validator.isNull(query_user)) {
        responseObj["errors"] = ["Please specify the User ID"];
        res.status(503);
        res.json(responseObj);
        return
    } else {
        if (query_user == req.user.results[0].value.id)
            allowUpdate = true;
        else
            allowUpdate = false;

        getUserInfo(query_user, allowUpdate)
    }

}]);


router.get('/', function (req, res) {

    if (!req.query.limit || req.query.limit < 0) req.query.limit = 10;
    if (!req.query.page || req.query.page < 1) req.query.page = 1;
    var limit = req.query.limit;
    var offset = (limit * (req.query.page - 1));
    var responseObj = {};
    var query_user = req.query.userId;
    var allowUpdate;

    var getUserInfo = function (userId, allowUpdate) {
        db.get('users', userId)
            .then(function (user) {
                db.newGraphReader()
                    .get()
                    .limit(limit)
                    .offset(offset)
                    .from('users', userId)
                    .related('related')
                    .then(function (result) {
                        responseObj["data"] = _.cloneDeep(user.body);
                        ;
                        responseObj["data"]["password"] = undefined;
                        responseObj["data"]["allowUpdate"] = allowUpdate;
                        if (result.body.count === 0) {
                            responseObj["more"] = false;
                            responseObj["data"]["paths"] = [];
                            res.json(responseObj);
                        } else {
                            if (result.body.next) responseObj["more"] = true;
                            else responseObj["more"] = false;

                            responseObj["data"]["paths"] = _.cloneDeep(result.body.results);
                            for (var i = 0 in responseObj["data"]["paths"]) {


                                if (responseObj["data"]["paths"][i].value.producer.id == userId) {
                                    responseObj["data"]["paths"][i].value.producer = undefined;
                                    responseObj["data"]["paths"][i].value["isOwner"] = true;
                                } else {
                                    responseObj["data"]["paths"][i].value["isOwner"] = false;
                                }

                                responseObj["data"]["paths"][i].value["id"] = responseObj["data"]["paths"][i].path.key;
                                responseObj["data"]["paths"][i] = responseObj["data"]["paths"][i].value;
                                responseObj["data"]["paths"][i]["allowEnroll"] = false;

                                for (var j = 0 in responseObj["data"]["paths"][i]["concepts"]) {
                                    if (responseObj["data"]["paths"][i]["concepts"][j].title != "Introduction")
                                        responseObj["data"]["paths"][i]["concepts"][j].objects = undefined;
                                }

                                responseObj["data"]["paths"][i].value = undefined;
                                responseObj["data"]["paths"][i].path = undefined;
                                responseObj["data"]["paths"][i].score = undefined;
                                responseObj["data"]["paths"][i].reftime = undefined;
                            }
                            res.json(responseObj);
                        }
                    })
            })
            .fail(function (err) {
                responseObj["errors"] = [err.body.message];
                res.status(503);
                res.json(responseObj);
            });
    }

    if (validator.isNull(query_user)) {
        responseObj["errors"] = ["Please specify the User ID"];
        res.status(503);
        res.json(responseObj);
        return
    } else {
        allowUpdate = false;
        getUserInfo(query_user, allowUpdate)
    }

});

/**
 * @api {patch} /user User Profile Settings Page
 * @apiName patchUserProfile
 * @apiGroup User
 *
 * @apiParam {String} access_token (in URL) Access Token of the current User
 * @apiParam {String} name (Optional) Name of the User
 * @apiParam {String} userDesc (Optional) User Description
 * @apiParam {String} avatar (Optional) User Profile Picture
 */
router.patch('/', [passport.authenticate('bearer', {session: false}), multer(), function (req, res, next) {
    console.log("patch user called, session attempt")

    //This code is everywhere to makesure
    //quickblox session does not expire while trying to make a patch request
    //dont land up with inconsistent data
    qbchat.getSession(function (err, session) {
        if (err) {
            console.log("Recreating session");
            qbchat.createSession(function (err, result) {
                if (err) {
                    res.status(503);
                    res.json({"errors": ["Can't connect to the chat server, try again later"]});
                } else next();
            })
        } else next();
    })
    }, function (req, res) {

    console.log("Debugging patch user")
    console.log(req.body)

    if (Object.keys(req.body).length === 0 && Object.keys(req.files).length === 0) {
        res.status(422);
        res.json({"errors": ["No key was sent in the body"]});
        return;
    }

    var responseObj = {};
    var userId = req.user.results[0].value.id;
    var filetype;
    var reqBody = req.body;
    var errors = new Array();

    // Dummy Promise that always resolves :D
    var promise = kew.resolve({
        "body": {
            "total_count": 0
        }
    });

    if (!validator.isNull(reqBody.name))
        if (!reqBody.name.match(/\w*/g)) errors.push("Name contains illegal characters");

    if (!validator.isNull(reqBody.userDesc))
        if (!validator.isLength(req.body.userDesc, 20)) errors.push("Description must be greater than 20 characters");

    if (!validator.isNull(reqBody.tagline))
        if (!validator.isLength(reqBody.tagline, 0, 40)) errors.push("Tagline must be less than 40 characters");

    if (!validator.isNull(req.files.avatar))
        if (!validator.isImage(req.files.avatar.mimetype)) errors.push("Avatar should be an image type");

    if (!validator.isNull(reqBody.email)) {
        if (!validator.isEmail(reqBody.email)) {
            errors.push("Email is invalid");
        } else {
            var errMsg = "Email ID already registered. Please register with a new one.";
            var promise = db.newSearchBuilder()
                .collection('users')
                .query('value.email:`' + reqBody.email + '`')
        }
    }

    if (!validator.isNull(reqBody.username)) {
        if (!validator.isLength(reqBody.username, 6, 20)) {
            errors.push("Username must be between 6-20 characters");
        } else if (!reqBody.username.match(/^[a-zA-Z0-9_]*$/)) {
            errors.push("Username must contain only alphabets, numbers or underscore")
        } else {
            var errMsg = "Username already in use. Please enter a new one.";
            var promise = db.newSearchBuilder()
                .collection('users')
                .query('value.username:`' + reqBody.username + '`')
        }
    }

    kew.all([promise])
        .then(function (content) {
            if (content[0].body.total_count > 0) {
                errors.push(errMsg);
            }
            if (errors.length > 0) {
                responseObj["errors"] = errors;
                res.status(422);
                res.json(responseObj);
            } else {
                customUtils.upload(req.files.avatar, function (avatarInfo) {

                    if (req.files.avatar) {
                        var payload = {
                            "avatar": avatarInfo.url,
                            "avatarThumb": avatarInfo.urlThumb,
                            "userDesc": req.body.userDesc,
                            "tagline": req.body.tagline,
                            "name": req.body.name,
                            "username": req.body.username,
                            "gcmId": req.body.gcmId
                        };
                    } else if (reqBody.email) {
                        var newToken = customUtils.generateToken();
                        var payload = {
                            "email": req.body.email,
                            "isVerified": newToken,
                            "username": req.body.username
                        };
                    } else if (req.body.avatar == 'null') {
                        var payload = {
                            "avatar": "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg",
                            "avatarThumb": "https://s3.amazonaws.com/pyoopil-prod-server/bhalu_umber.jpg"
                        }
                    } else {
                        var payload = {
                            "userDesc": req.body.userDesc,
                            "tagline": req.body.tagline,
                            "name": req.body.name,
                            "username": req.body.username,
                            "gcmId": req.body.gcmId
                        };
                    }

                    db.merge('users', userId, payload)
                        .then(function (result) {

                            if (req.files.avatar) {
                                notify.getRecieversAndUpdatePhotos('user', userId, payload['avatarThumb'], req.user.results[0].value.avatarThumb)
                            }

                            if (typeof payload['gcmId'] !== 'undefined') {
                                var date = new Date();
                                var chatObj = {
                                    "type": "newGcmId",
                                    "username": req.user.results[0].value.username,
                                    "created": date.getTime(),
                                    "id": date.getTime(),
                                    "gcmId": payload['gcmId']
                                }
                                notify.emit("wordForChat", chatObj)
                            }

                            qbchat.updateUser(req.user.results[0].value.qbId, {
                                login: payload.username,
                                email: payload.email,
                                full_name: payload.name,
                                custom_data: payload.avatarThumb
                            }, function (err, user) {
                                if (err) console.log(err);
                            })
                            responseObj["data"] = payload;
                            res.status(200);
                            res.json(responseObj);
                            /**
                             * TODO : This should be a firebase event that is fired
                             * and then the listeners do all the updating and patching etc.
                             */
                            updateUser(userId, payload);
                            if (reqBody.email) {
                                var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + userId + "&token=" + payload.isVerified;
                                var msg = "Hello! Your Email ID was changed. \nPlease click on the below link to verify your email, excuse our brevity.\n\n" + urlLink + " \n\nLooking forward to see you on Zaprep.\n\nBest Wishes,\nZapprep Team";
                                var subject = 'Email Change - Verify your new E-mail';
                                //sendEmail(reqBody.email, subject, msg);
                            }
                        })
                        .fail(function (err) {
                            responseObj["errors"] = [err.body.message];
                            res.status(503);
                            res.json(responseObj);
                        });
                });
            }
        })
        .fail(function (err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        })
}]);


/**
 * @api {post} /user/enroll Enroll User
 * @apiName userEnroll
 * @apiGroup User
 *
 * @apiParam {String} access_token (in URL) Access Token of the current User
 * @apiParam {String} pathId Path ID of the path to enroll in
 *
 * @apiSuccess {String} success Success Response
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "data": {
 *              "success": "You just got (En)rolled!"
 *              }
 *      }
 *
 */
router.post('/enroll', [passport.authenticate('bearer', {session: false}), function (req, res, next) {
    qbchat.getSession(function (err, session) {
        if (err) {
            console.log("Recreating session");
            qbchat.createSession(function (err, result) {
                if (err) {
                    res.status(503);
                    res.json({"errors": ["Can't connect to the chat server, try again later"]});
                } else next();
            })
        } else next();
    })
    }, function (req, res) {

    if (!req.body.pathId && !req.body.accessId) {
        res.status(400);
        res.json({"errors": ["Send either one of pathId or accessId"]});
        return;
    }

    var responseObj = {};
    var userId = req.user.results[0].value.id;
    var userQbId = req.user.results[0].value.qbId;
    var userName = req.user.results[0].value.username;
    var pathId = req.body.pathId;

    //Damnit this is terrible!!!
    //var getMeEnrolled = function (course) {
    //    db.newGraphReader()
    //        .get()
    //        .limit(100)
    //        .from('users', userId)
    //        .related('related')
    //        .then(function (result) {
    //
    //            var relatedPaths = result.body.results.map(function (path) {
    //                return path.path.key
    //            })
    //            if (relatedPaths.indexOf(course) > -1) {
    //                responseObj["errors"] = ["You are already a part of this course"];
    //                res.status(409);
    //                res.json(responseObj);
    //                return;
    //            } else {
    //                var consumes = db.newGraphBuilder()
    //                    .create()
    //                    .from('users', userId)
    //                    .related('consumes')
    //                    .to('paths', course);
    //
    //                var isConsumed = db.newGraphBuilder()
    //                    .create()
    //                    .from('paths', course)
    //                    .related('isConsumed')
    //                    .to('users', userId);
    //
    //                var getPath = db.get('paths', course)
    //
    //                kew.all([getPath, consumes, isConsumed])
    //                    .then(function (content) {
    //
    //                        var rooms = content[0].body.concepts.map(function (concept) {
    //                            return concept["qbId"]
    //                        });
    //                        rooms.push(content[0].body.qbId)
    //                        rooms.push(content[0].body.mentorRoomId)
    //                        async.each(rooms, function (room, callback) {
    //                            qbchat.addUserToRoom(room, [userQbId], function (err, result) {
    //                                if (err) callback(err);
    //                                else callback();
    //                            })
    //                        }, function (err) {
    //                            if (err) {
    //                                responseObj["errors"] = ["Enrolling was unsuccessful"];
    //                                console.log(err)
    //                                res.status(503);
    //                                res.json(responseObj);
    //                            } else {
    //                                db.newPatchBuilder("paths", course)
    //                                    .inc("studentCount")
    //                                    .inc("newStudents")
    //                                    .apply()
    //                                    .then(function (res1) {
    //                                        db.newGraphBuilder()
    //                                            .create()
    //                                            .from('users', userId)
    //                                            .related('related')
    //                                            .to('paths', course)
    //                                            .then(function (res2) {
    //                                                responseObj["data"] = {"success": "You have been successfully enrolled"};
    //                                                if (req.body.paymentId && req.body.amount) {
    //                                                    customUtils.captureRazorPayment(req.body.paymentId, req.body.amount);
    //                                                }
    //                                                res.status(200);
    //                                                res.json(responseObj);
    //                                            })
    //                                    })
    //                            }
    //                        });
    //                    })
    //                    .fail(function (err) {
    //                        responseObj["errors"] = [err.body.message];
    //                        res.status(503);
    //                        res.json(responseObj);
    //                    });
    //
    //                // var notifObj = {
    //                //         "who": userName,
    //                //         "producerId": result.body.producer.id,
    //                //         "where": userId,
    //                //         "producerGcmId": result.body.producer.gcmId
    //                //     };
    //                //     notify.emit('enrolment', notifObj);
    //            }
    //
    //        })
    //        .fail(function (err) {
    //            responseObj["errors"] = [err.body.message];
    //            res.status(422);
    //            res.json(responseObj);
    //        })
    //}

    if (req.body.accessId && !req.body.pathId) {
        db.newSearchBuilder()
            .collection('paths')
            .query('value.accessId:`' + req.body.accessId + '`')
            .then(function (path) {
                if (path.body.total_count == 1) {
                    pathId = path.body.results[0].path.key;
                } else {
                    res.status(422);
                    res.json({"errors": ["Please enter a valid access code"]});
                }
            })
            .then(function () {
                //getMeEnrolled(pathId);
                return Path.enrollIntoCourse(userId, pathId, userQbId, req.body.paymentId, req.body.amount)
            })
            .then(function (response) {
                res.status(200);
                res.json(response);
            })
            .fail(function (err) {
                res.status(422);
                res.json({"errors": [err]});
            })
    } else {
        Path.enrollIntoCourse(userId, pathId, userQbId, req.body.paymentId, req.body.amount)
            .then(function (response) {
                res.status(200);
                res.json(response);
            })
            .fail(function (err) {
                res.status(422);
                res.json({"errors": [err.body.message]});
            })
        //getMeEnrolled(pathId);
    }

}]);


/**
 * @api {get} /user/paths Dashboard
 * @apiName getDashboard
 * @apiGroup User
 * @apiParam {String} access_token (in URL) Access Token of the current User
 * @apiParam {String} limit (in URL) (Optional) Number of paths per page. Default 10
 * @apiParam {String} page (in URL) (Optional) Page Number. Default 1
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "more": true,
 *          "data": [
 *              {
 *                  "title": "Star Trek",
 *                  "desc": "Get to know the basics of movie industry by following this course on start trek",
 *                  "buying": "free",
 *                  "coverPhoto": "https://s3.amazonaws.com/test-bucket-ketan/f78947c74b788406a23591902c6e36e8.JPG",
 *                  "coverPhotoThumb": "https://s3.amazonaws.com/test-bucket-ketanresized/resized-f78947c74b788406a23591902c6e36e8.JPG",
 *                  "concepts": [
 *                      {
 *                         "id": "Q43rWW8oyLJbY1mY",
 *                         "title": "Introduction",
 *                         "qbId": "55574a626390d8ebef01ba98"
 *                      }
 *                  ],
 *                  "qbId": "55574a626390d8ab91035862",
 *                  "mentorRoomId": "55574a626390d8261901ebb1",
 *                  "studentCount": 1,
 *                  "isOwner": true,
 *                  "id": "0a11e1090960fead"
 *              },
 *              {
 *                  "title": "New Star Trek",
 *                  "desc": "Get to know the basics of movie industry by following this course on start trek",
 *                  "buying": "free",
 *                  "coverPhoto": "https://s3.amazonaws.com/test-bucket-ketan/f78947c74b788406a23591902c6e36e8.JPG",
 *                  "coverPhotoThumb": "https://s3.amazonaws.com/test-bucket-ketanresized/resized-f78947c74b788406a23591902c6e36e8.JPG",
 *                  "producer": {
 *                      "username": "ketanbhatt12345",
 *                      "email": "ktbt1012345@gmail.com",
 *                      "avatar": "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/default_profile_photo-1.png",
 *                      "qbId": 3066379
 *                  },
 *                  "concepts": [
 *                      {
 *                         "id": "Q43rWW8oyLJbY1mY",
 *                         "title": "Introduction",
 *                         "qbId": "55574a626390d8ebef01ba98"
 *                      }
 *                  ],
 *                  "qbId": "55574a626390d8ab91035862",
 *                  "mentorRoomId": "55574a626390d8261901ebb1",
 *                  "studentCount": 1,
 *                  "isOwner": false,
 *                  "id": "0a11e1090960fead"
 *              }
 *          ]
 *      }
 */
router.get('/paths', [passport.authenticate('bearer', {session: false}), function (req, res) {
    if (!req.query.limit || req.query.limit < 0) req.query.limit = 100;
    if (!req.query.page || req.query.page < 1) req.query.page = 1;

    var limit = req.query.limit;
    var offset = (limit * (req.query.page - 1));
    var responseObj = {};
    var userId = req.user.results[0].value.id;
    var username = req.user.results[0].value.username;
    var d = new Date();
    //var last_seen = req.user.results[0].path.reftime
    var last_seen = req.user.results[0].value.last_seen
    //var mobileAccess = false

    if (req.headers['user-agent'] == 'okhttp/2.3.0' && (d.getTime() - last_seen) / 60000 > 60) {
        //mobileAccess = true
        var notifObj = {
            userId: userId,
            name: req.user.results[0].value.name,
            photo: req.user.results[0].value.avatarThumb
        };
        notify.emit('mentorMaybe', notifObj)
    }

    async.parallel([
            function (callback) {
                db.newGraphReader()
                    .get()
                    //.withoutFields('value.concepts') //android is dependent
                    .limit(limit)
                    .offset(offset)
                    .from('users', userId)
                    .related('related')
                    .then(function (result) {
                        if (result.body.next) responseObj["more"] = true;
                        else responseObj["more"] = false;

                        //Dare you not put this inside the for loop.
                        responseObj["obsoleteVersion"] = config.appUpdate.obsoleteVersion;

                        responseObj["data"] = _.cloneDeep(result.body.results);
                        for (var i = 0 in responseObj["data"]) {

                            if (responseObj["data"][i].value.producer.id == userId) {
                                responseObj["data"][i].value.producer = undefined;
                                responseObj["data"][i].value["isOwner"] = true;
                            } else {
                                responseObj["data"][i].value["isOwner"] = false;
                            }

                            responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
                            responseObj["data"][i] = responseObj["data"][i].value;
                            responseObj["data"][i]["allowEnroll"] = false;

                            for (var j = 0 in responseObj["data"][i]["concepts"]) {
                                if (responseObj["data"][i]["concepts"][j].title != "Introduction")
                                    responseObj["data"][i]["concepts"][j].objects = undefined;
                            }

                            responseObj["data"][i].value = undefined;
                            responseObj["data"][i].path = undefined;
                            responseObj["data"][i].score = undefined;
                            responseObj["data"][i].reftime = undefined;
                        }
                        callback(null, responseObj)
                        //getUsersDialogs(username, function (err, dialogList) {
                        //    if (err) {
                        //        res.json(responseObj);
                        //        responseObj["dialogs"] = []
                        //    } else {
                        //        responseObj["dialogs"] = dialogList
                        //        res.json(responseObj);
                        //    }
                        //})

                    })
                    .fail(function (err) {
                        //responseObj["errors"] = [err.body.message];
                        //res.status(503);
                        //res.json(responseObj);
                        callback(err, null)
                    });
            },
            function (callback) {
                //if (mobileAccess) {
                getUsersDialogs(username, function (err, dialogList) {
                    if (err) {
                        //res.json(responseObj);
                        //responseObj["dialogs"] = []
                        callback(err, null)
                    } else {
                        //responseObj["dialogs"] = dialogList
                        //res.json(responseObj);
                        callback(null, dialogList)
                    }
                })
                //} else {
                //    callback(null, [])
                //}
            }
        ],
        function (err, results) {
            if (err) {
                responseObj["errors"] = ["A server error occured"];
                console.log(err)
                log.error(err)
                res.status(503);
                res.json(responseObj);
            } else {
                responseObj = results[0];
                responseObj["dialogs"] = results[1];
                res.json(responseObj);
            }
        })
    //db.newGraphReader()
    //    .get()
    //    .limit(limit)
    //    .offset(offset)
    //    .from('users', userId)
    //    .related('related')
    //    .then(function (result) {
    //        if (result.body.next) responseObj["more"] = true;
    //        else responseObj["more"] = false;
    //
    //        //Dare you not put this inside the for loop.
    //        responseObj["obsoleteVersion"] = config.appUpdate.obsoleteVersion;
    //
    //        responseObj["data"] = _.cloneDeep(result.body.results);
    //        for (var i = 0 in responseObj["data"]) {
    //
    //            if (responseObj["data"][i].value.producer.id == userId) {
    //                responseObj["data"][i].value.producer = undefined;
    //                responseObj["data"][i].value["isOwner"] = true;
    //            } else {
    //                responseObj["data"][i].value["isOwner"] = false;
    //            }
    //
    //            responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
    //            responseObj["data"][i] = responseObj["data"][i].value;
    //            responseObj["data"][i]["allowEnroll"] = false;
    //
    //            for (var j = 0 in responseObj["data"][i]["concepts"]) {
    //                if (responseObj["data"][i]["concepts"][j].title != "Introduction")
    //                    responseObj["data"][i]["concepts"][j].objects = undefined;
    //            }
    //
    //            responseObj["data"][i].value = undefined;
    //            responseObj["data"][i].path = undefined;
    //            responseObj["data"][i].score = undefined;
    //            responseObj["data"][i].reftime = undefined;
    //        }
    //        getUsersDialogs(username, function (err, dialogList) {
    //            if (err) {
    //                res.json(responseObj);
    //                responseObj["dialogs"] = []
    //            } else {
    //                responseObj["dialogs"] = dialogList
    //                res.json(responseObj);
    //            }
    //        })
    //
    //    })
    //    .fail(function (err) {
    //        responseObj["errors"] = [err.body.message];
    //        res.status(503);
    //        res.json(responseObj);
    //    });
}]);

router.post('/verify', function (req, res) {
    var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + userObj.id + "&token=" + userObj.isVerified;
    var msg = "Hello! You requested for verification email. \nPlease click on the below link to verify your email, excuse our brevity.\n\n" + urlLink + " \n\nLooking forward to see you on Zaprep.\n\nBest Wishes,\nZaprep Team";
    var subject = 'Verify your E-mail'
    sendEmail(req.query.email, subject, msg);
    res.json({
        "data": {
            "success": "Verification Email sent to registered Email ID"
        }
    });
});

router.get('/verify', function (req, res) {
    var token = req.query.token;
    var userId = req.query.user;

    db.get('users', userId)
        .then(function (user) {
            if (token == user.body.isVerified) {
                db.merge('users', userId, {
                    "isVerified": 'true'
                })
                    .then(function (result) {
                        res.render('verification', {
                            "title": "Verification Complete - Zaprep",
                            "heading": "Thank you for verifying your email"
                        });
                    });
            } else if (user.body.isVerified == 'true') {
                res.render('verification', {
                    "title": "Verification Complete - Zaprep",
                    "heading": "This email ID is already verified"
                })
            } else {
                var newToken = customUtils.generateToken();
                db.merge('users', userId, {
                    "isVerified": newToken
                })
                    .then(function (result) {
                        res.render('verification', {
                            "title": "Verification Failed - Zaprep",
                            "heading": "Verification token didn't match. Request for a new verification token from Zaprep"
                        });
                    });
            }
        })
        .fail(function (err) {
            res.render('verification', {
                "title": "Verification Failed - Zaprep",
                "heading": "There was some problem. Please apply for a verification email again"
            })
        })
})

router.get('/vimeo', function (req, res) {
    res.render('verification', {
        name: "https://player.vimeo.com/video/" + req.query.id
    })
})

router.get('/dummy', function (req, res) {
    res.render('dummyVimeo')
})

router.post('/forgotPassword', function (req, res) {
    var email = req.body.email;
    db.newSearchBuilder()
        .collection('users')
        .query('value.email:`' + email + '`')
        .then(function (users) {
            if (users.body.total_count == 1) {
                var userObj = _.cloneDeep(users.body.results[0].value);
                var newPass = customUtils.generateToken(4);
                var hashedPassword = bcrypt.hashSync(newPass, 8);
                db.merge('users', userObj.id, {
                    "password": hashedPassword
                })
                    .then(function (result) {
                        res.json({
                            "data": {
                                "success": "Please check your Email for the new password"
                            }
                        });
                        var msg = "Hello!\nYour new password for Zaprep is : " + newPass + "\n\nKindly note that this is a system generated password and we strongly recommend that you change your password once you login using this.\n\nLooking forward to providing you a great learning experience on Zaprep.\n\nRegards,\nZapprep Team";
                        var subject = 'Request for New Password'
                        sendEmail(userObj.email, subject, msg)
                    })
            } else {
                res.status(422);
                res.json({"errors": ["This email ID is not registered with us. Please enter the registered email ID"]});
            }
        })
        .fail(function (err) {
            res.status(503);
            res.json({"errors": [err.body.message]});
        })
});

router.post('/error', [passport.authenticate('bearer', {session: false}), function (req, res) {
    var d = new Date();
    var responseObj = {};
    var payload = {
        'userId': req.user.results[0].value.id,
        'timestamp': d.getTime(),
        'dump': req.body.dump
    }
    db.post('errors', payload)
        .then(function (result) {
            responseObj["data"] = {"success": "The error report was submitted successfully, thank you."};
            res.status(201);
            res.json(responseObj);
        })
        .fail(function (err) {
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        })
}]);

router.patch('/password', [passport.authenticate('bearer', {session: false}), function (req, res) {
    var responseObj = {};
    var userId = req.user.results[0].value.id;
    var reqBody = req.body;
    var errors = new Array();

    if (!validator.isLength(reqBody.newPass, 8, 20)) errors.push("Password must be between 8-20 characters");
    if (!validator.isNull(reqBody.currPass)) {
        if (reqBody.newPass != reqBody.rePass) {
            errors.push("Passwords don't match!");
        } else {
            var hash = req.user.results[0].value.password;
            if (!bcrypt.compareSync(reqBody.currPass, hash)) {
                errors.push("Entered password does not match your current password");
            }
        }
    }

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var hashedPassword = bcrypt.hashSync(req.body.newPass, 8);
        var payload = {
            "password": hashedPassword
        }

        db.merge('users', userId, payload)
            .then(function (result) {
                db.newSearchBuilder()
                    .collection('tokens')
                    .limit(100)
                    .query('value.user:`' + userId + '`')
                    .then(function (result) {
                        result.body.results.forEach(function (token) {
                            db.remove('tokens', token.path.key, true)
                                .then(function (result) {
                                    console.log("deleted token")
                                })
                        })
                    })
                res.json({
                    "data": {
                        "success": "Password changed. Please login again"
                    }
                });
            })
            .fail(function (err) {
                responseObj["errors"] = [err.body.message];
                res.status(503);
                res.json(responseObj);
            });
    }

}]);

module.exports = router;

var signUpFreshGoogleUser = function (payload, avatar, avatarThumb, res) {
    var responseObj = {};
    var id = customUtils.generateToken(8)
    var date = new Date();

    //ensure max length is 12 chars
    var generatedUsername = payload['name'].split(" ")[0].toLowerCase().substring(0, 7) + "_" + customUtils.generateToken(2);

    //use a dummyPassword to throw "incorrect password"
    //error just in case someone tries to normal login
    //can't keep password field undefined
    var user = {
        "id": id,
        "gcmId": undefined,
        "name": payload['name'],
        "username": generatedUsername,
        "email": payload['email'],
        "password": "dummyPassword",
        "avatar": avatar,
        "avatarThumb": avatarThumb,
        "userDesc": undefined,
        "tagline": undefined,
        "isVerified": true,
        "last_seen": date.getTime(),
        "google": payload['sub'],
        "created": date.getTime()
    };

    qbchat.createUser({
        login: user.username,
        email: user.email,
        password: config.qb.defaultPassword,
        full_name: user.name,
        custom_data: user.avatar
    }, function (err, newUser) {
        if (err) {
            responseObj["errors"] = err;
            res.status(409);
            res.json(responseObj);
        } else {
            user["qbId"] = newUser.id;
            db.put('users', id, user)
                .then(function (result) {
                    //var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + id + "&token=" + isVerified;
                    //var msg = "Hello" + user.name + ",\n Thank you for signing up with Pyoopil. Kindly verify your email by clicking on the following link. This shall help us serve you better. \n " + urlLink + " \nLooking forward to providing you a great learning experience on Pyoopil.\n\nRegards,\nPyoopil Team";
                    //var subject = 'Welcome to Pyoopil - Email Verification';
                    //sendEmail(user.email, subject, msg);
                    var date = new Date();
                    var chatObj = {
                        "type": "newUser",
                        "username": user['username'],
                        "qbId": user['qbId'],
                        "dbId": user['id'],
                        "created": date.getTime(),
                        "id": date.getTime()
                    }
                    if (typeof user['gcmId'] !== 'undefined')
                        chatObj['gcmId'] = user['gcmId']
                    else
                        chatObj['gcmId'] = 'undefined'

                    notify.emit("wordForChat", chatObj)

                    user['password'] = undefined;

                    var notifObj = {
                        user: id,
                        name: user.name
                    };
                    notify.emit('welcome', notifObj)
                })
                .then(function () {
                    var accessToken = customUtils.generateToken();
                    var userId = user.id;
                    db.put('tokens', accessToken, {
                        "user": userId
                    })
                        .then(function (result) {
                            db.newGraphBuilder()
                                .create()
                                .from('tokens', accessToken)
                                .related('hasUser')
                                .to('users', userId)
                                .then(function (result) {
                                    user["access_token"] = accessToken;
                                    responseObj["data"] = user;
                                    //prompt the user to change his randomly generated username
                                    responseObj["data"]["changeUsername"] = true;
                                    res.status(201);
                                    res.json(responseObj);
                                })
                        })
                })
                .fail(function (err) {
                    console.log("POST FAIL:" + err);
                    responseObj["errors"] = [err.body.message];
                    res.status(503);
                    res.json(responseObj);
                });
        }
    })
};

var signUpFreshFacebookUser = function (payload, avatar, avatarThumb, res, changeEmail, accessToken) {
    var responseObj = {};
    var id = customUtils.generateToken(8)
    var date = new Date();

    //ensure max length is 12 chars
    var generatedUsername = payload['name'].split(" ")[0].toLowerCase().substring(0, 7) + "_" + customUtils.generateToken(2);

    //use a dummyPassword to throw "incorrect password"
    //error just in case someone tries to normal login
    //can't keep password field undefined
    var user = {
        "id": id,
        "gcmId": undefined,
        "name": payload['name'],
        "username": generatedUsername,
        "email": payload['email'],
        "password": "dummyPassword",
        "avatar": avatar,
        "avatarThumb": avatarThumb,
        "userDesc": undefined,
        "tagline": undefined,
        "isVerified": true,
        "last_seen": date.getTime(),
        "facebook": payload['id'],
        "created": date.getTime()
    };

    qbchat.createUser({
        login: user.username,
        email: user.email,
        password: config.qb.defaultPassword,
        full_name: user.name,
        custom_data: user.avatar
    }, function (err, newUser) {
        if (err) {
            responseObj["errors"] = err;
            res.status(409);
            res.json(responseObj);
        } else {
            user["qbId"] = newUser.id;
            db.put('users', id, user)
                .then(function (result) {
                    //---------------merge facebook friends ------------------------
                    extractFacebookFriends(id, accessToken)
                    //---------------merge facebook friends end --------------------
                    //var urlLink = "http://api2.pyoopil.com:3000/user/verify?user=" + id + "&token=" + isVerified;
                    //var msg = "Hello" + user.name + ",\n Thank you for signing up with Pyoopil. Kindly verify your email by clicking on the following link. This shall help us serve you better. \n " + urlLink + " \nLooking forward to providing you a great learning experience on Pyoopil.\n\nRegards,\nPyoopil Team";
                    //var subject = 'Welcome to Pyoopil - Email Verification';
                    //sendEmail(user.email, subject, msg);
                    var date = new Date();
                    var chatObj = {
                        "type": "newUser",
                        "username": user['username'],
                        "qbId": user['qbId'],
                        "dbId": user['id'],
                        "created": date.getTime(),
                        "id": date.getTime()
                    }
                    if (typeof user['gcmId'] !== 'undefined')
                        chatObj['gcmId'] = user['gcmId']
                    else
                        chatObj['gcmId'] = 'undefined'

                    notify.emit("wordForChat", chatObj)

                    user['password'] = undefined;

                    var notifObj = {
                        user: id,
                        name: user.name
                    };
                    notify.emit('welcome', notifObj)
                })
                .then(function () {
                    var accessToken = customUtils.generateToken();
                    var userId = user.id;
                    db.put('tokens', accessToken, {
                        "user": userId
                    })
                        .then(function (result) {
                            db.newGraphBuilder()
                                .create()
                                .from('tokens', accessToken)
                                .related('hasUser')
                                .to('users', userId)
                                .then(function (result) {
                                    user["access_token"] = accessToken;
                                    responseObj["data"] = user;
                                    //prompt the user to change his randomly generated username
                                    responseObj["data"]["changeUsername"] = true;
                                    responseObj["data"]["changeEmail"] = changeEmail;
                                    res.status(201);
                                    res.json(responseObj);
                                })
                        })
                })
                .fail(function (err) {
                    console.log("POST FAIL:" + err);
                });
        }
    })
};

var generateTokenAndLogin = function (user, res) {
    var responseObj = {};
    var accessToken = customUtils.generateToken();
    var userId = user.body.results[0].value.id;

    db.put('tokens', accessToken, {
        "user": userId
    })
        .then(function (result) {
            db.newGraphBuilder()
                .create()
                .from('tokens', accessToken)
                .related('hasUser')
                .to('users', userId)
                .then(function (result) {
                    responseObj["data"] = user.body.results[0].value;
                    responseObj["data"]["access_token"] = accessToken;
                    res.status(201);
                    res.json(responseObj);
                })
        })
};

// To update producer info after PATCH to an user
// TODO: Update 
var updateUser = function (userId, userObj) {
    db.newSearchBuilder()
        .collection('paths')
        .limit(100)
        .query('value.producer.id:`' + userId + '`')
        .then(function (result) {
            pathIds = result.body.results.map(function (path) {
                return path.path.key;
            })
            pathIds.forEach(function (pathId) {
                db.newPatchBuilder("paths", pathId)
                    .merge("producer", userObj)
                    .apply()
                    .then(function (result) {
                        console.log("Denormalization wins");
                    })
            })
        })
        .fail(function (err) {
            console.log(err.body.message);
        });

    db.newSearchBuilder()
        .collection('reviews')
        .limit(100)
        .query('value.user.id:`' + userId + '`')
        .then(function (result) {
            reviewIds = result.body.results.map(function (review) {
                return review.path.key;
            })
            reviewIds.forEach(function (reviewId) {
                db.newPatchBuilder("reviews", reviewId)
                    .merge("user", userObj)
                    .apply()
                    .then(function (result) {
                        console.log("Denormalization wins again");
                    })
            })
        })
        .fail(function (err) {
            console.log(err.body.message);
        });
}

var sendEmail = function (email, subject, msg) {
    var data = {
        from: 'Pyoopil Team <no-reply@pyoopil.com>',
        to: email,
        subject: subject,
        text: msg
    };

    mailgun.messages().send(data, function (error, body) {
        if (error) {
            console.log(error)
        }
    });
}

//TODO: what are cronjobs doing here? shouldn't they be in some other file?
//TODO: shouldn't the crons dispatch at the users timezone and not at Asia/Kolkata only?
var enrolmentAggregate = new CronJob('00 00 10 * * 0-7', function () {
    db.newSearchBuilder()
        .collection('paths')
        .limit(100)
        .query('value.newStudents: (NOT 0)')
        .then(function (result) {
            if (result.body.total_count == 0)
                return;
            var pathIds = [];
            for (var i = 0; i < result.body.total_count; i++) {
                var pathId = result.body.results[i].path.key;
                var pathName = result.body.results[i].value.title;
                var pathPhoto = result.body.results[i].value.coverPhotoThumb;
                var producerId = result.body.results[i].value.producer.id;
                var producerGcmId = result.body.results[i].value.producer.gcmId;
                var newStudents = result.body.results[i].value.newStudents;

                pathIds.push(result.body.results[i].path.key)

                var notifObj = {
                    pathId: pathId,
                    pathName: pathName,
                    pathPhoto: pathPhoto,
                    producerId: producerId,
                    producerGcmId: producerGcmId,
                    newStudents: newStudents
                };

                notify.emit('enrolmentAggregate', notifObj)

            }

            pathIds.forEach(function (pathId) {
                db.newPatchBuilder("paths", pathId)
                    .replace("newStudents", 0)
                    .apply()
                    .then(function (result) {
                        console.log("Denormalization wins");
                    })
            })
        })
        .fail(function (err) {
            console.log(err.body)
        })
}, null, true, 'Asia/Kolkata');

var thefinalChatAggregate = new CronJob('00 00 10 * * 0-7', function () {
    unreader.dispatchUnreadNotifications()
}, null, true, 'Asia/Kolkata');

/**
 * If you don't nof.timestamp > (now / 1000 - 60)
 * then everytime you start your server
 * khata-khat notification jayega sabko.
 *
 * It's as good as child added.
 * 1 min time has been given as leeway for server restarts etc.
 */
//
// Listening to mentions notif from chatServer
// 
myFirebaseRef.child('chatServer/mentions').on("child_added", function (snapshot, prevChildKey) {
    var nof = snapshot.val();
    console.log("chatServer/mentions");
    console.log("nof.timestamp : " + nof.timestamp);
    console.log("> now - 60 : " + (now / 1000 - 60));
    if (nof.timestamp > (now / 1000 - 60))
        notify.emit('newMention', nof);
}, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
});

// 
// Listening to Mentor messages from chatServer
// 
myFirebaseRef.child('chatServer/mentorMessages').on("child_added", function (snapshot, prevChildKey) {
    var nof = snapshot.val();
    //console.log("chatServer/mentorMessages");
    //console.log("nof.timestamp : " + nof.timestamp);
    //console.log("> now - 60 : " + (now / 1000 - 60));
    if (nof.timestamp > (now / 1000 - 60))
        notify.emit('newMentorMessage', nof);
}, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
});

//
// Listening to atClass notifs from chatServer
//
myFirebaseRef.child('chatServer/atClass').on("child_added", function (snapshot, prevChildKey) {
    //console.log("user.js : npm hears a chatServer/atClass entry in firebase :O ")
    //console.log(snapshot.val());
    var nof = snapshot.val();
    //console.log("user.js : chatServer/atClass");
    //console.log("user.js : nof.timestamp : " + nof.timestamp);
    //console.log("user.js : > now - 60 : " + (now / 1000 - 60));

    if (nof.timestamp > (now / 1000 - 60)) {
        //console.log("user.js : npm emits atClass ")
        notify.emit('atClass', nof);
    }
}, function (errorObject) {
    console.log("The read failed: " + errorObject.code);
});

/**
 * /**
 * This is what a qbDialog looks like:
 //{ _id: '56794ff9a28f9ab1e5000374',
        //    created_at: '2015-12-22T13:28:25Z',
        //    last_message: null,
        //    last_message_date_sent: null,
        //    last_message_user_id: null,
        //    name: 'new concept',
        //    occupants_ids: [ 5372309, 7522231, 7522239, 7522718, 7523428, 7523544, 7533504 ],
        //    photo: null,
        //    silent_ids: [],
        //    type: 2,
        //    updated_at: '2015-12-24T15:17:36Z',
        //    user_id: 5372309,
        //    xmpp_room_jid: '28196_56794ff9a28f9ab1e5000374@muc.chat.quickblox.com',
 //    unread_messages_count: 0 }
 * @param username
 * @param mentorRoomList
 * @param callback
 */
var getUsersDialogs = function (username, callback) {
    ///**
    // * disable require cache to get a new QB object for consumer
    // * so that the sessions dont clash!
    // * TODO: fine a better way to do this. perhaps create an instance of QB
    // ***/
    //Object.keys(require.cache).forEach(function (key) {
    //    //delete require.cache[key]
    //    if (key.indexOf("node_modules/quickblox") > -1) {
    //        //console.log(key)
    //        delete require.cache[key]
    //    }
    //})

    //var QBconsumer = require('quickblox');
    //QBconsumer.init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);
    //QBconsumer.createSession(params, function (err, session) {
    //    if (err) {
    //        log.error({customMessage: "createSession failed for user", username: username, qbError: err})
    //        callback(err, null)
    //    } else {
    //        QBconsumer.chat.dialog.list({limit: config.qb.paginationLimit, skip: 0}, function (err, res) {
    //            if (err) {
    //                log.error({customMessage: "getDialoges failed for user", username: username, qbError: err})
    //                callback(err, null)
    //            } else {
    //                callback(null, res.items)
    //            }
    //        })
    //    }
    //})
    Chat.getAllDialogs(username)
        .then(function (results) {
            callback(null, results)
        })
        .fail(function (err) {
            log.error({customMessage: "getDialoges failed for user", username: username, qbError: err})
            callback(err, null)
        })
}