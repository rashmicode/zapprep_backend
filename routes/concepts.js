var express = require('express');
var router = express.Router();

var async = require('async');
var validator = require('validator');
validator.extend('isImage', function (mimetype) {
    if (mimetype == 'image/jpeg' || mimetype == 'image/png') return true;
    else return false;
});

var Notifications = require('../notifications');
var notify = new Notifications();

var passport = require('passport');

var config = require('../config.js'),
    customUtils = require('../utils.js');
var qbchat = require('../qbchat.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var multer = require('multer'),
    fs = require('fs');


router.route('/')
    //Get Objects of all Concepts for a Path
    .get([passport.authenticate('bearer', {session: false}), function (req, res) {
        var userId = req.user.results[0].value.id;
        var responseObj = {};
        var mentorAccess = true;
        var mentorRenewPopup = false;

        //TODO: converting this to a db.search would be more efficient in terms of data retrieved
        //but what exactly is efficient?
        db.search('paths', 'key:`' + req.query.pathId + "`", {
            //looks like you cant sort an inner json
            //sort: 'value.concepts.id:desc',
            limit: 100,
            offset: 0
        })
            .then(function (result) {
                if (result.body.total_count == 0) {
                    responseObj["errors"] = ["The Course/Path could not be found"];
                    res.status(503);
                    res.json(responseObj);
                } else {
                    var conceptData = result.body.results[0].value.concepts
                    var mentorPaid = result.body.results[0].value.mentorPaid

                    //check if the user is the mentor of this course
                    var isProducer = (result.body.results[0].value.producer.id == userId)
                    if (mentorPaid && !isProducer) {
                        /**
                         * This entire section is for getting mentorAccess flag
                         * get expires on data as well
                         */
                        var date = new Date()
                        var currentTime = date.getTime()
                        db.newSearchBuilder()
                            .collection('subscriptions')
                            .query('userId:`' + userId + '` AND pathId:`' + req.query.pathId)
                            .then(function (results) {
                                if (results.body.count > 0) {
                                    //subscribed previously
                                    //check if subscription still active
                                    var expiresOn = results.body.results[0].value.expiresOn
                                    if (currentTime > parseInt(expiresOn)) {
                                        //expired
                                        mentorAccess = false
                                        mentorRenewPopup = true
                                    } else {
                                        mentorAccess = true;
                                    }
                                } else {
                                    //never subscribed to
                                    mentorAccess = false
                                }
                                //time to sort this data
                                /**
                                 * If in the sorting process the introduction concept
                                 * gets shuffled to another position, android app will crash
                                 * POST and PATCH API should maintain this integrity
                                 */
                                conceptData.sort(function (a, b) {
                                    return parseFloat(a.pos) - parseFloat(b.pos);
                                });
                                conceptData.forEach(function (concept) {
                                    concept.objects.sort(function (a, b) {
                                        return parseFloat(a.pos) - parseFloat(b.pos);
                                    })
                                })
                                responseObj["data"] = conceptData;
                                responseObj['mentorAccess'] = mentorAccess;
                                responseObj['mentorRenewPopup'] = mentorRenewPopup;
                                res.status(200)
                                res.json(responseObj);
                            })
                            .fail(function (err) {
                                responseObj["errors"] = [err.body.message];
                                res.status(503);
                                res.json(responseObj);
                            });
                    } else {
                        //time to sort this data
                        /**
                         * If in the sorting process the introduction concept
                         * gets shuffled to another position, android app will crash
                         * POST and PATCH API should maintain this integrity
                         */
                        conceptData.sort(function (a, b) {
                            return parseFloat(a.pos) - parseFloat(b.pos);
                        });
                        conceptData.forEach(function (concept) {
                            concept.objects.sort(function (a, b) {
                                return parseFloat(a.pos) - parseFloat(b.pos);
                            })
                        })
                        responseObj["data"] = conceptData;
                        responseObj['mentorAccess'] = mentorAccess;
                        responseObj['mentorRenewPopup'] = mentorRenewPopup;
                        res.status(200)
                        res.json(responseObj);
                    }
                }
            })
            .fail(function (err) {
                responseObj["errors"] = [err.body.message, "Mostly the Path/Course Id does not exist"];
                res.status(503);
                res.json(responseObj);
            });
    }])

    //Create concepts for a Path
    .post([passport.authenticate('bearer', {session: false}), function (req, res, next) {
        qbchat.getSession(function (err, session) {
            if (err) {
                console.log("Recreating session");
                qbchat.createSession(function (err, result) {
                    if (err) {
                        res.status(503);
                        res.json({"errors": ["Can't connect to the chat server, try again later"]});
                    } else next();
                })
            } else next();
        })
    }, function (req, res) {
        var responseObj = {};
        var reqBody = req.body;

        var userObj = req.user.results[0].value;
        userObj.password = undefined;

        var errors = new Array();

        if (!validator.isLength(reqBody.title, 4, 65)) errors.push("Title must be between 8-65 characters");
        if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

        /**
         * Position for a new Concept is generated, not passed
         */
        //if (validator.isDecimal(reqBody.pos)) {
        //    reqBody.pos = parseFloat(reqBody.pos)
        //    /**
        //     * INTEGRITY CHECK:
        //     * Make sure introduction concept is always position 0
        //     */
        //    if (reqBody.pos <= 0) {
        //        errors.push("relative position(pos) of the concept cannot be <= 0");
        //    }
        //} else {
        //    errors.push("relative position of the concept must be a valid decimal number");
        //}

        if (errors.length > 0) {
            responseObj["errors"] = errors;
            res.status(422);
            res.json(responseObj);
        }
        else {
            getPositionOfLastConcept(reqBody.pathId, function (err, position) {
                var conceptId = customUtils.randomizer();
                if (err) {
                    responseObj["errors"] = [err.message];
                    res.status(503);
                    res.json(responseObj);
                } else {
                    var newPosition = position + 10000.0
                    qbchat.createRoom(2, reqBody.title, function (err, newRoom) {
                        if (err) {
                            responseObj["errors"] = [err.body.message];
                            res.status(503);
                            res.json(responseObj);
                        } else {
                            qbchat.addUserToRoom(newRoom._id, [userObj.qbId], function (err, result) {
                                if (err) console.log(err);
                            })
                            addConsumersToRoom(newRoom._id, reqBody.pathId)

                            db.newPatchBuilder("paths", reqBody.pathId)
                                .append("concepts", {
                                    "id": conceptId,
                                    "title": reqBody.title,
                                    "pos": newPosition,
                                    "objects": [],
                                    "qbId": newRoom._id
                                })
                                .apply()
                                .then(function (result) {
                                    var resBody = {
                                        "id": conceptId,
                                        "title": reqBody.title,
                                        "pos": newPosition,
                                        "qbId": newRoom._id
                                    };

                                    responseObj["data"] = resBody;
                                    res.status(201);
                                    res.json(responseObj);

                                    var notifObj = {
                                        "pathId": reqBody.pathId,
                                        "conceptName": reqBody.title
                                    };
                                    notify.emit('newConcept', notifObj);

                                    db.get('paths', reqBody.pathId)
                                        .then(function (path) {
                                            var date = new Date();
                                            var chatObj = {
                                                "created": date.getTime(),
                                                "id": date.getTime(),
                                                "type": "newChannel",
                                                "pathId": reqBody.pathId,
                                                "pathTitle": path.body.title,
                                                "channelName": reqBody.title,
                                                "channelId": newRoom._id
                                            }
                                            notify.emit('wordForChat', chatObj);
                                        })

                                })
                                .fail(function (err) {
                                    responseObj["errors"] = [err.body.message];
                                    res.status(503);
                                    res.json(responseObj);
                                })
                        }
                    })
                }
            })
        }
    }]);


//Edit concepts for a Path
router.patch('/', [passport.authenticate('bearer', {session: false}), function (req, res) {

    if (Object.keys(req.body).length === 0) {
        res.status(422);
        res.json({"errors": ["No key was sent in the body"]});
        return;
    }

    var responseObj = {};
    var reqBody = req.body;
    var errors = new Array();

    if (!validator.isNull(reqBody.title))
        if (!validator.isLength(reqBody.title, 8, 65)) errors.push("Title must be between 8-65 characters");
    //if (validator.isNull(reqBody.index)) errors.push("Index has to be specified");
    if (validator.isNull(reqBody.id)) errors.push("Concept id has to be specified");
    /**
     * proudly removed dependency on parent pathId
     * like it should always have been
     */
    //if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

    if (!validator.isNull(reqBody.pos))
        if (validator.isDecimal(reqBody.pos)) {
            reqBody.pos = parseFloat(reqBody.pos)
            /**
             * INTEGRITY CHECK:
             * Make sure introduction concept is always position 0
             */
            if (reqBody.pos <= 0) {
                errors.push("relative position(pos) of the concept cannot be <= 0");
            }
        } else {
            errors.push("relative position(pos) of the concept must be a valid decimal number");
        }

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    }
    else {
        //whichever key is not set will be undefined and hence ignored
        var payload = {
            "title": reqBody.title,
            "pos": reqBody.pos
        };
        convertConceptIdToIndex(reqBody.id, function (err, pathId, conceptIndex) {
            if (err) {
                responseObj["errors"] = [err.message];
                res.status(422);
                res.json(responseObj);
            } else if (conceptIndex == 0) {
                responseObj["errors"] = ["Introduction concept cannot be changed"];
                res.status(422);
                res.json(responseObj);
            } else {
                db.newPatchBuilder("paths", pathId)
                    .merge("concepts." + conceptIndex, payload)
                    .apply()
                    .then(function (result) {
                        responseObj["data"] = payload;
                        res.status(201);
                        res.json(responseObj);

                        if (typeof payload['title'] !== 'undefined') {
                            db.get('paths', pathId)
                                .then(function (result) {
                                    qbchat.changeRoomName(result.body.concepts[conceptIndex].qbId, reqBody.title, function (err, res) {
                                        console.log("changed Room name");
                                    })

                                    var date = new Date();
                                    var chatObj = {
                                        "type": "newChannelName",
                                        "pathId": pathId,
                                        "created": date.getTime(),
                                        "id": date.getTime(),
                                        "channelTitle": payload['title']
                                    }
                                    notify.emit("wordForChat", chatObj)
                                })
                        }
                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    })
            }
        });
    }
}]);


router.delete('/', [passport.authenticate('bearer', {session: false}), function (req, res, next) {
    qbchat.getSession(function (err, session) {
        if (err) {
            console.log("Recreating session");
            qbchat.createSession(function (err, result) {
                if (err) {
                    res.status(503);
                    res.json({"errors": ["Can't connect to the chat server, try again later"]});
                } else next();
            })
        } else next();
    })
}, multer(), function (req, res) {
    var responseObj = {};
    var reqBody = req.body;
    var errors = new Array();
    var channel;

    if (validator.isNull(reqBody.id)) errors.push("Concept id has to be specified");
    /**
     * Proudly removing the dependency of parent pathId
     * and bringing more integrity to humanity.
     */
    //if (validator.isNull(reqBody.pathId)) errors.push("PathId has to be specified");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    }
    else {
        convertConceptIdToIndex(reqBody.id, function (err, pathId, index) {
            if (err) {
                responseObj["errors"] = [err.message];
                res.status(422);
                res.json(responseObj);
            } else if (index == 0) {
                responseObj["errors"] = ["Introduction concept cannot be deleted"];
                res.status(422);
                res.json(responseObj);
            } else {
                db.get('paths', pathId)
                    .then(function (path) {
                        if (path.body.producer.id == req.user.results[0].value.id) {
                            channel = path.body.concepts[index].qbId
                            qbchat.deleteRoom(channel, function (err, res) {
                                if (err) console.log(err)
                            })
                        } else {
                            responseObj["errors"] = ["Only the mentor can delete the concepts"];
                            res.status(403);
                            res.json(responseObj);
                            return;
                        }
                    })
                    .then(function () {
                        db.newPatchBuilder("paths", pathId)
                            .remove("concepts." + index)
                            .apply()
                            .then(function (result) {
                                responseObj["data"] = {"success": "Concept Deleted"};
                                res.status(200);
                                res.json(responseObj);
                            })
                            .fail(function (err) {
                                responseObj["errors"] = [err.body.message];
                                res.status(503);
                                res.json(responseObj);
                            });
                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    });
            }
        })
    }
}]);

var addConsumersToRoom = function (room, path) {
    db.newGraphReader()
        .get()
        .limit(100)
        .from('paths', path)
        .related('isConsumed')
        .then(function (result) {
            var consumers = result.body.results.map(function (user) {
                return user.value.qbId
            })
            qbchat.addUserToRoom(room, consumers, function (err, result) {
                if (err) console.log(err);
            })
        })
        .fail(function (err) {
            console.log(err.body.message)
        })
}

/**
 * compare method for sort (ascending)
 * @param a first number
 * @param b second number
 * @returns {number}
 */
var compareNumbers = function (a, b) {
    return a.value - b.value;
}

/**
 * This method  is for converting the concept id to its array index
 * for easy editing in orchestrate
 * TODO: this is efficient if node-cache is implemented,
 * or else figure out a direct way to patch with conceptId
 * this is just another overhead, or maybe the orchestrate data should be more flat?
 *
 * @param String pathId
 * @param String conceptId
 * @param function callback
 */
var convertConceptIdToIndex = function (conceptId, callback) {
    db.search('paths', 'value.concepts.id: `' + conceptId + '`')
        .then(function (res) {
            //console.log("res.body")
            //console.log(res.body)
            if (res.body.total_count == 0) {
                callback(new Error("The Concept could not be found"), null, null)
            } else {
                var pathId = res.body.results[0].path.key
                //Means we definitely have a concept with that Id
                var conceptData = res.body.results[0].value.concepts
                //console.log("conceptData")
                //console.log(conceptData)
                var conceptIndex = 0
                //console.log("conceptData.length")
                //console.log(conceptData.length)
                for (; conceptIndex < conceptData.length; conceptIndex++) {
                    if (conceptData[conceptIndex]['id'] == conceptId) {
                        //console.log("Eureka!")
                        //console.log("conceptIndex " + conceptIndex)
                        //console.log("objectIndex" + objectIndex)
                        callback(null, pathId, conceptIndex)
                        return
                    }
                }
            }
            //impossible condition
            //console.log("impossible")
            callback(new Error("The Concept could not be found (id matched with some other object)"), null, null)
            //This condition was detected when the id matched the inner object's id
        })
        .fail(function (err) {
            //console.log("improbable")
            callback(new Error(err.body.message), null, null)
        });


    //db.get('paths', pathId)
    //    .then(function (result) {
    //        var conceptData = result.body.concepts
    //        var index = 0;
    //        while (index < conceptData.length && conceptId != conceptData[index]["id"]) {
    //            index++
    //        }
    //        if (index == conceptData.length) {
    //            /**
    //             * conceptId could not be found
    //             * callback params:
    //             * 1st - array index of the concept, null if no corresponding concept found
    //             * 2nd - error array with the problem details
    //             */
    //            callback(null, ["Concept id was not found"])
    //        } else if (index == 0) {
    //            callback(null, ["You cannot add/modify content in the Introduction concept"])
    //        } else {
    //            callback(index, [])
    //        }
    //    })
    //    .fail(function (err) {
    //        callback(null, [err.body.message, "Mostly the PathId does not exist"])
    //    });
}

/**
 * Method used to determine the pos value of the last concept
 * Intended use:
 * -- assigning a accurate pos (position) value when
 * -- creating a new concept
 * @param pathId
 * @param callback function(err, position)
 */
var getPositionOfLastConcept = function (pathId, callback) {
    db.get('paths', pathId)
        .then(function (result) {
            var lastConcept = {}
            var conceptData = result.body.concepts
            //1 concept will always exist - the Introduction
            var lastposition = conceptData[conceptData.length - 1].pos
            callback(null, lastposition)
        })
        .fail(function (err) {
            callback(new Error(err.body.message + " (Mostly the PathId does not exist) "), null)
        })
}

/**
 * Method used to determine the pos value of the last object
 * Intended use:
 * -- assigning a accurate pos (position) value when
 * -- creating a new concept Object
 * @param String pathid
 * @param Integer conceptArrayIndex the array index of the concept (convert id to index using convertConceptIdToIndex)
 * @param function(err, position) function(err, position)
 */
var getPositionOfLastObject = function (pathId, conceptArrayIndex, callback) {
    db.get('paths', pathId)
        .then(function (result) {
            var lastObject = {}
            var objectData = result.body.concepts[conceptArrayIndex].objects;
            if (objectData.length == 0) {
                callback(null, 10000)
            } else {
                var lastposition = objectData[objectData.length - 1].pos
                callback(null, lastposition)
            }
        })
        .fail(function (result) {
            callback(new Error(err.body.message + " (Mostly the PathId does not exist) "), null)
        })
}

var getConceptIndexAndObjectIndex = function (objectId, callback) {
    db.search('paths', 'value.concepts.objects.id: `' + objectId + '`')
        .then(function (res) {
            //console.log("res.body")
            //console.log(res.body)
            if (res.body.total_count == 0) {
                callback(new Error("The Object could not be found"), null, null)
            } else {
                var pathId = res.body.results[0].path.key
                //Means we definitely have a conceptObject with that Id, and therefore
                //parent conceptId as well
                var conceptData = res.body.results[0].value.concepts
                //console.log("conceptData")
                //console.log(conceptData)
                var conceptIndex = 0
                //console.log("conceptData.length")
                //console.log(conceptData.length)
                for (; conceptIndex < conceptData.length; conceptIndex++) {
                    var objectIndex = 0
                    //console.log("conceptData[" + conceptIndex+ "].['objects'].length = ")
                    //console.log(conceptData[conceptIndex]['objects'].length)
                    for (; objectIndex < conceptData[conceptIndex]['objects'].length; objectIndex++) {
                        if (conceptData[conceptIndex]['objects'][objectIndex]['id'] == objectId) {
                            //console.log("Eureka!")
                            //console.log("conceptIndex " + conceptIndex)
                            //console.log("objectIndex" + objectIndex)
                            callback(null, pathId, conceptIndex, objectIndex)
                            return
                        }
                    }
                }
            }
            //impossible condition
            //console.log("impossible")
            callback(new Error("The object was found but its position could not be ascertained"), null, null, null)
        })
        .fail(function (err) {
            //console.log("improbable")
            callback(new Error(err.body.message), null, null, null)
        });
}

module.exports = {
    router: router,
    convertConceptIdToIndex: convertConceptIdToIndex,
    getPositionOfLastConcept: getPositionOfLastConcept,
    getPositionOfLastObject: getPositionOfLastObject,
    getConceptIndexAndObjectIndex: getConceptIndexAndObjectIndex
}