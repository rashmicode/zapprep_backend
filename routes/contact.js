var express = require('express');
var router = express.Router();

var Notifications = require('../notifications');
var notify = new Notifications();

var passport = require('passport');
var _ = require('lodash');

var validator = require('validator');

var config = require('../config.js'),
    customUtils = require('../utils.js');

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var multer = require('multer'),
    fs = require('fs');

var mailgun = require('mailgun-js')({apiKey: config.mailgun.key, domain: config.mailgun.domain});


router.route('/')
    //Get Objects of all Concepts for a Path
    .get([function (req, res) {

        if (!req.query.limit || req.query.limit < 0) req.query.limit = 10;
        if (!req.query.page || req.query.page < 1) req.query.page = 1;
        if (!req.query.type) req.query.type = 'subscription'

        var limit = req.query.limit;
        var offset = limit * (req.query.page - 1);
        var responseObj = {};

        db.newSearchBuilder()
            .collection('contacts')
            .query('value.type:`' + req.query.type + '`')
            .then(function (result) {
                if (result.body.next) responseObj["more"] = true;
                else responseObj["more"] = false;

                responseObj["data"] = _.cloneDeep(result.body.results);
                for (var i = 0 in responseObj["data"]) {
                    responseObj["data"][i].value["id"] = responseObj["data"][i].path.key;
                    responseObj["data"][i] = responseObj["data"][i].value;
                }
                res.json(responseObj);
            })
            .fail(function (err) {
                responseObj["errors"] = [err.body.message];
                res.status(503);
                res.json(responseObj);
            });
    }])

    //Create concepts for a Path
    .post([multer(), function (req, res) {
        var responseObj = {};
        var reqBody = req.body;
        var errors = new Array();

        if (!reqBody.type) reqBody.type = "subscription"

        if (validator.isNull(reqBody.email)) errors.push("You didn't mention the Email");
        if (!validator.isNull(reqBody.email))
            if (!validator.isEmail(reqBody.email)) errors.push("Please Enter a valid Email ID")

        if (errors.length > 0) {
            responseObj["errors"] = errors;
            res.status(422);
            res.json(responseObj);
        }
        else {
            var payload = {
                "type": reqBody.type,
                "name": reqBody.name,
                "email": reqBody.email,
                "contact": reqBody.contact,
                "message": reqBody.message
            };
            console.log("A")
            sendEmail("tushar@pyoopil.com", "#PyoopilContact", payload);
            console.log("B")
            db.post('contacts', payload)
                .then(function (result) {
                    var contactID = result.headers.location.match(/[0-9a-z]{16}/)[0];
                    payload["id"] = contactID;
                    responseObj["data"] = payload;
                    res.status(201);
                    res.json(responseObj);
                })
                .fail(function (err) {
                    responseObj["errors"] = [err.body.message];
                    res.status(503);
                    res.json(responseObj);
                })
        }
    }]);

var sendEmail = function (email, subject, payload) {

    console.log("C")

    var from = payload.name + ' <' + payload.email + '>';
    var text = 'Contact : ' + payload.contact + '\n\n' + payload.message;
    console.log("D")
    var data = {
        from: from,
        to: email,
        subject: subject,
        text: text
    };
    console.log("E")
    mailgun.messages().send(data, function (error, body) {
        if (error) {
            console.log(error)
        }
    });
    console.log("F")
}

module.exports = router;
