var express = require('express');
var router = express.Router();

var validator = require('validator');
var passport = require('passport');

var config = require('../config.js')

var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);

var customUtils = require('../utils.js');

var _ = require('lodash');

router.route('/')
    //Get Objects of all Concepts for a Path
    .post([passport.authenticate('bearer', {session: false}), function (req, res) {

        var userObj = req.user.results[0].value;
        var userId = userObj.id;

        var reqBody = req.body;
        var responseObj = {};
        var errors = new Array();

        if (validator.isNull(reqBody.paymentId)) errors.push("payment ID cannot be empty");
        if (validator.isNull(reqBody.pathId)) errors.push("Path ID cannot be empty");
        if (!validator.isIn(reqBody.currency, ['USD', 'INR'])) errors.push("Currency must be either USD or INR");
        if (!validator.isIn(reqBody.product, ['mentor', 'course'])) errors.push("product must be mentor or course");
        if (!validator.isDecimal(reqBody.amount)) errors.push("Amount is invalid");

        if (errors.length > 0) {
            responseObj["errors"] = errors;
            res.status(422);
            res.json(responseObj);
        }
        else {
            //a sanitized way of assigning values
            //rather than payload = reqBody
            var date = new Date();
            var payload = {
                'paymentId': reqBody.paymentId,
                'pathId': reqBody.pathId,
                'currency': reqBody.currency,
                'amount': parseFloat(reqBody.amount),
                'product': reqBody.product,
                'discountCode': reqBody.discountCode,
                'userId': userId,
                'username': userObj.username,
                'name': userObj.name,
                'created': date.getTime()
            };
            console.log("about to post")
            //TODO: verify the payment with payment gateway
            db.post('payments', payload)
                .then(function (result) {
                    res.status(201);
                    //Don't need to return payment data I guess?
                    //definitely need to send a response but
                    payload["id"] = result.headers.location.match(/[0-9a-z]{16}/)[0];
                    responseObj["data"] = payload;
                    res.json(responseObj);
                    var amountInPaise = parseFloat(payload["amount"]) * 100
                    if (payload['product'] == "mentor") {
                        updateSubscription(userId, payload['pathId'], payload["id"], amountInPaise, payload["paymentId"])
                    }
                    //no action so far for course
                    //else if(payload['product'] == "course") {
                    //
                    //}
                })
                .fail(function (err) {
                    responseObj["errors"] = [err.body.message];
                    res.status(503);
                    res.json(responseObj);
                })
        }
    }])
    .get([passport.authenticate('bearer', {session: false}), function (req, res) {
        var userObj = req.user.results[0].value;
        var userId = userObj.id;
        var responseObj = {};

        var pathNameMap = new Object(); // or var map = {};
        function get(k) {
            return pathNameMap[k];
        }

        //TODO: this becomes faster with a promise and async

        db.search('subscriptions', 'userId:`' + userId + '`', {
            limit: 100
        })
            .then(function (result) {
                ///**
                // * Map of pathId : expiresOn ---------------------------------------
                // * TODO: when implementing pagination, this strategy wont work.
                // */
                //var expiresOnMap = new Object(); // or var map = {};
                //function getExpiresOn(k) {
                //    return expiresOnMap[k];
                //}

                //result.body.results.forEach(function (path) {
                //    expiresOnMap[path.value.pathId] = path.value.expiresOn
                //})
                //Map ends ----------------------------------------------------------
                //console.log(expiresOnMap)
                db.newGraphReader()
                    .get()
                    .limit(100)
                    .offset(0)
                    .from('users', userId)
                    .related('consumes')
                    .then(function (result) {
                        if (result.body.count > 0) {
                            result.body.results.forEach(function (path) {
                                pathNameMap[path.path.key] = path.value.title
                            })

                            db.search('payments', 'userId:`' + userId + '`', {
                                sort: 'value.created:desc'
                            })
                            /**
                             * Strangely adding .sort to the newSearchBuilder query just freezes
                             * the code from executing.
                             * replaced it with search.
                             * wierd stuff.
                             *
                             * orchestrate support traced it and realized that query must be put in the end.
                             * because this returns a builder, search returns a future
                             */
                                //db.newSearchBuilder()
                                //    .collection('payments')
                                //    .query('userId:`' + userId + '`')
                                //    //.sort('created', 'desc')
                                .then(function (result) {
                                    responseObj["data"] = _.cloneDeep(result.body.results);
                                    for (var i = 0 in responseObj["data"]) {
                                        var id = responseObj["data"][i].path.key;
                                        var pathId = responseObj["data"][i].value.pathId;
                                        responseObj["data"][i] = responseObj["data"][i].value;
                                        responseObj["data"][i]["id"] = id;
                                        responseObj["data"][i]["pathTitle"] = get(pathId);
                                        //if (responseObj["data"][i]["product"] == "mentor")
                                        //    responseObj["data"][i]["expiresOn"] = getExpiresOn(pathId);
                                    }
                                    /**
                                     * sort wasnt working
                                     */
                                    responseObj["data"].sort(function (a, b) {
                                        a.created - b.created
                                    })
                                    res.status(200)
                                    res.json(responseObj);
                                })
                                .fail(function (err) {
                                    responseObj["errors"] = [err.body.message];
                                    res.status(503);
                                    res.json(responseObj);
                                })
                        }
                        else {
                            responseObj = {
                                'data': []
                            }
                            res.status(200)
                            res.json(responseObj);
                        }
                    })
                    .fail(function (err) {
                        responseObj["errors"] = [err.body.message];
                        res.status(503);
                        res.json(responseObj);
                    })
            })
    }])

router.route('/mentor')
    .get([passport.authenticate('bearer', {session: false}), function (req, res) {

        var pathNameMap = new Object(); // or var map = {};
        function get(k) {
            return pathNameMap[k];
        }

        var userId = req.user.results[0].value.id;
        var responseObj = {};

        db.newGraphReader()
            .get()
            .limit(100)
            .offset(0)
            .from('users', userId)
            .related('produces')
            .then(function (result) {
                var searchPattern = "";
                //var consumedPaths = result.body.results.map(function (path) {
                //    return path.path.key
                //})
                if (result.body.count > 0) {
                    result.body.results.forEach(function (path) {
                        searchPattern = searchPattern + "pathId:`" + path.path.key + "` OR "
                        pathNameMap[path.path.key] = path.value.title
                    })
                    //TODO: can this stop working if searchPattern is too long?
                    //what is the max length of the lucene query supported by orchestrate?
                    searchPattern = searchPattern.substring(0, searchPattern.length - 4);

                    db.search('subscriptions', searchPattern, {
                        limit: 100
                    })
                        .then(function (result) {
                            ///**
                            // * Map of pathId : expiresOn ---------------------------------------
                            // * TODO: when implementing pagination, this strategy wont work.
                            // */
                            //var expiresOnMap = new Object(); // or var map = {};
                            //function getExpiresOn(k) {
                            //    return expiresOnMap[k];
                            //}

                            //result.body.results.forEach(function (path) {
                            //    expiresOnMap[path.value.pathId] = path.value.expiresOn
                            //})

                            //Map ends ----------------------------------------------------------
                            db.search('payments', searchPattern, {
                                sort: 'value.created:desc'
                            })
                            /**
                             * Strangely adding .sort to the newSearchBuilder query just freezes
                             * the code from executing.
                             * replaced it with search.
                             * wierd stuff.
                             */
                                //db.newSearchBuilder()
                                //    .collection('payments')
                                //    .query(searchPattern)
                                //    .sort('created', 'desc')
                                .then(function (result) {
                                    //plugin
                                    responseObj["data"] = _.cloneDeep(result.body.results);
                                    for (var i = 0 in responseObj["data"]) {
                                        var id = responseObj["data"][i].path.key;
                                        var pathId = responseObj["data"][i].value.pathId;
                                        responseObj["data"][i] = responseObj["data"][i].value;
                                        responseObj["data"][i]["id"] = id
                                        responseObj["data"][i]["pathTitle"] = get(pathId)
                                        //if (responseObj["data"][i]["product"] == "mentor")
                                        //    responseObj["data"][i]["expiresOn"] = getExpiresOn(pathId);
                                    }
                                    /**
                                     * sort wasnt working
                                     */
                                    responseObj["data"].sort(function (a, b) {
                                        a.created - b.created
                                    })
                                    res.status(200)
                                    res.json(responseObj);
                                })
                                .fail(function (err) {
                                    responseObj["errors"] = [err.body.message];
                                    res.status(503);
                                    res.json(responseObj);
                                })
                        })
                        .fail(function (err) {
                            responseObj["errors"] = [err.body.message];
                            res.status(503);
                            res.json(responseObj);
                        })
                } else {
                    responseObj = {
                        'data': []
                    }
                    res.status(200)
                    res.json(responseObj);
                }
            })
    }])

/**
 * Update the subscription of a userId to the mentor channel
 * of the pathId, based on the payment made
 * @param userId
 * @param pathId
 * @param transactionId
 * @param amount
 * @param razorpayId
 */
var updateSubscription = function (userId, pathId, transactionId, amount, razorpayId) {
    /**
     *  if previously subscribed to and not expired
     *      expiresOn = expiresOn + config.TimeOfSubscription
     *  else
     *      expiresOn = transactionTime + config.TimeOfSubscription
     */
    console.log("we are updating subscription");
    var date = new Date();
    var currentTime = date.getTime();
    var expiresOn;

    db.newSearchBuilder()
        .collection('subscriptions')
        .limit(1)
        .query('userId:`' + userId + '` AND pathId:`' + pathId)
        .then(function (results) {
            console.log("query executed successfully")
            if (results.body.total_count > 0) {
                console.log("subscription exists")
                expiresOn = results.body.results[0].value.expiresOn;
                console.log(expiresOn)
                console.log(currentTime)
                if (parseInt(expiresOn) < parseInt(currentTime)) {
                    console.log("expiresOn < currentTime")
                    expiresOn = currentTime + config.mentorSubscription.time;
                } else {
                    console.log("NOT expiresOn < currentTime")
                    expiresOn = expiresOn + config.mentorSubscription.time;
                }
                //update the subscription
                db.merge('subscriptions', results.body.results[0].path.key, {
                    "expiresOn": expiresOn
                })
                    .then(function (result) {
                        console.log("subscription updated")
                        //capture payment
                        customUtils.captureRazorPayment(razorpayId, amount)
                        updateExpiresOn(transactionId, expiresOn)
                    })
                    .fail(function (err) {
                        console.log("subscription update FAILED")
                    })

            } else {
                console.log("no subscription bro")
                expiresOn = currentTime + config.mentorSubscription.time
                console.log(expiresOn)
                //create a new one
                //this is so join tabley
                db.post('subscriptions', {
                    "userId": userId,
                    "pathId": pathId,
                    "expiresOn": expiresOn
                })
                    .then(function (result) {
                        console.log("new subscription created")
                        customUtils.captureRazorPayment(razorpayId, amount);
                        updateExpiresOn(transactionId, expiresOn)
                    })
                    .fail(function (err) {
                        console.log("subscription creation failed")
                    })
            }

            //responseObj['data'] = payments;
            //res.status(201);
            //res.json({allok : true});
            //res.json(responseObj);
        })
        .fail(function (err) {
            console.log("query failed")
            responseObj["errors"] = [err.body.message];
            res.status(503);
            res.json(responseObj);
        })
}

var updateExpiresOn = function (transactionId, expiresOn) {
    db.merge('payments', transactionId, {
        "expiresOn": expiresOn
    })
        .then(function (results) {
            console.log("merge sucksex")
        })
        .fail(function (err) {
            console.log(err.body.message)
            console.log("failed bro")
        })
}

module.exports = router;