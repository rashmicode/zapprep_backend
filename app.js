'use strict';

var express = require('express');

/**
 * globals
 * @type {string}
 * @private
 */
global.__base = __dirname + '/';

/**
 * TODO : Notes for you, ya you the awesome developer:
 * do something like forever -w -o foreverLog.log start ./bin/www
 * in package.json
 * w will watch for file changes, but then log files keep changing
 * sp you need to include those files in the .foreverignore
 * you might need to give write privileges to forever to create log files (perhaps)
 */

var path = require('path');
var fs = require('fs');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var request = require('request');
var CronJob = require('cron').CronJob;

var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;

var paths = require('./path');
var user = require('./user');
var User = require('./user/user.model');
var concepts = require('./concept');
var objects = require('./object');
// var chat = require('./routes/chat');
var announcements = require('./announcement');
var reviews = require('./review');
var invite = require('./invite');
// var feedback = require('./routes/feedback');
// var feedbackv2 = require('./routes/feedbackv2');
// var contacts = require('./routes/contact');
var payments = require('./payment');
// var enrollment = require('./enrollment');

var config = require('./config.js');
var customUtils = require('./utils.js');
var qbchat = require('./qbchat.js');

var validator = require('validator');
var mongoose = require('mongoose');

// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', function (err) {
    console.error('MongoDB connection error: ' + err);
    process.exit(-1);
  }
);

// var oio = require('orchestrate');
// oio.ApiEndPoint = config.db.region;
// var db = oio(config.db.key);

var Notifications = require('./notifications');
var notify = new Notifications();

var app = express();

qbchat.init();
qbchat.createSession(function (err, session) {
    if (session) {
      console.log("Session created");
    } else if (err) console.log(err)
  }
);
var bunyan = require('bunyan');
var log = bunyan.createLogger({
  name: 'fatalErrors',
  streams: [
    {
      path: __dirname + '/fatalErrors.log'  // log ERROR and above to a file
    }
  ]
});

function failure() {
  return false;
}

// app.use(favicon(__dirname + '/public/favicon.ico'));

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'})
//var corsOptions = {
//    origin: 'http://localhost:3000',
//   credentials: true
//};
var corsOptions = {
  origin: ['http://188.166.226.12', 'http://beta.zapprep.in', 'http://localhost', 'http://api.zapprep.in'],
  credentials: true
};

app.use(morgan(':remote-addr - [:date[clf]] - :method :url :status - :response-time ms', {stream: accessLogStream}));
app.use(morgan('dev'));
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(passport.initialize());
passport.use(new BearerStrategy({},
  function (token, done) {
    User.findOne({access_token: token}).exec(function (err, user) {
      if(err) return done(null, false);
      if(user) return done(null, user);
      else return done(null, false);
    });
  }
));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use('/public', express.static(path.join(__dirname, 'public')));
// app.use('/paths', paths);
app.use('/user', user);
app.use('/announcements', announcements);
app.use('/concepts', concepts);
app.use('/objects', objects);
app.use('/paths', paths);
app.use('/payments', payments);
app.use('/reviews', reviews);
app.use('/invite', invite);
// app.use('/concepts', concepts.router);
// app.use('/objects', objects);
// app.use('/chat', chat);
// app.use('/announcements', announcements);
// app.use('/reviews', reviews);
// app.use('/feedback', feedback);
// app.use('/feedbackv2', feedbackv2);
// app.use('/contacts', contacts)
// app.use('/payments', payments)

app.all('/ping', function (req, res) {
  res.send('Pong')
});

app.post('/util/link', function (req, res) {
  if (!validator.isURL(req.body.url)) {
    res.status(422);
    res.json({"errors": ["URL must be valid"]});
  } else {
    customUtils.getLinkInfo(req.body.url, function (info) {
      info["video"] = req.body.url;
      res.json({"data": info});
    })
  }
});

app.get('/preview', function (req, res) {
  var theRequest = "http://collex.io/c/get_site_content/?url=" + req.query.url
  console.log(theRequest);
  try {
    request(theRequest, function (err, response, body) {
      if (body) {
          res.send(JSON.parse(body))
      } else {
          return res.send({
              url: req.query.url,
              preview: {},
              card: {}
          });
      }
    })
  }
  catch (excpetion) {
    res.send({
        url: req.query.url,
        preview: {},
        card: {}
      }
    )
  }
})

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
app.use(function (err, req, res, next) {
  log.error(err, "oops");
  res.status(err.status || 500);
  res.json({
    errors: [err.message],
    errorObj: err
  });
});

// development error handler
// will print stacktrace
//if (app.get('env') === 'development') {
//app.use(function (err, req, res, next) {
//    res.status(err.status || 500);
//    res.send(err);
//});
//}

// production error handler
// no stacktraces leaked to user
//app.use(function (err, req, res, next) {
//    res.status(err.status || 500);
//    res.send(err);
//});


module.exports = app;
