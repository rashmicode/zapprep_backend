/**
 * TODO : Firebase discourages use of keys such as timestamp,
 * use their post.set for keys that handle it uniquely and well
 * @type {Logger|exports}
 */
var bunyan = require('bunyan');
var log = bunyan.createLogger({
    name: 'unreadCountErrors',
    streams: [
        {
            path: __dirname + '/unreadCountErrors.log'  // log ERROR and above to a file
        }
    ]
});
var fs = require('fs');

// Mentors is a special user type in my project. Modify/delete as per need

var config = require('./config.js');
var ChatUser = require("./chat");

//var QB = require('quickblox');
//QB.init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);

var async = require('async')
var Notifications = require('./notifications');
var notify = new Notifications();

var now = new Date().getTime() / 1000

/**
 * /**
 * This is what a qbDialog looks like:
 //{ _id: '56794ff9a28f9ab1e5000374',
        //    created_at: '2015-12-22T13:28:25Z',
        //    last_message: null,
        //    last_message_date_sent: null,
        //    last_message_user_id: null,
        //    name: 'new concept',
        //    occupants_ids: [ 5372309, 7522231, 7522239, 7522718, 7523428, 7523544, 7533504 ],
        //    photo: null,
        //    silent_ids: [],
        //    type: 2,
        //    updated_at: '2015-12-24T15:17:36Z',
        //    user_id: 5372309,
        //    xmpp_room_jid: '28196_56794ff9a28f9ab1e5000374@muc.chat.quickblox.com',
 //    unread_messages_count: 0 }
 * @param username
 * @param mentorRoomList
 * @param callback
 */
var getUnreadCountOfUser = function (username, mentorRoomList, callback) {
    var params = {
        'login': username,
        'password': 'pyoopilDevsRock'
    }

    /**
     * disable require cache to get a new QB object for consumer
     * so that the sessions dont clash!
     * TODO: fine a better way to do this. perhaps create an instance of QB
     ***/
    Object.keys(require.cache).forEach(function (key) {
        //delete require.cache[key]
        if (key.indexOf("node_modules/quickblox") > -1) {
            //console.log(key)
            delete require.cache[key]
        }
    })

    var QBconsumer = require('quickblox');
    QBconsumer.init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);
    QBconsumer.createSession(params, function (err, session) {
        if (err) {
            log.error({customMessage: "createSession failed for user", username: username, qbError: err})
            callback(err, null)
        } else {
            /**
             * TODO: you will need to make a recursive call to accomodate dialogs
             * more than config.qb.paginationLimit
             */
            QBconsumer.chat.dialog.list({limit: config.qb.paginationLimit, skip: 0}, function (err, res) {
                if (err) {
                    log.error({customMessage: "getDialoges failed for user", username: username, qbError: err})
                    callback(err, null)
                } else {
                    var unreadCount = 0
                    res.items.forEach(function (dialog) {
                        //if the dialog does not belong to the mentor channel lets
                        //not count the unread dialog messages
                        //console.log("dialog : " + dialog['_id'])
                        if (!(mentorRoomList.indexOf(dialog['_id']) > -1)) {
                            //console.log("true")
                            unreadCount = unreadCount + dialog['unread_messages_count']
                        }
                    })
                    callback(null, unreadCount)

                    /**
                     * Activate this code, when qb library starts working
                     * correctly. currently unread count is not being
                     * retrieved correctly for filtered set of dialogs
                     */
                    //var allDialogListIds = res.items.map(function (dialog) {
                    //    return dialog._id
                    //})
                    //
                    //var finalDialogListIds = allDialogListIds.filter(function (dialogid) {
                    //    //if mentorRoomList mey nahi hai, add it to your final array
                    //    return !(mentorRoomList.indexOf(dialogid) > -1)
                    //});
                    //
                    //var unreadParams = {chat_dialog_ids: finalDialogListIds};
                    ////console.log(unreadParams)
                    //
                    //QB.chat.message.unreadCount(unreadParams, function (err, res) {
                    //    if (err) {
                    //        console.log("unreadCount failed for user " + params.username)
                    //        console.log(err)
                    //        callback(err, null)
                    //    } else {
                    //        callback(null, res.total)
                    //    }
                    //})
                }
            })
        }
    })
}

var fireUsersNotif = function (offset, mentorRoomList) {
    var date = new Date()
    db.newSearchBuilder()
        .collection('users')
        .limit(config.pagination.limit)
        .offset(offset)
        .query('@path.kind:item')
        .then(function (users) {
            var userList = users.body.results.map(function (user) {
                return user.value
            })

            //1st para in async.each() is the array of items
            async.each(userList,
                // 2nd param is the function that each item is passed to
                function (user, callback) {
                    getUnreadCountOfUser(user.username, mentorRoomList, function (err, unreadCount) {
                        if (err) {
                            console.log("Unable to fetch unread count of user : " + user.username)
                        } else {
                            if (unreadCount > 0) {
                                log.info({username: user.username, unread: unreadCount})
                                var notifObj = {
                                    'userGcmId': user.gcmId,
                                    'unreadCount': unreadCount
                                }
                                notify.emit('unreadMessageCount', notifObj)
                            }
                        }
                        callback()
                    })
                },
                // 3rd param is the function to call when everything's done
                function (err) {
                    // All tasks are done now
                    console.log("unread notifications batch fired")
                })

            //userList.forEach(function (user) {
            //    getUnreadCountOfUser(user.username, mentorRoomList, function (err, unreadCount) {
            //        if (err) {
            //            console.log("Unable to fetch unread count of user : " + user.username)
            //        } else {
            //            console.log("User   : " + user.username)
            //            console.log("unread : " + unreadCount)
            //            //if (unreadCount > 0) {
            //            //    var notifObj = {
            //            //        'userGcmId': user.gcmId,
            //            //        'unreadCount': unreadCount
            //            //    }
            //            //    notify.emit('unreadMessageCount', notifObj)
            //            //}
            //        }
            //    })
            //})
            //console.log("unread notifications batch fired")


            offset += 100;
            var remainingUsers = users.body.total_count - offset;
            if (remainingUsers > 0) {
                fireUsersNotif(offset, mentorRoomList);
            } else {
                console.log("done :/")
            }
        })
        .fail(function (err) {
            console.log("Couldn't get the users for caching, you are doomed");
        });
};

//console.log("getting paths : ")
var dispatchUnreadNotifications = function () {
    /**
     * TODO: Assuming there are not more than 100 courses in our platform
     */
    db.list('paths', {limit: 100})
        .then(function (paths) {
            var mentorRoomJids = paths.body.results.map(function (path) {
                return path.value.mentorRoomId
            })
            fireUsersNotif(0, mentorRoomJids)

            /**
             * Single user testing ka code
             */
            //getUnreadCountOfUser("tushar_banka", mentorRoomJids, function(err, unreadCount) {
            //    if(err) {
            //        console.log("failed")
            //    } else {
            //        console.log("Unread messages : ")
            //        console.log(unreadCount)
            //    }
            //})
        })
        .fail(function (err) {
            log.error("Couldn't get the paths for caching, you are doomed");
        });

}
// dispatchUnreadNotifications()

module.exports = {
    dispatchUnreadNotifications: dispatchUnreadNotifications
}
