var config = require('./config.js');

var QuickBlox = require('quickblox').QuickBlox;
var QB = new QuickBlox();
var extras = [];

exports.init = function () {
    QB.init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);
};

exports.extraInit = function (index) {
    extras[index].init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);
};

exports.extraInitWithParams = function (index, appId, authKey, authSecret) {
    extras[index].init(appId, authKey, authSecret, false);
};

exports.extraQB = function () {
    extras.push(new QuickBlox());
    return extras.length - 1;
};

exports.extraCreateSession = function (index, cb) {
    if (index < extras.length) {
        extras[index].createSession(config.qb.params, function (err, session) {
            cb(err, session);
        });
    } else return undefined;
};

exports.extraCreateSessionWithParams = function (index, params, cb) {
    if (index < extras.length) {
        extras[index].createSession(params, function (err, session) {
            cb(err, session);
        });
    } else return undefined;
};

exports.createSession = function (cb) {
    QB.createSession(config.qb.params, function (err, session) {
        cb(err, session);
    });
};

exports.createUser = function (params, cb) {
    QB.users.create(params, function (err, newUser) {
        cb(err, newUser);
    });
};

exports.extraCreateUser = function (index, params, login_params, cb) {
    extras[index].createSession(login_params, function(err, session){
        if(!err && session) {
            extras[index].login(login_params, function (err, session) {
                if(!err && session) {
                    extras[index].users.create(params, function (err, newUser) {
                        cb(err, newUser);
                    });
                } else cb(err, session);
            });
        } else cb(err, session);
    });
};

exports.updateUser = function (id, params, cb) {
    QB.users.update(id, params, function (err, user) {
        cb(err, user);
    });
};

exports.extraUpdateUser = function (index, id, params, login_params, cb) {
    extras[index].login(login_params, function (err, newUser) {
        extras[index].users.update(id, params, function (err, user) {
            cb(err, user);
        });
    });
};

exports.createRoom = function (type, name, cb) {
    QB.chat.dialog.create({type: type, name: name}, function (err, newRoom) {
        cb(err, newRoom);
    });
};

exports.extraCreateRoom = function (index, type, name, login_params, cb) {
    extras[index].createSession(login_params, function(err, session){
        extras[index].login(login_params, function (err, newUser) {
            extras[index].chat.dialog.create({type: type, name: name}, function (err, newRoom) {
                cb(err, newRoom);
            });
        });
    });
};

exports.login = function (cb) {
    QB.login(config.qb.params, function (err, newRoom) {
        cb(err, newRoom);
    });
};

exports.extraLogin = function (index, params, cb) {
    extras[index].login(params, function (err, newRoom) {
        cb(err, newRoom);
    });
};

exports.addUserToRoom = function (roomId, userIds, cb) {
    QB.chat.dialog.update(roomId, {push_all: {occupants_ids: userIds}},
        function (err, result) {
            cb(err, result)
        }
    );
};

exports.extraAddUserToRoom = function (index, roomId, userIds, login_params, cb) {
    extras[index].createSession(login_params, function(err, session){
        extras[index].login(login_params, function (err, session) {
            extras[index].chat.dialog.update(roomId, {push_all: {occupants_ids: userIds}},
                function (err, result) {
                    cb(err, result)
                }
            );
        });
    });
};

exports.changeRoomName = function (roomId, newName, cb) {
    QB.chat.dialog.update(roomId, {name: newName},
        function (err, result) {
            cb(err, result);
        }
    );
};

exports.extraChangeRoomName = function (index, roomId, newName, login_params, cb) {
    extras[index].createSession(login_params, function(err, session){
        extras[index].login(login_params, function(err, session){
            extras[index].chat.dialog.update(roomId, {name: newName},
                function (err, result) {
                    cb(err, result);
                }
            );
        });
    });
};

exports.deleteRoom = function (roomId, cb) {
    QB.chat.dialog.delete(roomId,
        function (err, result) {
            cb(err, result);
        }
    );
};

exports.extraDeleteRoom = function (index, roomId, login_params, cb) {
    extras[index].createSession(login_params, function(err, session){
        extras[index].login(login_params, function(err, session){
            extras[index].chat.dialog.delete(roomId, function (err, result) {
                    cb(err, result);
                }
            );
        });
    });
};

exports.extraDeleteUser = function (index, userId, login_params, user_params, cb) {
    extras[index].createSession(login_params, function(err, session){
        extras[index].login(user_params, function(err, session){
            extras[index].users.delete(userId, function (err, result) {
                    cb(err, result);
                }
            );
        });
    });
};

exports.getSession = function (cb) {
    QB.getSession(function (err, res) {
        cb(err, res);
    })
};

exports.reCreateSession = function (req, res, next) {
    QB.getSession(function (err, res) {
        if (err) {
            QB.createSession(config.qb.params, function (err, session) {
                if (err) {
                    res.status(503);
                    res.json({"errors": ["Can't connect to the chat server, try again later"]});
                } else next();
            });
        } else next();
    });
};

exports.getUsers = function (users, cb) {
    var params = {filter: { field: 'id', param: 'in', value: users }};

    QB.users.listUsers(params, function (err, result) {
        cb(err, result);
    });
};

exports.extraGetUsers = function (index, users, login_params, cb) {
    var params = {filter: { field: 'id', param: 'in', value: users }};

    extras[index].login(login_params, function (err, newUser) {
        extras[index].users.listUsers(params, function (err, result) {
            cb(err, result);
        });
    });
};