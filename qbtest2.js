var qbchat = require('./qbchat');
var config = require('./config');
var mongoose = require('mongoose');

// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', function (err) {
        console.error('MongoDB connection error: ' + err);
        process.exit(-1);
    }
);

var QB = qbchat.extraQB();
qbchat.extraInit(QB);

var Path = require('./path/path.model');
var User = require('./user/user.model');
var Enroll = require('./enrollment/enroll.model');
var Concept = require('./concept/concept.model');
var async = require('async');

function escapeRegExpChars(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

var add_user_to_room = function(roomId, qbId, path_cb){
    console.log('add user to room -', roomId, qbId);
    qbchat.extraAddUserToRoom(QB, roomId, [parseInt(qbId)], config.qb.params, function(err, result){
        if(err) {
            console.log('error adding user Id to general room', err, qbId, roomId);
            path_cb();
        } else if(result) {
            console.log('finished adding user to general room', qbId, roomId);
            path_cb();
        } else {
            console.log('unknown error adding user Id to general room', err, qbId, roomId, result);
            path_cb();
        }
    });
};

var create_and_add_user_to_room = function(path, path_cb){
    qbchat.extraCreateRoom(QB, 2, "General", config.qb.params, function(err, newRoom){
        if(err) {
            console.log('error creating new room - ', err, ", path Id - ", path._id);
            path_cb();
        } else if(newRoom && newRoom._id) {
            path['qbId'] = newRoom._id;
            path.save(function(err){
                if(err) {
                    console.log('error saving created room details - ', newRoom, err, path._id);
                    path_cb();
                } else {
                    add_user_to_room(newRoom._id, path.producer.qbId, path_cb);
                }
            });
        } else {
            console.log("Unknown error occurred while creating room - ", err, newRoom, path._id);
            path_cb();
        }
    });
};

// Quickblox update ids script
async.series([
    function(cb){
        Path.find().deepPopulate('producer').exec(function(err, paths){
            async.eachSeries(paths, function(path, path_cb){
                if(path && path.qbId) {
                    if(path.producer && path.producer.qbId) {
                        qbchat.extraChangeRoomName(QB, path.qbId, "General", config.qb.params, function(err, result){
                            if(err) {
                                console.log('error updating room - ', err, ", path Id - ", path._id, ", room Id - ", path.qbId);
                                //TODO: create room and add user to room here
                                create_and_add_user_to_room(path, path_cb);
                            } else if(result) {
                                //TODO: debug this result to check occupant ids, if producer qbId not present then add it to room
                                console.log(result.occupants_ids, result.occupants_ids.indexOf(parseInt(path.producer.qbId)) > -1);
                                if(result.occupants_ids.indexOf(parseInt(path.producer.qbId)) > -1) {
                                    console.log('user added to room - ', result._id, result.occupants_ids, path.title, path._id);
                                    path_cb();
                                } else {
                                    add_user_to_room(result._id, path.producer.qbId, path_cb);
                                }
                            } else {
                                console.log('unknown error while changing room - ', err, result, path._id, path.qbId);
                                create_and_add_user_to_room(path, path_cb);
                            }
                        });
                    } else {
                        console.log('path producer qbId not found - ', path._id);
                        path_cb();
                    }
                } else {
                    console.log('path qbId not found - ', path._id);
                    create_and_add_user_to_room(path, path_cb);
                }
            }, function(done){
                console.log('finished paths');
                cb();
            });
        })
    }
], function(done){
    console.log('finished updating all results');
});