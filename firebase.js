var firebase = require('firebase');
var config = require('./config');
var serviceAccount = require("./serviceAccountKey.json");


var firebaseConfig = {
  apiKey: config.firebase.apiKey,
  authDomain: config.firebase.authDomain,
  databaseURL: config.firebase.databaseURL,
  storageBucket: config.firebase.storageBucket,
  serviceAccount: serviceAccount
};

var FbApp = firebase.initializeApp(firebaseConfig);
console.log('initializing firebase');
module.exports = FbApp.database();
