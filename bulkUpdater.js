var config = require('./config.js');
var oio = require('orchestrate');
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);
var customUtils = require('./utils');
var request = require('request')


var bulkUpdate = function (offset, collection, jsonToMerge) {
    db.newSearchBuilder()
        .collection(collection)
        .limit(100)
        .offset(offset)
        .query('@path.kind:item')
        .then(function (theResults) {
            theResults.body.results.forEach(function (theResult) {
                //jsonToMerge = {title: customUtils.generateToken(2)}
                //theResult = theResult.value;
                //do your operations here
                console.log(theResult.path.key)
                //if(theResult.path.key == "0e45c2dcbc60cdd9") {
                db.merge(collection, theResult.path.key, jsonToMerge)
                    .then(function (result) {
                        console.log("merged")
                    })
                    .fail(function (err) {
                        console.log("merged")
                    })
                //}
            });

            offset += 100;
            var remainingResults = theResults.body.total_count - offset;
            if (remainingResults > 0) {
                bulkUpdate(offset, collection, jsonToMerge);
            }
        })
        .fail(function (err) {
            console.log("Couldn't get the users for caching, you are doomed");
        });
};

var bulkUpdatePathPositions = function (offset) {
    db.newSearchBuilder()
        .collection("paths")
        .limit(100)
        .offset(offset)
        .query('@path.kind:item')
        .then(function (theResults) {
            theResults.body.results.forEach(function (theResult) {
                //jsonToMerge = {title: customUtils.generateToken(2)}
                //theResult = theResult.value;
                //do your operations here
                //console.log("---before -------------------------------------------------------")
                var fullConceptData = theResult.value.concepts
                //console.log(fullConceptData[0].objects)

                var conceptPosition = 0
                fullConceptData.forEach(function (oneConcept) {
                    oneConcept["pos"] = conceptPosition
                    conceptPosition += 1000

                    var objectPosition = 1000
                    var fullObjectData = oneConcept.objects
                    fullObjectData.forEach(function (oneObject) {
                        oneObject["pos"] = objectPosition
                        objectPosition += 1000
                    })

                    //console.log(oneConcept.objects)
                })

                //console.log("---after --------------------------------------------------------")
                //console.log(fullConceptData[0].objects)

                db.merge("paths", theResult.path.key, fullConceptData)
                    .then(function (result) {
                        console.log("merged")
                    })
                    .fail(function (err) {
                        console.log("failed merged")
                    })
            });

            offset += 100;
            var remainingResults = theResults.body.total_count - offset;
            if (remainingResults > 0) {
                bulkUpdatePathPositions(offset);
            }
        })
        .fail(function (err) {
            console.log("Couldn't get the users for caching, you are doomed");
        });
};

var bulkUpdatePathIsPrivate = function (offset) {
    db.newSearchBuilder()
        .collection("paths")
        .limit(100)
        .offset(offset)
        .query('@path.kind:item')
        .then(function (theResults) {
            theResults.body.results.forEach(function (theResult) {
                console.log(theResult.value.isPrivate)

                var newValue;
                switch (theResult.value.isPrivate) {
                    case "true":
                        console.log("quoted true")
                        newValue = true;
                    case "false":
                        console.log("quoted false")
                        newValue = false;
                    case true:
                        console.log("boolean true")
                        newValue = true;
                    case false:
                        console.log("boolean false")
                        newValue = false;
                    default :
                        console.log("unknown")
                        console.log(theResult.value.isPrivate)
                }
                //db.merge("paths", theResult.path.key, {concepts: fullConceptData})
                //    .then(function (result) {
                //        console.log("merged")
                //    })
                //    .fail(function (err) {
                //        console.log("failed merged")
                //    })
            });

            //offset += 100;
            //var remainingResults = theResults.body.total_count - offset;
            //if (remainingResults > 0) {
            //    bulkUpdatePathPositions(offset);
            //}
        })
        .fail(function (err) {
            console.log("Couldn't get the users for caching, you are doomed");
        });
};

var bulkUpdateLinkPreview = function (offset) {
    db.newSearchBuilder()
        .collection("paths")
        .limit(100)
        .offset(offset)
        .query('@path.kind:item')
        .then(function (theResults) {
            theResults.body.results.forEach(function (theResult) {
                var fullConceptData = theResult.value.concepts

                fullConceptData.forEach(function (oneConcept) {
                    var fullObjectData = oneConcept.objects
                    fullObjectData.forEach(function (oneObject) {
                        if (oneObject.type == "link") {
                            //if (!oneObject["image"]) {
                            //    //var theRequest = "http://collex.io/c/get_site_content/?url=" + encodeURIComponent(oneObject.url)
                            //    //request(theRequest, function (err, response, body) {
                            //    //    var response = JSON.parse(body)
                            //    //    console.log(response)
                            //    //})
                            //
                            //    oneObject["image"] = "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/ic_link_black_48dp.png";
                            //    console.log(oneObject)
                            //}
                            if (oneObject["image"] == "https://s3-ap-southeast-1.amazonaws.com/pyoopil-files/ic_link_black_48dp.png") {
                                //var theRequest = "http://collex.io/c/get_site_content/?url=" + encodeURIComponent(oneObject.url)
                                //request(theRequest, function (err, response, body) {
                                //    var response = JSON.parse(body)
                                //    console.log(response)
                                //})

                                oneObject["image"] = null;
                            }
                        }
                    })
                })

                db.merge("paths", theResult.path.key, {concepts: fullConceptData})
                    .then(function (result) {
                        console.log("merged")
                    })
                    .fail(function (err) {
                        console.log("failed merged")
                    })
            });

            offset += 100;
            var remainingResults = theResults.body.total_count - offset;
            if (remainingResults > 0) {
                bulkUpdateLinkPreview(offset);
            }
        })
        .fail(function (err) {
            console.log("Couldn't get the users for caching, you are doomed");
        });
};

var bulkUpdatePathCategories = function (offset) {
    db.newSearchBuilder()
        .collection("paths")
        .limit(100)
        .offset(offset)
        .query('@path.kind:item')
        .then(function (theResults) {
            theResults.body.results.forEach(function (theResult) {

                var jsonToMerge = {}
                switch (theResult.path.key) {
                    case "0c28b2330f60f74e":
                        jsonToMerge = {
                            category: "Business : Entrepreneurship",
                            mentorAvailable: false
                        };
                        break;
                    case "0c682eca0e6045c0":
                        jsonToMerge = {
                            category: "Others : Others",
                            mentorAvailable: false
                        };
                        break;
                    //case "0c23512c0e60abfd":
                    //    jsonToMerge = {
                    //        category: "Development : Game"
                    //    };
                    //    break;
                    //case "0c23f45f3860abfe":
                    //    jsonToMerge = {
                    //        category: "Development : Game"
                    //    };
                    //    break;
                    //case "0c80ef112060f529":
                    //    jsonToMerge = {
                    //        category: "Development : Android"
                    //    };
                    //    break;
                    //case "0c716fd0916045d3":
                    //    jsonToMerge = {
                    //        category: "Development : Android"
                    //    };
                    //    break;
                    //case "0c8060bd2d6045df":
                    //    jsonToMerge = {
                    //        category: "Development : Android"
                    //    };
                    //    break;
                    //case "0c7fda1e9d6045de":
                    //    jsonToMerge = {
                    //        category: "Development : Android"
                    //    };
                    //    break;
                    //case "0daf18a0dd6049bc":
                    //    jsonToMerge = {
                    //        category: "Gaming : Starcraft"
                    //    };
                    //    break;
                    //case "0db02b8de460db8a":
                    //    jsonToMerge = {
                    //        category: "Gaming : Starcraft"
                    //    };
                    //    break;
                    //case "0db057184a6049be":
                    //    jsonToMerge = {
                    //        category: "Gaming : Starcraft"
                    //    };
                    //    break;
                    //case "0c682eca0e6045c0":
                    //    jsonToMerge = {
                    //        category: "Business : Entrepreneurship",
                    //        mentorAvailable: false,
                    //        isPrivate: false
                    //    };
                    //    break;
                    //default :
                    //    jsonToMerge = {
                    //        category: "Others : Others"
                    //    };
                    //category = "Others : Others";
                }
                var category;
                //console.log(theResult.path.key)
                //if(category) {
                //    console.log(category)
                //}

                //if (jsonToMerge != {}) {
                db.merge("paths", theResult.path.key, jsonToMerge)
                    .then(function (result) {
                        console.log("merged")
                    })
                    .fail(function (err) {
                        console.log("failed merged")
                    })
                //}

            });

            //offset += 100;
            //var remainingResults = theResults.body.total_count - offset;
            //if (remainingResults > 0) {
            //    bulkUpdatePathPositions(offset);
            //}
        })
        .fail(function (err) {
            console.log("Couldn't get the users for caching, you are doomed");
        });
};

//bulkUpdatePathCategories(0)

//bulkUpdate(0, "paths", {
//    mentorAvailable: true,
//    mentorPaid: false,
//    coursePaid: false
//});
//bulkUpdatePathIsPrivate(0)

//db.search('payments', '*', {
//    sort: 'value.created:asc'
//})
//    .then(function (result) {
//        result.body.results.forEach(function (payment) {
//            console.log(payment.value.created)
//        })
//    })
bulkUpdateLinkPreview(0)