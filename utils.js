var config = require('./config.js');
var kew = require('kew');

var jsSHA = require('jssha');
var btoa = require('btoa');

var crypto = require('crypto');
var request = require('request')
var randomString = require('random-string');

var request = require('request')

var fs = require('fs'),
    S3FS = require('s3fs'),
    s3fsImpl = new S3FS(config.s3.bucket, {
        accessKeyId: config.s3.access,
        secretAccessKey: config.s3.secret
    });

var bunyan = require('bunyan');
var paymentlog = bunyan.createLogger({
    name: 'razorpayPayments',
    streams: [
        {
            path: __dirname + '/payments.log'  // log ERROR and above to a file
        }
    ]
});

/**
 * Gets information from the given URL
 * @param  {string} url
 * @return {json}
 */
function getLinkInfo(url, callback) {
    var MetaInspector = require('minimal-metainspector');
    var client = new MetaInspector(url, {});

    if (url != undefined) {
        client.on("fetch", function () {
            var info = {
                "title": client.title,
                "desc": client.description,
                "image": client.image
            };
            callback(info);
        });

        client.on("error", function (err) {
            callback(err);
        });

        client.fetch();
    } else {
        callback(undefined);
    }
}

/**
 * Gets information from the given URL
 * @param  {string} url
 * @return {json}
 */
function getLinkInfo2(url, callback) {
    var MetaInspector = require('minimal-metainspector');
    var client = new MetaInspector(url, {});

    if (url != undefined) {
        client.on("fetch", function () {
            var info = {
                "title": client.title,
                "desc": client.description,
                "image": client.image
            };
            callback(null, info);
        });

        client.on("error", function (err) {
            callback(err, null);
        });

        client.fetch();
    } else {
        callback(undefined);
    }
}

/**
 * Uploads file to S3
 * @param {file} file
 * @param {function} callback
 */
function upload(file, callback) {
    if (file != undefined) {
        var stream = fs.createReadStream(file.path);
        var randomString = generateToken(3)
        var originalname = file.originalname.replace(/[^\w.]/g, '_')
        var extension = originalname.match(/(\.[^.]+$)/)[0]
        var fileNameOnly = originalname.replace(/(\.[^.]+$)/, '')
        var filename = fileNameOnly + '_' + randomString + extension
        var thumb_filename = fileNameOnly + '_' + randomString + '.png'
        console.log("possible limbo up ahead:")
        s3fsImpl.writeFile(filename, stream).then(function (data) {
            console.log("s3fsImpl.writeFile")
            console.log(data)
            fs.unlink(file.path, function (err) {
                if (err) {
                    callback(err);
                }
                var info = {
                    url: "https://s3.amazonaws.com/" + config.s3.bucket + "/" + filename,
                    urlThumb: "https://s3.amazonaws.com/" + config.s3.bucket + "resized/resized-" + thumb_filename
                }
                callback(info);
            });
        });
    } else {
        callback(undefined);
    }
}

/**
 * Uploads file to S3 (promises version)
 * @param {file} file
 * @param {function} callback
 */
function uploadToS3(file) {
    var uploadedFile = kew.defer()
    try {
        if (file != undefined) {
            var stream = fs.createReadStream(file.path);
            var randomString = generateToken(3)
            var originalname = file.originalname.replace(/[^\w.]/g, '_')
            var extension = originalname.match(/(\.[^.]+$)/)[0]
            var fileNameOnly = originalname.replace(/(\.[^.]+$)/, '')
            var filename = fileNameOnly + '_' + randomString + extension
            var thumb_filename = fileNameOnly + '_' + randomString + '.png'
            s3fsImpl.writeFile(filename, stream)
                .then(function (data) {
                    //dont want to delete
                    //fs.unlink(file.path, function (err) {
                    //    if (err) {
                    //        callback(err);
                    //uploadedFile.reject(err)
                    //}
                    var info = {
                        url: "https://s3.amazonaws.com/" + config.s3.bucket + "/" + filename,
                        urlThumb: "https://s3.amazonaws.com/" + config.s3.bucket + "resized/resized-" + thumb_filename
                    }
                    uploadedFile.resolve(info);
                    //});
                })
            //uses the damn event system, does not have a fail :/
            //.fail(function (err) {
            //    uploadedFile.reject(err)
            //})
        } else {
            uploadedFile.reject({body: {message: "file is not defined"}});
        }
    } catch (exception) {
        uploadedFile.reject(exception)
    }
    return uploadedFile
}

/**
 * Capture a razorpay payment for authorization
 * @param paymentId the razorpay generated payment Id
 * @param amount authorized amount in paise
 */
var captureRazorPayment = function (paymentId, amount) {
    //example razorpay URL (with auth)
    //https://rzp_test_Ad93zqidD9eZwy:69b2e24411e384f91213f22a@api.razorpay.com/v1/payments
    // Prod Settings
    var baseURL = "https://" + config.razorpay.key + ":" + config.razorpay.secret + "@api.razorpay.com/v1/payments/" + paymentId + "/capture"
    // Dev settings
//    var baseURL = "https://" + config['razorpay-test'].key + ":" + config['razorpay-test'].secret + "@api.razorpay.com/v1/payments/" + paymentId + "/capture";
    console.log('trying to capture payment - ', baseURL);
    //https://www.npmjs.com/package/request
    var form = {
        amount: parseInt(amount)
    };

    return request.post({url: baseURL, form: form}, function (err, httpResponse, body) {
        if (!body || err) {
            console.log("payment capture failed");
            paymentlog.error({body: err}, "Razorpay payment capture failed");
        } else {
            console.log("payment succeeded");
            paymentlog.info({body: body}, "Razorpay payment capture succeeded")
        }
    });
};


/**
 * Generate an access token for login
 * refer http://stackoverflow.com/questions/8855687/secure-random-token-in-node-js
 * can update to base64 if needed
 * @returns {string|*} access token
 */
function generateToken(length) {
    if (length)
        return crypto.randomBytes(length).toString('hex');
    else
        return crypto.randomBytes(16).toString('hex');
}


/**
 * Generate unique key for concept and conceptObjects
 * @returns {string|*} key
 */
function randomizer() {
    return randomString({
        length: 16,
        numeric: true,
        letters: true,
        special: false
    });
}


function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function generateVidyoToken(key, appID, userName, expiresInSeconds, vCard) {
    var EPOCH_SECONDS = 62167219200;
    var expires = Math.floor(Date.now() / 1000) + expiresInSeconds + EPOCH_SECONDS;
    var shaObj = new jsSHA("SHA-384", "TEXT");
    shaObj.setHMACKey(key, "TEXT");
    jid = userName + '@' + appID;
    var body = 'provision' + '\x00' + jid + '\x00' + expires + '\x00' + vCard;
    shaObj.update(body);
    var mac = shaObj.getHMAC("HEX");
    var serialized = body + '\0' + mac;
    return (btoa(serialized));
}

exports.getLinkInfo = getLinkInfo;
exports.getLinkInfo2 = getLinkInfo2;
exports.captureRazorPayment = captureRazorPayment;
exports.upload = upload;
exports.uploadToS3 = uploadToS3;
exports.generateToken = generateToken;
exports.randomizer = randomizer;
exports.toTitleCase = toTitleCase;
exports.generateVidyoToken = generateVidyoToken;
