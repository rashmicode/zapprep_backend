var oio = require('orchestrate');
var config = require('../config')
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);
var constants = require('../constants')
var dbUtils = require('../dbUtils')
var kew = require('kew')
var qbchat = require('../qbchat.js');
var async = require('async');
customUtils = require('../utils.js');

/**
 * TODO list:
 * TODO inject allowEnroll based on user
 * TODO n API calls for n subcategories, can we construct only 1 query and return 8 paths per subcategory?
 * TODO the user's paths promise returns 100 (orchestrate max)
 */

/**
 * return the courses of a particular category
 * @param category
 * @returns {!Promise.<!Array>}
 */
function getPathsOfCategory(category, limit, page, requesterId) {
    var offset = (limit * (page - 1))
    var response = kew.defer()
    var pathArrays = []
    var subCats = getSubCategories(category)

    if (subCats.length == 0) {
        //this path has no subcategory!
        //so lets just dump all the data of that path
        getPathsOfCategoryPromise(category, limit, page)
            .then(function (injectedResults) {
                console.log("injected")
                /**
                 * this is the response required:
                 * more for pagination,
                 * hasSubCat indicates there are no subcategories
                 * and data gives the paths
                 */
                response.resolve({
                    more: dbUtils.checkHasMore(injectedResults),
                    hasSubCat: false,
                    data: dbUtils.injectId(conceptRemover(injectedResults))
                })
            })
            .fail(function (err) {
                response.reject({
                    errors: [err.body.message],
                    errorObj: err
                })
            })
    } else {
        //wow a path with subcategories!
        //get a limited set of items for all subcategories
        //the app design looks like this
        var promises = []
        subCats.forEach(function (subCat) {
            promises.push(getPathsOfSubCategoryPromise(category, subCat, constants.marketplace.max_horizontal_tiles, 1))
        })
        kew.all(promises)
            .then(function (resultsArray) {
                //console.log("promises resolved")
                //console.log(resultsArray)
                resultsArray.forEach(function (oneResultArray) {
                    var oneResultArray = conceptRemover(oneResultArray)
                    pathArrays = pathArrays.concat(dbUtils.injectId(oneResultArray))
                })
                /**
                 * this is the response required:
                 * no pagination
                 * order gives the order of getSubCategories
                 * and data gives the paths
                 */
                response.resolve({
                    hasSubCat: true,
                    data: pathArrays,
                    order: subCats
                })
            })
            .fail(function (err) {
                response.reject({
                    errors: [err.body.message],
                    errorObj: err
                })
            })
    }
    return response
}

function getPathsOfSubCategory(category, subCategory, limit, page, requesterUserId) {
    var response = kew.defer()
    getPathsOfSubCategoryPromise(category, subCategory, limit, page)
        //.then(function (results) {
        //    return injectAllowEnroll(results, requesterUserId)
        //})
        .then(function (enrollInjectedResults) {
            response.resolve({
                more: dbUtils.checkHasMore(enrollInjectedResults),
                data: dbUtils.injectId(enrollInjectedResults)
            })
        })
        .fail(function (err) {
            response.reject({
                errors: [err.body.message],
                errObj: err
            })
        })
    return response
}

function getLatestPathsPromise(limit, page) {
    var queries = []
    var offset = (limit * (page - 1))
    queries.push(createIsPrivateQuery())
    var finalQuery = dbUtils.andQueryJoiner(queries)

    return db.newSearchBuilder()
        .collection("paths")
        .limit(limit)
        .offset(offset)
        .sortBy('value.created:desc')
        .query(finalQuery)
}

function getPopularPathsPromise(limit, page) {
    var queries = []
    var offset = (limit * (page - 1))
    queries.push(createIsPrivateQuery())
    var finalQuery = dbUtils.andQueryJoiner(queries)

    return db.newSearchBuilder()
        .collection("paths")
        .limit(limit)
        .offset(offset)
        .sortBy('value.studentCount:desc')
        .query(finalQuery)
}


function getPathIdByAccessCode(accessCode) {
    console.log(accessCode)
    var pathId = kew.defer()
    var query = 'value.accessId:`' + accessCode + "`"
    db.newSearchBuilder()
        .collection("paths")
        .limit(1)
        .offset(0)
        .withFields('value.accessId')
        //.sort('location', 'distance:asc')
        .query(query)
        .then(function (results) {
            console.log(results.body.count)
            if (results.body.count == 1) {
                var path = dbUtils.injectId(results)
                pathId.resolve(path[0].id)
            } else {
                pathId.reject({
                    body: {
                        message: "Invalid Access Code"
                    }
                })
            }
        })
        .fail(function (err) {
            pathId.reject(err)
        })
    return pathId
}

function getPathIdByPromoCode(promoCode) {
    var pathId = kew.defer()
}

function getHomePaths() {
    var response = kew.defer()

    kew.all(
        [
            getLatestPathsPromise(constants.marketplace.max_horizontal_tiles, 1),
            getPathsOfCategoryPromise("Business", constants.marketplace.max_horizontal_tiles, 1),
            getPathsOfCategoryPromise("Computer Science", constants.marketplace.max_horizontal_tiles, 1),
            getPopularPathsPromise(constants.marketplace.max_horizontal_tiles, 1)
        ]
    )
        .then(function (resultsArray) {
            resultsArray[0] = injectHomeCategory(resultsArray[0], "Latest")
            resultsArray[1] = injectHomeCategory(resultsArray[1], "Business")
            resultsArray[2] = injectHomeCategory(resultsArray[2], "Computer Science")
            resultsArray[3] = injectHomeCategory(resultsArray[3], "Popular")

            resultsArray = resultsArray.map(function (oneResultArray) {
                return conceptRemover(oneResultArray)
            })
            var pathArrays = []
            resultsArray.forEach(function (oneResultArray) {
                pathArrays = pathArrays.concat(dbUtils.injectId(oneResultArray))
            })
            var responseObj = {
                categories: getAllCategories(),
                data: pathArrays,
                order: [
                    "Latest",
                    "Popular",
                    "Computer Science",
                    "Business",
                ],
                tags: [
                    "Latest",
                    "Popular"
                ]
            }
            response.resolve(responseObj)
        })
        .then(function (err) {
            response.reject(err)
        })

    return response
}

function injectHomeCategory(results, customCategory) {
    results.body.results = results.body.results.map(function (result) {
        result.value.homeCat = customCategory
        return result
    })
    return results
}

/**
 * get all sub categories of a particular category
 * @param category
 * @returns {*}
 */
function getSubCategories(category) {
    var subs = constants.paths.categories[category].sub
    if (subs)
        return subs
    else
        return []
}

/**
 * get all categories of courses
 * @returns {*}
 */
function getAllCategories() {
    var cats = Object.keys(constants.paths.categories);
    var catsNthumb = cats.map(function (cat) {
        var obj = {
            cat: cat,
            thumb: constants.paths.categories[cat].thumbnail
        }
        return obj
    })
    return catsNthumb
}

function createCategoryQuery(category) {
    return dbUtils.createFieldQuery('cat', category)
}

function createSubCatQuery(subCategory) {
    return dbUtils.createFieldQuery('subCat', subCategory)
}

function createIsPrivateQuery() {
    return "value.isPrivate:false"
}

function getPathPromise(pathId) {
    var finalQuery = dbUtils.createSearchByIdQuery(pathId)
    return db.newSearchBuilder()
        .collection("paths")
        //.sort('location', 'distance:asc')
        .query(finalQuery)
}


function getPathsOfSubCategoryPromise(category, subCategory, limit, page) {
    var response = kew.defer()
    var offset = (limit * (page - 1))
    var queries = []
    queries.push(createCategoryQuery(category))
    queries.push(createSubCatQuery(subCategory))
    queries.push(createIsPrivateQuery())
    var finalQuery = dbUtils.andQueryJoiner(queries)

    db.newSearchBuilder()
        .collection("paths")
        .limit(limit)
        .offset(offset)
        .sortBy('value.studentCount:desc')
        //.sort('location', 'distance:asc')
        .query(finalQuery)
        .then(function (results) {
            response.resolve(results)
        })
        .fail(function (err) {
            response.reject({
                errors: [err.body.message],
                errObj: err
            })
        })

    return response
}

function getPathsOfCategoryPromise(category, limit, page) {
    var queries = []
    var offset = (limit * (page - 1))
    queries.push(createIsPrivateQuery())
    queries.push(createCategoryQuery(category))
    var finalQuery = dbUtils.andQueryJoiner(queries)

    return db.newSearchBuilder()
        .collection("paths")
        .limit(limit)
        .offset(offset)
        .sortBy('value.studentCount:desc')
        //.sort('location', 'distance:asc')
        .query(finalQuery)
}

/**
 * takes bunch of pathResults
 * (raw from orchestrate)
 * and injects the flag allowEnroll = true or false
 * depending if the user is already enrolled into the course
 * @param results
 */
function injectAllowEnroll(results, userId) {
    var injectedResults = kew.defer()
    getUsersPathIds(userId, config.pagination.limit, 1)
        .then(function (pathIds) {
            results.body.results = results.body.results.map(function (result) {
                if (pathIds.indexOf(result.path.key) > -1)
                    result.value.allowEnroll = false
                else
                    result.value.allowEnroll = true
                return result
            })
            injectedResults.resolve(results)
        })
        .fail(function (err) {
            injectedResults.reject(err)
        })
    return injectedResults
}


/**
 * gets the courses the user is enrolled in
 * @param userId
 */
function getUsersPathsPromise(userId, limit, page) {
    var offset = (limit * (page - 1))
    return db.newGraphReader()
        .get()
        .limit(limit)
        .offset(offset)
        .from('users', userId)
        .related('related')
}

function getUsersPathIds(userId, limit, page) {
    var pathIds = kew.defer()
    getUsersPathsPromise(userId, limit, page)
        .then(function (results) {
            var thePathIds = results.body.results.map(function (path) {
                return path.path.key
            })
            pathIds.resolve(thePathIds)
        })
        .fail(function (err) {
            pathIds.reject(err)
        })
    return pathIds
}

function checkIfAlreadyEnrolled(userId, pathId) {
    var thePromise = kew.defer()
    var query = dbUtils.createGetOneOnOneGraphRelationQuery('users', userId, 'related', 'paths', pathId)
    db.newSearchBuilder()
        .collection('users')
        .query(query)
        .then(function (result) {
            if (result.body.count == 1) {
                thePromise.resolve(true)
            } else {
                thePromise.resolve(false)
            }
        })
        .fail(function (err) {
            thePromise.reject(err)
        })
    return thePromise
}

/**
 * TODO : orchestrate has released partial field queries
 * maybe you can use that
 *
 * raw concept remover
 */
function conceptRemover(results) {
    results.body.results = results.body.results.map(function (result) {
        result.value.concepts = [result.value.concepts[0]]
        return result
    })
    return results
}

function getPathIdByDiscountCode(accessCode) {
    console.log(accessCode)
    var thePath = kew.defer()
    var thePathDetails = constants.paths.discountCodes[accessCode]
    console.log(thePathDetails)
    if (thePathDetails) {
        thePath.resolve(thePathDetails)
    } else {
        //thePath.reject({body: {message: "Invalid promo-code"}})
        thePath.reject({body: {message: "Invalid promo-code"}})
    }
    return thePath
}

function getAutoSuggestList(searchKey) {
    function createFuzzyQuery(field, value) {
        var theFieldQuery = "value." + field + ":" + value + "*"
        return theFieldQuery
    }

    var queries = []
    queries.push(createFuzzyQuery('title', searchKey))
    queries.push(createFuzzyQuery('producer.name', searchKey))
    queries.push(createFuzzyQuery('concepts.title', searchKey))
    var finalQuery = dbUtils.orQueryJoiner(queries)
}

function enrollIntoCourse(userId, pathId, userQbId, paymentId, amount) {
    var rooms = []
    var enrolled = kew.defer()
    checkIfAlreadyEnrolled(userId, pathId)
        .then(function (isEnrolled) {
            console.log("checking is enrolled")
            console.log(isEnrolled)
            if (isEnrolled) {
                console.log("enrolled, time to reject")
                return kew.reject({body: {message: "You are already a part of this course"}})
            }
            else {
                console.log("not enrolled, time to get in!")
                var consumes = db.newGraphBuilder()
                    .create()
                    .from('users', userId)
                    .related('consumes')
                    .to('paths', pathId);

                var isConsumed = db.newGraphBuilder()
                    .create()
                    .from('paths', pathId)
                    .related('isConsumed')
                    .to('users', userId);

                var getPath = db.get('paths', pathId)

                return kew.all([getPath, consumes, isConsumed])
            }
        })
        .then(function (content) {
            console.log("kew.all baby!")
            content[0].body.concepts.forEach(function (concept) {
                rooms.push(concept["qbId"])
            });
            if (content[0].body.qbId)
                rooms.push(content[0].body.qbId)
            if (content[0].body.mentorRoomId)
                rooms.push(content[0].body.mentorRoomId)
            console.log("roooms -->")
            console.log(rooms)
            async.each(rooms, function (room, callback) {
                console.log("lets do this")
                qbchat.addUserToRoom(room, [userQbId], function (err, result) {
                    console.log("iterating")
                    //if (err)
                    ////callback(err);
                    //    ; //ignore if joining a room fails
                    //else
                    callback();
                })
            }, function (err) {
                console.log("async done")
                if (err) {
                    //this wont be called
                    //responseObj["errors"] = ["Enrolling was unsuccessful"];
                    //console.log(err)
                    //res.status(503);
                    //res.json(responseObj);
                } else {
                    console.log("time to patch it up")
                    db.newPatchBuilder("paths", pathId)
                        .inc("studentCount")
                        .inc("newStudents")
                        .apply()
                        .then(function (res1) {
                            return res1
                        })
                        .then(function (res2) {
                            return db.newGraphBuilder()
                                .create()
                                .from('users', userId)
                                .related('related')
                                .to('paths', pathId)
                        })
                        .then(function (res3) {
                            enrolled.resolve({"success": "You have been successfully enrolled"})
                            //responseObj["data"] = {"success": "You have been successfully enrolled"};
                            if (paymentId && amount) {
                                customUtils.captureRazorPayment(req.body.paymentId, req.body.amount);
                            }
                            //res.status(200);
                            //res.json(responseObj);
                        })
                        .fail(function (err) {
                            enrolled.reject({body: {message: "Unable to enroll into course", obj: err}})
                        })
                }
            });
        })
        .fail(function (err) {
            console.log("rejected bro")
            enrolled.reject({body: {message: "Unable to enroll into course", obj: err}})
        })
    return enrolled
}

function getAutoSuggestList(query) {
    var theAutoSuggestList = kew.defer()

    //http://stackoverflow.com/questions/4374822/javascript-regexp-remove-all-special-characters#
    //remove all special characters
    query = query.replace(/[^\w\s]/gi, '')
    console.log("sanitized query : " + query)

    //if the query has a space, escape it, because lucene behaves weirdly!
    //https://dismantledtech.wordpress.com/2011/05/15/handling-spaces-in-lucene-wild-card-searches/#comment-229
    /**
     * Excerpt :
     * I tried to get around the issue using quotes, but to no avail –
     * a search term like ‘”SFP YGF”*’ isn’t parsed in the way that you’d expect,
     * and doesn’t produce the desired effect.
     * Adding a backslash makes the query parser interpret
     * the space character as being part of the search term,
     * and so a search for something like ‘SFP\ YGF\:\ FGY’ will return
     * everything that beings with the string “SFP YGP: FGY”.
     */

    var spaceEscapedQuery = query.replace(/\s/g, '\\ ')
    console.log("space escaped query")
    console.log(spaceEscapedQuery)
    var responseObj = {}
    var totalCount = 0

    //var finalQuery = "((value.isPrivate:false) AND (value.title:\"" + spaceEscapedQuery + "*\" OR value.producer.name:\"" + spaceEscapedQuery + "*\" OR value.concepts.title:\"" + spaceEscapedQuery + "*\"))"
    var finalQuery = "((value.isPrivate:false) AND (value.title:" + spaceEscapedQuery + "* OR value.producer.name:" + spaceEscapedQuery + "* OR value.concepts.title:" + spaceEscapedQuery + "*))"

    db.newSearchBuilder()
        .collection("paths")
        .withFields('value.title', 'value.concepts.title', 'value.producer.name')
        .limit(100)
        .offset(0)
        //.sort('location', 'distance:asc')
        .query(finalQuery)
        .then(function (results) {
            var autoSuggest = []
            var suggestArrays = {
                producers : [],
                concepts : [],
                courses : []
            }

            function pushIfUnique(item, type) {
                var pushProducer = !(suggestArrays.producers.indexOf(item) > -1)
                var pushConcept = !(suggestArrays.concepts.indexOf(item) > -1)
                var pushCourse = !(suggestArrays.courses.indexOf(item) > -1)
                if (pushProducer && pushConcept && pushCourse) {
                    switch(type) {
                        case "producer":
                            suggestArrays.producers.push(item)
                            break
                        case "concept":
                            suggestArrays.concepts.push(item)
                            break
                        case "course":
                            suggestArrays.courses.push(item)
                            break
                    }
                }
            }

            totalCount = results.body.total_count

            var results = dbUtils.injectId(results)
            results.forEach(function (path) {
                pushIfUnique(path.producer.name, "producer")
                pushIfUnique(path.title, "course")
                path.concepts.forEach(function (concept) {
                    pushIfUnique(concept.title, "concept")
                })
            })

            autoSuggest = suggestArrays.courses.concat(suggestArrays.producers, suggestArrays.concepts)
            autoSuggest = autoSuggest.filter(function (item) {
                return item.match(new RegExp(query, 'gi'));
            })
            //console.log(autoSuggest)
            responseObj = {"total_count": autoSuggest.length, "data": autoSuggest};
            theAutoSuggestList.resolve(responseObj)
        })
        .fail(function (err) {
            responseObj = {"malformed": err, "total_count": 0, "data": []};
            theAutoSuggestList.resolve(responseObj)
        })
    return theAutoSuggestList
}

/**
 * delete a course with purge true!
 * While delete operations do not delete associated graph relations or events,
 * delete operations with the `purge` parameter will delete all
 * associated events and any relations directed from that key.
 * @param pathId
 * @returns {!Promise}
 */
function deletePath(pathId) {
    var purgeCollection = true
    var channels = []
    var deletedPathStatus = kew.defer()
    var getChatSession = kew.defer()
    qbchat.getSession(function (err, session) {
        if (err) {
            console.log("Recreating session");
            qbchat.createSession(function (err, result) {
                if (err) getChatSession.reject(err)
                else getChatSession.resolve(result)
            })
        } else {
            getChatSession.resolve(session)
        }
    })

    getChatSession
        .then(function (result) {
            return db.get('paths', pathId)
        })
        .then(function (path) {
            path.body.concepts.forEach(function (concept) {
                //this handles the case of courses that don't have concept channels
                if (concept.qbId) channels.push(concept.qbId);
            })
            //no ifs here, because all courses, minimum have these channels always
            //if they dont its an anomaly
            channels.push(path.body.qbId);
            channels.push(path.body.mentorRoomId);

            //hua toh hua
            channels.forEach(function (channel) {
                qbchat.deleteRoom(channel, function (err, res) {
                    if (err) console.log(err)
                })
            })
            return db.remove("paths", pathId, purgeCollection)
        })
        .then(function (result) {
            deletedPathStatus.resolve({"success": "Path Deleted"});
        })
        .fail(function (err) {
            deletedPathStatus.reject(err)
        })
    return deletedPathStatus
}

module.exports = {
    //getAllCategories: getAllCategories,
    //getSubCategories: getSubCategories,
    getPathsOfCategory: getPathsOfCategory,
    getPathsOfSubCategory: getPathsOfSubCategory,
    getHomePaths: getHomePaths,
    getPathIdByDiscountCode: getPathIdByDiscountCode,
    getPathIdByAccessCode: getPathIdByAccessCode,
    enrollIntoCourse: enrollIntoCourse,
    getAutoSuggestList: getAutoSuggestList,
    deletePath: deletePath
}