var oio = require('orchestrate');
var config = require('../config')
oio.ApiEndPoint = config.db.region;
var db = oio(config.db.key);
var constants = require('../constants')
var dbUtils = require('../dbUtils')
var kew = require('kew')
var qbchat = require('../qbchat.js');
var async = require('async');
var customUtils = require('../utils.js');
var date = new Date()
var Notifications = require('../notifications');
var notify = new Notifications();
var bunyan = require('bunyan');
var log = bunyan.createLogger({
    name: 'pyoopil2chatServer',
    streams: [
        {
            path: __dirname + '/chatServer_errors.log'  // log ERROR and above to a file
        }
    ]
});

/**
 * Abstracting out the creation of a chat channel
 * @param channelName
 */
function createChatChannel(channelName) {
    var chatObj = {}
    var createRoom = kew.defer()
    qbchat.createRoom(2, channelName, function (err, newRoom) {
        if (err)
            return kew.reject(err)
        else
            return kew.resolve(newRoom)
    })

    createRoom
        .then(function (theNewRoom) {
            switch (channelName) {
                case "Introduction":
                    chatObj["id"] = date.getTime() + "@3";
                    chatObj["channelName"] = "Introduction";
                    chatObj["channelId"] = theNewRoom._id;
                    break
                case "Mentor":
                    chatObj["id"] = date.getTime() + "@2";
                    chatObj["channelName"] = "Mentor Channel";
                    chatObj["channelId"] = theNewRoom._id;
                    break
                case "General":
                    chatObj["id"] = date.getTime() + "@1";
                    chatObj["channelName"] = "General Channel";
                    chatObj["channelId"] = theNewRoom._id;
                    break
                default:

            }
            notify.emit('wordForChat', chatObj);
        })
        .fail(function (err) {

        })
}

function createIntroductionChannel() {
    return createChatChannel("Introduction")
}

function createMentorChannel() {
    return createChatChannel("Mentor")
}

function createGeneralChannel() {
    return createChatChannel("General")
}

function addAllUsersToRoom(roomQbId, pathId) {

}

/**
 *
 * @param params
 */
function getAllDialogs(username) {
    var params = {
        'login': username,
        'password': 'pyoopilDevsRock'
    }

    /**
     * Closure : get dialogs of the specified offset
     * @param limit
     * @param offset
     * @returns {!Promise}
     */
    function getDialogs(limit, offset) {
        var theRooms = kew.defer()
        QBconsumer.chat.dialog.list({limit: limit, skip: offset}, function (err, res) {
            if (err)
                theRooms.reject(err)
            else {
                //var rooms = res.items.map(function (room) {
                //    return room.xmpp_room_jid;
                //})
                theRooms.resolve(res.items)
            }
        });
        return theRooms
    }

    /**
     * Closure : get all dialogs promise list
     * @returns {!Promise}
     */
    function getAllDialogPromises() {
        var promiseListArray = []
        var promiseList = kew.defer()
        var offset = 0

        QBconsumer.chat.dialog.list({limit: 1, skip: 0}, function (err, res) {
            if (err)
                promiseList.reject(err)
            else {
                var totalCount = res.total_entries
                console.log("totalCount" + totalCount)
                var remaining = 0
                do {
                    promiseListArray.push(getDialogs(400, offset))
                    offset += 400
                    remaining = totalCount - offset;
                } while (remaining > 0)
                promiseList.resolve(promiseListArray)
            }
        });
        return promiseList
    }

    /**
     * disable require cache to get a new QB object for consumer
     * so that the sessions dont clash!
     * TODO: fine a better way to do this. perhaps create an instance of QB
     ***/
    Object.keys(require.cache).forEach(function (key) {
        //delete require.cache[key]
        if (key.indexOf("node_modules/quickblox") > -1) {
            //console.log(key)
            delete require.cache[key]
        }
    })

    var allDialogs = kew.defer()
    var QBconsumer = require('quickblox');
    QBconsumer.init(config.qb.appId, config.qb.authKey, config.qb.authSecret, false);
    QBconsumer.createSession(params, function (err, session) {
        if (err) {
            log.error({customMessage: "createSession failed for user", username: username, qbError: err})
            allDialogs.reject(err)
        } else {
            //QBconsumer.chat.dialog.list({limit: config.qb.paginationLimit, skip: 0}, function (err, res) {
            //    if (err) {
            //        log.error({customMessage: "getDialoges failed for user", username: username, qbError: err})
            //        callback(err, null)
            //    } else {
            //        callback(null, res.items)
            //    }
            //})
            getAllDialogPromises()
                .then(function (promiseArrayList) {
                    console.log('promise array resolved')
                    return kew.all(promiseArrayList)
                })
                .then(function (promiseResults) {
                    console.log("items resolved")
                    var allItemsList = []
                    promiseResults.forEach(function (item) {
                        allItemsList = allItemsList.concat(item)
                    })
                    allDialogs.resolve(allItemsList)
                })
                .fail(function (err) {
                    allDialogs.reject(err)
                })
        }
    })
    return allDialogs
}

module.exports = {
    getAllDialogs: getAllDialogs
}