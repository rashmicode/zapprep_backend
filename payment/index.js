'use strict';

var express = require('express');
var controller = require('./payment.controller');
var config = require('../config');
var passport = require('passport');
var qbchat = require('../qbchat');
var multer = require('multer');
var router = express.Router();


var qbchat_session = function(req, res, next) {
  debugger;
  qbchat.getSession(function (err, session) {
    if (err) {
      debugger;
      console.log("Recreating session");
      qbchat.createSession(function (err, result) {
        debugger;
        if (err) {
          res.status(503);
          res.json({"errors": ["Can't connect to the chat server, try again later"]});
        } else next();
      })
    } else next();
  })
};

router.post('/', passport.authenticate('bearer', {session: false}), controller.postPayment);
router.get('/', passport.authenticate('bearer', {session: false}), controller.getPayment);
router.get('/mentor', passport.authenticate('bearer', {session: false}), controller.getMentorPayment);

module.exports = router;
