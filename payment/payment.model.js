'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PaymentSchema = new Schema({
  path: {
    type: Schema.ObjectId,
    ref: 'Path'
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  amount: Number,
  paymentId: String,
  currency: String,
  product: String,
  discountCode: String,
  created: Date
});
PaymentSchema.virtual('id').get(function() {
    return this._id;
});
module.exports = mongoose.model('Payment', PaymentSchema);
var deepPopulate = require('mongoose-deep-populate')(mongoose);

PaymentSchema.plugin(deepPopulate, {
  whitelist: ['path'],
  populate: {
    path: {
      select: 'title desc category subCat coverPhoto coverPhotoThumb producer created'
    }
  }
});

