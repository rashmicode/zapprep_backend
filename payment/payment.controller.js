var validator = require('validator');
var config = require('../config.js');
var customUtils = require('../utils.js');
var _ = require('lodash');

var Payment = require('./payment.model');
var Path = require('./../path/path.model');
var User = require('./../user/user.model');
var Enrollment = require('./../enrollment/enroll.model');

exports.postPayment = function (req, res) {
    var reqBody = req.body;
    var responseObj = {};
    var errors = new Array();

    if (validator.isNull(reqBody.paymentId)) errors.push("payment ID cannot be empty");
    if (validator.isNull(reqBody.pathId)) errors.push("Path ID cannot be empty");
    if (!validator.isIn(reqBody.currency, ['USD', 'INR'])) errors.push("Currency must be either USD or INR");
    if (!validator.isIn(reqBody.product, ['mentor', 'course'])) errors.push("product must be mentor or course");
    if (!validator.isDecimal(reqBody.amount)) errors.push("Amount is invalid");

    if (errors.length > 0) {
        responseObj["errors"] = errors;
        res.status(422);
        res.json(responseObj);
    } else {
        var payload = {
            'paymentId': reqBody.paymentId,
            'path': reqBody.pathId,
            'currency': reqBody.currency,
            'amount': parseFloat(reqBody.amount),
            'product': reqBody.product,
            'created': new Date()
        };
        if (reqBody.discountCode) payload['discountCode'] = reqBody.discountCode;
        console.log("payment payload - ", payload);
        var user_search_obj = {};
        if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
        else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
        else if (req.query._id) user_search_obj['_id'] = req.query._id;
        console.log("user search object - ", user_search_obj);
        if (user_search_obj) {
            User.findOne(user_search_obj).exec(function (err, user) {
                if (err || !user) return handleError('Error finding user details. Retry logging in again', res);
                console.log("user find result - ", err, user && user._id, '. trying path - ', reqBody.pathId);
                Path.findOne({_id: reqBody.pathId}).exec(function (err, path) {
                    if (err || !path) return handleError('Error finding path details ', res);
                    payload['user'] = user._id;
                    payload['path'] = path._id;
                    console.log("found path details - ", payload);
                    Payment.create(payload, function (err, payment) {
                        if (err) return handleError('Error creating payment', res);
                        var amountInPaise = parseFloat(payment.amount) * 100;
                        if (payment.product == "mentor") {
                            Enrollment.findOne({path: path._id, producer: user._id}).exec(function (err, enroll) {
                                if (err) return handleError('Error finding subscription details', res);
                                console.log("payment enrollment status - ", enroll);
                                if (enroll) {
                                    var expiresOn = (new Date(enroll.expiresOn)).getTime();
                                    var currentTime = (new Date()).getTime();
                                    if (expiresOn < currentTime) {
                                        expiresOn = currentTime + config.mentorSubscription.time;
                                    } else {
                                        expiresOn = expiresOn + config.mentorSubscription.time;
                                    }
                                    enroll['expiresOn'] = new Date(expiresOn);
                                    enroll['payment_ref'] = payment._id;
                                    enroll['isEnrolled'] = true;
                                    enroll.save(function (err) {
                                        if (err) return handleError('error saving subscription details', res);
                                        var resp = {data: payment.toJSON()};
                                        resp.data['id'] = payment._id;
                                        console.log('finished payment process');
                                        customUtils.captureRazorPayment(payment.paymentId, amountInPaise);
                                        return res.status(201).json(resp);
                                    })
                                } else {
                                    var expiresOn = (new Date()).getTime() + config.mentorSubscription.time;
                                    var enrollment_data = {
                                        path: path._id,
                                        producer: user._id,
                                        expiresOn: new Date(expiresOn),
                                        isMentorPaid: true,
                                        isCoursePaid: false,
                                        payment_ref: payment._id,
                                        isEnrolled: true
                                    };
                                    console.log("enrollment create - ", enrollment_data);
                                    Enrollment.create(enrollment_data, function (err, enroll) {
                                        if (err) return handleError('Error creating subscription details.', res);
                                        var resp = {data: payment.toJSON()};
                                        resp.data['id'] = payment._id;
                                        console.log("finished creating enrollment. returning now");
                                        customUtils.captureRazorPayment(payment.paymentId, amountInPaise);
                                        return res.status(201).json(resp);
                                    });
                                }
                            })
                        } else return res.status(201).json({data: payment});
                    })
                });
            });
        } else return handleError('error finding user details. please try logging in again.', res);
    }
};

exports.getPayment = function (req, res) {
    var user_search_obj = {};
    if (req.user && req.user._id) user_search_obj['_id'] = req.user._id;
    else if (req.query.access_token) user_search_obj['access_token'] = req.query.access_token;
    else if (req.query._id) user_search_obj['_id'] = req.query._id;
    User.findOne(user_search_obj).select('username name').exec(function (err, user) {
        if (err) return handleError('Error finding user details.', res);
        if (user) {
            Payment.find({user: user._id}).deepPopulate('path').exec(function (err, payments) {
                if (err) return handleError('Error finding payment details', res);
                var resp = {data: []};
                var path_ids = [], payment_ids = [];
                payments.forEach(function (payment) {
                    var item = payment.toJSON();
                    item['id'] = payment._id;
                    item['paymentId'] = payment._id;
                    item['pathId'] = payment.path._id;
                    item['userId'] = user._id;
                    item['username'] = user.username;
                    item['name'] = user.name;
                    item['username'] = user.username;
                    item['pathTitle'] = payment.path.title;
                    path_ids.push(payment.path._id);
                    payment_ids.push(payment._id);
                    delete item.path;
                    resp.data.push(item);
                });
                Enrollment.find({path: {$in: path_ids}, producer: user._id, payment_ref: {$in: payment_ids}}).select('expiresOn payment_ref').exec(function (err, enrolls) {
                    if (err) return handleError('Error finding enrollment details', res);
                    if (enrolls) {
                        _.forEach(enrolls, function (enroll) {
                            var idx = findIndexOfKeyInObjectArr(resp.data, 'paymentId', enroll.payment_ref);
                            if (idx > -1 && enroll.expiresOn) resp.data[idx]['expiresOn'] = enroll.expiresOn;
                        })
                    }
                    return res.status(200).json(resp);
                });
            })
        }
    })
};

exports.getMentorPayment = function (req, res) {
    User.findOne({_id: req.user._id}).exec(function (err, user) {
        if (err) return handleError('Error finding user details.', res);
        if (user) {
            Payment.find({user: user._id}).deepPopulate('path').exec(function (err, payments) {
                if (err) return handleError('Error finding payment details', res);
                var resp = {data: []};
                var path_ids = [], payment_ids = [];
                payments.forEach(function (payment) {
                    var item = payment.toJSON();
                    item['id'] = payment._id;
                    item['paymentId'] = payment._id;
                    item['pathId'] = payment.path._id;
                    item['userId'] = user._id;
                    item['username'] = user.username;
                    item['name'] = user.name;
                    item['username'] = user.username;
                    item['pathTitle'] = payment.path.title;
                    path_ids.push(payment.path._id);
                    payment_ids.push(payment._id);
                    delete item.path;
                    resp.data.push(item);
                });
                Enrollment.find({path: {$in: path_ids}, producer: user._id, payment_ref: {$in: payment_ids}}).select('expiresOn payment_ref').exec(function (err, enrolls) {
                    if (err) return handleError('Error finding enrollment details', res);
                    if (enrolls) {
                        _.forEach(enrolls, function (enroll) {
                            var idx = findIndexOfKeyInObjectArr(resp.data, 'paymentId', enroll.payment_ref);
                            if (idx > -1 && enroll.expiresOn) resp.data[idx]['expiresOn'] = enroll.expiresOn;
                        })
                    }
                    return res.status(200).json(resp);
                });
            })
        }
    })
};

function handleError(message, res) {
    res.status(422);
    res.json({"errors": [message]});
}

function findIndexOfKeyInObjectArr(obj_arr, key, val) {
    for (var item = 0; item < obj_arr.length; item++) {
        if (obj_arr[item][key] && obj_arr[item][key].toString() == val.toString()) return item;
    }
    return -1;
}
