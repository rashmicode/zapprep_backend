var qbchat = require('./qbchat');
var config = require('./config');
var mongoose = require('mongoose');

// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', function (err) {
        console.error('MongoDB connection error: ' + err);
        process.exit(-1);
    }
);

var QB = qbchat.extraQB();
qbchat.extraInit(QB);

var Path = require('./path/path.model');
var User = require('./user/user.model');
var Enroll = require('./enrollment/enroll.model');
var Concept = require('./concept/concept.model');
var async = require('async');

function escapeRegExpChars(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

var add_user_to_room = function(roomId, qbId, concept_cb){
    console.log('add user to room -', roomId, qbId);
    qbchat.extraAddUserToRoom(QB, roomId, [parseInt(qbId)], config.qb.params, function(err, result){
        if(err) {
            console.log('error adding user Id to general room', err, qbId, roomId);
            concept_cb();
        } else if(result) {
            console.log('finished adding user to general room', qbId, roomId);
            concept_cb();
        } else {
            console.log('unknown error adding user Id to general room', err, qbId, roomId, result);
            concept_cb();
        }
    });
};

var create_and_add_user_to_room = function(concept, concept_cb){
    qbchat.extraCreateRoom(QB, 2, concept.title, config.qb.params, function(err, newRoom){
        if(err) {
            console.log('error creating new room - ', err, ", concept Id - ", concept._id);
            concept_cb();
        } else if(newRoom && newRoom._id) {
            concept['qbId'] = newRoom._id;
            concept.save(function(err){
                if(err) {
                    console.log('error saving created room details - ', newRoom, err, concept._id);
                    concept_cb();
                } else {
                    add_user_to_room(newRoom._id, concept.producer.qbId, concept_cb);
                }
            });
        } else {
            console.log("Unknown error occurred while creating room - ", err, newRoom, concept._id);
            concept_cb();
        }
    });
};

// Quickblox update ids script
async.series([
    function(cb){
        Concept.find().select('path').exec(function(err, concepts){
            async.eachSeries(concepts, function(concept, concept_cb){
                Path.findOne({_id: concept.path}).exec(function(err, path){
                    if(err) {
                        console.log('error found for concept - ', concept._id, concept.path);
                        concept_cb();
                    } else if(path && path.concepts) {
                        if(path.concepts.indexOf(concept._id) > -1) {
                            concept_cb();
                        } else {
                            console.log('going to push it to path - ', path._id);
                            path.concepts.push(concept._id);
                            path.save(function(err){
                                if(err) console.log('error saving concept details to path - ', err);
                                console.log('pushed in path - ', path._id, concept.title);
                                concept_cb();
                            })
                        }
                    } else if(path) {
                        console.log('concepts not found in path - ', path._id, concept._id);
                        path['concepts'] = [concept._id];
                        path.save(function(err){
                            concept_cb();
                        })
                    } else {
                        console.log('path not found for - ', concept._id);
                        concept_cb();
                    }
                })
            }, function(done){
                console.log('finished');
                cb();
            })
        })
    }
], function(done){
    console.log('finished updating all results');
});